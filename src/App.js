import React,{useEffect,useState} from 'react';
import { Provider } from "react-redux";
import {store, persistor} from "./redux/store";
import './App.css';
import CreateRoutes from "./routes/CreateRoutes";
import {auth} from './services/search'
import { Helmet } from 'react-helmet';
import {PersistGate} from "redux-persist/integration/react"
import ErrorBoundary from "./components/Common/ErrorBoundry/ErrorBoundry"
import Maintenance from "./components/Common/Maintenance/Maintenance";
import {isSiteUnderMaintenance} from "./utils/commonFunc";
import {setTiming,pageLoadTimeWithPageview} from './services/Analytics/universalProp';


function App() {  
  const [isSiteInMaintenance, setPortatState] = useState({maintenance:false,loading:true});
  useEffect( () => {
    async function authAndSetting() {    
      await auth();
      if(isSiteUnderMaintenance()) {
        setPortatState({maintenance:true,loading:false})
      } else {
        setPortatState({maintenance:false,loading:false})
      }
      setTiming({category: "App load", variable: "loading application",label:"Total time required for application load", value: Math.round(performance.now()) });
         
    }
    authAndSetting();     
  }, []) 
  
  return (  
    <React.Fragment>   
      {!isSiteInMaintenance.loading && isSiteInMaintenance.maintenance && <Maintenance/> }
      {!isSiteInMaintenance.loading && !isSiteInMaintenance.maintenance && <React.Fragment> 
        <Helmet>
          <title>Wadiia</title>
          <meta name="description" content="Flight Booking"></meta>          
        </Helmet>
        <Provider store={store}> 
          <PersistGate persistor={persistor}>      
            <div className="layout">
              <ErrorBoundary>
                <CreateRoutes/>
              </ErrorBoundary>
            </div>
          </PersistGate>
        </Provider> 
        </React.Fragment>
      }    
      </React.Fragment>   
    );
  }

export default App;
