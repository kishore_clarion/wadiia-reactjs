import React from 'react';
import './FlightListTopSearch.css';
import {  DatePicker } from "antd";
import "antd/dist/antd.css";
import moment from 'moment';
import {DebounceInput} from 'react-debounce-input';


const FlightListTopSearch = (props) => { 
    const disabledDate=(current,forArrival=false)=> {  
        if(forArrival && props.searchData.fromDate) {
            return current < moment(props.searchData.fromDate).startOf('day')
        }
        // Can not select days before today 
        return current < moment().startOf('day')
      }

    let toggleSelection = false;      
    return(
        <div className="listing-top-section">
            <div className="container">
                <div className="listing-top-section__top">
                    <div className="listing-top-section__select-type">
                        <label>
                            <input type="radio" value="Oneway" name="one" checked={props.searchData.tripType === 'Oneway'} onChange={()=>{props.changeTripType('Oneway')}}/>
                            One Way
                        </label>
                        <label>
                            <input type="radio" value="Roundtrip" name="one"  checked={props.searchData.tripType === 'Roundtrip'} onChange={()=>{props.changeTripType('Roundtrip')}}/> 
                            Round Trip 
                        </label>
                        <label className="d-none">
                            <input type="radio" value="Multicity" name="one"  />
                            Multy City
                        </label>
                    </div>
                </div>

                <div className="listing-top-section__filter">
                    <div className="filter-input" style={{position:"relative"}}>
                        <span>From</span>                     
                        {/*<input type="text" value={ typeof props.searchData.fromLocation ==="string" ? props.searchData.fromLocation : props.searchData.fromLocation===null? "": props.searchData.fromLocation.city  } name="From" onChange={(e)=>{props.changeDeparture(e)}}/>*/}
                        <DebounceInput type="text" minLength={3} debounceTimeout={300} value={ typeof props.searchData.fromLocation ==="string" ? props.searchData.fromLocation : props.searchData.fromLocation===null? "": props.searchData.fromLocation.city } name="From" onChange={(e)=>{props.changeDeparture(e)}}/>                      
                        <ul className="location-result" style={{ display: props.showResults ? "block" : "none" }} >
                                            {   props.locationsDept ?
                                                props.locationsDept.map( location => <li key={location.id} onClick={()=>props.selectDepartureFrom(location)}> { location.address } </li>):null
                                            }
                                        </ul>
                    </div>
                    <div className="direction">
                        <i className="fa fa-arrow-right"></i>
                        <i className="fa fa-arrow-left"></i>
                    </div>
                    <div className="filter-input"  style={{position:"relative"}}>
                        <span>To</span>
                        {/*<input type="text" value={ typeof props.searchData.toLocation ==="string"? props.searchData.toLocation : props.searchData.toLocation===null?"":props.searchData.toLocation.city } name="From"  onChange={(e)=>{props.changeArrival(e)}}/>*/}
                        <DebounceInput type="text" minLength={3} debounceTimeout={300} value={ typeof props.searchData.toLocation ==="string" ? props.searchData.toLocation : props.searchData.toLocation===null? "": props.searchData.toLocation.city } name="From" onChange={(e)=>{props.changeArrival(e)}}/>
                        <ul className="location-result" style={{ display: props.showResultsArr ? "block" : "none" }} >
                                            {   props.locationsArr ?
                                                props.locationsArr.map( location => <li key={location.id} onClick={()=>props.selectArrivalTo(location)}> { location.address } </li>):null
                                            }
                                        </ul>
                    </div>
                    <div className="filter-input">
                        <span>Depart</span>
                        {/* <input type="text" name="From" placeholder="Fri, 3 April 2020"/> */}
                        <DatePicker 
                        onChange={(date,dateString)=>{props.updateFromDate(date)}}
                        value={props.searchData.fromDate?moment(props.searchData.fromDate):null}
                                        placeholder=""                                       
                                        format={"Do MMM YY"}
                                        bordered={false} 
                                        disabledDate={disabledDate} 
                                        allowClear={false}  
                                        />
                    </div>
                    <div className="filter-input">
                        <span>Return</span>
                        <DatePicker 
                        onChange={(date,dateString)=>{props.updateToDate(date)}} 
                         value={props.searchData.toDate?moment(props.searchData.toDate):null}
                                        placeholder=""                                       
                                        format={"Do MMM YY"}
                                        bordered={false} 
                                        disabledDate={(current)=>disabledDate(current,true)} 
                                        allowClear={true}  
                                        />
                    </div>
                    <div className="filter-input passengers" style={{position:"relative"}}>
                        <span>Passengers &amp; Class {toggleSelection} </span>
                        <input type="text" name="From" 
                        value={`${props.searchData.passanger.adult+props.searchData.passanger.child+props.searchData.passanger.infant} Traveler, ${props.searchData.serviceClass}`} 
                        placeholder="" onClick={()=>{ props.toggleClass()}}
                        onChange={()=>{}}
                        />                        
                        {props.toggleSelection && (
                        <ul className="location-result res">
                            <li>
                                <div className="location-result__wrapper">
                                    <span> Adults: {props.searchData.passanger.adult} </span>
                                    <div className="location-result__button">
                                        <button onClick={()=>{props.updateTraveller("adult","p")}} disabled={props.disbAdultPlus}>+</button>
                                        <button onClick={()=>{props.updateTraveller("adult","m")}} disabled={props.disbAdultMinus}>-</button>
                                    </div>
                                </div>
                            </li>
                            <li>
                                <div className="location-result__wrapper">
                                    <span> Child:{props.searchData.passanger.child} </span>
                                    <div className="location-result__button">
                                        <button onClick={()=>{props.updateTraveller("children","p")}} disabled={props.disbChildPlus} >+</button>
                                        <button onClick={()=>{props.updateTraveller("children","m")}} disabled={props.disbChildMinus} >-</button>
                                    </div>
                                </div>
                            </li>
                            <li>
                                <div className="location-result__wrapper">
                                    <span> Infant:{props.searchData.passanger.infant} </span>
                                    <div className="location-result__button">
                                        <button onClick={()=>{props.updateTraveller("infant","p")}} disabled={props.disbInfantPlus}>+</button>
                                        <button onClick={()=>{props.updateTraveller("infant","m")}} disabled={props.disbInfantMinus}>-</button>
                                    </div>
                                </div>
                            </li>
                            <li>                                 
                                {/*<label htmlFor="economy"><input type="radio" id="economy"  value="Economy" checked={props.searchData.serviceClass==="Economy"} onChange={(e)=>{props.handleServiceClass(e)}} /> {"Economy"}</label>                                
                                <label htmlFor="premium"><input type="radio" id="premium"  value="Premium Economy" checked={props.searchData.serviceClass==="Premium Economy"} onChange={(e)=>{props.handleServiceClass(e)}} /> {"Premium Economy"}</label>                                 
                                <label htmlFor="business"><input type="radio" id="business"  value="Business" checked={props.searchData.serviceClass==="Business"} onChange={(e)=>{props.handleServiceClass(e)}}/> {"Business"}</label> 
                                <label htmlFor="PremiumFirst"> <input type="radio" id="business"  value="Premium First" checked={props.searchData.serviceClass==="Premium First"} onChange={(e)=>{props.handleServiceClass(e)}}/> {"Premium First"}</label>
                                <label htmlFor="PremiumFirst"><input type="radio" id="business"  value="Economic Standard(M)" checked={props.searchData.serviceClass==="Economic Standard(M)"} onChange={(e)=>{props.handleServiceClass(e)}} /> {"Economic Standard(M)"}</label>
                                */}
                                {
                                    props.serviceClasses.map((serviceClass,index)=>{
                                        return <label htmlFor={serviceClass} key={"labelServiceClass"+index}>
                                                <input type="radio" id={serviceClass}
                                                    key={"serviceClass"+index}
                                                    checked={props.searchData.serviceClass===serviceClass} 
                                                    onChange={(e)=>{props.handleServiceClass(e)}}                                                     
                                                    value={serviceClass}
                                                /> {serviceClass}
                                                </label> 
                                    })                                                        
                                }
                            </li>
                            <li><button className="btn btn-primary" onClick={()=>{ props.toggleClass()}}>Done</button></li>                          
                        </ul>)}
                    </div>                    
                    <button className={props.disableSearch?"btn disabled": "btn btn-primary"} onClick={props.search}  disabled={props.disableSearch}>Search Again</button>                    
                </div>
            </div>
        </div>
    )
}


export default FlightListTopSearch;