import React from 'react';
import moment from 'moment';
import {minHHMM,addDefaultSrc,returnFlightLogo} from "../../utils/commonFunc";
import {isMobileOnly} from "react-device-detect";

const FlightDetail = props => {
    const time=minHHMM(props.stopsDetails.journeyDuration);  
    return(       
        <div>
            <div className="airline-details-wrapper">
             <div className="row">
                 <div className="col-md-6 airline-details-wrapper__left">
                     <div className="airline-information">
                         <div className="airline-text wd-flex-center">
                             <span className="airline-text__img">
                                 <img width="30" onError={addDefaultSrc} src={returnFlightLogo(props.stopsDetails.images[0].url,isMobileOnly)} alt="I5" title="I5"/>
                             </span>
                             <span className="airline-text__flight">
                                 {props.stopsDetails.vendors[0].item.name} | {props.stopsDetails.code}
                             </span>
                         </div>
                         <div className="airline-time-options wd-flex-center">
                             <div className="airline-departure-option">
                                 <span className="flight-time departure font-size-20">{`${ moment(props.stopsDetails.dateInfo.startDate).format("HH:mm")}`}</span>
                                 <p className="date_details" id="dateId">{`${ moment(props.stopsDetails.dateInfo.startDate).format('ddd D MMM YY')}`} </p>
                                 <p className="city_details" id="cityId">{props.stopsDetails.locationInfo.fromLocation.city}, {props.stopsDetails.locationInfo.fromLocation.country}</p>
                             </div>
                             <div className="stops-option">
                                 <p className="clearfix append_bottom7 append_right24 append_left10">
                                     <span className="line-map">
                                         <span className="flight-duration">
                                             <strong>{time.hrs}</strong> hrs 
                                             <strong>{time.min}</strong> mins 
                                         </span>
                                     </span>
                                 </p>
                             </div>
                             <div className="arrival-option">
                                 <span className="flight-time arrival font-size-20">{`${ moment(props.stopsDetails.dateInfo.endDate).format("HH:mm")}`}</span>
                                 <p className="date_details" id="dateId">{`${ moment(props.stopsDetails.dateInfo.endDate).format('ddd D MMM YY')}`} </p>
                                 <p className="city_details" id="cityId">{props.stopsDetails.locationInfo.toLocation.city}, {props.stopsDetails.locationInfo.toLocation.country}</p>
                             </div>
                         </div>
                     </div>
                 </div>
                 <div className="col-md-6 airline-details-wrapper__right">
                     <div className="baggage-info">
                         <div className="baggage-info__details baggage-info__name">
                             <span className="itnry-flt-footer-col">BAGGAGE : </span>
                             <span className="itnry-flt-footer-col">Check-in</span>
                             <span className="itnry-flt-footer-col">Cabin</span>
                         </div>
                         <div className="baggage-info__details baggage-info__desc">
                             <span className="itnry-flt-footer-col">ADULT</span>
                             <span className="itnry-flt-footer-col">15 Kgs</span>
                             <span className="itnry-flt-footer-col">7 Kgs</span>
                         </div>
                     </div>
                 </div>
             </div>
             </div>
        </div>
    )
}
export default FlightDetail;    