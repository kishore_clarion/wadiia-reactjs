import React, { Component } from 'react';
import { connect } from "react-redux";
import { compose } from "redux";
import * as actions from "../../redux/index";
import './flight.css';
import moment from 'moment';
import FlightDetail from "./flightDetailTab";
import FlightDetailDevider from "./flightDetailDevider";
import { addDefaultSrc,distinctAirline, searchObj,prepareMessageAvalability,dateDiffHrsMin,returnFlightLogo,minHHMM,statucDateSectionFlight,staticViewFare} from "../../utils/commonFunc";
import history from '../../utils/history';
import {createCart,viewCart,fareRules} from '../../services/flightBooking';
import { message, Spin } from 'antd';
import LoadingOverlay from 'react-loading-overlay';
import {messageDuration} from "../../services/constants";
import FareDetails from "../Common/FareDetails/FareDetails";
import {isMobileOnly} from "react-device-detect";
import {selectItem,addToCart} from '../../services/Analytics/ga4';

class Flight extends Component {
    state = { activeTab: "flightDetails",fareRules:[],loadingRule:false,airline:[] }

    componentDidMount=async()=>{
        const airline=distinctAirline(this.props.flight.items);     
        await this.setState({airline:[...airline]}) 
    }

    handleActiveTab = (tabName) => {
        this.setState({
            activeTab: tabName
        })
        if(tabName==="cancellation") {
            this.fetchRules();
        }
    }

    reviewBooking = async ()=>{
        this.setState({loading:true}); 
        //Google Analytics
        selectItem(this.props.flight);
        addToCart(this.props.flight);             
        const res=await createCart(this.props.tkn,this.props.flight.token);
        if(res && res.status){            
            //localStorage.setItem("cartLocal",res.cartId);
            this.props.setCart({cartLocal:res.cartId});
            //this.setState({loading:false}); 
            history.push('/review-flight');
        } else {                               
            this.setState({loading:false});
            //Flight is not available
            if(res.code===260030){
                const resView = await viewCart(res.cartId);
                console.log("view cart",resView);
                const msg=prepareMessageAvalability(resView,res.code,res.message);
                message.error(msg,messageDuration);
            } else {
                message.error(res.code +" : "+res.message,messageDuration);
            }
        }
    }

    fetchRules=async ()=>{             
        const data={searchToken:this.props.tkn,flightToken:this.props.flight.token};
        if(this.state.fareRules.length<1){
            this.setState({loadingRule:true})
            const res=await fareRules(data);
            if(res.status && res.data.length>0 && res.data[0].item.length>0) {
                this.setState({fareRules:res.data,loadingRule:false});                                          
            } else {
                message.error(res.message);
                this.setState({loadingRule:false})               
            }         
            // if(res.status && res.data.length>0 && res.data[0].item.length>0) {
            //     this.setState({fareRules:res.data[0].item,loadingRule:false});                                          
            // } else {
            //     message.error(res.message);
            //     this.setState({loadingRule:false})               
            // }
        }        
    }

    showFareRules=()=>{        
        // if(this.state.fareRules.length>0) {
        //     return this.state.fareRules && this.state.fareRules.map((farerule,index)=>{
        //         return <div key={"fareRule"+index}>
        //         <div><b>{farerule.key}</b></div>
        //         <div>{farerule.value}</div>
        //         </div>
        //     })
        // } 
        // list.map((item, index) => {
        //     return (
        //       <div key={index}>
        //         <ul >{item.value}</ul>
        //        {
        //         item.list.map((subitem, i) => {
        //           return (
        //              <ul ><li>{subitem.value}</li></ul>
        //           )
        //         })
        //        }
        //       </div>
        //     )
        //   }
        if(this.state.fareRules.length>0) {
            return this.state.fareRules && this.state.fareRules.map((sequence,seqIndex)=>{
                return (
                <div key={seqIndex}> 
                <ul >
                    <b style={{'textTransform':'capitalize','color':'orange' }}>{`${this.props.flight.items[0].item[seqIndex].locationInfo.fromLocation.name} - ${this.props.flight.items[0].item[seqIndex].locationInfo.toLocation.name}`}</b>
                </ul>
                {
                    sequence.item.map((farerule,index)=>{
                        return (
                        <div key={"fareRule"+index}>
                        <div><b style={{'textTransform':'capitalize' ,'fontWeight': 'bold'}}>{farerule.key}</b></div>
                        <div>{farerule.value}</div>
                        </div>
                        )
                    })
                }
                </div>
                )
            })
            
        } 
    }

    showImages=()=>{          
        let totalAirline=this.state.airline.length;
        if(totalAirline>0) {
            if(totalAirline>=2) {
                return <React.Fragment>
                        <img src={returnFlightLogo(this.state.airline[0].url,true)} onError={addDefaultSrc} />
                        {totalAirline===2? <img src={returnFlightLogo(this.state.airline[1].url,true)} onError={addDefaultSrc} />:null}                    
                        {totalAirline>2? `+ ${ totalAirline-2} more` : null}
                        {<a onClick={()=>{this.setState({viewDetails:!this.state.viewDetails})}}><span>Multiple carrier</span></a>}
                        </React.Fragment>
            } else {
                return <React.Fragment>
                        <img src={returnFlightLogo(this.state.airline[0].url,true)} onError={addDefaultSrc} />                    
                        </React.Fragment> 
            }
        }
    }

    adtBaseRateInfo = searchObj(this.props.flight.paxInfo[0].displayRateInfo, "description", "BASEFARE");
    adtTotRateInfo = searchObj(this.props.flight.paxInfo[0].displayRateInfo, "description", "TOTALAMOUNT");    

    render() {      
        let totalTime=0
        if(this.props.flight ) {
            if(this.props.flight.items[0].item.length>0) {
                this.props.flight.items[0].item.map(item=>{
                    totalTime=totalTime+item.journeyDuration;
                    totalTime=totalTime+item.layOverDuration;
                })
            }
            totalTime=minHHMM(totalTime);
            //totalTime=dateDiffHrsMin(this.props.flight.items[0].dateInfo.startDate,this.props.flight.items[0].dateInfo.endDate);
        }
        return (
        <LoadingOverlay
            active={this.state.loading}
            spinner={true}
            text='Loading your content...'
        >
        
            <div className="w-flight-listing">
                {<div className="w-flight-list"  >
                    <ul className="wd-flex-center">
                        <li>
                            <div className="air-flight d-flex">
                                <div className="air-flight__img">
                                {this.showImages()}
                                    {/* <img src={returnFlightLogo(this.props.flight.images[0].url,isMobileOnly)} onError={addDefaultSrc} /> */}
                                        <div className="air-flight__details font-size-11">
                                        <span className="d-none d-md-block">{this.props.flight.items[0].item[0].vendors[0].item.name}</span>
                                        <span className="text-grey d-none">{this.props.flight.items[0].item[0].code}</span>
                                        <span>{this.props.flight.vendors[0].item.name}</span>
                                    </div>
                                </div>
                                
                            </div>

                        </li>
                        <li>
                            <div className="air-flight-departure font-size-11">
                                <span>{`${moment(new Date(this.props.flight.dateInfo.startDate)).format('hh:mm A')}`}</span>
                                {this.props.flight.locationInfo.fromLocation.city}
                                <span>{moment(this.props.flight.dateInfo.startDate).format('DD MMM YY')}</span>
                            </div>
                        </li>
                        <li>
                            <div className="air-flight-arrival font-size-11">
                                <span>{`${moment(new Date(this.props.flight.dateInfo.endDate)).format('hh:mm A')}`}</span>
                                {this.props.flight.locationInfo.toLocation.city}
                            </div>
                        </li>
                        <li>
                            <div className="air-flight-duration font-size-11" date-placement="left" data-toggle="tooltip" data-html="true" title="<div className='tooltip-content'><p>Plane Change</p><p><span>Chennai</span> (MMA) | 8hrs layover</p></div>">
                            {totalTime && (<span> <strong>{`${totalTime.hrs}`}</strong>h <strong>{`${totalTime.min}`}</strong>m</span>)}
                                <div className="air-flight-duration__divider">
                                    <span></span>
                                </div>
                                <div className="clearfix"></div>
                                {this.props.flight.stops} stop {this.props.flight.items[0].item.length > 1 ? `via ${this.props.flight.items[0].item[0].locationInfo.toLocation.city}` : null}
                            </div>

                        </li>
                        <li>
                            <div className="air-flight-price wd-flex-center">
                                <span>{this.props.flight.displayAmount}</span>
                                {
                                /*<button className="btn btn-primary" onClick={()=>{this.setState({viewFare:!this.state.viewFare})}}> View Fare </button>	*/}
                                <button className="btn btn-primary" onClick={this.reviewBooking}> Book Now</button>
                            </div>
                        </li>
                    </ul>
                    {
                        this.state.viewFare && (
                            staticViewFare()
                        )
                    }
                    <div className="flight-details">
                        <div className="text-left">
                            <a onClick={() => { this.setState({ viewDetails: !this.state.viewDetails }) }}><span>Flight Details <i className="fa fa-chevron-down"></i></span></a>
                        </div>
                    </div>
                    {
                        this.state.viewDetails && (
                            <div className="flight-details__wrapper">
                                <div className="flight-details__wrapper-list" role="tablist">
                                    <div className={this.state.activeTab == "flightDetails" ? "list-item active" : "list-item"}>
                                        <a href="#flightDetails" onClick={() => this.handleActiveTab("flightDetails")} data-toggle="tab">Flight Details</a>
                                    </div>
                                    <div className={this.state.activeTab == "fareSummory" ? "list-item active" : "list-item"}><a href="#fareSummory" onClick={() => this.handleActiveTab("fareSummory")} data-toggle="tab">Fare Summary</a></div>
                                    <div className={this.state.activeTab == "cancellation" ? "list-item active" : "list-item"}><a href="#cancellation" onClick={() => this.handleActiveTab("cancellation")} data-toggle="tab">Cancellation</a></div>
                                    <div className={this.state.activeTab == "dateChange" ? "list-item active" : "list-item"}><a href="#dateChange" onClick={() => this.handleActiveTab("dateChange")} data-toggle="tab">Date Change</a></div>
                                </div>

                                <div className="tab-content">
                                    <div className={this.state.activeTab == "flightDetails" ? "tab-pane active" : "tab-pane"} id="flightDetails">
                                        <div className="flight-details__wrapper-oneway">
                                            <div className="flight-details__wrapper-heading">
                                                <span className="flight_details__name">
                                                    {`${this.props.flight.locationInfo.fromLocation.city} to ${this.props.flight.locationInfo.toLocation.city}, ${moment(this.props.flight.dateInfo.startDate).format('DD MMM')}`}
                                                </span>
                                                {totalTime && (<span> <strong>{`${totalTime.hrs} `}</strong> Hrs <strong>{`${totalTime.min} `}</strong> mins</span>)}
                                            </div>
                                        </div>
                                        {this.props.flight.stops === 0 && (<FlightDetail direct={true} stopsDetails={this.props.flight.items[0].item[0]} />)}
                                        {
                                            this.props.flight.stops === 1 && (
                                                <div>
                                                    <FlightDetail stopsDetails={this.props.flight.items[0].item[0]} />
                                                    <FlightDetailDevider stopsDetails={this.props.flight.items[0].item[0]} />
                                                    <FlightDetail stopsDetails={this.props.flight.items[0].item[1]} />
                                                </div>
                                            )}
                                        {this.props.flight.stops > 1 && (

                                            this.props.flight.items[0].item.map((stopsDetails, index) => {
                                                return <div key={"multistop"+index}>
                                                    <FlightDetail stopsDetails={stopsDetails} />
                                                    {index < this.props.flight.items[0].item.length - 1 ? <FlightDetailDevider stopsDetails={stopsDetails} /> : null}
                                                    </div>
                                            })

                                        )}



                                    </div>
                                    <div className={this.state.activeTab == "fareSummory" ? "tab-pane active" : "tab-pane"} id="fareSummory">
                                        <FareDetails paxInfo={this.props.flight.paxInfo}/>                                        
                                    </div>
                                    <div className={this.state.activeTab == "cancellation" ? "tab-pane active" : "tab-pane"} id="cancellation">
                                        <div className="airline-fare-rules">
                                            <Spin tip="Loading..." spinning={this.state.loadingRule}/>
                                            {this.showFareRules()}
                                            {/* <div className="airline-text m-b-10">                                                
                                            </div>
                                            <div className="d-flex rules-heading">
                                                <div className="time-gap-cond">
                                                    <p className="text-black LatoBold append_bottom2">Time frame</p>
                                                    <p className="font10 lightGreyText">(From Scheduled flight departure)</p>
                                                </div>
                                                <div className="time-gap-cond">
                                                    <p className="text-black LatoBold append_bottom2">Airline Fee + MMT Fee</p>
                                                    <p className="font10 lightGreyText">(Per passenger)</p>
                                                </div>
                                            </div>
                                            <div className="d-flex">
                                                <div className="time-gap-cond">
                                                    <p className="text-black LatoBold append_bottom2">0 hours to 4 hours*</p>
                                                </div>
                                                <div className="time-gap-cond text-black">
                                                    <p>ADULT :
                                                        <b>Non Refundable</b>
                                                        <br />
                                                    </p>
                                                </div>
                                            </div>
                                            <div className="d-flex">
                                                <div className="time-gap-cond">
                                                    <p className="text-black LatoBold append_bottom2">4 hours to 3 days*</p>
                                                </div>
                                                <div className="time-gap-cond text-black">
                                                    <p>ADULT :
                                                        <b>₹ 3,500 + ₹ 0</b>
                                                        <br />
                                                    </p>
                                                </div>
                                            </div>
                                            <div className="d-flex">
                                                <div className="time-gap-cond">
                                                    <p className="text-black LatoBold append_bottom2">3 days to 365 days*</p>
                                                </div>
                                                <div className="time-gap-cond text-black" >
                                                    <p>ADULT :
                                                        <b>₹ 3,000 + ₹ 0</b>
                                                        <br />
                                                    </p>
                                                </div>
                                            </div>
                                            <p className="append_top10 append_bottom5">*From the Date of Departure</p> */}
                                        </div>
                                    </div>
                                    <div className={this.state.activeTab == "dateChange" ? "tab-pane active" : "tab-pane"} id="dateChange">
                                        {statucDateSectionFlight()}
                                        {/* <div className="airline-fare-rules">
                                            <div className="airline-text m-b-10">                                               
                                            </div>
                                            <div className="d-flex rules-heading">
                                                <div className="time-gap-cond">
                                                    <p className="text-black LatoBold append_bottom2">Time frame</p>
                                                    <p className="font10 lightGreyText">(From Scheduled flight departure)</p>
                                                </div>
                                                <div className="time-gap-cond">
                                                    <p className="text-black LatoBold append_bottom2">Airline Fee + MMT Fee + Fare difference</p>
                                                    <p className="font10 lightGreyText">(Per passenger)</p>
                                                </div>
                                            </div>
                                            <div className="d-flex">
                                                <div className="time-gap-cond">
                                                    <p className="text-black LatoBold append_bottom2">0 hours to 4 hours*</p>
                                                </div>
                                                <div className="time-gap-cond text-black">
                                                    <p>ADULT :
                                                        <b>Non Changeable</b>
                                                        <br />
                                                    </p>
                                                </div>
                                            </div>
                                            <div className="d-flex">
                                                <div className="time-gap-cond">
                                                    <p className="text-black LatoBold append_bottom2">4 hours to 3 days*</p>
                                                </div>
                                                <div className="time-gap-cond text-black">
                                                    <p>ADULT :
                                                        <b>₹ 3,500 + ₹ 0  + Fare difference</b>
                                                        <br />
                                                    </p>
                                                </div>
                                            </div>
                                            <div className="d-flex">
                                                <div className="time-gap-cond">
                                                    <p className="text-black LatoBold append_bottom2">3 days to 365 days*</p>
                                                </div>
                                                <div className="time-gap-cond text-black">
                                                    <p>ADULT :
                                                        <b>₹ 3,000 + ₹ 0  + Fare difference</b>
                                                        <br />
                                                    </p>
                                                </div>
                                            </div>
                                            <p className="append_top10 append_bottom5">*From the Date of Departure</p>
                                        </div> */}
                                    </div>
                                </div>


                            </div>
                        )
                    }
                </div>

                }
            </div>
        </LoadingOverlay>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        tkn: state.flights.token,
        resFlight: state.flights.response,
        fromLocation: state.flights.fromLocation,
        toLocation: state.flights.toLocation,
        tripType: state.flights.tripType,
        fromDate: state.flights.fromDate,
        toDate: state.flights.toDate,
        passanger: state.flights.passanger,
        serviceClass: state.flights.serviceClass
    };
};

export default compose(connect(mapStateToProps, actions))(Flight);
