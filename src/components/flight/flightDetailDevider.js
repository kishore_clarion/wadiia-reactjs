import React from 'react';
import {minHHMM} from "../../utils/commonFunc";

const FlightDetailDevider = props => {
    const time=minHHMM(props.stopsDetails.layOverDuration);
    return(
        <div className="fli-list-flight_details__divider">
            <div className="flight_details__infoSctn">
                <p>
                    <b>Change of Planes</b> | 
                    <b> <font color="#757575"> {time.hrs}hrs {time.min}min</font> </b> layover in  
                    <b>{` ${props.stopsDetails.locationInfo.toLocation.city}, ${props.stopsDetails.locationInfo.toLocation.country} `}</b> 
                    <b> ({props.stopsDetails.locationInfo.toLocation.id})</b>
                </p>
            </div>
        </div>
    )
}
export default FlightDetailDevider;