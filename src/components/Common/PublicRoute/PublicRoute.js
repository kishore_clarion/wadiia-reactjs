import React from 'react';
import { Route, Redirect } from 'react-router-dom';
import { useSelector,useDispatch  } from "react-redux";
import ErrorBoundary from "../ErrorBoundry/ErrorBoundry";
import { resetRedirection } from "../../../redux/user/userActions";


const PublicRoute = ({Component, restricted,props, ...rest}) => {
    const user = useSelector(state => state.user);   
    const dispatch = useDispatch();    
    let route;
    if(user.status==="in" && restricted && !user.redirect) {
        route=<Route render={()=>(<Redirect to="/" />)} />
    } else if(user.status==="in" && restricted && user.redirect) { 
        const redirect=user.redirect;          
        route=<Route render={()=>(<Redirect to={redirect} />)} />
    } else {
        route=<Route {...rest} render={()=>(<ErrorBoundary><Component {...props} /> </ErrorBoundary>)} />
    }
    return (route);
};

export default PublicRoute;