import React, { Component } from 'react';

class Maintenance extends Component {
    state = {  }
    render() { 
        return ( 
            <div classname="layout page-maintainance" style={{marginTop: "100px"}}>
                <div className="header text-center">
                    <div className="container">
                        <div className="logo" style={{padding: "15px 0 20px", display: "block"}}>
                            <img width="120" src={process.env.PUBLIC_URL +"images/wadiia-logo.png"} title="Wadiia" alt="Wadiia"/>
                        </div>
                    </div>
                </div>
                <div className="container">
                    <div className="bgimg">
                        <div className="bgimg_title">
                            <h1>Site is under Maintenance</h1> 
                        </div>
                        <div className="bottomleft">
                        <h3>We will be <span className="text-primary">Happy to serve</span> you soon...</h3>
                        </div>
                    </div>
                </div>
            </div>
         );
    }
}
 
export default Maintenance;