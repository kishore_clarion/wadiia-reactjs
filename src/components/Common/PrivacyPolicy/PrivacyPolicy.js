import React, { Component } from 'react';
import { Link } from "react-router-dom";
import { Helmet } from "react-helmet";
import "./PrivacyPolicy.css";

class PrivacyPolicy extends Component {
    render() { 
        return ( 
            <React.Fragment>               
            <Helmet>
                <title>{'Privacy Policy'}</title>
                <meta name="description" content='Privacy policy details' />
            </Helmet> 
            <div className="container m-t-20">
                <div className="privacy-policy">
                    <h1>Privacy Policy</h1>
                    <em>Effective May 25, 2018</em>
                    <p className="font-size-15 m-t-15"> Wadiia is fanatical about protecting your privacy. </p>
                    <p>If you have any questions about our Privacy Policy, you can contact us at privacy@wadiia.com and include ‘Privacy Policy’ in the subject line.</p>

                    <h2>1. Privacy Policy Overview</h2>
                    <ol>
                        <li> This Policy explains how we may process your information. This Policy may be amended or updated from time to time, so please check it regularly for updates. </li>
                        <li> Wadiia with its affiliates and its subsidiary Wadiia Packages and Tours Private Limited (collectively referred to as “Wadiia”, “us”, “our” or “we”), provides travel related services and leisure activities (“Services”) through its websites www.Wadiia.com; </li>
                        <li> This Privacy Policy applies to all information collected about you by Wadiia, regardless of how it is collected or stored, and describes, among other things, the types of information collected about you when you interact with the Services, how your information may be used, when your information may be disclosed, how you can control the use and disclosure of your information, and how your information is protected. </li>
                        <li> Except as otherwise noted in this Privacy Policy, Wadiia is a data controller under the EU General Data Protection Regulation (“GDPR”), which means that we decide how and why the information you provide to us is processed. Contact details are provided in Section 17 below. This Policy may be amended or updated from time to time to reflect changes in our practices with respect to the Processing of your information, or changes in applicable law. We encourage you to read this Policy carefully, and to regularly check this page to review any changes we might make. </li>
                    </ol>

                    <h2>2. What Categories of Information We May Process</h2>
                    <ol>
                        <li> 
                            2.1 On your accessing of the Booking Platform, we may process the following categories of information about you, such as: 
                            <ul>
                                <li> <strong>Personal details</strong>: your username or login details; e-mail id; contact number(s); </li>
                                <li> <strong>Booking information</strong>: which includes information about your travel, PNR details, bookings, co-passengers, travel preferences etc. </li>
                                <li> <strong>Demographic information</strong>: gender; age/date of birth; nationality; </li>
                                <li> <strong>Location information</strong>: location data that describes the precise geographic location of your device (“Precise Location Data”). </li>
                                <li> <strong>Purchase and payment details</strong>: records of travel services purchases and prices; invoice records; payment records; payment method; cardholder or account-holder name; payment amount; and payment date. </li>
                            </ul>
                        </li>
                        <li> Wadiia with its affiliates and its subsidiary Wadiia Packages and Tours Private Limited (collectively referred to as “Wadiia”, “us”, “our” or “we”), provides travel related services and leisure activities (“Services”) through its websites www.Wadiia.com; </li>
                        <li> This Privacy Policy applies to all information collected about you by Wadiia, regardless of how it is collected or stored, and describes, among other things, the types of information collected about you when you interact with the Services, how your information may be used, when your information may be disclosed, how you can control the use and disclosure of your information, and how your information is protected. </li>
                        <li> Except as otherwise noted in this Privacy Policy, Wadiia is a data controller under the EU General Data Protection Regulation (“GDPR”), which means that we decide how and why the information you provide to us is processed. Contact details are provided in Section 17 below. This Policy may be amended or updated from time to time to reflect changes in our practices with respect to the Processing of your information, or changes in applicable law. We encourage you to read this Policy carefully, and to regularly check this page to review any changes we might make. </li>
                    </ol>

                    <h2> 3. Sensitive Personal Information </h2>
                    <ol>
                        <li> We do not seek to collect or otherwise Process your Sensitive Personal Information. Where we need to Process your Sensitive Personal Information for legitimate purpose, we do so in accordance with applicable law. The Services are not intended for use by children. </li>
                        <li> We do not collect or otherwise Process Personal Information about race, religion, sexual orientation or health or any other information that may be deemed to be sensitive under GDPR (collectively, “Sensitive Personal Information”) in the ordinary course of our business. </li>
                        <li> Children: The Travel Services are not intended for the use by Children below the age of 18 years. For availing the services on Booking Platform by the children below 18 years, parental consent will be necessary. </li>
                    </ol>
                </div>
            </div>
        </React.Fragment>
        );
    }
}

export default PrivacyPolicy;