import React, { Component } from 'react';
import {prepareFareDetailsSearch} from '../../../utils/commonFunc';

class FareDetails extends Component {
    state = {  }
    componentDidMount=async()=>{
        const fareDetails=await prepareFareDetailsSearch(this.props.paxInfo);
        this.setState({fareDetails:{...fareDetails}})
    }
    
    render() {         
        return ( 
            <React.Fragment>
            {this.state.fareDetails && this.state.fareDetails.fareSummary.length>0  && 
        
            <div className="fli-list-fare_details__left"  >

                <div className="fli-list-fare_details__left-heading m-b-15">Fare breakup</div>
                
            { this.state.fareDetails.fareSummary.map((fareD,index)=>{
               return  <div className="d-flex align-items-start justify-content-between" key={"Fare"+index}>
                            <span>
                                {fareD.fareType}
                            </span>
                            <span className="flight-list__summary">
                                {
                                fareD.values.map((fare,index)=>{
                                    return <span key={"detailedFare"+index}>{this.state.fareDetails.currency}{fare.rate}*{fare.quantity} {fare.pax}={this.state.fareDetails.currency} {fare.rate*fare.quantity}</span> 
                                }) 
                                }
                            </span>
                        </div>
                })}
                <div className="d-flex align-items-center justify-content-between top-border">
                    <span>
                        <strong> TOTAL </strong>: 
                    </span>
                    <span>
                        {this.state.fareDetails.currency}{this.state.fareDetails.total}
                    </span>
                </div>
        </div>
            }
            </React.Fragment>
        );
    }
}
 
export default FareDetails;