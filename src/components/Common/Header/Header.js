import React, { Component,PureComponent } from 'react';
import { 
  Link
} from 'react-router-dom';
import './Header.css';
import { connect } from 'react-redux';
import * as actions from "../../../redux/index";
import { Spin } from 'antd';
import _ from 'lodash';
import {addDefaultProfileSrc} from "../../../utils/commonFunc";
import {getUser} from '../../../services/user';
import { stat } from 'fs';

//const Header = props => {
class Header extends PureComponent {
    state={
        user:null,
        update:""
    }
    logout=async ()=>{
        await this.props.logout();
        //const user=localStorage.removeItem('user');
        this.setState({user:null});
    }

    componentDidMount=async ()=>{ 
        const res= await getUser();
        if(res.status) {
            this.setState({user:{...res.data}});
            this.props.loginSuccess({message:"",status:true});           
        } else {
            this.setState({user:null});
            this.props.loginSuccess({message:"",status:false});           
        }        
    }

   
    componentDidUpdate=async(prevProps,prevState)=>{ 
        if((this.props.status !==prevProps.status)&&!this.props.loading && this.props.status==="out") {
            this.setState({user:null})
            this.props.loginSuccess({message:"",status:false});
        }else if((this.props.status !==prevProps.status)&& !this.props.loading && (this.props.status==="update"|| this.props.status==="in")) {
            const res= await getUser();
            if(res.status) {
                this.setState({user:{...res.data}});                
            } else {
                this.setState({user:null}); 
            } 
        };

        if((this.props.status !==prevProps.status)&&!this.props.loading) {
            const res= await getUser();
            if(res.status) {
                this.setState({user:{...res.data}});
                this.props.loginSuccess({message:"",status:true});
            } else {
                this.setState({user:null});
                this.props.loginSuccess({message:"",status:false});
            }
        }; 
    }
    render() { 
        return(           
            <div className="header">
                    <div className="container">
                        <div className="header__inner">
                            <Link to="/" className="logo">
                                <img width="120" src={process.env.PUBLIC_URL +"images/wadiia-logo.png"} title="Wadiia" alt="Wadiia"/>
                            </Link>
                            
                            <ul className="nav nav-tabs main-nav">
                                <li className="active">
                                    <a href="#">
                                        <img src={process.env.PUBLIC_URL +"images/icon_flight.png"} alt="flight" titlle="flight"/>
                                        <span>Flights</span>
                                    </a>
                                </li>
                                <li className="d-none">
                                    <a href="#">
                                        <img src={process.env.PUBLIC_URL +"images/icon_hotel.png"} alt="hotel" titlle="hotel"/>
                                        <span>Hotels</span>
                                    </a>
                                </li>
                                <li className="d-none">
                                    <a href="#">
                                        <img src={process.env.PUBLIC_URL +"images/icon_holidays.png"} alt="holidays" titlle="holidays"/>
                                        <span>Holidays</span>
                                    </a>
                                </li>
                            </ul> 
                            <ul className="my-account">                          
                                <li className="dropdown">
                                {this.state.user && (<img width="30" height="30" src={this.state.user.profilePicture.url} onError={addDefaultProfileSrc} />)}
                                {( <a href="#" className="has-dropdown dropdown-toggle" data-toggle="dropdown" >My Account</a>)}
                                    <ul className="dropdown-menu dropdown-menu-right">
                                        {/*<li><Link className="dropdown-item" to="/my-booking">My Bookings</Link></li>
                                        <li><Link className="dropdown-item" to="/">My eCash</Link></li>  */}                                      
                                        {!this.state.user && (
                                            <li> 
                                                <Link href="#" className="dropdown-item" to="/login"> Login </Link> 
                                                <Link href="#" className="dropdown-item" to="/signup"> Sign up </Link> 
                                            </li>
                                        )}
                                        {this.state.user && (
                                           <li> 
                                                <Link href="#" className="dropdown-item" to="/manage-bookings"> My Bookings </Link>
                                                <Link href="#" className="dropdown-item" to="/my-profile"> My Profile </Link>
                                                <Link to="#" className="dropdown-item no-arrow" onClick={()=>this.logout()}> Log Out </Link> 
                                                <div className="switch-section temp-hide">
                                                   <span>Switch to</span>
                                                   <Link href="#" className="dropdown-item" to="/"> Wadiia for Business </Link>
                                                   <Link href="#" className="dropdown-item" to="/"> Wadiia for Travel Agent </Link>
                                               </div>
                                           </li>
                                        )}
                                    </ul>
                                </li>
                                <li className="dropdown temp-hide">
                                    <a href="#" className="has-dropdown dropdown-toggle" data-toggle="dropdown">Support</a>
                                    <ul className="dropdown-menu dropdown-menu-right">
                                        <li>
                                            <a className="dropdown-item" href="#">Contact Us</a>
                                            <a className="dropdown-item" href="#">Complete Booking</a>
                                            <a className="dropdown-item" href="#">Make a Payment</a>
                                            <a className="dropdown-item" href="#">Flight Cancellation Charges</a>
                                        </li>
                                    </ul>
                                </li>
                            </ul>
                        </div> 
                    </div>
                </div>
            
        )
    }
}

const mapStateToProps = (state) => {
    return {               
        loading:state.user.loading,
        status:state.user.status
    };
  };
export default (connect(mapStateToProps,actions))(Header);