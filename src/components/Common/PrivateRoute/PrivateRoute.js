import React from 'react';
import { Route, Redirect } from 'react-router-dom';
//import { getUser} from '../../../services/user';
import { useSelector  } from "react-redux";
import ErrorBoundary from "../ErrorBoundry/ErrorBoundry";


const PrivateRoute = ({Component,props, ...rest}) => {
    const user = useSelector(state => state.user);
    let route;
    if(user.status==="in") {
        route=<Route {...rest} render={()=>(<ErrorBoundary><Component {...props} /></ErrorBoundary>)} />
    } else {
        route=<Route render={()=>(<Redirect to="/login" />)} />
    }    
    return (route);
};

export default PrivateRoute;