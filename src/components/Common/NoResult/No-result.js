import React from "react";
import './No-result.css';

import {    
    Link
  } from "react-router-dom";

const NoResult = props => {
    return(
        <div className="no-result container">
            <Link to="/" className="logo">
            <img src={process.env.PUBLIC_URL +"images/icon_no_result.png"} width="200" />
            </Link>
            <h1 className="no-result__heading text-danger">There were no flights found for this route and date combination</h1>
            <p>We suggest you modify your search and try again.</p>
        </div>
    )
}

export default NoResult;