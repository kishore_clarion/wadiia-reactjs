import React, { Component } from 'react';
import {message} from 'antd';
import { connect } from "react-redux";
import * as actions from "../../../redux/index";

class ErrorNotification extends Component {
    state = {  }
    handleClose=()=>{
        this.props.errorClear();
    }
    render() { 
        message.config({maxCount: 1})
        return ( 

           this.props.err? message.error(this.props.err,10,this.handleClose) : null
         );
    }
}

const mapStateToProps = (state) => {    
    return {
        err: state.error.err
    };
};

export default connect(mapStateToProps, actions)(ErrorNotification);