import React, { Component } from 'react';
import { Link } from "react-router-dom";
import "./Page404.css";

class Page404 extends Component {
    state = {  }
    render() { 
        return (  
            <div className="container">
                <div className="page-not-found">
                    <div className="page-not-found__title">404</div>
                    <h1>Page Not Found</h1>
                    <Link to="/"><button className="btn btn-primary" >Go to home</button></Link>
                </div>
            </div>
        );
    }
}
 
export default Page404;