import React, { Component } from "react";
import "./FlightPayment.css";
import {searchObj,getStorageFor,getCurrency,checkIsRefundable} from "../../utils/commonFunc";
import {bookFlight} from "../../services/flightBooking";
import {message,Button} from 'antd';
import {messageDuration} from "../../services/constants";
import ReactGA from 'react-ga';
import { connect } from "react-redux";
import * as actions from "../../redux/index";
import {addPaymentInfo,purchase} from '../../services/Analytics/ga4';


class FlightPayment extends Component {
	state={
		paymentGateway:null,
		error:null,
		cancellationCheck:false,
		termCheck:false,
		coupon:"",		
		isRefundable:true
	}

	handleCheck=(name)=>{
		this.setState({[name]: !this.state[name]});
	}

	bookFlight=async ()=>{		
		const data={paymentGatewayId:this.state.paymentGateway?this.state.paymentGateway[0]:null}
		data.cartId=this.props.cartId;
		if(!this.state.paymentGateway) {
			message.error("Please select payment gateway",messageDuration);			
			return;
		}
		if(!this.state.termCheck) {
			this.setState({error:"check1"});
			message.error("Please accept the terms and conditions",messageDuration);					
			return;
		}
		if(!this.state.cancellationCheck) {	
			this.setState({error:"check2"});
			message.error("Please accept the non-refundable conditions",messageDuration);	
			return;
		}		
		if(data && data.paymentGatewayId && data.cartId) {
			this.setState({loadingButton:true});
			ReactGA.event({
				category: 'Flight Booking',
				action: 'User Proceed to book flight after payment process',
				label:'Flight Booking'
			});
			purchase({currency:this.state.currency,items:this.props.cart})
			const res=await bookFlight(data);			
			if(res.status) {
				const setting=this.props.appSettings?{...this.props.appSettings}:null;//getStorageFor("setting");
				const token=res.token;
				if(setting) {
					const url=setting.paymentHandlerUrl.split("{token}")[0];
					window.location.href = url+token;				
				}
			}else{
				message.error(res.message,messageDuration);								
				if(res.code===260030) {
					this.props.avalabilityChange();
				}
			}
			this.setState({loadingButton:false});
		}
	}

	subcharges=(values)=>{
		let total=0;
		values.map((val)=>{			
			if(val.purpose!=1 && val.purpose!=10) {				
				total=total+val.amount;
			}
		})
		return total.toFixed(2);
		
	}

	checkRefundable=()=>{	
		const refundable=checkIsRefundable(this.props.cartItems);		
		this.setState({isRefundable:refundable})	
	}

	componentDidMount=async()=>{
		this.checkRefundable();
		const currency=await getCurrency();
		this.setState({currency:currency});
	}

	render(){		
		return (
			<div className="m-t-20 flight-payment m-b-30">
				<div className="row">
					<div className="col-md-12">
						<div className="row">
							<div className="col-md-6">
								<div className="input-group">
									<div className="input-group-prepend">
										<div className="input-group-text">
											<img src={process.env.PUBLIC_URL +"images/icon_gift.png"} width="15" />Offer code
										</div>
									</div>
									<input type="text"  onChange={(e)=>this.setState({coupon:e.target.value})} className="form-control" placeholder="Enter offer code" value={this.state.coupon}/>
									<div className="input-group-append"><button className="btn btn-primary" onClick={()=>this.props.applyCoupon(this.state.coupon)}>Apply</button>
									</div>
								</div>
							</div>
							<div className="col-md-12 error">
							{(this.state.error && this.state.error!=="check1" && this.state.error!=="check2")?this.state.error:null }
							</div>
							<div className="col-md-12">
								<div className="white-bg m-t-20">
									<div className="d-flex align-items-center flight-payment__header">
										<img src={process.env.PUBLIC_URL +"images/icon_payment.png"} width="30"/> <h2>How would you like to pay?</h2>
									</div>
									<ul className="list-unstyled flight-payment__list">
										{
											this.props.paymentGateways.map(gateway=>
												<li key={gateway.id}>
													<h4>
														<label form="test">
															<input id="test" type="radio" name="test"  onChange={()=>{this.setState({paymentGateway:gateway.id}); addPaymentInfo({paymentType:gateway.name,coupon:this.state.coupon,currency:this.state.currency})}}/> {gateway.name}
														</label>
														<span>{this.state.currency+""}{searchObj(gateway.values,"purpose","10").amount}</span>
													</h4>
													<div className="row-wrapper">
													<div className="col-md-6">
														<div className="bg-light">
															<div className="row">
																<span className="col-8">Itinerary Amount : </span><b className="col-4">{this.state.currency+""}{searchObj(gateway.values,"purpose","1").amount}</b>
															</div>
															<div className="row">
																<span className="col-8">Service Charges / Gateway Charges : </span><b className="col-4">{this.state.currency+""}{this.subcharges(gateway.values)}</b>
															</div>
															<div className="row">
																<div className="col-md-12 border-top"></div>
															</div>
															<div className="row text-danger">
																<span className="col-8">Total Price : </span><b className="col-4">{this.state.currency+""}{searchObj(gateway.values,"purpose","10").amount}</b>
															</div>
														</div>
													</div>
													</div>
												</li>
											)
										}
									</ul>
								</div>
								<div className="m-t-20"><b className="text-primary">Please do not refresh the page on any of the next step(s) of this transaction. Kindly do not use duplicate tab for doing this transaction.</b></div>

								<div className="m-t-20 term-of-use">
									<label form="test-checkbox01">
										<input className={this.state.error=="check1"?"error":null} id="test-checkbox01" onChange={()=>this.handleCheck("termCheck")} type="checkbox" name="test-checkbox01" /> 
										I have READ and AGREED to all the <a href="#"> Terms of Use </a>  (These are different from the booking policies of individual items listed above)
									</label>

									{//this.props.cartItems && !this.props.cartItems.item[0].flags.isRefundable  
									!this.state.isRefundable && <label form="test-checkbox02">
										<input className={this.state.error=="check2"?"error":null} id="test-checkbox02" type="checkbox" onChange={()=>this.handleCheck("cancellationCheck")} name="test-checkbox02"/>I agree and understand that this is a non-refundable booking. Cancellation charges may apply at the time of cancellation.
									</label>}
								</div>
								<div className="m-t-20 complete-booking-wrap">
									<Button htmlType="submit" loading={this.state.loadingButton} className="btn btn-primary complete-booking" onClick={()=>{this.bookFlight()}}>Complete Booking</Button>
								</div>
							</div>
						</div>

					</div>
				</div>
			</div>
		)
	}
}

const mapStateToProps = (state) => {
    return {       
        appSettings: Object.keys(state.appSetting.setting).length>0?{...state.appSetting.setting}:null
    };
};

export default connect(mapStateToProps, actions)(FlightPayment);