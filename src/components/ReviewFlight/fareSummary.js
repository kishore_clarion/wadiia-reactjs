import React, { Component } from "react";
class FareSummary extends Component {
    state = {  }
    render() {
        let indx; 
        let paxArr=[{code:0,name:"Adult"},{code:1,name:"Child"},{code:2,name:"Infant"}]
        
        return ( 
            <div className="panel-group review-accordion" id="accordion">
            { this.props.rateInfo.fareBreakup.map((item,index)=>{ 
                indx=index;              
               let calculatedRate=0;
                if(item.item.length>1) {
                    item.item.map((obj)=>{
                        calculatedRate=calculatedRate+obj.totalAmount;
                    })
                }
                if(item.item[0].displayRateInfo[0].description!=="Total Amount") {
                    return <div className="panel panel-default" key={index}>
                        <div className="panel-heading">
                            <h4 className="panel-title">
                                <a data-toggle="collapse" data-parent="#accordion" href={`#collapse${index}`}>{`${item.item[0].displayRateInfo[0].description}            ${item.item.length>1?calculatedRate:item.item[0].displayTotalAmount}`}</a>
                            </h4>                                              
                        </div>
                        {
                            item.item.length>0 && <div id={`collapse${index}`} className="panel-collapse collapse in">
                            <div className="panel-body">
                                <div>
                                {
                                    item.item.map((paxRate,index)=>{  
                                        return  <div className="acc-basic-fare" key={"pax"+index}> <span> {`${paxRate.quantity} ${paxArr[+paxRate.code].name} (* ${paxRate.displayTotalAmount}) `} </span>  <strong> {`${paxRate.displayTotalAmount}`}</strong>  </div>
                                    })
                                } 
                                </div>
                            </div>
                        </div> }
                    </div>
                }
            })
               
            }
            
            <div className="d-flex review-fare-total">
                <span>Total Amount:</span>
                <strong>{`${this.props.rateInfo.fareBreakup[indx].item[0].displayTotalAmount}`}</strong>
            </div>
        </div>
         );
    }
}
 
export default FareSummary;