import React, { Component, PureComponent } from "react";
import "./ReviewFlight.css";
import { viewCart, clearCart, addTravellerToCart, removeCartItem, applyCoupon,fareRules,addCartAddons} from "../../services/flightBooking";
import { connect } from "react-redux";
import * as actions from "../../redux/index";
import FlightReview from "./flightReview";
import FareSummary from "./fareSummary";
import history from '../../utils/history';
import PhoneInput from 'react-phone-input-2'
import 'react-phone-input-2/lib/style.css';
import { countries } from "../../services/constants"
import 'react-phone-input-2/lib/material.css'
import _ from 'lodash';
import { Formik, Field, Form, ErrorMessage } from "formik";
import * as Yup from "yup";
import { message, Alert,Button, Spin } from 'antd';
import { minHHMM, getUserDataFromStore, preparePaymentGatewayAvailableList, getStorageFor, searchObj, getCurrency } from "../../utils/commonFunc";
import Traveller from "./traveller";
import LoadingOverlay from 'react-loading-overlay';
import FlightPayment from "./FlightPayment";
import moment from 'moment';
import { api } from "../../services/Api"
import {getUser} from "../../services/user"
import { Modal } from 'react-bootstrap';
import { Helmet } from "react-helmet";
import {ga4react,ga4Pageview,viewCartGA,removeFromCart} from '../../services/Analytics/ga4';
import {pageLoadTimeWithPageview} from '../../services/Analytics/universalProp';


class ReviewFlight extends PureComponent {
    state = {
        country: null,
        cartData: null,
        rawPhone: "",
        user: null,
        loading: true,
        phoneNumber: "",
        phoneCode: "",
        travellerList: [
            {
                firstNameTraveller: "",
                lastNameTraveller: ""
            }
        ],
        travellerSave: false,
        paymentGateways: [],
        uploadDoc: false,
        availabilityStatusMessage: [],
        currency: null,
        errors: {
            firstName: null,
            lastName: null,
            gender: null,
            phoneNumber:"",
            email:null,
            city:null,
            country:null
        },
        contact:{firstName: "", lastName: "",gender: "M",phoneNumber:"",email:"",city:"", country:"KE",PhoneNumberCountryCode:"",iternary:""},
        showDateChangeRules:false,
        fareRules:[],
        fareRulesArr:[],
        loadingRule:false,
        addons:[],
        pageTitle:"Review Flight",
        activeRoute:0
    }

    contactDivToFocus = React.createRef()
    travDivToFocus = React.createRef()

    componentDidMount = async () => {
        document.body.classList.add('hide-header-menu');        
        const cartId = this.props.cartLocal;//localStorage.getItem("cartLocal");        
        if (cartId) {
            const currency=await getCurrency();
            this.setState({ loading: true, currency: currency, cartData: null });
            const res = await viewCart(cartId);
            let uploadDoc = false; let usr; let country;let contact={...this.state.contact};           
            if (res.status && res.data.cartID !== "cart") {
                const cartD = { ...res.data };                
                const userRes = await api.post("user/details", { "Request": "", "Flags": {} });                
                if (userRes && userRes.data.status.code === 0) {
                    let user = userRes.data.response                                       
                    if (user.contactInformation.phoneNumberCountryCode) {
                        country = user.contactInformation.phoneNumberCountryCode;
                    }
                    user = { firstName: user.firstName, lastName: user.lastName, gender: user.gender, location: { countryID: user.location.countryID, city: user.location.city }, contactInformation: { ...user.contactInformation } }
                    contact={firstName: user.firstName, lastName: user.lastName, gender: user.gender,country:user.location.countryID,city: user.location.city,email:user.contactInformation.email,phoneNumber:user.contactInformation.phoneNumber,phoneNumberCountryCode:user.contactInformation.phoneNumberCountryCode?user.contactInformation.phoneNumberCountryCode:254,iternary:""};                    
                    usr = { ...user }
                }
                let setting = {...this.props.appSettings};//localStorage.getItem("setting");
                if (setting) {
                    //setting = typeof setting == "string" ? JSON.parse(setting) : null;
                    if (setting && setting.isUploadDocument) {
                        uploadDoc = true;
                    }
                }
                //Google Analytics
                viewCartGA(cartD);
                pageLoadTimeWithPageview({location:window.location.pathname,loadTime:Math.round(performance.now())});
                ga4Pageview({location:window.location.pathname,title:this.state.pageTitle});
                await this.setState({ user: usr, country, cartData: cartD, loading: false, uploadDoc: uploadDoc,contact:contact });                
            } else {                
                this.setState({ cartData: null, loading: false })
            }            
        } else {
            message.error("CartId is not available");
            history.push('/');
        }
    }

    componentWillUnmount() {
        document.body.classList.remove('hide-header-menu');
    }

    countryList = () => {
        let options = "";
        options = countries.map(country => {
            return <option value={country.code} key={country.code}>{country.name}</option>
        })
        return options;
    }
    componentDidUpdate = async (prevProps, prevState) => {
        //if user is logout then clear cart and user data
        if(!this.props.loading && this.props.status==="out") {
            const res = await clearCart(this.props.cartLocal);
            if (res.status) {               
                //clear cart using api and delete statedata for cart item
                await this.setState({ user: null, country: 254, cartData: null });
                history.push("/")
            } else {                
                await this.setState({ user: null, country: 254, cartData: null });
                message.error(res.code + " : " + res.message);
            }
        }               
    }

    handleOnChange = (value, data, event, formattedValue) => {        
        const contact = { ...this.state.contact };        
        if (Object.keys(data).length > 0) {
            if (Object.keys(contact).length > 0) {
                contact.phoneNumber = value.slice(data.dialCode.length);
                contact.phoneNumberCountryCode = data.dialCode
                this.setState({ contact: { ...contact }, phoneCode: data.dialCode })
            } else {
                const phoneNumber = value.slice(data.dialCode.length);
                const phoneNumberCountryCode = data.dialCode;
                this.setState({ phoneNumber: phoneNumberCountryCode + phoneNumber, phoneCode: data.dialCode })
            }
        }

    }
    FILE_SIZE = 4 * 1024 * 1024;
    SUPPORTED_FORMATS = [
        "image/jpg",
        "image/jpeg",
        "image/png"
    ];
    phoneRegExp = /^((\\+[1-9]{1,4}[ \\-]*)|(\\([0-9]{2,3}\\)[ \\-]*)|([0-9]{2,4})[ \\-]*)*?[0-9]{3,4}?[ \\-]*[0-9]{3,4}?$/;
    nameRegExp = /^[a-zA-Z]+$/;
    emailRegExp=/^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/
    fileObjShape = () => {
        if (this.state.uploadDoc) {
            return Yup.mixed()
                .when({
                    is: (value) => value === null,
                    then: Yup.mixed().test(
                        "file",
                        "File is required",
                        value => { return value != null }
                    )
                })
                .when({
                    is: (value) => value !== undefined,
                    then: Yup.mixed().test(
                        "fileSize",
                        "File is large than 4mb",
                        value => value && value.size <= this.FILE_SIZE
                    )
                        .test(
                        "fileFormat",
                        "Unsupported Format",
                        value => value && this.SUPPORTED_FORMATS.includes(value.type)
                        )
                })
        } else {
            return Yup.mixed().notRequired();
        }
    }
    validationShape = Yup.object().shape({    
        travellerList: Yup.array().of(
            Yup.object().shape({
                gender: Yup.string().required("Gender is required"),
                firstName: Yup.string()
                    .required("Name is required")
                    .matches(/^[a-zA-Z]*$/, "Only alphabets are allowed"),
                lastName: Yup.string()
                    .required("Surname is required")
                    .matches(/^[a-zA-Z]*$/, "Only alphabets are allowed"),
                birthDate: Yup.date()
                    .required("Birth date is required")
                    .nullable(),
                country: Yup.string()
                    .required("Nationality is required"),
                docType: Yup.string()
                    .notRequired(),
                    //.required("Document type is required"),
                issueCountry: Yup.string()
                    .required("Issue country is required"),
                file: this.fileObjShape(),
                passportNumber: Yup.string()
                    .when('docType', {
                        is: 'passport',
                        then: Yup.string()
                            .required("Passport Number is required")
                            .matches(/^[A-Z][0-9]{8}$/, "Passport Number is not valid")
                    }),
                expiryDate: Yup.date()
                    .nullable()
                    .when('docType', {
                        is: 'passport',
                        then: Yup.date()
                            .required("Expiry date is required")
                            .min(moment().add(6, 'M'), "Passport Expiry Date should be greater than 6 month of arrival date")
                            .nullable()
                    })
            })
        )/**/
    })


    setInitialValues = () => {
        const initialValues = {}        
        if (this.state.cartData && this.state.cartData.items.length>0 && this.state.cartData.items[0].data.paxInfo) {
            initialValues.travellerList = [];
            this.state.cartData.items[0].data.paxInfo.map((elm,index)=>{
                for(let i=0;i<elm.quantity;i++) {                    
                    initialValues.travellerList.push({
                        firstName: "", lastName: "", gender: "M", birthDate: null, country: JSON.stringify({ "name": "Kenya", "flag": "🇰🇪", "code": "KE", "dial_code": "+254" }),
                        docType: "", issueCountry: JSON.stringify({ "name": "Kenya", "flag": "🇰🇪", "code": "KE", "dial_code": "+254" }), file: null,
                        passportNumber: "", expiryDate: null
                    })                   
                }                
            })            
        }       
        return initialValues;
    }

    showError = (errors) => {
        if (errors.length > 0) {
            errors.map((error, index) => {               
                return <div key={index}>{error}</div>
            })
        }
    }

    prepareTraveller = (dataArrPax) => {
        const Arr = [];        
        dataArrPax.map(async (data, index) => {
            const arr = new Array(data.quantity).fill(0);
            return arr.forEach((d, ind) => {
                Arr.push({ type: data.typeString });               
            })
        })
        return Arr;
    }

    validateNames = (value, fieldName, error) => {        
        const commaRegex = /^[a-zA-Z]+$/
        if (commaRegex.test(value)) {
            error[fieldName] = "Not valid";
        }        
    }

    handleFlightChange = async () => {
        let recentSearch;// = localStorage.getItem("lastSearch");
        if(this.props.recentSearch) {
            recentSearch=[...this.props.recentSearch].reverse()[0];//localStorage.getItem('lastSearch');
        }
        if (recentSearch) {
            //GA event
            removeFromCart(this.state.cartData);
            const res = await clearCart(this.props.cartLocal);
            const passanger = typeof recentSearch.passanger === "string" ? recentSearch.passanger : JSON.stringify(recentSearch.passanger);
            if (res.status) {
                history.push(`/flight-listing?fromDate=${recentSearch.fromDate}&toDate=${recentSearch.toDate}&fromLocation=${JSON.stringify({ ...recentSearch.fromLocation })}&toLocation=${JSON.stringify({ ...recentSearch.toLocation })}&passanger=${passanger}&serviceClass=${recentSearch.serviceClass}&tripType=${recentSearch.tripType}&direct=${recentSearch.direct}`)
            } else {
                message.error(res.code + " : " + res.message);
            }
        } else {
            let res = await clearCart(this.props.cartLocal);
            history.push(`/`);
        }
    }

    handleAvailabilityChange = async () => {
        await this.setState({ loading: true });
        const cartId = this.props.cartLocal;//localStorage.getItem("cartLocal");
        const errMess = [];
        if (cartId) {
            const res = await viewCart(cartId);
            if (res.status && res.data.cartID !== "cart") {
                const cartData = res.data;
                await this.setState({ cartData: { ...res.data }, loading: false });
                if (cartData.items.length > 0) {
                    cartData.items.map((item, index) => {
                        if (item.availabilityStatus === 1) {
                            // You can proceed with updated price or can change flight!`});
                            errMess.push(`Price Changed ${item.data.config[1].value}: Old Price ${item.data.displayOriginalAmount} New Price ${item.data.displayAmount}.You can  change flight!`)

                        }
                        if (item.availabilityStatus === 2 || item.availabilityStatus === 3) {
                            errMess.push(`${item.data.config[1].value}:Selected flight is not available. Please Select another flight`)
                        }
                        if (errMess.length > 0)
                            this.setState({ travellerSave: false, availabilityStatusMessage: [...errMess] });                       
                    })
                }
            } else {
                await this.setState({ cartData: null, loading: false })
            }
        }
    }

    prepareWarningMess = () => {
        if (this.state.availabilityStatusMessage.length > 0) {
            return this.state.availabilityStatusMessage.map((msg) => {               
                return <div style={{ marginBottom: "14px" }}><Alert message={msg} type="info" /></div>
            })
        }
    }

    handleApplyCoupon = async (coupon) => {        
        if (coupon) {
            const data = { coupon: coupon, cartId: this.state.cartData.cartID }
            this.setState({ loading: true });
            const res = await applyCoupon(data);
            if (res.status) {
                if (res.discount < 0) {
                    const resView = await viewCart(this.state.cartData.cartID);                    
                    const setting = this.props.appSettings ? {...this.props.appSettings} : null;
                    if (resView.status && resView.data.cartID !== "cart" && setting) {
                        const arr = preparePaymentGatewayAvailableList(resView.data.paymentGatewayCharges, setting.paymentGatewayInputInfo);
                        const resObj={ travellerSave: true, paymentGateways: [...arr] }
                        await this.setState({ cartData: { ...resView.data }, loading: false, ...resObj })
                    } else {
                        await this.setState({ cartData: null, loading: false })
                    }
                }
                await this.setState({ loading: false })
            } else {
                await this.setState({ loading: false })
                message.error(res.code + " : " + res.message, );
            }
        }
    }

    getreviewItems = (flight) => {
        let revItems;
        if (flight && flight.items.length > 0) {
            const items = flight.items;
            revItems = items.map((item, index) => {
                if (item.data.business === "air" && item.isBookable) {                    
                    return <FlightReview key={index} flight={item} arrival={index === 1 ? true : false} />
                }
            })
            return revItems;
        }
    }
    getFareItems = (flight) => {
        let fareItems;
        if (flight && flight.items.length > 0) {
            const items = flight.items;
            fareItems = items.map((item, index) => {
                if (item.isBookable) {
                    return <FareSummary key={"fare" + index} rateInfo={item} />
                }
            })
            return fareItems;            
        }

    }

    handleContactValidation = async() => {
        const err={}
        if (!this.state.contact.firstName) {
            err.firstName = "Firstname is required"
        }
        if (!this.state.contact.firstName.match(this.nameRegExp)) {
            err.firstName = "Firstname is not valid"
        }
        if (!this.state.contact.lastName) {
            err.lastName = "Lastname is required"
        }
        if (!this.state.contact.lastName.match(this.nameRegExp)) {
            err.lastName = "Lastname is not valid"
        }
        if (!this.state.contact.gender) {
            err.gender = "This field is required"
        }
        if (!this.state.contact.phoneNumber) {
            err.phoneNumber = "Phone number is required"
        }
        if (this.state.contact.phoneNumber && !this.state.contact.phoneNumber.match(this.phoneRegExp)) {
            err.phoneNumber = "Phone number is not valid"
        }
        if (!this.state.contact.city) {
            err.city = "City is required"
        }
        if (!this.state.contact.city.match(/^[a-z A-Z]+$/)) {
            err.city = "City is not valid"
        }
        if (!this.state.contact.email) {
            err.email = "Email is required"
        }
        if (this.state.contact.email && !this.state.contact.email.match(this.emailRegExp)) {
            err.email = "Email is not valid"
        }
        if (!this.state.contact.country) {
            err.country = "Country is required"
        }
        await this.setState({errors:{...err}});
        this.handleScrollToError()
    }
    handleChange = (e, name) => {
        const value = e.target.value;
        const contact = { ...this.state.contact }
        contact[name] = value;
        this.setState({ contact: { ...contact } });
    }
    handleScrollToError=()=>{
        if(Object.keys(this.state.errors).length>0) {
            if(this.contactDivToFocus.current){
                this.contactDivToFocus.current.scrollIntoView({ 
                   behavior: "smooth"
                })
            }        

        } else if(!this.state.loadingButton) {                         
            if(this.travDivToFocus.current){
                this.travDivToFocus.current.scrollIntoView({ 
                   behavior: "smooth"
                })
            }
        }
    }

    fetchRules=async ()=>{
        //showDateChangeRules
        const data={searchToken:this.state.cartData.items[0].token,flightToken:this.state.cartData.items[0].detailsID};        
        if(!this.state.showDateChangeRules && this.state.fareRules.length<1){
            this.setState({loadingRule:true});
            const res=await fareRules(data);  
            //for oneway and grouped roundtrip          
            if(res.status) {
                //code for old structure
                //this.setState({fareRules:res.data[0].item,loadingRule:false});    
                //changes for new structure of API res
                this.setState({fareRules:res.data,loadingRule:false});                         
            } else {
                this.setState({loadingRule:false});
                message.error(res.message);               
            }
            //This is for separate view
            if(this.state.cartData.items.length>1) {
                const dataArr={searchToken:this.state.cartData.items[0].token,flightToken:this.state.cartData.items[1].detailsID};
                const resArr=await fareRules(dataArr);
                if(resArr.status) {
                    this.setState({fareRulesArr:resArr.data[0].item,loadingRule:false});                               
                } else {
                    this.setState({loadingRule:false});
                    message.error(resArr.message);               
                }
            }         
        }
        this.setState({showDateChangeRules:!this.state.showDateChangeRules});
    }
    
    showFareRules=(arr)=>{        
        if(arr.length>0) {
            return arr && arr.map((farerule,index)=>{
                return <div className="m-b-15 fare-rules-section" key={"fareRule"+index}>
                <div className="text-secondary">{farerule.key}</div>
                <div>{farerule.value}</div>
                </div>
            })
        } 
    }
    
    //Show farerule for oneway and group as per updated structure
    showFareRulesOneWayAndGroped=(airlineItemArr)=>{
        if(this.state.fareRules.length>0 && this.state.cartData.items[0].data.tripType==="Oneway") {
            return this.state.fareRules && this.state.fareRules.map((sequence,seqIndex)=>{
                return (
                <div key={seqIndex}> 
                <ul >
                    <b style={{'textTransform':'capitalize','color':'orange' }}>{`${this.state.cartData.items[0].data.items[0].item[seqIndex].locationInfo.fromLocation.name} - ${this.state.cartData.items[0].data.items[0].item[seqIndex].locationInfo.toLocation.name}`}</b>
                </ul>
                {
                    sequence.item.map((farerule,index)=>{
                        return (
                        <div key={"fareRule"+index}>
                        <div><b style={{'textTransform':'capitalize' ,'fontWeight': 'bold'}}>{farerule.key}</b></div>
                        <div>{farerule.value}</div>
                        </div>
                        )
                    })
                }
                </div>
                )
            });            
        } else if(this.state.fareRules.length>0 && this.state.cartData.items[0].data.tripType==="Roundtrip") {
            return airlineItemArr.map((item,seqIndex)=>{
                let fareRules=this.state.fareRules.filter((obj)=>{
                    return obj.sequence===item.sequenceNumber;
                });
                fareRules=fareRules[0];                
                return (
                <div key={item.sequenceNumber}> 
                <div>
                    {<b style={{'textTransform':'capitalize','color':'orange' }}>{`${item.locationInfo.fromLocation.name} - ${item.locationInfo.toLocation.name}`}</b> }
                </div>
                 {
                    fareRules.item.map((farerule,index)=>{
                        return (
                        <div key={"fareRule"+index}>
                        <div><b style={{'textTransform':'capitalize' ,'fontWeight': 'bold'}}>{farerule.key}</b></div>
                        <div>{farerule.value}</div>
                        </div>
                        )
                    })
                }
                </div>
                )
            })
        } 
    }

    handleClose = () => this.setState({showDateChangeRules:false});
    prepareAddonList=(travellerList)=>{
        const addonsList=[];
        travellerList.map(traveller=>{
            if(traveller.departureAddons){
                addonsList.push(traveller.departureAddons) 
            }
            if(traveller.arrivalAddons){
                addonsList.push(traveller.arrivalAddons) 
            }
        })
        if(addonsList.length>0)
            this.setState({addons:[...addonsList]})
    }
    render() {        
        this.setInitialValues();
        let flight, time, reviewItems, fareItems, traveller;
        if (!this.state.loading && this.state.cartData && !this.state.cartData.isBooked) {
            if (this.state.cartData.items.length === 0) {                
                history.push("/")
            }
            flight = { ...this.state.cartData };            
            time = minHHMM(flight.journeyDuration);
            if (flight && flight.items.length > 0) {                              
                traveller = this.prepareTraveller(flight.items[0].data.paxInfo);
            }
        } else {
            if (!this.state.loading){
                 history.push("/");
            }
        }
        let i = 0;
        return (
            <React.Fragment> 
            <Helmet>
                <title>{this.state.pageTitle}</title>
                <meta name="description" content="Review flight details before booking" />
            </Helmet>           
            <LoadingOverlay
                active={this.state.loading}
                spinner={true}
                text='Loading your content...'
            >
                <Modal show={this.state.showDateChangeRules} onHide={this.handleClose} size="lg">
                <Modal.Header closeButton>
                    <Modal.Title>Fare Rules</Modal.Title>
                </Modal.Header>
                <Modal.Body> 
                        {this.state.showDateChangeRules && <div> 
                            <div className="m-b-15"><strong> {this.state.cartData.items.length>1?"Departure":null} </strong></div>
                            {this.state.cartData.items[0].data.tripType==='Roundtrip' && this.state.cartData.items.length===1 &&
                            <React.Fragment>
                            <div className="airline-fare-rules__btn">
                            <button className={this.state.activeRoute==0?'btn btn-primary active':'btn btn-primary'} onClick={()=>this.setState({activeRoute:0})} >{`${this.state.cartData.items[0].data.items[0].locationInfo.fromLocation.id}-${this.state.cartData.items[0].data.items[0].locationInfo.toLocation.id}`}</button>
                            <button className={this.state.activeRoute==1?'btn btn-primary active':'btn btn-primary'} onClick={()=>this.setState({activeRoute:1})}>{`${this.state.cartData.items[0].data.items[1].locationInfo.fromLocation.id}-${this.state.cartData.items[0].data.items[1].locationInfo.toLocation.id}`}</button>                             
                            {this.state.activeRoute===0 && this.showFareRulesOneWayAndGroped(this.state.cartData.items[0].data.items[0].item)}                                             
                            {this.state.activeRoute===1 && this.showFareRulesOneWayAndGroped(this.state.cartData.items[0].data.items[1].item)}
                            </div>
                            </React.Fragment>
                            }
                            {
                            this.state.cartData.items[0].data.tripType==='Oneway' &&
                            this.showFareRulesOneWayAndGroped()                          
                            }
                        </div>}
                        {this.state.showDateChangeRules && this.state.cartData.items.length>1 && 
                        <div > 
                            <div className="m-b-15"><strong>{this.state.cartData.items.length>1?"Arrival":null} </strong></div>
                            {this.showFareRules(this.state.fareRulesArr)}
                        </div> }
                        
                </Modal.Body>
            </Modal>
                <div className="container">
                    {flight && Object.keys(flight).length > 0 && (
                        <React.Fragment>
                            <div className="review-booking-section">
                                <div className="row">
                                    <div className="col-lg-9 review-booking">
                                        <div className="review-booking__heading d-flex align-items-center justify-content-between">
                                            <div className="rvw-heading LatoBold">
                                                Itinerary
                                            </div>
                                            {!this.state.travellerSave && <div>
                                                <a onClick={this.handleFlightChange} className="btn btn-primary change-flight">Change Flight</a></div>}
                                        </div>
                                        {/*reviewItems*/this.getreviewItems({ ...flight })}
                                        {this.prepareWarningMess()}
                                        
                                                        {/*<div> {this.showError(errors)}</div>*/}
                                                        <div className="w-traveller-detail" ref={this.contactDivToFocus}>
                                                            <div className="d-flex align-items-center">
                                                                <h3>Enter Traveller Details: </h3>

                                                                {!this.state.user && <div className="review-login">
                                                                    <a className="text-danger" onClick={() => history.push({ pathname: '/login', state: { path: "/review-flight" } })}>Log in</a>
                                                                    <a className="text-danger" onClick={() => history.push({ pathname: '/signup', state: { path: "/review-flight" } })}>Sign Up</a> to book faster</div>}
                                                            </div>
                                                            <div className="white-bg">

                                                                <div className="row">
                                                                    <div className="col-md-3 w-traveller-detail__left">
                                                                        <div className="text-right"><strong>Contact Details</strong></div>
                                                                    </div>
                                                                    <div className="col-md-9 w-traveller-detail__right">
                                                                        <div className="w-traveller-detail__right-input row">
                                                                            <div className="input-wrapper col-md-6" >
                                                                            <input id="firstName" value={this.state.contact.firstName} name="firstName" maxLength="100" type="text" onChange={(e)=>this.handleChange(e,"firstName")} className={`form-control` + (this.state.errors.firstName ? " is-invalid" : "")} placeholder="First Name" />
                                                                                <div name="firstName" component="div" className="invalid-feedback" >{this.state.errors.firstName}</div>
                                                                            </div>
                                                                            <div className="input-wrapper col-md-6">
                                                                            <input id="lastName" value={this.state.contact.lastName} name="lastName" onChange={(e)=>this.handleChange(e,"lastName")} maxLength="100" type="text" className={`form-control` + (this.state.errors.lastName ? " is-invalid" : "")} placeholder="Last Name" />
                                                                                <div name="lastName" component="div" className="invalid-feedback" >{this.state.errors.lastName}</div>
                                                                            </div>
                                                                        </div>
                                                                        <div className="w-traveller-detail__right-input row">
                                                                            <div className="input-wrapper col-md-6">
                                                                            <input id="iternary" value={this.state.contact.iternary} name="iternary" maxLength="100" type="text" onChange={(e)=>this.handleChange(e,"iternary")} className={`form-control` + (this.state.errors.iternary ? " is-invalid" : "")} placeholder="Itinerary Name" />
                                                                                <div name="iternary" component="div" className="invalid-feedback" > {this.state.errors.iternary}</div>
                                                                            </div>
                                                                            <div className="input-wrapper col-md-6">
                                                                            <select value={this.state.contact.gender} name="gender" id="gender" as="select" placeholder="Gender" onChange={(e)=>this.handleChange(e,"gender")}
                                                                                    className={`form-control` + (this.state.errors.gender ? " is-invalid" : "")}>
                                                                                    <option value="M">Male</option>
                                                                                    <option value="F">Female</option>
                                                                                </select>
                                                                                <div name="gender" component="div" className="invalid-feedback" >{this.state.errors.gender}</div>
                                                                            </div>
                                                                        </div>
                                                                        <div className="w-traveller-detail__right-input row">
                                                                            <div className="input-wrapper col-md-6">
                                                                            <input id="email" name="email" value={this.state.contact.email} onChange={(e)=>this.handleChange(e,"email")} maxLength="100" type="text" className={`form-control` + (this.state.errors.email ? " is-invalid" : "")} placeholder="Email" />
                                                                                <div name="email" component="div" className="invalid-feedback" >{this.state.errors.email}</div>
                                                                            </div>
                                                                            <div className="input-wrapper col-md-6">
                                                                                <PhoneInput
                                                                                    name={"phoneNumber"}
                                                                                    placeholder={"Phone Number"}
                                                                                    enableSearch={true}
                                                                                    inputClass={`form-control` + (this.state.errors.phoneNumber ? " is-invalid" : "")}
                                                                                    country={this.state.contact.PhoneNumberCountryCode ? this.state.contact.PhoneNumberCountryCode : 254}
                                                                                    value={this.state.contact.phoneNumber&&this.state.contact.phoneNumberCountryCode ?  this.state.contact.phoneNumberCountryCode + this.state.contact.phoneNumber :  "254" + ""}
                                                                                    onChange={(value, data, event, formattedValue) => {
                                                                                        this.handleOnChange(value, data, event, formattedValue)
                                                                                        //setFieldValue("phoneNumber", value);
                                                                                    }}
                                                                                />
                                                                                <div name="phoneNumber" component="div" className="invalid-feedback show" >{this.state.errors.phoneNumber} </div>
                                                                            </div>
                                                                        </div>
                                                                        <div className="w-traveller-detail__right-input row">
                                                                            <div className="input-wrapper col-md-6">
                                                                            <input id="city"  name="city" value={this.state.contact.city}  onChange={(e)=>this.handleChange(e,"city")} maxLength="100" type="text" className={`form-control` + (this.state.errors.city ? " is-invalid" : "")} placeholder="City" />
                                                                                <div name="city" component="div" className="invalid-feedback" >{this.state.errors.city}</div>
                                                                            </div>
                                                                            <div className="input-wrapper col-md-6">
                                                                            <select as="select" id="country" name="country" value={this.state.contact.country} onChange={(e)=>this.handleChange(e,"country")} className={`form-control` + (this.state.errors.country ? " is-invalid" : "")} placeholder="Country" >
                                                                            <option value="">Select Country</option>
                                                                            {this.countryList()}
                                                                        </select>
                                                                        <div name="country" component="div" className="invalid-feedback" >{this.state.errors.country}</div>
                                                                            </div>
                                                                        </div>
                                                                        <p>our booking details will be sent to this email address.</p>{/* and mobile number.*/}

                                                                        <div className="traveller-info" ref={this.travDivToFocus}>
                                                                            <h4>Traveller Information</h4>
                                                                            <div>
                                                                                <span className="text-warning">Important Note</span>:
                                                                                    Please ensure that the names of the passengers on the travel documents is the same as on their government issued identity proof.
                                                                            </div>

                                                                        </div>

                                                                    </div>
                                                                </div>
                                                            </div>
                                                            </div>


                                                            {!this.state.travellerSave &&
                                            <Formik
                                                enableReinitialize={false}
                                                validateOnChange={false}
                                                validateOnBlur={false}
                                                validateOnMount={false}
                                                initialValues={this.setInitialValues()}
                                                //Validation schema
                                                validationSchema={this.validationShape}
                                                //Post data to API
                                                onSubmit={async (data, actions) => { 
                                                    if(Object.keys(this.state.errors).length===0) {
                                                        this.setState({ loadingButton: true})
                                                        const cartId = this.props.cartLocal;//localStorage.getItem("cartLocal");
                                                        const cartItemId = this.state.cartData.items[0].cartItemID;
                                                        data.cartId = cartId;
                                                        data.cartItemId = cartItemId;
                                                        data.phoneNumber=this.state.contact.phoneNumber;
                                                        data.firstName=this.state.contact.firstName;
                                                        data.lastName=this.state.contact.lastName;
                                                        data.gender=this.state.contact.gender;
                                                        data.email=this.state.contact.email;
                                                        data.city=this.state.contact.city;
                                                        data.country=this.state.contact.country;
                                                        data.iternary=this.state.contact.iternary;                                                        
                                                        data.phoneCode = this.state.phoneCode ? this.state.phoneCode : this.state.contact.phoneNumberCountryCode;
                                                        data.Token = this.state.cartData.items[0].token;
                                                        data.cartItemIdSep = null;
                                                        if (this.state.cartData.items.length > 1) {
                                                            data.cartItemIdSep = this.state.cartData.items[1].cartItemID;
                                                        }
                                                        this.prepareAddonList(data.travellerList);                                                    
                                                        const res = await addTravellerToCart(data);
                                                        if (res && res.status) {                                                           
                                                            if(this.state.addons.length>0){
                                                                const addonsRes=await addCartAddons({cartId:data.cartId,addons:[...this.state.addons]})
                                                                if (addonsRes && !addonsRes.status) {
                                                                    message.error(addonsRes.code + " : " + addonsRes.message);
                                                                }
                                                            }
                                                            const resView = await viewCart(this.state.cartData.cartID);    
                                                            const setting = this.props.appSettings?{...this.props.appSettings}:null;//getStorageFor("setting");                
                                                            if (resView.status && resView.data.cartID !== "cart" && setting) {                                                                
                                                                const arr = preparePaymentGatewayAvailableList(resView.data.paymentGatewayCharges, setting.paymentGatewayInputInfo);
                                                                const resObj={ travellerSave: true, paymentGateways: [...arr] }
                                                                await this.setState({ cartData: { ...resView.data }, loading: false, ...resObj })
                                                            } else {
                                                                await this.setState({ cartData: null, loading: false })
                                                            }
                                                        } else {
                                                            message.error(res.code + " : " + res.message);
                                                        }
                                                        this.setState({ loadingButton: false})
                                                    }
                                                }}
                                            >
                                                {({ values, errors, handleSubmit, setFieldValue, setFieldError, handleChange }) => (
                                                    
                                                    <Form>
                                                            <div className="w-traveller-detail">
                                                                <div className="white-bg">
                                                                <div className="row">
                                                                    {traveller.map((trav, index) => {
                                                                        let addons=null;
                                                                        if(flight.items[0].addons) {
                                                                            addons=flight.items[0].addons.availableAddons;
                                                                        }
                                                                        return <Traveller  index={index} type={trav.type} setFieldValue={setFieldValue} key={"traveller" + index} errors={{ ...errors }} value={{ ...values }} addons={addons}/>
                                                                    })

                                                                    }
                                                                </div>

                                                            </div>
                                                        </div>


                                                        <div className="review-add-on" className="d-none">
                                                            <h3>Add-ons</h3>
                                                            <div className="white-bg">
                                                                <div className="review-add-on__heading">
                                                                    <h4>Secure your trip</h4>
                                                                    <span>See all the benefits you get for just Rs. 299 (18% GST included)</span>
                                                                </div>
                                                                <div className="benefits-list row">
                                                                    <div className="col-md-3">
                                                                        <div className="benefits-item">
                                                                            <div className="benefits-item-wrap">
                                                                                <p className="benefit-type">Total loss of checked-in baggage</p>
                                                                                <p>Claim upto
                                                                    <span className="block">
                                                                                        <span className="font18">₹ 20,000</span>
                                                                                    </span>
                                                                                </p>
                                                                            </div>
                                                                        </div>
                                                                    </div>

                                                                    <div className="col-md-3">
                                                                        <div className="benefits-item">
                                                                            <div className="benefits-item-wrap">
                                                                                <p className="benefit-type">Personal Accident</p>
                                                                                <p>Claim upto
                                                                    <span className="block">
                                                                                        <span className="font18">₹ 10,00,000</span>
                                                                                    </span>
                                                                                </p>
                                                                            </div>
                                                                        </div>
                                                                    </div>

                                                                    <div className="col-md-3">
                                                                        <div className="benefits-item">
                                                                            <div className="benefits-item-wrap">
                                                                                <p className="benefit-type">Total loss of checked-in baggage</p>
                                                                                <p>Claim upto
                                                                    <span className="block">
                                                                                        <span className="font18">₹ 20,000</span>
                                                                                    </span>
                                                                                </p>
                                                                            </div>
                                                                        </div>
                                                                    </div>

                                                                    <div className="col-md-3">
                                                                        <div className="benefits-item">
                                                                            <div className="benefits-item-wrap">
                                                                                <p className="benefit-type">Total loss of checked-in baggage</p>
                                                                                <p>Claim upto
                                                                    <span className="block">
                                                                                        <span className="font18">₹ 20,000</span>
                                                                                    </span>
                                                                                </p>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>

                                                            <h3>Travel Protection <span>(Recommended)</span></h3>
                                                            <div className="review-add-on__travel white-bg">
                                                                <label><input type="checkbox" /> Yes, Add Travel Protection to protect my trip (Rs.269 per traveller)</label>
                                                                <div className="text-warning">
                                                                    6000+ travellers on Yatra protect their trip daily. <a href="#">Learn More</a>
                                                                </div>
                                                                <div className="review-add-on__cover">
                                                                    <div className="review-add-on__cover-heading">Cover includes:</div>
                                                                    <div className="review-add-on__cover--trip">
                                                                        <div className="row">
                                                                            <div className="text-center col-md-3">
                                                                                <div className="review-add-on__cover--trip-img">
                                                                                    <img width="80" src="images/icon_plane.png" alt="Trip Cancellation" title="Trip Cancellation" />
                                                                                </div>
                                                                                <span className="review-add-on__cover--trip-label">		Trip Cancellation
                                                                </span>
                                                                                <span className="flight-status">
                                                                                    <img src="images/icon_cancel.png" width="20" />
                                                                                </span>
                                                                                Claim upto  Rs.25,000
                                                            </div>
                                                                            <div className="text-center col-md-3">
                                                                                <div className="review-add-on__cover--trip-img">
                                                                                    <img width="80" src="images/icon_plane.png" alt="Trip Cancellation" title="Trip Cancellation" />
                                                                                </div>
                                                                                <span className="review-add-on__cover--trip-label">		Flight Delay
                                                                </span>
                                                                                <span className="flight-status">
                                                                                    <img src="images/icon_time_delay.png" width="25" />
                                                                                </span>
                                                                                Claim upto  Rs.25,000
                                                            </div>
                                                                            <div className="text-center col-md-3">
                                                                                <div className="review-add-on__cover--trip-img">
                                                                                    <img width="80" src="images/icon_plane.png" alt="Trip Cancellation" title="Trip Cancellation" />
                                                                                </div>
                                                                                <span className="review-add-on__cover--trip-label">		Loss of Baggage
                                                                </span>
                                                                                <span className="flight-status">
                                                                                    <img src="images/icon_baggage.png" width="25" />
                                                                                </span>
                                                                                Claim upto  Rs.25,000
                                                            </div>

                                                                            <div className="text-center col-md-3">
                                                                                <div className="review-add-on__cover--trip-img">
                                                                                    <img width="80" src="images/icon_plane.png" alt="Trip Cancellation" title="Trip Cancellation" />
                                                                                </div>
                                                                                <span className="review-add-on__cover--trip-label">		Medical Emergency
                                                                </span>
                                                                                <span className="flight-status">
                                                                                    <img src="images/icon_emergency.png" width="25" />
                                                                                </span>
                                                                                Claim upto  Rs.25,000
                                                            </div>

                                                                            <a href="#" className="review-add-on__travel-more">
                                                                                <img width="12" src="images/icon_more-arrow.png" alt="arrow" />
                                                                            </a>

                                                                        </div>

                                                                    </div>
                                                                    <div className="terms-conditions">
                                                                        Note: Travel Protection is applicable only for Indian citizens below the age of 70 years. <a href="#">Terms &amp; Conditions</a>
                                                                    </div>
                                                                </div>
                                                            </div>

                                                            <h3>Service Requests <span>(Optional)</span></h3>
                                                            <div className="review-add-on__service white-bg">
                                                                <h4>Add-ons (Optional)</h4>
                                                                <label><input type="checkbox" /> Flat Rs.1000 OFF on Domestic Flights round the year with the Yatra SBI Card. I want to be contacted by the SBI representative to receive more information.</label>

                                                                <label>
                                                                    <input type="checkbox" />
                                                                    <strong> TAKE OFF - Giving children a second chance at life!</strong> <br />
                                                                    <span>Rs. 10 will be added as a donation to Save the Children India. </span>
                                                                </label>

                                                            </div>

                                                        </div>
                                                        <div className="proceed-payment">
                                                            <Button htmlType="submit" loading={this.state.loadingButton} className="btn btn-primary" onClick={this.handleContactValidation}>Proceed to Payment</Button>
                                                        </div>
                                                    </Form>
                                                )}
                                            </Formik>
                                        }
                                    {/*** Payment component***/}
                                    {(this.state.travellerSave && this.state.paymentGateways.length > 0 && this.state.cartData.cartID) && <FlightPayment cart={this.state.cartData.items[0]} cartItems={this.state.cartData.items[0].data.items} cartId={this.state.cartData.cartID} paymentGateways={this.state.paymentGateways} avalabilityChange={this.handleAvailabilityChange} applyCoupon={this.handleApplyCoupon} />}

                                    </div>
                                    <div className="col-lg-3 review-right">
                                        <div className="review-right__inner">
                                            <div className="rvw-heading LatoBold">Fare Summary</div>
                                            {/*fareItems*/this.getFareItems({ ...flight })}
                                            <div className="d-flex rvw-heading">
                                                <span>Grand Total:</span>
                                                <strong>{this.state.currency + " " + searchObj(flight.displayCharges, "description", "Total").amount}</strong>
                                            </div>
                                            <div className="review-canlcellation">
                                                <div>Cancellation &amp; Date change charges</div>
                                                <a className="text-primary" href="#" data-toggle="modal" data-target="#reviewCancellation" onClick={()=>{this.fetchRules()}}>KNOW MORE</a>
                                            </div>
                                             <Spin tip="Loading..." spinning={this.state.loadingRule}/>                                          
                                        </div>
                                    </div>
                                </div>
                            </div>
                            
                        </React.Fragment>
                    )}
                </div>
            </LoadingOverlay>
            </React.Fragment>
        )
    }
}



const mapStateToProps = (state) => {
    return {
        //cartId: state.flights.cartId,
        loading: state.user.loading,
        status:state.user.status,
        cartLocal:state.user.cartLocal,
        recentSearch: state.user.recentSearch,
        appSettings: Object.keys(state.appSetting.setting).length>0?{...state.appSetting.setting}:null
    };
};

export default connect(mapStateToProps, actions)(ReviewFlight);
