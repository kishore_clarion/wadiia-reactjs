import React, { Component,PureComponent } from "react";
import { Field, ErrorMessage,FieldArray,getIn,FastField} from "formik";
import { countries } from "../../services/constants";
import {  DatePicker,Calendar } from "antd";
import moment from 'moment';
import _ from 'lodash';
import { connect } from "react-redux";
import * as actions from "../../redux/index";

class Traveller extends PureComponent {
    state = {
        docType:"",
        uploadDoc:false,
        travellerAddons:[],
        appliedAddonsList:[],
        showPreference:false
    }
    
    countryList = () => {
        let options = "";
        options = countries.map(country => {
            return <option value={JSON.stringify(country)} key={country.code}>{country.name}</option>
        })
        return options;
    }

    setDefaultDatePicker=()=>{
        if(this.props.type==="ADT") {
            return moment( moment().subtract(12, 'years'), "DD MMM, YYYY");
        }
        if(this.props.type==="CHD") {
            return moment( moment().subtract(2, 'years'), "DD MMM, YYYY");
        }     
    }

    disabledDate=(current)=> {
        // Can not select days before today 
        if(this.props.type==="ADT") {
            return current > moment().subtract(12, 'years') //&& current > moment().startOf('day')
        }
        if(this.props.type==="CHD"){
            //return current < moment().subtract(12, 'years')
            const customDate = new Date(new Date().setFullYear(new Date().getFullYear() - 12));
            return current && current > moment().startOf("day") || current && current < moment(customDate, "YYYY-MM-DD");
        }
        if(this.props.type==="INF") {
            //return current <= moment().subtract(2, 'years')
            const customDate = new Date(new Date().setFullYear(new Date().getFullYear() - 2));
            return current && current > moment().startOf("day") || current && current < moment(customDate, "YYYY-MM-DD");
        }
    }

    componentDidMount=()=>{
        let setting={...this.props.appSetting};//localStorage.getItem("setting");
        if(setting) {
            //setting=typeof setting=="string"?JSON.parse(setting):null;
            if(setting && setting.isUploadDocument) {
                this.setState({uploadDoc:true})
            } 
        }
        this.availableAddons();        
    }
   
    availableAddons=async ()=>{        
        const travellerAddons=[];
        if(this.props.addons){            
            const addons=[...this.props.addons];                     
            let i=0;
            const seq0=[];const seq1=[];
            
            //create two separet array for seq 0 and  1 and follow below step  
            addons.map((item)=>{
                if(item.sequence===0) {
                    return seq0.push(item)
                }
                if(item.sequence===1) {
                    return seq1.push(item)
                }                              
            })                
            seq0.length>0 && seq0.map((item,index)=>{
                const deprtureOption=[];
                const addonsItem=[...item.item];
                addonsItem.map((addonItem,i)=>{                    
                    return deprtureOption.push(<option key={"depatureAddon"+i} value={addonItem.id}>{addonItem.name}</option>)
                }) 
                travellerAddons[index]=[];
                if(deprtureOption.length>0){                   
                    return travellerAddons[index].push({departure:[...deprtureOption]})
                }           
            })             
            seq1.length>0 && seq1.map((item,index)=>{
                const arrivalOption=[];
                const addonsItem=[...item.item];
                addonsItem.map((addonItem,i)=>{                   
                    return arrivalOption.push(<option key={"arrivalAddon"+i} value={addonItem.id}>{addonItem.name}</option>);                                                          
                }) 
                return travellerAddons[index][0].arrival=[...arrivalOption];              
            })           
        }        
        await this.setState({travellerAddons:travellerAddons});         
    }

    optionList=(arr)=>{
        let options = "";
        options = arr.map(option => {
            return option
        })
        return options;
    }

    render() {
        return (
            <FieldArray
            name="travellerList"
            render={() => (
              <div className="col-md-12">
                <div className="adt-form"> 
                   <div className="w-traveller-detail__left">
                        <div><strong>{this.props.type}</strong></div>
                    </div>
                {                     
                  
                    <div key={this.props.index}>   
                        <div className="w-traveller-detail__right-input row">
                            <div className="input-wrapper col-md-3">
                                <FastField className="form-control" name={`travellerList[${this.props.index}].gender`} as="select" placeholder="Gender" >
                                    <option>Select Gender</option>
                                    <option value="M">Male</option>
                                    <option value="F">Female</option>
                                </FastField>
                                <div className="error"> {this.props.errors && this.props.errors.travellerList && this.props.errors.travellerList[this.props.index]? this.props.errors.travellerList[this.props.index].gender:null} </div>
                            </div>
                            <div className="input-wrapper col-md-3">
                                <FastField className="form-control" name={`travellerList[${this.props.index}].firstName`} placeholder="First Name" />
                                <div className="error"> {this.props.errors && this.props.errors.travellerList && this.props.errors.travellerList[this.props.index]? this.props.errors.travellerList[this.props.index].firstName:null} </div>
                            </div>
                            <div className="input-wrapper col-md-3">
                                <Field className="form-control" name={`travellerList[${this.props.index}].lastName`} placeholder="Last Name" />                               
                               <div className="error"> {this.props.errors && this.props.errors.travellerList && this.props.errors.travellerList[this.props.index]? this.props.errors.travellerList[this.props.index].lastName:null} </div>
                               
                            </div>
                            <div className="input-wrapper col-md-3">
                                <DatePicker                                 
                                dateFormat="yyyy-MM-dd"
                                name={`travellerList[${this.props.index}].birthDate`} 
                                placeholder="Birth Date" 
                                defaultPickerValue={this.setDefaultDatePicker}
                                bordered={true}                                
                                onChange={async (date,dateString)=>{                                   
                                    this.props.value.travellerList[this.props.index].birthDate=dateString; 
                                    this.props.value.travellerList[this.props.index].type=this.props.type;
                                }}
                                disabledDate={this.disabledDate} allowClear={true} />
                                <div className="error"> {this.props.errors && this.props.errors.travellerList && this.props.errors.travellerList[this.props.index]? this.props.errors.travellerList[this.props.index].birthDate:null} </div>
                            </div>
                        </div>
                        <div className="w-traveller-detail__right-input row">
                            
                            <div className="input-wrapper col-md-3">
                                <FastField className="form-control" as="select" name={`travellerList[${this.props.index}].country`} placeholder="Nationality">
                                    <option value="">Select Country</option>
                                    {this.countryList()}
                                </FastField>
                                <div className="error"> {this.props.errors && this.props.errors.travellerList && this.props.errors.travellerList[this.props.index]? this.props.errors.travellerList[this.props.index].country:null} </div>
                            </div>
                            <div className="input-wrapper col-md-3">
                                <FastField className="form-control" as="select" name={`travellerList[${this.props.index}].docType`} placeholder="Document Type">       
                                    <option value="">Document Type</option> 
                                    <option value="passport">Passport</option>                       
                                </FastField>
                               <div className="error"> {this.props.errors && this.props.errors.travellerList && this.props.errors.travellerList[this.props.index]? this.props.errors.travellerList[this.props.index].docType:null} </div>                              
                            </div>
                            <div className="input-wrapper col-md-3">
                                <Field name={`travellerList[${this.props.index}].issueCountry`} as="select" className="form-control">
                                <option value="">Select Country</option>
                                    {this.countryList()}
                                </Field> 
                                <div className="error"> {this.props.errors && this.props.errors.travellerList && this.props.errors.travellerList[this.props.index]? this.props.errors.travellerList[this.props.index].issueCountry:null} </div>                               
                            </div>
                            <div className="input-wrapper col-md-3">
                                {this.state.uploadDoc && 
                                    (<><input className="form-control" type="file" name={`travellerList[${this.props.index}].file`} 
                                        onChange={(event) => {
                                            this.props.setFieldValue(`travellerList[${this.props.index}].file`, event.currentTarget.files[0]);
                                        }}
                                    />
                                    <div className="error"> {this.props.errors && this.props.errors.travellerList && this.props.errors.travellerList[this.props.index]? this.props.errors.travellerList[this.props.index].file:null} </div></>)
                                }                                
                            </div>
                        </div>
                        { this.props.value.travellerList[this.props.index] && this.props.value.travellerList[this.props.index].docType && <div className="w-traveller-detail__right-input row">                            
                            <div className="input-wrapper col-md-3">
                                <Field className="form-control" name={`travellerList[${this.props.index}].passportNumber`} placeholder="Passport Number" />                               
                               <div className="error"> {this.props.errors && this.props.errors.travellerList && this.props.errors.travellerList[this.props.index]? this.props.errors.travellerList[this.props.index].passportNumber:null} </div>                               
                            </div>
                            <div className="input-wrapper col-md-3">
                            <DatePicker
                                dateFormat="yyyy-MM-dd"
                                name={`travellerList[${this.props.index}].expiryDate`} 
                                placeholder="Expiry Date" 
                                bordered={true} 
                                disabledDate={(current)=>current < moment().startOf('day')}
                                onChange={async (date,dateString)=>{this.props.value.travellerList[this.props.index].expiryDate=dateString;}}
                                allowClear={true} />
                                <div className="error"> {this.props.errors && this.props.errors.travellerList && this.props.errors.travellerList[this.props.index]? this.props.errors.travellerList[this.props.index].expiryDate:null} </div>
                            </div>
                        </div> }
                    </div>
                
                }

                <div className="flight-preferences m-t-15">
                <a 
                className={this.state.travellerAddons[this.props.index]&&this.state.travellerAddons[this.props.index].length>0?"":"d-none"}
                onClick={()=>{this.setState({showPreference:!this.state.showPreference})}}
                >
                    {/*<i class="fa fa-plus-circle" aria-hidden="true"></i>*/}
                    <i className={this.state.showPreference?"fa fa-minus-circle":"fa fa-plus-circle"} aria-hidden="true"></i>
                     Preferences &amp; Requests
                </a>
                {this.state.showPreference && (
                    <div className="row w-traveller-detail__right-input">
                        <div className="input-wrapper col-md-6">
                            { 
                                this.state.travellerAddons[this.props.index]&&this.state.travellerAddons[this.props.index].length>0 && this.state.travellerAddons[this.props.index][0].departure?
                                    <Field className="form-control" name={`travellerList[${this.props.index}].departureAddons`} as="select"> {/*onChange={(e)=>{this.setAppliedAddons(e.target.value,this.props.index,"departure")}}*/}
                                    <option value="">Departure Addons</option>
                                    {this.optionList(this.state.travellerAddons[this.props.index][0].departure)}
                                    </Field>
                                :null
                            }
                            </div>
                            <div className="input-wrapper col-md-6">
                            { 
                                this.state.travellerAddons[this.props.index]&&this.state.travellerAddons[this.props.index].length>0 && this.state.travellerAddons[this.props.index][0].arrival?
                                    <Field className="form-control" name={`travellerList[${this.props.index}].arrivalAddons`} as="select"> {/*onChange={(e)=>{this.setAppliedAddons(e.target.value,this.props.index,"arrival")}}*/}
                                    <option value="">Arrival Addons</option>
                                    {this.optionList(this.state.travellerAddons[this.props.index][0].arrival)}
                                    </Field>
                                :null
                            }
                        </div>
                    </div>
                )}
                    </div>                    
              </div>  
                              
              </div>
            ) }
          />
        )
    }
}
const mapStateToProps = (state) => {
    return {       
        appSetting: state.appSetting.setting
    };
};

export default connect(mapStateToProps, actions)(Traveller);