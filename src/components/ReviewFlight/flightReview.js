import React, { Component } from "react";
import FlightDetailDevider from "../flight/flightDetailDevider";
import FlightDetail from "../flight/flightDetailTab";
import RoundTripFlightGroupDetail from "../RoundTripFlight/RoundTripFlightGroupDetailTab";
//import RoundTripFlightGroupDetailDevider from "../RoundTripFlight/RoundTripFlightGroupDetailDevider";
import "./ReviewFlight.css";
import  moment  from "moment";
import { minHHMM,checkPartiallyRefundable } from "../../utils/commonFunc";

class FlightReview extends Component {
    state = {  }
    flightStops=(flight,index,separateType=false)=>{
        if(flight.data.tripType==="Oneway" ||  separateType) {
            if(flight.data.stops === 0)
               return <FlightDetail direct={true} stopsDetails={flight.data.items[0].item[0]} />
            if(flight.data.stops === 1) {
                return <div>
                            <FlightDetail stopsDetails={flight.data.items[0].item[0]} />
                            <FlightDetailDevider stopsDetails={flight.data.items[0].item[0]} />
                            <FlightDetail stopsDetails={flight.data.items[0].item[1]} />
                        </div>
            }
            if(flight.data.stops > 1 ) {
                return flight.data.items[0].item.map((stopsDetails, index) => {
                    return <div key={index}>
                        <FlightDetail stopsDetails={stopsDetails} />
                        {index < flight.data.items[0].item.length - 1 ? <FlightDetailDevider stopsDetails={stopsDetails} /> : null}
                    </div>
                })
            }
        }       
        if(flight.data.tripType==="Roundtrip" && !separateType){
            if(flight.data.stopDetails[index]===0) {
                return <RoundTripFlightGroupDetail direct={true} stopsDetails={flight.data.items[index].item[0]}/>
            }
            if(flight.data.stopDetails[index]===1){
                return <div>
                    <RoundTripFlightGroupDetail stopsDetails={flight.data.items[index].item[0]}/>
                    <FlightDetailDevider stopsDetails={flight.data.items[index].item[0]}/>
                    <RoundTripFlightGroupDetail stopsDetails={flight.data.items[index].item[1]}/>
                </div>
             }
            if(flight.data.stopDetails[index]>1 ) {               
                return flight.data.items[index].item.map((stopsDetails,ind)=> {
                    return <div key={ind}>
                                <RoundTripFlightGroupDetail stopsDetails={stopsDetails}/>
                                {ind<flight.data.items[index].item.length-1 ? <FlightDetailDevider stopsDetails={stopsDetails}/> : null }                                                        
                            </div>
                 })
            }                
            
        }
    }    
    render() {
        let time2;
        const flight=this.props.flight;
        const time =flight.data.tripType==="Oneway"? minHHMM(flight.data.journeyDuration):{hrs:flight.data.items[0].tpExtension[0].value,min:flight.data.items[0].tpExtension[1].value};
        time2=flight.data.tripType==="Roundtrip" && flight.data.items.length>1?{hrs:flight.data.items[1].tpExtension[0].value,min:flight.data.items[1].tpExtension[1].value}:null;
        return ( 
            <div className="w-flight-listing">
            <div className="w-flight-list">
                <div className="review-depart-info">
                    <div className="review-depart-info__left">
                        <div className="review-depart-info__title">
                            {this.props.arrival?"ARRIVAL": "DEPART"} 
                        <strong>{` `+moment(flight.data.dateInfo.startDate).format('ddd D MMM')}</strong>
                        </div>
                        <div className="review-depart-info__details">

                            {flight.data.locationInfo ? flight.data.locationInfo.fromLocation.id + "-" + flight.data.locationInfo.toLocation.id : null}
                            <span>{flight.data.tripType==="Oneway"?flight.data.stops:flight.data.stopDetails[0]} stop | {time.hrs} hrs {time.min} mins | Economy</span>
                        </div>
                    </div>
                    <div className="review-depart-info__right">
                        {checkPartiallyRefundable(flight.data.items[0]) &&<button>Partially Refundable</button>}
                        {/* <span className="fare-text">Fare Rules</span>
                        <span className="info-circle"><i className="fa fa-info-circle"></i> </span> */}
                    </div>
                </div>                 
            {flight.data.tripType==="Oneway"?this.flightStops(flight):null}
            {flight.data.tripType==="Roundtrip" && flight.data.items.length>1 ?this.flightStops(flight,0):null}           
                {flight.data.tripType==="Roundtrip" && flight.data.items.length>1 && <div className="review-depart-info">
                    <div className="review-depart-info__left">
                        <div className="review-depart-info__title">
                            RETURN 
                        <strong>{` `+moment(flight.data.items[1].dateInfo.startDate).format('ddd D MMM')}</strong>
                        </div>
                        <div className="review-depart-info__details">
                            {flight.data.items[1].locationInfo.fromLocation.id}-{flight.data.items[1].locationInfo.toLocation.id}
                            <span>{flight.data.stopDetails[1]} stop | {time2.hrs} hrs {time2.min} mins | Economy</span>
                        </div>
                    </div>
                    <div className="review-depart-info__right">
                        {checkPartiallyRefundable(flight.data.items[1]) && <button>Partially Refundable</button>}
                        {/* <span className="fare-text">Fare Rules</span>
                        <span className="info-circle"><i className="fa fa-info-circle"></i> </span> */}
                    </div>
            </div> }
                {flight.data.tripType==="Roundtrip" && flight.data.items.length>1?this.flightStops(flight,1):null}          
            {/**separate**/}

            {flight.data.tripType==="Roundtrip" && flight.data.items.length===1?this.flightStops(flight,0,1):null}
            
                <div className="baggage-info">
                    <div className="baggage-info__details baggage-info__name">
                        <span className="itnry-flt-footer-col">BAGGAGE : </span>
                        <span className="itnry-flt-footer-col">Check-in</span>
                        <span className="itnry-flt-footer-col">Cabin</span>
                    </div>
                    <div className="baggage-info__details baggage-info__desc">
                        <span className="itnry-flt-footer-col">ADULT</span>
                        <span className="itnry-flt-footer-col">15 Kgs</span>
                        <span className="itnry-flt-footer-col">7 Kgs</span>
                    </div>
                </div>

                <div className="extra-baggage-section" className="d-none">
                    <div className="extra-baggage-section__left">
                        <div className="text-danger extra-baggage-section__bag"> <span><i className="fa fa-briefcase"></i></span> Add Extra check-in baggage </div>
                        <p>Add Extra check-in baggage to this trip at a discounted price</p>
                    </div>
                    <div className="extra-baggage-section__right">
                        <button className="btn btn-primary" data-toggle="modal" data-target="#myModal"> + Add </button>
                    </div>
                </div>
            </div>
        </div>
         );
    }
}
 
export default FlightReview;