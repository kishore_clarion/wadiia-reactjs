

import React, { Component } from 'react';
import './FlightLists.css';
import Flight from '../flight/flight';
import { connect } from 'react-redux';
import { compose } from 'redux';
import * as actions from '../../redux/index';
import {getListFlights} from '../../services/search';
import InfiniteScroll from 'react-infinite-scroller';
import { Spin,message } from 'antd';
import { isMobile,isMobileOnly,isTablet,withOrientationChange} from 'react-device-detect';
import history from '../../utils/history';
import moment from 'moment';
import _ from 'lodash';
import LoadingOverlay from 'react-loading-overlay';
import RoundTripFlight from '../../components/RoundTripFlight/RoundTripFlight';
import RoundTripFlightGroup from '../../components/RoundTripFlight/RoundTripFlightGroup';
import ReviewStickyPanel from "../../components/RoundTripFlight/ReviewStickyPanel";
import { searchObj } from '../../utils/commonFunc';
import {messageDuration} from '../../services/constants';
import FlexiSearch from '../../components/FlightLists/FlexiSearch';
import NoResult from '../../components/Common/NoResult/No-result'

class FlightLists  extends Component {
    state = {        
        list: [],    
        loading:false,       
        sort:{
            departure:0,
            arrival:0,
            duration:0,
            price:1
        },
        departureSort:{
            departure:0,
            arrival:0,
            duration:0,
            price:1 
        },
        arrivalSort:{
            departure:0,
            arrival:0,
            duration:0,
            price:1 
        },
        selectedSepFlight:{departure:null,arrival:null},
        selectedSortSepFlightDep:"Price",
        selectedSortSepFlightArr:"Price",
        selectedProvider:null,
        flexiData:[]
       // reviewLoading:false
    }    
    componentDidMount=async()=> {
        document.body.classList.add('hide-header-menu');
        document.body.classList.add('header-scroll-up');
        /*Check response for separate flight */
        if(searchObj(this.props.resFlight.data,"type","departure") || searchObj(this.props.resFlight.data,"type","arrival")) {            
            this.setState({                           
                list: this.props.resFlight.data,
                loading: false
            });
        } else { 
            /*Oneway and roundtrip group */
            //for setting list for oneway and group roundtrip
            await this.flexSearchFilter(0, this.props.resFlight.data[0].item);
            if(this.state.flexiData.length>0 && this.state.list.length>0) {
                const flexiData=[...this.state.flexiData];
                flexiData.push(this.state.list[0]);
                this.setState({flexiData:[...flexiData]});
            }
        }       
        let selObj={}; 
        if(searchObj(this.props.resFlight.data,"type","departure")){
            let index=this.props.resFlight.data.findIndex(x => x.type === "departure");
            selObj.departure=this.props.resFlight.data[index].item[0];            
        }
        if(searchObj(this.props.resFlight.data,"type","arrival")){
            let index=this.props.resFlight.data.findIndex(x => x.type === "arrival");
            selObj.arrival=this.props.resFlight.data[index].item[0];
        }
        if(Object.keys(selObj).length>0) {            
            this.setState({selectedSepFlight:{...selObj},selectedProvider:selObj.departure.vendors[0].item.name})
        }      
        
    }

    flexSearchFilter=async(cnt,resList)=>{        
        //For oneway and grouped round trip        
        const list=[];const flexiData=[...this.state.flexiData];
        let index;            
        resList.forEach(flight => { 
            if(moment(flight.dateInfo.startDate).format('YYYY-MM-DD')===this.props.fromDate) {                             
                list.push(flight);
            } else { 
                //check if any flexi amount is greater that current flight amount with same date
                index = flexiData.findIndex((item,index)=>{
                    return moment(item.dateInfo.startDate).format('YYYY-MM-DD')===moment(flight.dateInfo.startDate).format('YYYY-MM-DD') && item.amount>flight.amount;                    
                });
                //if such record found replace that data with min price
                if(index>-1) {
                    flexiData[index]={...flight}
                } else {
                    //IF not, dont push current data directly else it create dublication for item wich is not less but it can be equal or less with same date
                    index = flexiData.findIndex((item,index)=>{ 
                        return moment(item.dateInfo.startDate).format('YYYY-MM-DD')===moment(flight.dateInfo.startDate).format('YYYY-MM-DD') && item.amount<=flight.amount;                    
                    });
                    //if again such index not found then and then only push data
                    if(index===-1) {
                        flexiData.push(flight);
                    } 
                }
            }
        }); 
        //Adding current search data in fliexi
        if(flexiData.length>0 && list.length>0) {
            const index = flexiData.findIndex((item,index)=>{ 
                return moment(item.dateInfo.startDate).format('YYYY-MM-DD')===moment(this.props.startDate).format('YYYY-MM-DD');                    
            });
            if(index===-1) {
                flexiData.push(list[0]);
            }
        }
        await this.setState({
            loading: false,            
            list: [...list],         
            flexiData:[...flexiData]
        });
        //IF current page search data is 0 and next page is true then call this function
        //&& !this.state.loading && (this.props.resFlight.pageInfoIndex[0].item.hasNextPage || this.hasMoreSeparateFlight())        
        if(cnt===this.state.list.length && !this.state.loading && (this.props.resFlight.pageInfoIndex[0].item.hasNextPage || this.hasMoreSeparateFlight())) {
            this.onLoadMore();
        }

    }

    componentWillUnmount() {
        document.body.classList.remove('hide-header-menu');
        document.body.classList.remove('header-scroll-up');
    }

    componentDidUpdate=async (prevProps,prevState)=> {            
        if(this.props.fromLocation!==prevProps.fromLocation || this.props.toLocation!==prevProps.toLocation ||
            this.props.tripType!==prevProps.tripType || this.props.fromDate !==prevProps.fromDate || this.props.toDate !==prevProps.toDate ||
            this.props.serviceClass!==prevProps.serviceClass ||  !_.isEqual(this.props.passanger,prevProps.passanger)  
            || (!_.isEqual(this.props.resFlight.appliedSorting, prevProps.resFlight.appliedSorting) && !this.props.rdxLoading)
            || (!_.isEqual(this.props.resFlight.data[0], prevProps.resFlight.data[0]) && !this.props.rdxLoading && this.props.type!=="pagination")
            || this.props.resFlight.data.length>=2 && (!_.isEqual(this.props.resFlight.data[1], prevProps.resFlight.data[1]) && !this.props.rdxLoading && this.props.type!=="pagination")
        ) {      
            //For separate flights
            if(searchObj(this.props.resFlight.data,"type","departure") || searchObj(this.props.resFlight.data,"type","arrival")) {
                let selObj={};
                if(searchObj(this.props.resFlight.data,"type","departure")){
                    let index=this.props.resFlight.data.findIndex(x => x.type === "departure");
                    selObj.departure=this.props.resFlight.data[index].item[0];            
                }                
                if(Object.keys(selObj).length>0) {
                    if(selObj.departure) {
                        this.setState({selectedSepFlight:{...selObj},
                            selectedProvider:selObj.departure.vendors[0].item.name,
                            list: this.props.resFlight.data,
                            loading: false})
                    } else {
                        this.setState({
                        list: this.props.resFlight.data,
                        loading: false})
                    }
                } else {
                    await this.setState({                               
                        list: this.props.resFlight.data,
                        loading: false
                    }); 
                }
            } else { 
                //For oneway and grouped round trip                
                this.flexSearchFilter(this.props.resFlight.data[0].item.length,this.props.resFlight.data[0].item);                
            }
        } 
        if(this.state.list.length===0 && !this.props.resFlight.pageInfoIndex[0].item.hasNextPage) {           
            this.props.showNoResult();
        }
    }    
    onLoadMore = async () => {
        this.setState({
            loading: true
        }); 
        if(this.checkSeparateFlights()){
            let departure=searchObj(this.props.resFlight.pageInfoIndex,"code","departure");
            let arrival=searchObj(this.props.resFlight.pageInfoIndex,"code","arrival");
            if(departure.item.hasNextPage || arrival.item.hasNextPage) {                
                let page=departure.item.currentPage+1;                
                let res=await getListFlights(this.props.tkn,this.props.resFlight.appliedFiltersIndex,page,this.props.resFlight.pageInfoIndex);
                if(res && res.status) {
                    await this.props.searchRequest({res:res.result,type:"pagination"});
                    if(res.code!==0) {
                        this.setState({loading: false});
                        message.warning(`${res.code}: ${res.message}`); 
                    }
                    if(!this.props.resFlight.pageInfoIndex[0].item.hasNextPage && !this.props.resFlight.pageInfoIndex[1].item.hasNextPage)
                        message.warning('All list is loaded !');                     
                    this.setSeperateList();                                 
                }
            }            
        } else if(this.props.resFlight.pageInfoIndex[0].item.hasNextPage) {
            let res=await getListFlights(this.props.tkn,this.props.resFlight.appliedFiltersIndex[0].item,this.props.resFlight.pageInfoIndex[0].item.currentPage+1);
            if(res && res.status) {
                await this.props.searchRequest({res:res.result,type:"pagination"});
                if(res.code!==0) {
                    this.setState({loading: false});
                    message.warning(`${res.code}: ${res.message}`); 
                }
                if(!this.props.resFlight.pageInfoIndex[0].item.hasNextPage)
                    message.warning('All list is loaded !');                    
                    this.flexSearchFilter(this.state.list.length,this.state.list.concat(this.props.resFlight.data[0].item));                  
                //this.setState({loading:false,list:this.state.list.concat(this.props.resFlight.data[0].item)})                                
            } 
        } else {
            this.setState({
                loading: false
            });
            message.warning('All list is loaded !'); 
        } 
    };

    setSeperateList=()=> {       
        const departure=searchObj(this.props.resFlight.data,"type","departure");
        const arrival=searchObj(this.props.resFlight.data,"type","arrival"); 
        const departureList=searchObj(this.state.list,"type","departure");        
        const arrivalList=searchObj(this.state.list,"type","arrival");        
        arrivalList.item=[...arrivalList.item,...arrival.item]//.concat(arrival.item);
        departureList.item=[...departureList.item,...departure.item]//.concat(departure.item);        
        this.setState({loading:false,list:[{...departureList},{...arrivalList}]});        
    } 
    checkSeparateFlights=()=> {
        if(this.props.resFlight.data && this.props.resFlight.data.length>1) {
            let departure=searchObj(this.props.resFlight.data,"type","departure");
            let arrival=searchObj(this.props.resFlight.data,"type","arrival");
            if(departure && arrival) {
                return true
            }
            return false;
        }
    }
    toggleOrderOneWayGroup = async (key)=> {
        let sortObj={departure:0, arrival:0,duration:0,price:0};
        if(this.state.sort[key]===0) {                       
            sortObj[key] = -1
            this.setState({sort:{...sortObj}})
            await this.props.sorting({sort:{"type": null,"order": -1, "name": `${key}`}});
        } else {
            let val=this.state.sort[key]===1?-1:1
            sortObj[key] = val;
            this.setState({sort:{...sortObj}})
            await this.props.sorting({sort:{"type": null,"order": val, "name": `${key}`}});
        }       
    } 
    
    toggleOrder = async (key,sepFlightSortType=0)=> {        
        let sortObj={departure:0, arrival:0,duration:0,price:0};
        let order=1;
        let apiSortObj={sort:{"type": null,"order": -1, "name": `${key}`}}
        let sortField;
        if(sepFlightSortType==="departure") {
           sortField="departureSort"
        } else if(sepFlightSortType==="arrival") {
            sortField="arrivalSort"
        } else {
            sortField="sort"
        } 
        //sepFlightSortType        
        if(key!=="price" && key!=="duration") {
            if(sortField && this.state[sortField][key]===0) {   // || this.state.sort[key]===0                    
                sortObj[key] = 1;            
            } else { 
                let val=this.state[sortField][key]===1?-1:this.state[sortField][key]===-1?1:-1
                sortObj[key] = val;
                order=val;  
                apiSortObj={sort:{"type": null,"order": order, "name": `${key}`}}        
            } 
        } else { 
            sortObj[key] = 1;             
            apiSortObj={sort:{"type": null,"order": 1, "name": `${key}`}}
        }         
        let obj={sort:{...sortObj}};
        if(sepFlightSortType=="departure") { 
            obj={departureSort:{...sortObj}}
            obj.selectedSortSepFlightDep=key;
            apiSortObj.type=sepFlightSortType;            
        }
        if(sepFlightSortType=="arrival") { 
            obj={arrivalSort:{...sortObj}}
            obj.selectedSortSepFlightArr=key;
            apiSortObj.type=sepFlightSortType;            
        }          
        await this.setState({...obj})       
        await this.props.sorting({...apiSortObj}); 
        //let res=sepFlightSortType?this.setSeperateList():null;
    }
    prepareListForSeparateFlights=()=>{
        let arr=[...this.state.list];
        if(arr.length>0) {
            let departure=searchObj(arr,"type","departure");            
            let arrival=searchObj(arr,"type","arrival");
            let departureArr=departure.item;
            let arrivalArr=arrival.item;
            let maxLength=departureArr.length>arrivalArr.length?departureArr.length:arrivalArr.length;
            let finalArr=[];
            for(let i=0; i<maxLength; i++) {  
                let obj={};                         
                if(departureArr[i]) {
                    obj.departure={...departureArr[i]}
                }
                if(arrivalArr[i]) {                   
                    obj.arrival={...arrivalArr[i]}
                }                
                finalArr.push(obj);
            }                       
            return finalArr;
        }
    }
    handleSelectionState=(obj)=>{                
        if(obj && Object.keys(obj).length>0) {
            let stateObj={...this.state.selectedSepFlight};
            stateObj[obj.key]=obj[obj.key];
            if(obj.key==="departure"){
                stateObj.arrival=null;
                this.setState({selectedSepFlight:{...stateObj},selectedProvider:obj[obj.key].vendors[0].item.name});
            } else
                this.setState({selectedSepFlight:{...stateObj}});
        }
    }    
    prepareSeparateFlightComp=()=> {
        let selObj={};
        let list=this.prepareListForSeparateFlights();               
        if(list && list.length>0) {            
            return list.map((flight,index)=>{
                let prop={}
                if(flight.departure)
                    prop.departureFlight=flight.departure;
                if(flight.arrival){
                    prop.arrivalFlight=flight.arrival;                    
                }               
                prop.selected=this.state.selectedSepFlight;
                return  <RoundTripFlight  key={index} {...prop} selectedProvider={this.state.selectedProvider}  handleSelection={this.handleSelectionState} />               
            })
        }  
    }

    hasMoreSeparateFlight=()=>{
        if(this.props.tripType==="Roundtrip" && this.state.list.length>0 && searchObj(this.props.resFlight.data,"type","departure")){
            let departure=searchObj(this.props.resFlight.pageInfoIndex,"code","departure");
            let arrival=searchObj(this.props.resFlight.pageInfoIndex,"code","arrival");
            if(departure.item.hasNextPage || arrival.item.hasNextPage) {
                return true;
            } else {
                return false;
            }
        } else {
            return false
        }
    }
        
    render() {                
        return ( 
            <div>
            <LoadingOverlay
                active={this.props.rdxLoading && this.props.type!=="filter"}
                spinner={true}
                text='Loading your content...'
            >
            </LoadingOverlay>
            { this.state.list.length>0 && !this.props.rdxLoading &&(
                <div className="flight-lists">
                    {(isMobileOnly || (isTablet && this.props.isPortrait)) && (
                        <div className="flight-short-dtails">
                            <header className="header-listing-page">
                                <div className="header-listing-page__back">
                                    <button onClick={()=>{history.push('/');}}><img src={process.env.PUBLIC_URL +"images/icon_back-arrow.png"} alt="Back-arrow" width="25" /></button>
                                </div>
                                <div className="header-listing-page__details">
                                    <div className="header-listing-page__details--left">
                                        <div>
                                            {typeof this.props.fromLocation ==="string" ? this.props.fromLocation : this.props.fromLocation.city }                                    
                                        </div>
                                            <span><img src={process.env.PUBLIC_URL +"images/icon_front-light-arrow.png"} width="15"/></span>
                                        <div>
                                            {typeof this.props.toLocation ==="string" ? this.props.toLocation : this.props.toLocation.city }                                    
                                        </div>
                                    </div>
                                    <div className="header-listing-page__details--details">{moment(this.props.fromDate).format("Do MMM YY")} | {this.props.total} Flights | {this.props.traveller} Traveler(s)</div>
                                </div>
                                <div className="header-listing-page__filter">
                                    <button onClick={()=>{this.props.showFilter("show")}}><img src={process.env.PUBLIC_URL +"images/icon_filter.png"} width="25"/></button>
                                </div>
                            </header>
                        </div>
                    )}
                    
                    {this.state.flexiData.length>0 && <FlexiSearch setFlexToSearch={this.handleSetFlexToSearch} flexiData={this.state.flexiData} searchDate={this.props.fromDate} searchToDate={this.props.toDate?this.props.toDate:null}/>}
                    { this.props.tripType==="Oneway" && (
                        <div className="w-flight-listing w-flight-list-heading-wrap">
                            <div className="w-flight-list-heading">
                                <ul className="wd-flex-center">
                                    <li> <strong>  Sorted By: </strong></li>
                                    <li onClick={()=>this.toggleOrderOneWayGroup("departure")}> Departure <i className={this.state.sort.departure?(this.state.sort.departure===1?"arrow up":"arrow down"):null} ></i> </li>
                                    <li onClick={()=>this.toggleOrderOneWayGroup("arrival")}> Arrival <i className={this.state.sort.arrival?(this.state.sort.arrival===1?"arrow up":"arrow down"):null} ></i></li>
                                    <li onClick={()=>this.toggleOrderOneWayGroup("duration")}> Duration <i className={this.state.sort.duration?(this.state.sort.duration===1?"arrow up":"arrow down"):null} ></i> </li>
                                    <li onClick={()=>this.toggleOrderOneWayGroup("price")}> Price <i className={this.state.sort.price?(this.state.sort.price===1?"arrow up":"arrow down"):null} ></i></li>
                                </ul>
                            </div>                                   
                        </div>  
                    )} 
                    { this.props.tripType==="Roundtrip" && !searchObj(this.props.resFlight.data,"type","departure") && this.state.list[0].items.length>1 && (                   
                        <div className="w-flight-listing w-flight-list-heading-wrap">
                                <div className="w-flight-list-heading">
                                    <ul className="wd-flex-center">
                                        <li> <strong>  Sorted By: </strong></li>
                                        <li onClick={()=>this.toggleOrderOneWayGroup("departure")}> Departure <i className={this.state.sort.departure?(this.state.sort.departure===1?"arrow up":"arrow down"):null} ></i> </li>
                                        <li onClick={()=>this.toggleOrderOneWayGroup("arrival")}> Arrival <i className={this.state.sort.arrival?(this.state.sort.arrival===1?"arrow up":"arrow down"):null} ></i></li>
                                        <li onClick={()=>this.toggleOrderOneWayGroup("duration")}> Duration <i className={this.state.sort.duration?(this.state.sort.duration===1?"arrow up":"arrow down"):null} ></i> </li>
                                        <li onClick={()=>this.toggleOrderOneWayGroup("price")}> Price <i className={this.state.sort.price?(this.state.sort.price===1?"arrow up":"arrow down"):null} ></i></li>
                                    </ul>
                                </div>                                   
                        </div>
                    )}  

                    { this.props.tripType==="Roundtrip" && this.state.list.length>1 && searchObj(this.props.resFlight.data,"type","departure") && (
                        <div className="row">                    
                            <div className="col-md-6 col flight-lists__sort-left">
                                <div className="row sort-by">
                                    <div className="col-md-6">
                                        <h2 className="sort-by__heading">Departure Flight</h2>
                                        <p className="sort-by__detail">{this.state.list.length>0 && this.state.list[0].item>0 && this.state.list[0].item[0].locationInfo?`${this.state.list[0].item[0].locationInfo.fromLocation.city} to ${this.state.list[0].item[0].locationInfo.toLocation.city} | ${moment(this.state.list[0].item[0].dateInfo.startDate).format("ddd, DD MMM")}`:null}</p>
                                    </div>
                                    <div className="col-md-6">
                                        <div className="dropdown">
                                            <button className="btn btn-default dropdown-toggle" type="button" data-toggle="dropdown">
                                            <span>Sorted By: 
                                                <strong style={{textTransform: "capitalize"}}>{this.state.selectedSortSepFlightDep}</strong>
                                            </span>
                                            <span className="caret"></span></button>
                                            <ul className="dropdown-menu">
                                                <li onClick={()=>this.toggleOrder("price","departure")}><a>Price</a></li>
                                                <li onClick={()=>this.toggleOrder("duration","departure")}><a>Duration</a></li>
                                                <li onClick={()=>this.toggleOrder("departure","departure")}><a>Departure({this.state.list[0].item[0].locationInfo.fromLocation.id})<i className={this.state.departureSort.departure?(this.state.departureSort.departure===1?"arrow up":"arrow down"):null} ></i></a></li>
                                                <li onClick={()=>this.toggleOrder("arrival","departure")}><a>Arrival({this.state.list[0].item[0].locationInfo.toLocation.id})<i className={this.state.departureSort.arrival?(this.state.departureSort.arrival===1?"arrow up":"arrow down"):null} ></i></a></li>
                                            </ul>
                                        </div>
                                    </div>	
                                </div>   
                            </div>
                            <div className="col-md-6 col flight-lists__sort-right">
                                <div className="row sort-by">
                                    <div className="col-md-6">
                                        <h2 className="sort-by__heading">Return Flight</h2>
                                        <p className="sort-by__detail">{this.state.list.length>0 && this.state.list[1].item>0 ?`${this.state.list[1].item[0].locationInfo.fromLocation.city} to ${this.state.list[1].item[0].locationInfo.toLocation.city} | ${moment(this.state.list[1].item[0].dateInfo.startDate).format("ddd, DD MMM")}`:null}</p>
                                    </div>
                                    <div className="col-md-6">
                                        <div className="dropdown">
                                            <button className="btn btn-default dropdown-toggle" type="button" data-toggle="dropdown">
                                            <span>Sorted By: 
                                            <strong style={{textTransform: "capitalize"}}>{this.state.selectedSortSepFlightArr}</strong>
                                            </span>
                                            <span className="caret"></span></button>
                                            <ul className="dropdown-menu">
                                                <li onClick={()=>this.toggleOrder("price","arrival")}><a>Price </a></li>
                                                <li onClick={()=>this.toggleOrder("duration","arrival")}><a>Duration</a></li>
                                                <li onClick={()=>this.toggleOrder("departure","arrival")}><a>Departure({this.state.list[0].item[0].locationInfo.toLocation.id})<i className={this.state.arrivalSort.departure?(this.state.arrivalSort.departure===1?"arrow up":"arrow down"):null} ></i></a></li>
                                                <li onClick={()=>this.toggleOrder("arrival","arrival")}><a>Arrival({this.state.list[0].item[0].locationInfo.fromLocation.id})<i className={this.state.arrivalSort.arrival?(this.state.arrivalSort.arrival===1?"arrow up":"arrow down"):null} ></i></a></li>
                                            </ul>
                                        </div>
                                    </div>	
                                </div>
                            </div>
                        </div>
                    )}                     
                    {  
                        <InfiniteScroll
                            initialLoad={false}
                            pageStart={0}
                            loadMore={this.onLoadMore}
                            hasMore={!this.state.loading && (this.props.resFlight.pageInfoIndex[0].item.hasNextPage || this.hasMoreSeparateFlight())}
                        >
                            { 
                                (!searchObj(this.props.resFlight.data,"type","departure")) ?                                     
                                    this.state.list.map((flight,index)=>{                                          
                                        if(this.props.tripType==="Oneway")  {                    
                                            return <Flight key={index} flight={flight} />
                                        } else if(this.props.tripType==="Roundtrip" && this.state.list.length>0 && this.state.list[0].items.length>0) {
                                            return  <RoundTripFlightGroup key={index} flight={flight} /> 
                                        }            
                                    }) 
                                : null
                            }  
                            {
                                (searchObj(this.props.resFlight.data,"type","departure") && searchObj(this.props.resFlight.data,"type","arrival")) ? 
                                    this.prepareSeparateFlightComp()
                                : null
                            }
                            {
                                this.state.loading && this.props.resFlight.pageInfoIndex[0].item.hasNextPage && (
                                <div className="demo-loading-container">
                                    <Spin />
                                </div>
                            )}
                        </InfiniteScroll>                                 
                    }                        
                </div>
            )}
            { this.state.list.length===0 && this.props.type==='filter'&& !this.props.rdxLoading &&( <NoResult/> )}
            {
                this.state.selectedSepFlight.departure && this.state.selectedSepFlight.arrival && (
                <ReviewStickyPanel 
                    departure={{...this.state.selectedSepFlight.departure}} 
                    arrival={{...this.state.selectedSepFlight.arrival}}
                />)
            }     
            </div>
        );
    }
}
 
//export default FlightLists;
const mapStateToProps = (state) => {
    return {
        type:state.flights.reqType,
        rdxLoading:state.flights.loading, 
        tkn : state.flights.token,
        resFlight:state.flights.response,
        fromLocation:state.flights.fromLocation,
        toLocation:state.flights.toLocation,
        tripType:state.flights.tripType,
        fromDate:state.flights.fromDate,
        toDate:state.flights.toDate,
        passanger:state.flights.passanger,
        serviceClass:state.flights.serviceClass
    };
  };
FlightLists = withOrientationChange(FlightLists)
export default compose(connect(mapStateToProps, actions))(FlightLists);