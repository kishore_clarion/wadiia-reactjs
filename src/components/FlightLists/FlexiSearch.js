import React from 'react';
import './FlightLists.css';
import moment from 'moment';
import { useDispatch } from 'react-redux';
import {setSearchWithFlexiDate} from '../../redux/flight/flightActions';
import {getDatesBetweenDates} from '../../utils/commonFunc';

const FlexiSearch =(props)=> {
    const dispatch = useDispatch();

    const updateSearch=(obj)=>{
        if(!obj.startDate) {
            return;
        } else {
            dispatch(setSearchWithFlexiDate(obj));
        }
    }
    const prepareFinalList=(dateList,dataList,tripType)=>{
        const finalList=[];    
        dateList.forEach((date)=>{
            const dateItr=moment(date).format('YYYY-MM-DD');            
            let index=dataList.findIndex(item=> dateItr===moment(item.dateInfo.startDate).format('YYYY-MM-DD'));           
            if(index<0) {
                const obj={tripType:tripType,dateInfo:{startDate:dateItr},displayAmount:"-"};
                if(tripType==="Roundtrip") {
                    const daysDiff=moment(props.searchToDate).diff(moment(props.searchDate), 'days');
                    console.log(daysDiff);
                    obj.dateInfo.endDate=moment(obj.dateInfo.startDate).add(daysDiff,'days').format('YYYY-MM-DD')
                }
                finalList.push(obj);
            } else {
                finalList.push(dataList[index]);
            }            
        });        
        return finalList;
    }
    const showList=()=>{
        let startDate,endDate;
        //Sort in ascending order
        let sortList=props.flexiData.sort(function(a,b){            
            return new Date(a.dateInfo.startDate) - new Date(b.dateInfo.startDate);
        });
        //console.log("Flexi list",sortList)
        startDate=moment(sortList[0].dateInfo.startDate).format('YYYY-MM-DD'); endDate=moment(sortList[sortList.length-1].dateInfo.startDate).format('YYYY-MM-DD');
        //console.log("start end date",startDate,endDate);
        const dateList=getDatesBetweenDates(startDate,endDate);
        //console.log("Date List",dateList);
        const finalList=prepareFinalList(dateList,sortList,sortList[0].tripType);
       // console.log("finalList",finalList);
        const list= finalList.map((item,index)=>{ 
            let obj={};
            if(item.tripType==="Oneway") {
               obj={startDate:moment(item.dateInfo.startDate).format('YYYY-MM-DD'),tripType:item.tripType}
            } else if(item.tripType==="Roundtrip"){
                obj={startDate:moment(item.dateInfo.startDate).format('YYYY-MM-DD'),endDate:moment(item.dateInfo.endDate).format('YYYY-MM-DD'),tripType:item.tripType};               
            }                      
            return  <li className={!obj.startDate?"disabled":null} onClick={()=>{dispatch(setSearchWithFlexiDate(obj))}} key={"flexiItem"+index} className={props.searchDate===moment(item.dateInfo.startDate).format('YYYY-MM-DD')?"active":null}>
                    <div className="items">
                        <div className="weekly-dates">
                            {moment(item.dateInfo.startDate).format('ddd,D MMM')}                            
                        </div>
                        <div className="weekly-price">
                            {item.displayAmount}
                        </div>
                    </div>
                </li>
        });       
        return list;
    }    
    return (
        <React.Fragment>
        <div className="date-selection">
        <a href="" className="date-selection__control"> <i className="fa fa-chevron-left"></i>  </a>
        <ul>
            {showList()}            
        </ul>
        <a href="" className="date-selection__control right"> <i className="fa fa-chevron-right"></i> </a>
        </div>
        </React.Fragment>
    )
}
 
export default FlexiSearch;