import React,{ Component } from  'react';
import './FlightListSideFilter.css';
import InputRange from 'react-input-range';
import 'react-input-range/lib/css/index.css';
import { isBrowser, isMobile, isMobileOnly, isTablet,withOrientationChange } from "react-device-detect";
import { connect } from "react-redux";
import { compose } from "redux";
import * as actions from "../../redux/index";
import { searchObj } from "../../utils/commonFunc";

class FlightListSideFilter extends Component {   
    // constructor(props) {
    //     super(props);
    //     this.state = {
    //         showAllFlights:false ,
    //         value: { min: +this.props.minVal, max: +this.props.maxVal },
    //         filterItems: this.props.resFlight.availableFiltersIndex[0].item,
    //         stops:[]
    //     }
    // }

    state = {
        showAllFlights:false ,
        value: {min:0,max:0},
        filterItems: [...this.props.resFlight.appliedFiltersIndex[0].item],
        stops:[],
        departurestarttime:[],
        departureendtime:[],
        arrivalstarttime:[],
        arrivalendtime:[],
        airline:[],
        typeFilter:searchObj(this.props.resFlight.appliedFiltersIndex,"code","departure")?"separate":null,
        filterItemsArrival:[...this.props.resFlight.appliedFiltersIndex[0].item]
    }
     
    // componentDidUpdate(prevProps,prevState) {

    // }
    componentDidMount() {
        const pricerange=this.checkFilterExist("pricerange");
        if(pricerange && this.state.value.min===0 && this.state.value.max===0) {
            this.setState({value:{min:+pricerange.minValue,max:+pricerange.maxValue}})
        }
    }
//const FlightListSideFilter = (props) => {
    // const [this.state,stateFun]=useState({
    //     showAllFlights:false ,
    //     value: { min: +props.minVal, max: +props.maxVal }
    // })
    toggleContent=()=> {        
        //stateFun({showAllFlights:!this.state.showAllFlights,value:this.state.value}); 
        this.setState({showAllFlights:!this.state.showAllFlights,value:this.state.value})       
    }
    checkFilterExist=(filterType)=>{
        const __FOUND = this.props.resFlight.availableFiltersIndex[0].item.find(function(filter, index) {            
            if(filter.name == filterType) {
                return true;
            }
            else {
                return false;
            }
        });
        return __FOUND;
    }
    stopFilter=async (checked,obj,reset)=>{
        let stops=[...this.state.stops];
        if(checked) {
            stops.push(obj.name);
        } else { 
            if(reset) {                
                stops=[];
            } else {           
                const index = stops.indexOf(obj.name);
                if(index>-1) {
                    stops.splice(index, 1)   
                }          
            }          
        }
        await this.setState({stops:stops});        
        const filtersArr=[...this.state.filterItems];
        filtersArr.forEach((element, index) => {
            if(element.name === "stops") {
                const tempObj = {...filtersArr[index]};
                if(stops.length > 0) {              
                    tempObj.values= [...this.state.stops];
                } else {
                    tempObj.values= ["direct","1stop","1plusstop"];   
                }             
                filtersArr[index] = tempObj;
            }
        });
       // this.setState({filterItems:filtersArr}); 
       // let objFiler={filters:this.state.filterItems,type:this.state.typeFilter,default:this.state.filterItemsArrival};                
        //await this.props.filter({filters:this.state.filterItems,type:this.state.typeFilter}); 
        //for separate view filter
        if(this.state.typeFilter){
            const secArr=[...this.state.filterItemsArrival]; console.log(filtersArr);
            const arrIndx=secArr.findIndex(x => x.name === "stops"); console.log(filtersArr);
            const tempObj = {...secArr[arrIndx]};
            if(stops.length > 0) {              
                tempObj.values= [...this.state.stops];
            } else {
                tempObj.values= ["direct","1stop","1plusstop"]; 
            }               
                secArr[arrIndx] = tempObj;
                this.setState({filterItems:filtersArr,filterItemsArrival:secArr});
        } else {
            this.setState({filterItems:filtersArr});
        }
        const objFiler={filters:this.state.filterItems,type:this.state.typeFilter,default:this.state.filterItemsArrival};
       // end separate view filter.   
        await this.props.filter({...objFiler});    
    }

    departureFilter=async (checked,obj,type,reset)=>{
        console.log(checked,obj);
        let departure=[...this.state[type]];
        const list=this.checkFilterExist(type);
        //type=="departurestarttime"?departure=[...this.state.departurestarttime]:departure=[...this.state.departureendtime]
        if(checked) {
            departure.push(obj);
        } else { 
            if(reset) {                
                departure=[];
            } else {           
                const index = departure.findIndex(elm=>elm.minValue===obj.minValue && elm.maxValue===obj.maxValue);
                if(index>-1) {
                    departure.splice(index, 1)
                }             
            }          
        }
        console.log(departure);
        await this.setState({[type]:departure});        
        const filtersArr=[...this.state.filterItems];
        filtersArr.forEach((element, index) => {
            if(element.name === type) {
                const tempObj = {...filtersArr[index]};
                if(departure.length > 0) {              
                    tempObj.minMaxList= [...this.state[type]];
                } else {
                    tempObj.minMaxList= [...list.minMaxList] ;     
                }           
                filtersArr[index] = tempObj;
                console.log(tempObj);
            }
        });        
        this.setState({filterItems:filtersArr});        
        await this.props.filter({filters:this.state.filterItems,type:this.state.typeFilter,default:this.state.filterItemsArrival});  
             
    }

    arrivalFilter=async (checked,obj,type,reset)=>{
        console.log(checked,obj);
        let arrival=[...this.state[type]];
        const list=this.checkFilterExist(type);
        //type=="departurestarttime"?departure=[...this.state.departurestarttime]:departure=[...this.state.departureendtime]
        if(checked) {
            arrival.push(obj);
        } else { 
            if(reset) {                
                arrival=[];
            } else {           
                const index = arrival.findIndex(elm=>elm.minValue===obj.minValue && elm.maxValue===obj.maxValue);
                if(index>-1) {
                    arrival.splice(index, 1)     
                }        
            }          
        }
        console.log(arrival);
        await this.setState({[type]:arrival});        
        const filtersArr=this.state.typeFilter?[...this.state.filterItemsArrival]:[...this.state.filterItems];
        filtersArr.forEach((element, index) => {
            if(element.name === type) {
                const tempObj = {...filtersArr[index]};
                if(arrival.length > 0) {              
                    tempObj.minMaxList= [...this.state[type]];
                } else {
                    tempObj.minMaxList= [...list.minMaxList] ;  
                }              
                filtersArr[index] = tempObj;
                console.log(tempObj);
            }
        });
        console.log(filtersArr);     
        if(this.state.typeFilter){            
            this.setState({filterItemsArrival:filtersArr});   
        } else {
            this.setState({filterItems:filtersArr});
        }
        const objFiler={filters:this.state.filterItems,type:this.state.typeFilter,default:this.state.filterItemsArrival};
        
        //await this.props.filter({filters:this.state.filterItems,type:this.state.typeFilter});        
        await this.props.filter({...objFiler}); 
        
        console.log(objFiler);
        //this.setState({filterItems:filtersArr});        
        await this.props.filter({...objFiler});  
             
    }

    airlineFilter=async (checked,obj,reset)=>{        
        let airline=[...this.state.airline];
        const list=this.checkFilterExist("airline");
        let airlineIndex=0;
        if(checked) {
            airline.push(obj.name);
        } else { 
            if(reset) {                
                airline=[];
            } else {           
            const index = airline.indexOf(obj.name);
            if(index>-1) {
                airline.splice(index, 1) 
            }            
            }          
        }
        await this.setState({airline:airline});        
        const filtersArr=[...this.state.filterItems];
        filtersArr.forEach((element, index) => {
            if(element.name === "airline") {
                airlineIndex=index;
                const tempObj = {...filtersArr[index]};
                if(airline.length > 0) {              
                    tempObj.values= [...this.state.airline];
                } else {
                    tempObj.values= [...list.values]; 
                }
                filtersArr[index] = tempObj;
            }
        });
        //this.setState({filterItems:filtersArr}); 
        console.log(filtersArr);     
        if(this.state.typeFilter){
            const secArr=[...this.state.filterItemsArrival]; console.log(filtersArr);
            const arrIndx=secArr.findIndex(x => x.name === "airline"); console.log(filtersArr);
            secArr[arrIndx]=arrIndx>-1 && airlineIndex? {...filtersArr[airlineIndex]} : null; console.log(filtersArr);
            this.setState({filterItems:filtersArr,filterItemsArrival:secArr});   
        } else {
            this.setState({filterItems:filtersArr});
        }
        const objFiler={filters:this.state.filterItems,type:this.state.typeFilter,default:this.state.filterItemsArrival};
        
        //await this.props.filter({filters:this.state.filterItems,type:this.state.typeFilter});        
        await this.props.filter({...objFiler}); 
    }

    priceFilter=async(val,reset)=>{
        let value=val;
        console.log(value);
       // this.setState({showAllFlights:this.state.showAllFlights,value:value})
        let priceRange;
        const obj=this.checkFilterExist("pricerange");
        console.log("obj",obj);
        if(reset) {  
            value={min:+obj.minValue,max:+obj.maxValue}              
            priceRange={};
        } else {
            priceRange={minValue:+value.min,maxValue:+value.max};
        }        
        await this.setState({value:value});        
        const filtersArr=[...this.state.filterItems];
        filtersArr.forEach((element, index) => {
            if(element.name === "pricerange") {
                const tempObj = {...filtersArr[index]};
                if(Object.keys(priceRange).length > 0) {              
                    tempObj.minValue= this.state.value.min+"";
                    tempObj.maxValue= this.state.value.max+"";
                } else {
                    tempObj.minValue= obj.minValue+"";
                    tempObj.maxValue= obj.maxValue+"";
                    this.setState({value:{min:+obj.minValue,max:+obj.maxValue}})
                }                                   
                filtersArr[index] = tempObj;
            }
        });
        this.setState({filterItems:filtersArr});        
        await this.props.filter({filters:this.state.filterItems,type:this.state.typeFilter,default:this.state.filterItemsArrival});
    }

    render(){ 
        const stops=this.checkFilterExist("stops");
        const departurestarttime=this.checkFilterExist("departurestarttime");
        const departureendtime=this.checkFilterExist("departureendtime");
        const airline=this.checkFilterExist("airline");
        const pricerange=this.checkFilterExist("pricerange");
        const arrivalstarttime=this.checkFilterExist("arrivalstarttime");
        const arrivalendtime=this.checkFilterExist("arrivalendtime");
    return (
        <div className="w-filters-sidebar">
            <div className="w-filter-wrap">
            {(isMobileOnly || (isTablet && this.props.isPortrait)) && (
                <div className="w-filter-wrap__header">
                    <button onClick={()=>{this.props.hideFilter("hide")}}> <img src={process.env.PUBLIC_URL +"images/icon_back-arrow.png"} alt="Back-arrow" width="25" /> </button>
                    <span>Filters</span>
                </div>
                )}
                { stops && (<div className="w-filters__stops">
                    <div className="w-filters__sec"> 
                        <div className="w-filter__heading">
                            <span> <strong>Stops</strong> </span>
                            <span><a onClick={(e)=>{this.stopFilter(null,null,true)}}>Reset</a></span>
                        </div>
                        <div className="w-filters__options">
                           {                              
                            stops.minMaxList.map((obj,index)=>{
                                return <label key={"stops"+index}> <input key={"stops"+index} type="checkbox" name="stops" checked={this.state.stops.indexOf(obj.name)>-1} value={obj.name} onChange={(e)=>{this.stopFilter(e.target.checked,obj)}} /> {obj.name} </label>
                            }) 
                            }                           
                        </div>
                    </div>
                </div>)}
                {departurestarttime && (<div className="w-filters__departure">
                    <div className="w-filters__sec">
                        <div className="w-filter__heading">
                            <span> <strong>  Departure From {this.props.departure.city}  </strong></span>
                            <span><a onClick={(e)=>this.departureFilter(null,null,"departurestarttime",true)} >Reset</a></span>
                        </div>
                        <div className="w-filters__options">
                            <div className="w-filters__options-col2">
                                {
                                    departurestarttime.minMaxList.map((time,index)=>{
                                        return <label key={"departure"+index}> <input type="checkbox"  checked={this.state.departurestarttime.find(elm=>elm.minValue==time.minValue && elm.maxValue===time.maxValue)}  name="departurestarttime" key={"departure"+index} value={time} onChange={(e)=>this.departureFilter(e.target.checked,time,"departurestarttime")}/>{`${time.minValue} - ${time.maxValue}`} </label>  
                                    })
                                }
                               
                            </div>                            
                        </div>
                    </div>
                </div>)}
                {departureendtime && (<div className="w-filters__arrival">
                    <div className="w-filters__sec">
                        <div className="w-filter__heading">
                            <span> <strong>  Arrival at {this.props.arrival.city} </strong></span>
                            <span><a onClick={(e)=>this.departureFilter(null,null,"departureendtime",true)} >Reset</a></span>
                        </div>
                        <div className="w-filters__options">
                            <div className="w-filters__options-col2">
                            {
                                departureendtime.minMaxList.map((time,index)=>{
                                    return <label key={"arrival"+index}> <input type="checkbox"  checked={this.state.departureendtime.find(elm=>elm.minValue==time.minValue && elm.maxValue===time.maxValue)} key={"arrival"+index} name="departureendtime" value={time} onChange={(e)=>this.departureFilter(e.target.checked,time,"departureendtime")}/> {`${time.minValue} - ${time.maxValue}`} </label>
                                })
                            }
                            </div>                            
                        </div>
                    </div>
                </div>)}
                {arrivalstarttime && (<div className="w-filters__departure">
                    <div className="w-filters__sec">
                        <div className="w-filter__heading">
                            <span> <strong>  Departure From {this.props.arrival.city}  </strong></span>
                            <span><a onClick={(e)=>this.arrivalFilter(null,null,"arrivalstarttime",true)} >Reset</a></span>
                        </div>
                        <div className="w-filters__options">
                            <div className="w-filters__options-col2">
                                {
                                    arrivalstarttime.minMaxList.map((time,index)=>{
                                        return <label key={"departure"+index}> <input type="checkbox"  checked={this.state.arrivalstarttime.find(elm=>elm.minValue==time.minValue && elm.maxValue===time.maxValue)}  name="arrivalstarttime" key={"arrival"+index} value={time} onChange={(e)=>this.arrivalFilter(e.target.checked,time,"arrivalstarttime")}/>{`${time.minValue} - ${time.maxValue}`} </label>  
                                    })
                                }
                               
                            </div>                            
                        </div>
                    </div>
                </div>)}
                {arrivalendtime && (<div className="w-filters__arrival">
                    <div className="w-filters__sec">
                        <div className="w-filter__heading">
                            <span> <strong>  Arrival at {this.props.departure.city} </strong></span>
                            <span><a onClick={(e)=>this.arrivalFilter(null,null,"arrivalendtime",true)} >Reset</a></span>
                        </div>
                        <div className="w-filters__options">
                            <div className="w-filters__options-col2">
                            {
                                arrivalstarttime.minMaxList.map((time,index)=>{
                                    return <label key={"arrival"+index}> <input type="checkbox"  checked={this.state.arrivalendtime.find(elm=>elm.minValue==time.minValue && elm.maxValue===time.maxValue)} key={"arrival"+index} name="arrivalendtime" value={time} onChange={(e)=>this.arrivalFilter(e.target.checked,time,"arrivalendtime")}/> {`${time.minValue} - ${time.maxValue}`} </label>
                                })
                            }
                            </div>                            
                        </div>
                    </div>
                </div>)}
                {airline && (<div className="w-filter-airlines">
                    <div className="w-filters__sec">
                        <div className="w-filter__heading"> 
                        <span> <strong>Airlines</strong> {airline.minMaxList.length>4 && <a onClick={()=>this.toggleContent()} >{`+${airline.minMaxList.length-4}`} airlines</a>} </span>
                            <span><a onClick={(e)=>{this.airlineFilter(null,null,true)}}>Reset</a></span>
                        </div>
                        <div className="w-filters__options">
                            <div className="w-filters__options-col2">
                                {  
                                    airline.minMaxList.map((air,index)=> { 
                                        if(index<4)                                   
                                        return <label key={"airline"+index}> <input type="checkbox" checked={this.state.airline.indexOf(air.name)>-1}
                                        onChange={(e)=>{this.airlineFilter(e.target.checked,air)}} key={"airline"+index} name="airline" value={air.name} 
                                        /> {`${air.name}  ${air.minValue}`}  </label>
                                    })
                                }
                                {  
                                    airline.minMaxList.length>4 && this.state.showAllFlights ? 
                                    airline.minMaxList.map((air,index)=> { 
                                        if(index>3)                                   
                                        return <label key={"airline"+index}> <input type="checkbox" checked={this.state.airline.indexOf(air.name)>-1}
                                        onChange={(e)=>{this.airlineFilter(e.target.checked,air)}}  key={"airline"+index} name="airline"
                                         value={air.name} 
                                         /> {`${air.name}  ${air.minValue}`}  </label>
                                    }) 
                                    : null
                                }                                
                            </div>                            
                        </div>
                    </div>
                </div>)}

                {pricerange &&  (
                    <div className="w-filters__sec">
                        <div className="w-filter__heading">
                            <span> 
                                <strong>Price</strong> 
                            </span>
                            <span><a onClick={(e)=>{this.priceFilter(null,true)}} >Reset</a></span>
                        </div>
                        <div className="w-filters__options">                    
                            <div className="input-range-wrapper">   
                            <InputRange
                                formatLabel={value => `$${value}`}
                                allowSameValues={true}
                                maxValue={+pricerange.maxValue}
                                minValue={+pricerange.minValue}
                                value={this.state.value}
                                onChange={value => this.setState({ value:value })}
                                onChangeComplete={value => this.priceFilter(value)} />               
                            </div>
                        </div>
                    </div>
                )}
               

            </div>
        </div>
    )
    }
}

const mapStateToProps = (state) => {
    return { 
        tkn : state.flights.token,
        resFlight:state.flights.response,
        fromLocation:state.flights.fromLocation,
        toLocation:state.flights.toLocation,
        tripType:state.flights.tripType,
        fromDate:state.flights.fromDate,
        toDate:state.flights.toDate,
        passanger:state.flights.passanger,
        serviceClass:state.flights.serviceClass
    };
  };

FlightListSideFilter = withOrientationChange(FlightListSideFilter)  
export default compose(connect(mapStateToProps, actions))(FlightListSideFilter);
//export default FlightListSideFilter;