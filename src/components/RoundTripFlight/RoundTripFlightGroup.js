import React, { Component } from 'react';
import { connect } from "react-redux";
import { compose } from "redux";
import * as actions from "../../redux/index";
import './RoundTripFlightGroup.css';
import moment from 'moment';
import RoundTripFlightGroupDetail from "./RoundTripFlightGroupDetailTab";
//import RoundTripFlightGroupDetailDevider from "./RoundTripFlightGroupDetailDevider";
import FlightDetailDevider from '../flight/flightDetailDevider';
import {searchObj,distinctAirline,addDefaultSrc,prepareMessageAvalability,dateDiffHrsMin,returnFlightLogo,minHHMM,statucDateSectionFlight,staticViewFare} from "../../utils/commonFunc";
import history from '../../utils/history';
import {createCart,viewCart,fareRules} from '../../services/flightBooking';
import { message, Spin } from 'antd';
import LoadingOverlay from 'react-loading-overlay';
import {messageDuration} from "../../services/constants";
import FareDetails from "../Common/FareDetails/FareDetails";
import {isMobileOnly} from "react-device-detect";
import {selectItem,addToCart} from '../../services/Analytics/ga4';


class RoundTripFlightGroup extends Component {
    state = { 
        activeTab:"flightDetails",
        fareRules:[],
        airline:[],
        activeRoute:0
    } 

    componentDidMount=async()=>{
        const airline=distinctAirline(this.props.flight.items);     
        await this.setState({airline:[...airline]}) 
    }
    
    handleActiveTab=(tabName)=>{
        this.setState({
            activeTab:tabName
        })
        if(tabName==="cancellation") {
            this.fetchRules();
        }
    }
    
    reviewBooking = async ()=>{
        this.setState({loading:true});
        selectItem(this.props.flight); 
        addToCart(this.props.flight);                 
        const res=await createCart(this.props.tkn,this.props.flight.token);
        if(res && res.status){            
            //localStorage.setItem("cartLocal",res.cartId);
            this.props.setCart({cartLocal:res.cartId});            
            history.push('/review-flight');
        } else {                               
            this.setState({loading:false});
            if(res.code===260030){
                const resView = await viewCart(res.cartId);                
                const msg=prepareMessageAvalability(resView,res.code,res.message);
                message.error(msg,messageDuration);
            } else {
                message.error(res.code +" : "+res.message,messageDuration);
            }
        }
    } 

    //This is old structure where sequence is only for inbound and outbound
    // fetchRules=async ()=>{             
    //     const data={searchToken:this.props.tkn,flightToken:this.props.flight.token};
    //     if(this.state.fareRules.length<1){
    //         this.setState({loadingRule:true})
    //         const res=await fareRules(data); 
    //         console.log(res);           
    //         if(res.status && res.data.length>0 && res.data[0].item.length>0) {
    //             this.setState({fareRules:res.data[0].item,loadingRule:false});                                          
    //         } else {
    //             message.error(res.message);
    //             this.setState({loadingRule:false})               
    //         }
    //     }        
    // }

    //Current structure where fare rule is for each flight instead of inbound and outbound
    fetchRules=async ()=>{
        const data={searchToken:this.props.tkn,flightToken:this.props.flight.token};
        if(this.state.fareRules.length<1){
            this.setState({loadingRule:true})
            const res=await fareRules(data);
            if(res.status && res.data.length>0 && res.data[0].item.length>0) {
                this.setState({fareRules:res.data,loadingRule:false});                                          
            } else {
                message.error(res.message);
                this.setState({loadingRule:false})               
            }
        }
    }

    //old structure to show farerules
    // showFareRules=()=>{        
    //     if(this.state.fareRules.length>0) {
    //         return this.state.fareRules && this.state.fareRules.map((farerule,index)=>{
    //             return <div key={"fareRule"+index}>
    //             <div><b>{farerule.key}</b></div>
    //             <div>{farerule.value}</div>
    //             </div>
    //         })
    //     } 
    // }

    showFareRules=(airlineItemArr)=>{     
        if(this.state.fareRules.length>0) {
            return airlineItemArr.map((item,seqIndex)=>{
                let fareRules=this.state.fareRules.filter((obj)=>{
                    return obj.sequence===item.sequenceNumber;
                });
                fareRules=fareRules[0];                
                return (
                <div key={item.sequenceNumber}> 
                <div>
                    {<b style={{'textTransform':'capitalize','color':'orange' }}>{`${item.locationInfo.fromLocation.name} - ${item.locationInfo.toLocation.name}`}</b> }
                </div>
                 {
                    fareRules.item.map((farerule,index)=>{
                        return (
                        <div key={"fareRule"+index}>
                        <div><b style={{'textTransform':'capitalize' ,'fontWeight': 'bold'}}>{farerule.key}</b></div>
                        <div>{farerule.value}</div>
                        </div>
                        )
                    })
                }
                </div>
                )
            })            
        } 
    }


    showImages=()=>{          
        let totalAirline=this.state.airline.length;
        if(totalAirline>0) {
            if(totalAirline>=2) {
                return <React.Fragment>
                        <img src={returnFlightLogo(this.state.airline[0].url,true)} onError={addDefaultSrc} />
                        {totalAirline===2? <img src={returnFlightLogo(this.state.airline[1].url,true)} onError={addDefaultSrc} />:null}                    
                        {totalAirline>2? `+ ${ totalAirline-2} more` : null}
                        {<a className="multiple-carrier" onClick={()=>{this.setState({viewDetails:!this.state.viewDetails})}}><span>Multiple carrier</span></a>}
                        </React.Fragment>
            } else {
                return <React.Fragment>
                        <img src={returnFlightLogo(this.state.airline[0].url,true)} onError={addDefaultSrc} />                    
                        </React.Fragment> 
            }
        }
    }

    totalDurationDepartureHrs=searchObj(this.props.flight.items[0].tpExtension,"key","durationHours");
    totalDurationDepartureMin=searchObj(this.props.flight.items[0].tpExtension,"key","durationMinutes");
    totalDurationArrivalHrs=searchObj(this.props.flight.items[1].tpExtension,"key","durationHours");
    totalDurationArrivalMin=searchObj(this.props.flight.items[1].tpExtension,"key","durationMinutes");
    adtBaseRateInfo=searchObj(this.props.flight.paxInfo[0].displayRateInfo,"description","BASEFARE");
    adtTotRateInfo=searchObj(this.props.flight.paxInfo[0].displayRateInfo,"description","TOTALAMOUNT");
    render() {         
        let departureStopLabel, arrivalStopLabel;
        this.props.flight.stopDetails[0]===0 ? departureStopLabel="Non Stop" : (this.props.flight.stopDetails[0]===1?departureStopLabel="stop" : departureStopLabel="stops")
        this.props.flight.stopDetails[1]===0 ? arrivalStopLabel="Non Stop" : (this.props.flight.stopDetails[1]===1?arrivalStopLabel="stop" : arrivalStopLabel="stops")
        let totalDeparture=0,totalArrival=0;
        if(this.props.flight.items[0]) {
            //totalDeparture=dateDiffHrsMin(this.props.flight.items[0].dateInfo.startDate,this.props.flight.items[0].dateInfo.endDate);
            if(this.props.flight.items[0].item.length>0) {
                this.props.flight.items[0].item.map(item=>{
                    totalDeparture=totalDeparture+item.journeyDuration;
                    totalDeparture=totalDeparture+item.layOverDuration;
                })
            }
            totalDeparture=minHHMM(totalDeparture);
        }
        if(this.props.flight.items[1]) {
            //totalArrival=dateDiffHrsMin(this.props.flight.items[1].dateInfo.startDate,this.props.flight.items[1].dateInfo.endDate);
            if(this.props.flight.items[1].item.length>0) {
                this.props.flight.items[1].item.map(item=>{
                    totalArrival=totalArrival+item.journeyDuration;
                    totalArrival=totalArrival+item.layOverDuration;
                })
            }
            totalArrival=minHHMM(totalArrival);
        }
       
        return ( 
            <LoadingOverlay
            active={this.state.loading}
            spinner={true}
            text='Loading your content...'
             >
            <div className="w-flight-listing roundtrip-group-flight">           
            { <div className="w-flight-list"  >
                        <ul className="wd-flex-center">
                            <li> 
                                <div className="air-flight wd-flex-center">
                                    <div className="air-flight__img">
                                        <div className="air-flight__img-wrap">
                                            {this.showImages()}                                           
                                        </div>
                                        <div className="air-flight__details font-size-11">
                                            {this.state.airline.length===1 && <span>{this.props.flight.items[0].item[0].vendors[0].item.name}</span> }
                                            <span className="text-grey d-none">{this.props.flight.items[0].item[0].code}</span>
                                            <span>{this.props.flight.vendors[0].item.name}</span>{/*Provider NAme*/}
                                        </div>
                                    </div>
                                </div>
                                
                            </li>
                            <li> 
                                <div className="air-flight-departure font-size-11">
                                    <span>{`${moment(new Date(this.props.flight.items[0].dateInfo.startDate)).format('hh:mm A')}`}</span>
                                    <span>{`${moment(new Date(this.props.flight.items[0].dateInfo.startDate)).format('DD-MM-YY')}`}</span> 
                                    {this.props.flight.items[0].locationInfo.fromLocation.city}	
                                </div>
                                
                            </li>
                            <li>
                                <div className="air-flight-duration font-size-11" date-placement="left" data-toggle="tooltip" data-html="true" title="<div className='tooltip-content'><p>Plane Change</p><p><span>Chennai</span> (MMA) | 8hrs layover</p></div>">
                                {totalDeparture && (<span> <strong>{`${totalDeparture.hrs}`}</strong>h <strong>{`${totalDeparture.min}`}</strong>m</span>)}
                                    <div className="air-flight-duration__divider">
                                        <span></span>
                                    </div>
                                    <div className="clearfix"></div>
                                    {`${this.props.flight.stopDetails[0]?this.props.flight.stopDetails[0]:''} ${departureStopLabel} ${this.props.flight.stopDetails[0] > 0 ? `via ${this.props.flight.items[0].item[0].locationInfo.toLocation.city}` : ''}`}
                                </div>
                                
                            </li>
                            <li> 
                                <div className="air-flight-arrival font-size-11">
                                    <span>{`${moment(new Date(this.props.flight.items[0].dateInfo.endDate)).format('hh:mm A')}`}</span> 
                                    {this.props.flight.items[0].locationInfo.toLocation.city}	 	
                                </div>
                                
                            </li>
                            <li> 
                                <div className="air-flight-price d-flex">
                                    <span>{this.props.flight.displayAmount}</span>  
                                    {/*<button className="btn btn-primary" onClick={()=>{this.setState({viewFare:!this.state.viewFare})}}> View Fare </button> */}	
                                    <button className="btn btn-primary" onClick={this.reviewBooking}> Book Now</button>
                                </div>
                                
                            </li>
                        </ul>

                        {/*Return flight ui*/}

                        <ul className="wd-flex-center roundtrip-return-flight">
                            <li> 
                                <div className="air-flight wd-flex-center">
                                    {/*<div className="air-flight__img">
                                        <img src={returnFlightLogo(this.props.flight.items[1].item[0].images[0].url,isMobileOnly)} onError={addDefaultSrc} />
                                        <div className="air-flight__details font-size-11">
                                            <span>{this.props.flight.items[1].item[0].vendors[0].item.name}</span> 
                                            <span className="text-grey d-none">{this.props.flight.items[1].item[0].code}</span>
                                            <span>{this.props.flight.vendors[0].item.name}</span>
                                        </div>
                                    </div>*/}
                                </div>
                                
                            </li>
                            <li> 
                                <div className="air-flight-departure font-size-11">
                                    <span>{`${moment(new Date(this.props.flight.items[1].dateInfo.startDate)).format('hh:mm A')}`}</span> 
                                    {this.props.flight.items[1].locationInfo.fromLocation.city}	
                                </div>
                                
                            </li>
                            <li>
                                <div className="air-flight-duration font-size-11" date-placement="left" data-toggle="tooltip" data-html="true" title="<div className='tooltip-content'><p>Plane Change</p><p><span>Chennai</span> (MMA) | 8hrs layover</p></div>">
                                {totalArrival && (<span> <strong>{`${totalArrival.hrs}`}</strong>h <strong>{`${totalArrival.min}`}</strong>m</span>)}
                                     
                                    <div className="air-flight-duration__divider">
                                        <span></span>
                                    </div>
                                    <div className="clearfix"></div>
                                    {`${this.props.flight.stopDetails[1]?this.props.flight.stopDetails[1]:''} ${arrivalStopLabel} ${this.props.flight.stopDetails[1] > 0 ? `via ${this.props.flight.items[1].item[0].locationInfo.toLocation.city}` : ''}`}
                                </div>
                                
                            </li>
                            <li> 
                                <div className="air-flight-arrival font-size-11">
                                    <span>{`${moment(new Date(this.props.flight.items[1].dateInfo.endDate)).format('hh:mm A')}`}</span> 
                                    {this.props.flight.items[1].locationInfo.toLocation.city}	 	
                                </div>
                                
                            </li>
                            <li>
                            </li>
                        </ul>
                         <div className="flight-details">
                            <div className="text-left">
                                <a onClick={()=>{this.setState({viewDetails:!this.state.viewDetails})}}><span>Flight Details <i className="fa fa-chevron-down"></i></span></a>
                            </div>
                        </div>
                        { 
                            this.state.viewFare && (   
                                staticViewFare()
                            )
                        }   
                         
                        { 
                        this.state.viewDetails && (  
                            <div className="flight-details__wrapper">
                                <div className="flight-details__wrapper-list" role="tablist">
                                    <div  className={this.state.activeTab=="flightDetails"?"list-item active":"list-item"}>
                                        <a href="#flightDetails"  onClick={()=>this.handleActiveTab("flightDetails")} data-toggle="tab">Flight Details</a>
                                    </div>
                                    <div className={this.state.activeTab=="fareSummory"?"list-item active":"list-item"}><a href="#fareSummory" onClick={()=>this.handleActiveTab("fareSummory")} data-toggle="tab">Fare Summary</a></div>
                                    <div className={this.state.activeTab=="cancellation"?"list-item active":"list-item"}><a href="#cancellation"   onClick={()=>this.handleActiveTab("cancellation")} data-toggle="tab">Cancellation</a></div>
                                    <div className={this.state.activeTab=="dateChange"?"list-item active":"list-item"}><a href="#dateChange"  onClick={()=>this.handleActiveTab("dateChange")} data-toggle="tab">Date Change</a></div>
                                </div>
        
                                <div className="tab-content">
                                    <div className={this.state.activeTab=="flightDetails"?"tab-pane active":"tab-pane"} id="flightDetails">
                                        <div className="flight-details__wrapper-oneway">
                                            <div className="flight-details__wrapper-heading">
                                                <span className="flight_details__name">
                                                {`${this.props.flight.items[0].locationInfo.fromLocation.city} to ${this.props.flight.items[0].locationInfo.toLocation.city}, ${ moment(this.props.flight.items[0].dateInfo.startDate).format('DD MMM')}`}                                                   
                                                </span>
                                                {totalDeparture && (<span> <strong>{`${totalDeparture.hrs} `}</strong> Hrs <strong>{`${totalDeparture.min} `}</strong> mins</span>)}
                                            </div>		
                                        </div>
                                        {this.props.flight.stopDetails[0]===0 && (<RoundTripFlightGroupDetail direct={true} stopsDetails={this.props.flight.items[0].item[0]}/>)}
                                        { 
                                         this.props.flight.stopDetails[0]===1 && (
                                            <div>
                                                <RoundTripFlightGroupDetail stopsDetails={this.props.flight.items[0].item[0]}/>
                                                <FlightDetailDevider stopsDetails={this.props.flight.items[0].item[0]}/>
                                                <RoundTripFlightGroupDetail stopsDetails={this.props.flight.items[0].item[1]}/>
                                            </div>
                                         )}
                                        {this.props.flight.stopDetails[0]>1 && (
                                           
                                             this.props.flight.items[0].item.map((stopsDetails,index)=> {
                                                return <div>
                                                            <RoundTripFlightGroupDetail stopsDetails={stopsDetails}/>
                                                            {index<this.props.flight.items[0].item.length-1 ? <FlightDetailDevider stopsDetails={stopsDetails}/> : null }                                                        
                                                        </div>
                                             })
                                            
                                        )}        
                                       
                                       {/***/}
                                       <div className="flight-details__wrapper-oneway">
                                            <div className="flight-details__wrapper-heading">
                                                <span className="flight_details__name">
                                                {`${this.props.flight.items[1].locationInfo.fromLocation.city} to ${this.props.flight.items[1].locationInfo.toLocation.city}, ${ moment(this.props.flight.items[1].dateInfo.startDate).format('DD MMM')}`}                                                   
                                                </span>
                                                {totalArrival && (<span> <strong>{`${totalArrival.hrs} `}</strong> Hrs <strong>{`${totalArrival.min} `}</strong> mins</span>)}
                                     
                                            </div>		
                                        </div>
                                        {this.props.flight.stopDetails[1]===0 && (<RoundTripFlightGroupDetail direct={true} stopsDetails={this.props.flight.items[1].item[0]}/>)}
                                        { 
                                         this.props.flight.stopDetails[1]===1 && (
                                            <div>
                                                <RoundTripFlightGroupDetail stopsDetails={this.props.flight.items[1].item[0]}/>
                                                <FlightDetailDevider stopsDetails={this.props.flight.items[1].item[0]}/>
                                                <RoundTripFlightGroupDetail stopsDetails={this.props.flight.items[1].item[1]}/>
                                            </div>
                                         )}
                                        {this.props.flight.stopDetails[1]>1 && (
                                           
                                             this.props.flight.items[1].item.map((stopsDetails,index)=> {
                                                return <div>
                                                            <RoundTripFlightGroupDetail stopsDetails={stopsDetails}/>
                                                            {index<this.props.flight.items[1].item.length-1 ? <FlightDetailDevider stopsDetails={stopsDetails}/> : null }                                                        
                                                        </div>
                                             })
                                            
                                        )}
                                        

                                    </div>
                                    <div  className={this.state.activeTab=="fareSummory"?"tab-pane active":"tab-pane"} id="fareSummory">
                                    <FareDetails paxInfo={this.props.flight.paxInfo}/> 
                                       {/* <div className="fli-list-fare_details__left">
                                            <div className="fli-list-fare_details__left-heading">Fare breakup</div>
                                            <div className="d-flex align-items-center justify-content-between">
                                                <span>
                                                    TOTAL
                                                </span>
                                                <span>
                                                    {`${this.props.flight.displayAmount.split(" ")[0]} ${this.adtTotRateInfo.amount}`}
                                                </span>
                                            </div>
                                            <div className="d-flex align-items-center justify-content-between">
                                                <span>
                                                    Base Fare
                                                </span>
                                                <span>
                                                {`${this.props.flight.displayAmount.split(" ")[0]} ${this.adtBaseRateInfo.amount}`}
                                                </span>
                                            </div>
                                            <div className="d-flex align-items-center justify-content-between">
                                                <span>
                                                    Surcharges
                                                </span>
                                                <span>
                                                    0
                                                </span>
                                            </div>
                                        </div>*/}
                                    </div>
                                    <div  className={this.state.activeTab=="cancellation"?"tab-pane active":"tab-pane"} id="cancellation">
                                        <div className="airline-fare-rules">
                                            <div className="airline-text m-b-10">
                                            <div className="text-center"><Spin tip="Loading..." spinning={this.state.loadingRule}/></div>
                                            {this.state.fareRules.length>0 && 
                                                <div className="airline-fare-rules__btn">
                                                    <button className={this.state.activeRoute===0?"btn btn-primary active":"btn btn-primary"} onClick={()=>this.setState({activeRoute:0})} >{`${this.props.flight.items[0].locationInfo.fromLocation.id}-${this.props.flight.items[0].locationInfo.toLocation.id}`}</button>
                                                    <button className={this.state.activeRoute===1?"btn btn-primary active":"btn btn-primary"} onClick={()=>this.setState({activeRoute:1})} >{`${this.props.flight.items[1].locationInfo.fromLocation.id}-${this.props.flight.items[1].locationInfo.toLocation.id}`}</button> 
                                                </div>
                                            }
                                            {this.state.activeRoute===0 && this.showFareRules(this.props.flight.items[0].item)}                                             
                                            {this.state.activeRoute===1 && this.showFareRules(this.props.flight.items[1].item)}
                                            </div>
                                            {/* <div className="d-flex rules-heading">
                                                <div className="time-gap-cond">
                                                    <p className="text-black LatoBold append_bottom2">Time frame</p>
                                                    <p className="font10 lightGreyText">(From Scheduled flight departure)</p>
                                                </div>
                                                <div className="time-gap-cond">
                                                    <p className="text-black LatoBold append_bottom2">Airline Fee + MMT Fee</p>
                                                    <p className="font10 lightGreyText">(Per passenger)</p>
                                                </div>
                                            </div>
                                            <div className="d-flex">
                                                <div className="time-gap-cond">
                                                    <p className="text-black LatoBold append_bottom2">0 hours to 4 hours*</p>
                                                </div>
                                                <div className="time-gap-cond text-black">
                                                    <p>ADULT : 
                                                        <b>Non Refundable</b>
                                                        <br/>
                                                    </p>
                                                </div>
                                            </div>
                                            <div className="d-flex">
                                                <div className="time-gap-cond">
                                                    <p className="text-black LatoBold append_bottom2">4 hours to 3 days*</p>
                                                </div>
                                                <div className="time-gap-cond text-black">
                                                    <p>ADULT : 
                                                        <b>₹ 3,500 + ₹ 0</b>
                                                        <br/>
                                                    </p>
                                                </div>
                                            </div>
                                            <div className="d-flex">
                                                <div className="time-gap-cond">
                                                    <p className="text-black LatoBold append_bottom2">3 days to 365 days*</p>
                                                </div>
                                                <div className="time-gap-cond text-black" >
                                                    <p>ADULT : 
                                                        <b>₹ 3,000 + ₹ 0</b>
                                                        <br/>
                                                    </p>
                                                </div>
                                            </div>
                                            <p className="append_top10 append_bottom5">*From the Date of Departure</p> */}
                                        </div>
                                    </div>
                                    <div className={this.state.activeTab=="dateChange"?"tab-pane active":"tab-pane"} id="dateChange">
                                        {statucDateSectionFlight()}
                                        {/* <div className="airline-fare-rules">
                                            <div className="airline-text m-b-10">                                               
                                            </div>
                                            <div className="d-flex rules-heading">
                                                <div className="time-gap-cond">
                                                    <p className="text-black LatoBold append_bottom2">Time frame</p>
                                                    <p className="font10 lightGreyText">(From Scheduled flight departure)</p>
                                                </div>
                                                <div className="time-gap-cond">
                                                    <p className="text-black LatoBold append_bottom2">Airline Fee + MMT Fee + Fare difference</p>
                                                    <p className="font10 lightGreyText">(Per passenger)</p>
                                                </div>
                                            </div>
                                            <div className="d-flex">
                                                <div className="time-gap-cond">
                                                    <p className="text-black LatoBold append_bottom2">0 hours to 4 hours*</p>
                                                </div>
                                                <div className="time-gap-cond text-black">
                                                    <p>ADULT : 
                                                        <b>Non Changeable</b>
                                                        <br/>
                                                    </p>
                                                </div>
                                            </div>
                                            <div className="d-flex">
                                                <div className="time-gap-cond">
                                                    <p className="text-black LatoBold append_bottom2">4 hours to 3 days*</p>
                                                </div>
                                                <div className="time-gap-cond text-black">
                                                    <p>ADULT : 
                                                        <b>₹ 3,500 + ₹ 0  + Fare difference</b>
                                                        <br/>
                                                    </p>
                                                </div>
                                            </div>
                                            <div className="d-flex">
                                                <div className="time-gap-cond">
                                                    <p className="text-black LatoBold append_bottom2">3 days to 365 days*</p>
                                                </div>
                                                <div className="time-gap-cond text-black">
                                                    <p>ADULT : 
                                                        <b>₹ 3,000 + ₹ 0  + Fare difference</b>
                                                        <br/>
                                                    </p>
                                                </div>
                                            </div>
                                            <p className="append_top10 append_bottom5">*From the Date of Departure</p>
                                        </div> */}
                                    </div>
                                </div>
                                
        
                            </div>
                        )
                        } 
                    </div>
                        
            }
        </div> 
        </LoadingOverlay>       
         );
    }
}
 
const mapStateToProps = (state) => {
    return { 
        tkn : state.flights.token,
        resFlight:state.flights.response,
        fromLocation:state.flights.fromLocation,
        toLocation:state.flights.toLocation,
        tripType:state.flights.tripType,
        fromDate:state.flights.fromDate,
        toDate:state.flights.toDate,
        passanger:state.flights.passanger,
        serviceClass:state.flights.serviceClass
    };
  };

export default compose(connect(mapStateToProps, actions))(RoundTripFlightGroup);
