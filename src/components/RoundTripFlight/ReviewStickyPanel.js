import React, { Component } from "react";
import "./RoundTripFlight.css"
import moment from 'moment';
import {minHHMM,addDefaultSrc,searchObj,dateDiffHrsMin,prepareMessageAvalability,returnFlightLogo} from "../../utils/commonFunc";
import history from '../../utils/history';
import {createCart,viewCart,fareRules} from '../../services/flightBooking';
import { message, Spin } from 'antd';
import { connect } from "react-redux";
import { compose } from "redux";
import * as actions from "../../redux/index";
import LoadingOverlay from 'react-loading-overlay';
import FlightDetail from "../flight/flightDetailTab";
import FlightDetailDevider from "../flight/flightDetailDevider";
import {messageDuration} from "../../services/constants";
import FareDetails from "../Common/FareDetails/FareDetails"
import {isMobileOnly} from "react-device-detect";
import {selectItem,addToCart} from '../../services/Analytics/ga4';


class ReviewStickyPanel extends Component {
    state={
        showDetails:false,
        activeTab:"flightDetails",
        fareRules:[],
        fareRulesArr:[],
        loadingRule:false
    }
   
    handleActiveTab=(tabName)=>{
        this.setState({
            activeTab:tabName
        })
        if(tabName==="cancellation") {
            this.fetchRules();
        }
    }

    reviewBooking = async ()=>{ 
        if(this.props.departure.vendors[0].item.name===this.props.arrival.vendors[0].item.name) {
            this.setState({loading:true});
            selectItem(this.props.flight); 
            addToCart(this.props.flight);              
            const res=await createCart(this.props.tkn,this.props.departure.token,this.props.arrival.token);
            if(res && res.status){           
                //localStorage.setItem("cartLocal",res.cartId);
                this.props.setCart({cartLocal:res.cartId});
                //this.setState({loading:false}); 
                history.push('/review-flight');
            } else {          
                this.setState({loading:false});
                if(res.code===260030){
                    const resView = await viewCart(res.cartId);
                    console.log("view cart",resView);
                    const msg=prepareMessageAvalability(resView,res.code,res.message);
                    message.error(msg,messageDuration);
                } else {
                    message.error(res.code +" : "+res.message,messageDuration);
                }
            }
        } else {
            message.error(`Selected flight are not from same provider!
            Please select different combination of departure and arrival flights
            `);
        }
    }  
    
    fetchRules=async ()=>{    
        const dataDepart={searchToken:this.props.tkn,flightToken:this.props.departure.token};
        const dataArrival={searchToken:this.props.tkn,flightToken:this.props.arrival.token};
        if(this.state.fareRules.length<1){
            this.setState({loadingRule:true})
            const res=await fareRules(dataDepart);  
            let resArr=await fareRules(dataArrival);           
            if(res.status && res.data.length>0 && res.data[0].item.length>0) {
                this.setState({fareRules:res.data[0].item,loadingRule:false});                                          
            } else {
                message.error(res.message);
                this.setState({loadingRule:false})               
            }
            if(resArr.status && resArr.data.length>0 && resArr.data[0].item.length>0) {
                this.setState({fareRulesArr:res.data[0].item,loadingRule:false});                                          
            } else {
                message.error(resArr.message);
                this.setState({loadingRule:false})               
            }
        }        
    }

    showFareRules=(arr)=>{        
        if(arr.length>0) {
            return arr && arr.map((farerule,index)=>{
                return <div className="m-b-15 fare-rules-section" key={"fareRule"+index}>
                <div className="text-secondary">{farerule.key}</div>
                <div>{farerule.value}</div>
                </div>
            })
        } 
    }

    render(){
        let totalDeparture=0,totalArrival=0,total=0,currency,timeDeparture,timeArrival,totalDepartureJourney,totalArrivalJourney,departureBaseRateInfo,departureTotRateInfo,arrivalBaseRateInfo,arrivalTotRateInfo;
        if(this.props.departure.displayAmount && this.props.arrival.displayAmount){            
            currency=this.props.departure.displayAmount.split(" ")[0];            
            total=`${+(this.props.departure.amount) + (+this.props.arrival.amount)}`;
            total=+total;
            timeDeparture=minHHMM(this.props.departure.items[0].item[0].journeyDuration);
            timeArrival=minHHMM(this.props.arrival.items[0].item[0].journeyDuration);
            totalDepartureJourney=minHHMM(this.props.departure.journeyDuration);
            totalArrivalJourney=minHHMM(this.props.arrival.journeyDuration);
            departureBaseRateInfo=searchObj(this.props.departure.paxInfo[0].displayRateInfo,"description","BASEFARE");
            departureTotRateInfo=searchObj(this.props.departure.paxInfo[0].displayRateInfo,"description","TOTALAMOUNT");
            arrivalBaseRateInfo=searchObj(this.props.arrival.paxInfo[0].displayRateInfo,"description","BASEFARE");
            arrivalTotRateInfo=searchObj(this.props.arrival.paxInfo[0].displayRateInfo,"description","TOTALAMOUNT");            
        }
        if(this.props.departure.items && this.props.arrival.items) {
            if(this.props.departure.items[0].item.length>0) {
                this.props.departure.items[0].item.map(item=>{ console.log(totalDeparture,item.journeyDuration);
                    totalDeparture=totalDeparture+item.journeyDuration;
                    totalDeparture=totalDeparture+item.layOverDuration;
                })
            }
            totalDeparture=minHHMM(totalDeparture);
            if(this.props.arrival.items[0].item.length>0) {
                this.props.arrival.items[0].item.map(item=>{
                    totalArrival=totalArrival+item.journeyDuration;
                    totalArrival=totalArrival+item.layOverDuration;
                })
            }
            totalArrival=minHHMM(totalArrival);
            //totalDeparture=dateDiffHrsMin(this.props.departure.items[0].dateInfo.startDate,this.props.departure.items[0].dateInfo.endDate);
            //totalArrival=dateDiffHrsMin(this.props.arrival.items[0].dateInfo.startDate,this.props.arrival.items[0].dateInfo.endDate);
        }        
        return (
            <LoadingOverlay
            active={this.state.loading}
            spinner={true}
            text='Loading your content...'
            >
            <div>
                {Object.keys(this.props.departure).length>0 && Object.keys(this.props.arrival).length>0 && (
                    <div className="splitted-flight-fare-wrap">
                        <div className="splitted-flight-fare">
                            <div className="col-md-8">
                                <div className="wd-flex-center">
                                    <div className="splitVw-footer-left col-md-6 pl-none">
                                        <div className="splitVw-footer-left__departure">
                                            <span className="color-light-grey font12">Departure</span>
                                            <span className="seperator color-light">|</span>
                                            <span className="font12 inlineB insertSep">{this.props.departure.items[0].item[0].vendors[0].item.name}
                                                <span className="seperator">|</span>
                                            <span className="prepend_left5">{this.props.departure.items[0].item[0].code}</span>
                                            </span>
                                        </div>
                                        
                                        <div className="air-flight">
                                            <div className="air-flight__img">
                                                <img width="40" onError={addDefaultSrc}  src={returnFlightLogo(this.props.departure.items[0].item[0].images[0].url,isMobileOnly)}  />
                                            </div>
                                            <div className="air-flight-departure">
                                                <div className="air-flight-departure__dertails">
                                                    <div className="wd-flex-center">
                                                        <span>{`${moment(new Date(this.props.departure.dateInfo.startDate)).format('hh:mm')}`}</span> 
                                                        <span>&#8594;</span>	
                                                        <span>{`${moment(new Date(this.props.departure.dateInfo.endDate)).format('hh:mm')}`}</span>	
                                                    </div>
                                                    <span className="actual-price">{this.props.departure.displayAmount}</span>
                                                    
                                                </div>
                                                <a className="font-size-11 ">Flight Details</a>
                                            </div>
                                        </div>
                                        
                                    </div>

                                    <div className="splitVw-footer-left col-md-6">
                                        <div className="splitVw-footer-left__departure">
                                            <span className="color-light-grey font12">Departure</span>
                                            <span className="seperator color-light">|</span>
                                            <span className="font12 inlineB insertSep">{this.props.arrival.items[0].item[0].vendors[0].item.name}
                                                <span className="seperator">|</span>
                                                <span className="prepend_left5">{this.props.arrival.items[0].item[0].code}</span>
                                            </span>
                                        </div>
                                        
                                        <div className="air-flight">
                                            <div className="air-flight__img">
                                                <img width="40" onError={addDefaultSrc}   src={returnFlightLogo(this.props.arrival.items[0].item[0].images[0].url,isMobileOnly)}/>
                                            </div>
                                            <div className="air-flight-departure">
                                                <div className="air-flight-departure__dertails">
                                                    <div className="wd-flex-center">
                                                        <span>{`${moment(new Date(this.props.arrival.dateInfo.startDate)).format('hh:mm')}`}</span> 
                                                        <span>&#8594;</span>	
                                                        <span>{`${moment(new Date(this.props.arrival.dateInfo.endDate)).format('hh:mm')}`}</span>	
                                                    </div>
                                                    <span className="actual-price">{this.props.arrival.displayAmount}</span>
                                                </div>
                                                <a  className="font11 ">Flight Details</a>
                                            </div>
                                        </div>
                                        
                                    </div>
                                </div>
                            </div>

                            <div className="col-md-4">
                                <div className="splitted-flight-fare__fare">
                                    <div className="splitted-flight-fare__fare-price">
                                        <div>{total? `${currency} ${+total.toFixed(2)}`:null}</div>
                                        <a >Fare Details</a>
                                    </div>
                                    <button className="btn btn-primary" onClick={this.reviewBooking}>
                                        Book Now
                                    </button>
                                    <span className="booknow-arrow">
                                        <a onClick={()=>{this.setState({showDetails:!this.state.showDetails})}}>
                                        <span className={this.state.showDetails?"booknow-arrow__up":"booknow-arrow__down"}> 
                                        <img width="16" src={this.state.showDetails? process.env.PUBLIC_URL +"images/icon_white-down.png":process.env.PUBLIC_URL +"images/icon_white-up.png"} alt="Arrow" /> </span></a>
                                        
                                    </span>
                                </div>
                            </div>
                        </div>

                        <div className={this.state.showDetails?"flight-details__wrapper": "hideContent"}>
                            <div className="flight-details__wrapper-list" role="tablist"> 
                                <div className={this.state.activeTab=="flightDetails"?"list-item active":"list-item"}><a href="#flightDetails" onClick={()=>this.handleActiveTab("flightDetails")}  data-toggle="tab">Flight Details</a></div>
                                <div className={this.state.activeTab=="fareSummory"?"list-item active":"list-item"}><a href="#fareSummory" href="#fareSummory" onClick={()=>this.handleActiveTab("fareSummory")} data-toggle="tab">Fare Summary</a></div>
                                <div className={this.state.activeTab=="cancellation"?"list-item active":"list-item"}><a href="#cancellation" onClick={()=>this.handleActiveTab("cancellation")} data-toggle="tab">Cancellation</a></div>
                                <div className={this.state.activeTab=="dateChange"?"list-item active":"list-item"}><a href="#dateChange" onClick={()=>this.handleActiveTab("dateChange")} data-toggle="tab">Date Change</a></div>
                            </div>

                            <div className="tab-content">
                                <div  className={this.state.activeTab=="flightDetails"?"tab-pane active":"tab-pane"} id="flightDetails">
                                    <div className="row">
                                        <div className="col-md-6 flight-details__wrapper-col1">
                                            <div className="m-b-15">
                                                <strong>Departure</strong> Flight
                                            </div>
                                            <div className="flight-details__wrapper-col1-sec">
                                            <div className="flight-details__wrapper-oneway">
                                                <div className="flight-details__wrapper-heading">
                                                    <span className="flight_details__name">
                                                        {`${this.props.departure.locationInfo.fromLocation.city} to ${this.props.departure.locationInfo.toLocation.city}, ${moment(this.props.departure.dateInfo.startDate).format('DD MMM')}`}
                                                    </span>
                                                    {totalDeparture && (<span> <strong>{`${totalDeparture.hrs} `}</strong> Hrs <strong>{`${totalDeparture.min} `}</strong> mins</span>)}
                                                </div>
                                            </div>
                                            {this.props.departure.stops === 0 && (<FlightDetail direct={true} stopsDetails={this.props.departure.items[0].item[0]} />)}
                                            {
                                                this.props.departure.stops === 1 && (
                                                <div>
                                                    <FlightDetail stopsDetails={this.props.departure.items[0].item[0]} />
                                                    <FlightDetailDevider stopsDetails={this.props.departure.items[0].item[0]} />
                                                    <FlightDetail stopsDetails={this.props.departure.items[0].item[1]} />
                                                </div>
                                            )}
                                            {this.props.departure.stops > 1 && (
                                                this.props.departure.items[0].item.map((stopsDetails, index) => {
                                                    return <div key={"multistop"+index}>
                                                        <FlightDetail stopsDetails={stopsDetails} />
                                                        {index < this.props.departure.items[0].item.length - 1 ? <FlightDetailDevider stopsDetails={stopsDetails} /> : null}
                                                        </div>
                                                })
                                            )}
                                                {/*<div className="flight-details__wrapper-oneway">
                                                    <div className="flight-details__wrapper-heading">
                                                        <span className="flight_details__name">
                                                            {this.props.departure.locationInfo.fromLocation.city} to {this.props.departure.locationInfo.toLocation.city} , 20 Mar
                                                        </span>
                                                        <span>
                                                            <strong>{totalDepartureJourney.hrs}</strong> hrs <strong>{totalDepartureJourney.min}</strong> mins 
                                                        </span>
                                                    </div>		
                                                </div>
                                                <div className="airline-details-wrapper">
                                                    <div className="row">
                                                        <div className="col-md-12">
                                                            <div className="airline-information">
                                                                <div className="airline-text">
                                                                    <span className="airline-text__img">
                                                                        <img width="30"onError={addDefaultSrc} src={this.props.departure.items[0].item[0].images[0].url} alt="I5" title="I5"/>
                                                                    </span>
                                                                    <span className="airline-text__flight">
                                                                        {`${this.props.departure.items[0].item[0].vendors[0].item.name} | ${this.props.departure.items[0].item[0].code}`}
                                                                    </span>
                                                                </div>
                                                                <div className="airline-time-options">
                                                                    <div className="airline-departure-option">
                                                                        <span className="flight-time departure">{`${moment(new Date(this.props.departure.items[0].item[0].dateInfo.startDate)).format('hh:mm')}`}</span>
                                                                        <p className="date_details" id="dateId">{`${ moment(this.props.departure.items[0].item[0].dateInfo.startDate).format('ddd, D MMM YY')}`}</p>
                                                                        <p className="city_details" id="cityId">{`${this.props.departure.items[0].item[0].locationInfo.fromLocation.city}, ${this.props.departure.items[0].item[0].locationInfo.fromLocation.country}`}</p>
                                                                    </div>
                                                                    <div className="stops-option">
                                                                        <p className="clearfix append_bottom7 append_right24 append_left10">
                                                                            <span className="line-map">
                                                                                <span className="flight-duration">
                                                                                    <strong>{timeDeparture.hrs}</strong> hrs 
                                                                                    <strong>{timeDeparture.min}</strong> mins 
                                                                                </span>
                                                                            </span>
                                                                        </p>
                                                                    </div>
                                                                    <div className="arrival-option">
                                                                        <span className="flight-time arrival">{`${moment(new Date(this.props.departure.items[0].item[0].dateInfo.endDate)).format('hh:mm')}`}</span>
                                                                        <p className="date_details" id="dateId">{`${ moment(this.props.departure.items[0].item[0].dateInfo.endDate).format('ddd, D MMM YY')}`} </p>
                                                                        <p className="city_details" id="cityId">{`${this.props.departure.items[0].item[0].locationInfo.toLocation.city}, ${this.props.departure.items[0].item[0].locationInfo.toLocation.country}`}</p>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div className="col-md-12 baggage-info-wrapper">
                                                            <div className="baggage-info">
                                                                <div className="baggage-info__details baggage-info__name">
                                                                    <span className="itnry-flt-footer-col">BAGGAGE : </span>
                                                                    <span className="itnry-flt-footer-col">Check-in</span>
                                                                    <span className="itnry-flt-footer-col">Cabin</span>
                                                                </div>
                                                                <div className="baggage-info__details baggage-info__desc">
                                                                    <span className="itnry-flt-footer-col">ADULT</span>
                                                                    <span className="itnry-flt-footer-col">15 Kgs</span>
                                                                    <span className="itnry-flt-footer-col">7 Kgs</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>*/}
                                            </div>
                                        </div>

                                        <div className="col-md-6 flight-details__wrapper-col1">
                                            <div className="m-b-15">
                                                <strong>Return</strong> Flight
                                            </div>
                                            <div className="flight-details__wrapper-col1-sec">
                                            <div className="flight-details__wrapper-oneway">
                                                <div className="flight-details__wrapper-heading">
                                                    <span className="flight_details__name">
                                                        {`${this.props.arrival.locationInfo.fromLocation.city} to ${this.props.arrival.locationInfo.toLocation.city}, ${moment(this.props.arrival.dateInfo.startDate).format('DD MMM')}`}
                                                    </span>
                                                    {totalArrival && (<span> <strong>{`${totalArrival.hrs} `}</strong> Hrs <strong>{`${totalArrival.min} `}</strong> mins</span>)}
                                                </div>
                                            </div>
                                            {this.props.arrival.stops === 0 && (<FlightDetail direct={true} stopsDetails={this.props.arrival.items[0].item[0]} />)}
                                            {
                                                this.props.arrival.stops === 1 && (
                                                <div>
                                                    <FlightDetail stopsDetails={this.props.arrival.items[0].item[0]} />
                                                    <FlightDetailDevider stopsDetails={this.props.arrival.items[0].item[0]} />
                                                    <FlightDetail stopsDetails={this.props.arrival.items[0].item[1]} />
                                                </div>
                                            )}
                                            {this.props.arrival.stops > 1 && (
                                                this.props.arrival.items[0].item.map((stopsDetails, index) => {
                                                    return <div key={"multistop"+index}>
                                                        <FlightDetail stopsDetails={stopsDetails} />
                                                        {index < this.props.arrival.items[0].item.length - 1 ? <FlightDetailDevider stopsDetails={stopsDetails} /> : null}
                                                        </div>
                                                })
                                            )}
                                                {/*<div className="flight-details__wrapper-oneway">
                                                    <div className="flight-details__wrapper-heading">
                                                        <span className="flight_details__name">
                                                        {this.props.arrival.locationInfo.fromLocation.city} to {this.props.arrival.locationInfo.toLocation.city} , 20 Mar
                                                        </span>
                                                        <span>
                                                            <strong>{totalArrivalJourney.hrs}</strong> hrs <strong>{totalArrivalJourney.min}</strong> mins 
                                                        </span>
                                                    </div>		
                                                </div>
                                                <div className="airline-details-wrapper">
                                                    <div className="row">
                                                        <div className="col-md-12">
                                                            <div className="airline-information">
                                                                <div className="airline-text">
                                                                    <span className="airline-text__img">
                                                                        <img width="30" onError={addDefaultSrc} src={this.props.arrival.items[0].item[0].images[0].url} alt="I5" title="I5" />
                                                                    </span>
                                                                    <span className="airline-text__flight">
                                                                    {`${this.props.arrival.items[0].item[0].vendors[0].item.name} | ${this.props.arrival.items[0].item[0].code}`}
                                                                    </span>
                                                                </div>
                                                                <div className="airline-time-options">
                                                                    <div className="airline-departure-option">
                                                                        <span className="flight-time departure">{`${moment(new Date(this.props.arrival.items[0].item[0].dateInfo.startDate)).format('hh:mm')}`}</span>
                                                                        <p className="date_details" id="dateId">{`${ moment(this.props.arrival.items[0].item[0].dateInfo.startDate).format('ddd, D MMM YY')}`} </p>
                                                                        <p className="city_details" id="cityId">{`${this.props.arrival.items[0].item[0].locationInfo.fromLocation.city}, ${this.props.arrival.items[0].item[0].locationInfo.fromLocation.country}`}</p>
                                                                    </div>
                                                                    <div className="stops-option">
                                                                        <p className="clearfix append_bottom7 append_right24 append_left10">
                                                                            <span className="line-map">
                                                                                <span className="flight-duration">
                                                                                    <strong>{timeArrival.hrs}</strong> hrs 
                                                                                    <strong>{timeArrival.min}</strong> mins 
                                                                                </span>
                                                                            </span>
                                                                        </p>
                                                                    </div>
                                                                    <div className="arrival-option">
                                                                        <span className="flight-time arrival">{`${moment(new Date(this.props.arrival.items[0].item[0].dateInfo.endDate)).format('hh:mm')}`}</span>
                                                                        <p className="date_details" id="dateId">{`${ moment(this.props.arrival.items[0].item[0].dateInfo.endDate).format('ddd, D MMM YY')}`} </p>
                                                                        <p className="city_details" id="cityId">{`${this.props.arrival.items[0].item[0].locationInfo.toLocation.city}, ${this.props.arrival.items[0].item[0].locationInfo.toLocation.country}`}</p>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div className="col-md-12 baggage-info-wrapper">
                                                            <div className="baggage-info">
                                                                <div className="baggage-info__details baggage-info__name">
                                                                    <span className="itnry-flt-footer-col">BAGGAGE : </span>
                                                                    <span className="itnry-flt-footer-col">Check-in</span>
                                                                    <span className="itnry-flt-footer-col">Cabin</span>
                                                                </div>
                                                                <div className="baggage-info__details baggage-info__desc">
                                                                    <span className="itnry-flt-footer-col">ADULT</span>
                                                                    <span className="itnry-flt-footer-col">15 Kgs</span>
                                                                    <span className="itnry-flt-footer-col">7 Kgs</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>*/}
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div className={this.state.activeTab=="fareSummory"?"tab-pane active":"tab-pane"} id="fareSummory">
                                    {/*<div className="fli-list-fare_details__left">
                                        <p className="LatoBold font16 text-black grey_btm_border append_bottom10 ">Fare breakup</p>
                                        <p className="fareBreakup-item text-black font-wt-600">
                                            <span className="">
                                                <font>TOTAL</font>
                                            </span>
                                            <span className="">
                                                <font>{`${currency} ${departureTotRateInfo.amount+arrivalTotRateInfo.amount}`}</font>
                                            </span>
                                        </p>
                                        <p className="fareBreakup-item">
                                            <span className="">
                                                <font>Base Fare</font>
                                            </span>
                                            <span className="">
                                                <font>{`${currency} ${departureBaseRateInfo.amount+arrivalBaseRateInfo.amount}`}</font>
                                            </span>
                                        </p>
                                        <p className="fareBreakup-item">
                                            <span className="">
                                                <font>Surcharges</font>
                                            </span>
                                            <span className="">
                                                <font>{0}</font>
                                            </span>
                                        </p>
                                    </div>*/}
                                    <div className="row">
                                        <div className="col-md-6">
                                            <div className="m-b-15">
                                                <strong>Departure</strong>
                                            </div>
                                            <FareDetails paxInfo={this.props.departure.paxInfo}/> 
                                        </div>
                                        <div className="col-md-6">
                                            <div className="m-b-15">
                                                <strong> Arrival </strong>
                                            </div>
                                            <FareDetails paxInfo={this.props.arrival.paxInfo}/> 
                                        </div>
                                    </div>
                                </div>

                                <div className={this.state.activeTab=="cancellation"?"tab-pane active":"tab-pane"} id="cancellation">
                                    <div className="row"> {/*airline-fare-rules*/}
                                        <div className="text-center col-md-12"><Spin tip="Loading..." spinning={this.state.loadingRule}/></div>
                                        <div className="col-md-6">
                                            <div className="m-b-15"> <strong> Departure </strong></div>
                                            {this.showFareRules(this.state.fareRules)}
                                        </div>
                                        <div className="col-md-6">
                                            <div className="m-b-15"> <strong> Arrival </strong></div>
                                            {this.showFareRules(this.state.fareRulesArr)}
                                        </div>
                                        {/* <div className="fare-row rules-heading">
                                            <div className="time-gap-cond">
                                                <p className="text-black LatoBold append_bottom2">Time frame</p>
                                                <p className="font10 lightGreyText">(From Scheduled flight departure)</p>
                                            </div>
                                            <div className="time-gap-cond">
                                                <p className="text-black LatoBold append_bottom2">Airline Fee + MMT Fee</p>
                                                <p className="font10 lightGreyText">(Per passenger)</p>
                                            </div>
                                        </div>
                                        <div className="fare-row">
                                            <div className="time-gap-cond">
                                                <p className="text-black LatoBold append_bottom2">0 hours to 4 hours*</p>
                                            </div>
                                            <div className="time-gap-cond text-black">
                                                <p>ADULT : 
                                                    <b>Non Refundable</b>
                                                    <br/>
                                                </p>
                                            </div>
                                        </div>
                                        <div className="fare-row">
                                            <div className="time-gap-cond">
                                                <p className="text-black LatoBold append_bottom2">4 hours to 3 days*</p>
                                            </div>
                                            <div className="time-gap-cond text-black">
                                                <p>ADULT : 
                                                    <b>₹ 3,500 + ₹ 0</b>
                                                    <br/>
                                                </p>
                                            </div>
                                        </div>
                                        <div className="fare-row">
                                            <div className="time-gap-cond">
                                                <p className="text-black LatoBold append_bottom2">3 days to 365 days*</p>
                                            </div>
                                            <div className="time-gap-cond text-black">
                                                <p>ADULT : 
                                                    <b>₹ 3,000 + ₹ 0</b>
                                                    <br/>
                                                </p>
                                            </div>
                                        </div>
                                        <p className="append_top10 append_bottom5">*From the Date of Departure</p> */}
                                    </div>
                                </div>

                                <div className={this.state.activeTab=="dateChange"?"tab-pane active":"tab-pane"} id="dateChange">
                                    dateChange 
                                </div>

                            </div>
                        </div>
                    </div>
                )}
            </div>
            </LoadingOverlay>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        tkn: state.flights.token
    };
};

export default compose(connect(mapStateToProps, actions))(ReviewStickyPanel);
//export default ReviewStickyPanel;