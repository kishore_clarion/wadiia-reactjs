import React, { Component,PureComponent } from "react";
import "./RoundTripFlight.css";
import { connect } from "react-redux";
import { compose } from "redux";
import * as actions from "../../redux/index";
import ReviewStickyPanel from '../RoundTripFlight/ReviewStickyPanel';
import moment from 'moment';
import {addDefaultSrc,minHHMM,dateDiffHrsMin,returnFlightLogo} from "../../utils/commonFunc"
import {isMobileOnly} from "react-device-detect";


class RoundTripFlight extends PureComponent {
    state={
        selectedDeparture:{},
        selectedArrival:{}
    }
    componentDidMount=()=> {
        if(this.props.departureFlight && this.props.arrivalFlight) {
            this.setState({
                selectedDeparture:{...this.props.departureFlight},
                selectedArrival:{...this.props.arrivalFlight}               
            });
        }
    }
    selectionHandler=async(key)=>{
        console.log(this.props.departureFlight,this.props.arrivalFlight);        
        //if(this.props.departureFlight.items[0].item[0].code) { 
        if(this.props.departureFlight || this.props.arrivalFlight) {            
            if(key==="depart") {                
                this.props.handleSelection({key:"departure",departure:this.props.departureFlight})
                this.setState({selectedDeparture:{...this.props.departureFlight}}); 
            }
            if(key==="arrival") {                
                this.props.handleSelection({key:"arrival",arrival:this.props.arrivalFlight})
                this.setState({selectedArrival:{...this.props.arrivalFlight}});
            }
        }        
    }
    disableSelection=(arrivalProviderName)=>{
        console.log(arrivalProviderName);
        if(this.props.departureFlight.vendors[0].item.name===arrivalProviderName) {
            return false
        }
        return true;
    }  
    
    totalDurationDeparture=this.props.departureFlight?minHHMM(this.props.departureFlight.totalDuration):null;//this.searchObj(.tpExtension,"key","durationHours");
    //totalDurationDeparture=this.props.departureFlight?this.props.departureFlight.totalDuration:null;//this.searchObj(this.props.departureFlight.items[0].tpExtension,"key","durationMinutes");
    totalDurationArrival=this.props.arrivalFlight?minHHMM(this.props.arrivalFlight.totalDuration):null;//this.searchObj(this.props.arrivalFlight.items[1].tpExtension,"key","durationHours");
    //totalDurationArrival=this.props.arrivalFlight?this.props.arrivalFlight.totalDuration:null;//this.searchObj(this.props.arrivalFlight.items[1].tpExtension,"key","durationMinutes");
    
    render(){        
        let departureStopLabel, arrivalStopLabel; 
        if(this.props.departureFlight) {
            this.props.departureFlight.stops===0 ? departureStopLabel="Non Stop" : (this.props.departureFlight.stops===1?departureStopLabel="stop" : departureStopLabel="stops");
        }
        if(this.props.arrivalFlight) {
            this.props.arrivalFlight.stops===0 ? arrivalStopLabel="Non Stop" : (this.props.arrivalFlight.stops===1?arrivalStopLabel="stop" : arrivalStopLabel="stops");
        }
        let totalDeparture,totalArrival=0;
        if(this.props.departureFlight) {
            totalDeparture=dateDiffHrsMin(this.props.departureFlight.items[0].dateInfo.startDate,this.props.departureFlight.items[0].dateInfo.endDate);
        }
        if(this.props.arrivalFlight) {
            totalArrival=dateDiffHrsMin(this.props.arrivalFlight.items[0].dateInfo.startDate,this.props.arrivalFlight.items[0].dateInfo.endDate);
        }
        return(                    
            <div className="w-flight-listing w-flight-listing-roundtrip">
                <div className="row">                    
                    <div className="col-md-6 col w-flight-listing-roundtrip__left">                   
                    {this.props.departureFlight && Object.keys(this.props.departureFlight).length>0 && this.props.departureFlight.items[0] && (
                        <div className={(this.props.selected.departure && this.props.selected.departure.items[0].item[0].token === this.props.departureFlight.items[0].item[0].token)? "w-flight-list selected" : "w-flight-list"}>
                                <div className="wd-flex-center">                                  
                                    <input type="radio" className="w-flight-list__radio" name="departureFlight" 
                                    checked={this.props.selected.departure && this.props.selected.departure.items[0].item[0].token === this.props.departureFlight.items[0].item[0].token } 
                                    onChange={()=>{this.selectionHandler("depart")}}
                                    />
                                    <div className="font-size-12 w-flight-list__air-details">
                                        <span>{this.props.departureFlight.items[0].item[0].vendors[0].item.name}</span>
                                        <span>{`${this.props.departureFlight.items[0].item[0].code}`}</span>
                                        <span>{this.props.departureFlight.vendors[0].item.name}</span>
                                    </div>
                                </div>
                                <ul className="wd-flex-center">
                                    <li>
                                        <div className="air-flight wd-flex-center">
                                            <div className="air-flight__img">
                                                <img width="40" src={returnFlightLogo(this.props.departureFlight.items[0].item[0].images[0].url,isMobileOnly)} onError={addDefaultSrc}/>
                                            </div>
                                            <div className="air-flight-departure">
                                                <span className="air-flight-departure__heading">{`${moment(new Date(this.props.departureFlight.items[0].dateInfo.startDate)).format('hh:mm A')}`}</span> 
                                                <span className="font-size-10 air-flight-departure__location">{this.props.departureFlight.locationInfo.fromLocation.city}</span> 	
                                            </div>
                                        </div>                                        
                                    </li>
                                    <li>
                                        <div className="air-flight-duration" date-placement="left" data-toggle="tooltip" data-html="true" title="">
                                        {totalDeparture && (<span> <strong>{`${totalDeparture.hrs} `}</strong> Hrs <strong>{`${totalDeparture.min} `}</strong> mins</span>)}
                                            <div className="air-flight-duration__divider">
                                                <span></span>
                                            </div>
                                            <a href="#">{this.props.departureFlight.stopDetails[0]?this.props.departureFlight.stopDetails[0]:''} {departureStopLabel} {this.props.departureFlight.stopDetails[0] > 0 ? "via"+this.props.departureFlight.items[0].item[0].locationInfo.toLocation.city : ''} </a>	{/*.stopDetails[0]?this.props.flight.stopDetails[0]:''} {departureStopLabel} {this.props.flight.stopDetails[0] > 0 ? "via"+this.props.flight.items[0].item[0].locationInfo.toLocation.city : ''}*/}
                                        </div>
                                        
                                    </li>
                                    <li> 
                                        <div className="air-flight-arrival">
                                            <span>{`${moment(new Date(this.props.departureFlight.items[0].dateInfo.endDate)).format('hh:mm A')}`}</span> 
                                            <span className="font-size-10 air-flight-arrival__location">{this.props.departureFlight.locationInfo.toLocation.city}</span>
                                        </div>
                                        
                                    </li>
                                    <li>
                                        <div className="air-flight-price">
                                            <span>{this.props.departureFlight.displayAmount}</span>
                                        </div>
                                    </li>
                                </ul>
                            
                        </div>
                    )}
                    </div>

                    <div className="col-md-6 col w-flight-listing-roundtrip__right" >                    
                    {this.props.arrivalFlight && Object.keys(this.props.arrivalFlight).length>0 && this.props.arrivalFlight.items[0] && 
                    //(this.props.selected.departure&&this.props.selectedProvider===this.props.arrivalFlight.vendors[0].item.name) &&
                    (
                        <div className={(this.props.selected.departure&&this.props.selectedProvider===this.props.arrivalFlight.vendors[0].item.name)? "w-flight-list selected" : "w-flight-list "} >
                            <div className="wd-flex-center">                               
                               { (this.props.selected.departure&&this.props.selectedProvider===this.props.arrivalFlight.vendors[0].item.name) && <input type="radio" className="w-flight-list__radio" name="arrivalFlight"
                                checked={this.props.selected.arrival && this.props.selected.arrival.items[0].item[0].token === this.props.arrivalFlight.items[0].item[0].token?true:false } 
                                onChange={()=>{this.selectionHandler("arrival")}}
                                disabled={(!this.props.selected.departure||this.props.selectedProvider!==this.props.arrivalFlight.vendors[0].item.name)?true:false}
                                /> }
                                <div className="font-size-12 w-flight-list__air-details">
                                <span>{this.props.arrivalFlight.items[0].item[0].vendors[0].item.name}</span>
                                <span>{this.props.arrivalFlight.items[0].item[0].code}</span>
                                <span>{this.props.arrivalFlight.vendors[0].item.name}</span>
                                </div>
                            </div>
                            <ul className="wd-flex-center">
                                <li>
                                    <div className="air-flight wd-flex-center">
                                        <div className="air-flight__img">
                                            <img width="40" src={returnFlightLogo(this.props.arrivalFlight.items[0].item[0].images[0].url,isMobileOnly)}  onError={addDefaultSrc}/>
                                        </div>
                                        <div className="air-flight-departure">
                                            <span className="air-flight-departure__heading">{`${moment(new Date(this.props.arrivalFlight.items[0].dateInfo.startDate)).format('hh:mm A')}`}</span> 
                                            <span className="font-size-10 air-flight-departure__location">{this.props.arrivalFlight.locationInfo.fromLocation.city}</span> 	
                                        </div>
                                    </div>
                                    
                                </li>
                                <li>
                                    <div className="air-flight-duration" date-placement="left" data-toggle="tooltip" data-html="true" title="<div class='tooltip-content'><p>Plane Change</p><p><span>Chennai</span> (MMA) | 8hrs layover</p></div>">
                                    {totalArrival && (<span> <strong>{`${totalArrival.hrs} `}</strong> Hrs <strong>{`${totalArrival.min} `}</strong> mins</span>)}
                                        <div className="air-flight-duration__divider">
                                            <span></span>
                                        </div>
                                        <a href="#">{this.props.arrivalFlight.stopDetails[0]?this.props.arrivalFlight.stopDetails[0]:''} {arrivalStopLabel} {this.props.arrivalFlight.stopDetails[0] > 0 ? "via"+ this.props.arrivalFlight.items[0].item[0].locationInfo.toLocation.city:""} </a>	
                                    </div>
                                    
                                </li>
                                <li> 
                                    <div className="air-flight-arrival">
                                        <span>{`${moment(new Date(this.props.arrivalFlight.items[0].dateInfo.endDate)).format('hh:mm A')}`}</span> 
                                        <span className="font-size-10 air-flight-arrival__location">{this.props.arrivalFlight.locationInfo.toLocation.city}</span>
                                    </div>
                                    
                                </li>
                                <li>
                                    <div className="air-flight-price">
                                        <span>{this.props.arrivalFlight.displayAmount}</span>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    )}
                    </div>                    
                </div>                
            </div>                  
        )
    }
}

//export default RoundTripFlight;
const mapStateToProps = (state) => {
    return {
        type:state.flights.reqType,
        rdxLoading:state.flights.loading, 
        tkn : state.flights.token,
        resFlight:state.flights.response,
        fromLocation:state.flights.fromLocation,
        toLocation:state.flights.toLocation,
        tripType:state.flights.tripType,
        fromDate:state.flights.fromDate,
        toDate:state.flights.toDate,
        passanger:state.flights.passanger,
        serviceClass:state.flights.serviceClass
    };
};

export default compose(connect(mapStateToProps, actions))(RoundTripFlight);
//export default RoundTripFlight;