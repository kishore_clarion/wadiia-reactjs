import React, { Component,Fragment } from "react";
import './LeftPanel.css';
import {getAllSearchResult,getSearchFlights} from '../../services/search';
import {days,messageDuration,domestic} from '../../services/constants';
import Spinner from 'react-bootstrap/Spinner'
import { connect } from "react-redux";
import * as actions from "../../redux/index";
import {  DatePicker,Spin } from "antd";
import "antd/dist/antd.css";
import moment from 'moment';
import history from '../../utils/history';
import LoadingOverlay from 'react-loading-overlay';
import {searchObj} from "../../utils/commonFunc";
import { message } from 'antd';
import DayPicker from 'react-day-picker';
import {DebounceInput} from 'react-debounce-input';
import 'react-day-picker/lib/style.css';

class LeftPanel extends Component {          
    state = {        
        locationsDept : [],
        locationsArr : [],
        name: '',
        address: '',
        departureFrom:null,
        departureFromCity:'',
        arrivalToCity:"",
        fromDate: moment().format("YYYY-MM-DD"),        
        loadingDept:false,
        loadingArr:false,
        activeTab:"Oneway",
        adult:1,
        children:0,
        infant:0,
        showTraveller:false,
        disbAdltPlus:false,
        disbAdltMinus:true,
        disbChildPlus:false,
        disbChildMinus:true,
        disbInfantPlus:false,
        disbInfantMinus:true,
        toggleSelection:false,
        serviceClass:null,
        direct:false,
        error:"",
        loadingList:false,
        isActive:false,
        serviceClasses:[],
        activeSpin:false,
        labels:[
            { 
                date: new Date(), 
                textColor: '#46c4f3',
                text: '$ 100'
            },{ 
                recurring: { 
                    repeat: 'monthly',day: 1
                },
                color: 'red',
                title: '$ 100'
            }
        ],
        showCalendar:true,
        showReturnCalendar:true,
        hover:false
    } 
    
    componentDidMount=async()=>{
        let lastLoc;
        if(this.props.recentSearch) {
            lastLoc=[...this.props.recentSearch].reverse()[0];//localStorage.getItem('lastSearch');
        }         
        if(lastLoc) {            
            //lastLoc=JSON.parse(lastLoc);
            await this.setState({
                departureFrom:lastLoc.fromLocation,
                departureFromCity:lastLoc.fromLocation.city,
                arrivalTo:lastLoc.toLocation,
                arrivalToCity:lastLoc.toLocation.city,
                serviceClasses:lastLoc.serviceClasses?lastLoc.serviceClasses:[]
            });
            if(this.state.serviceClasses && this.state.serviceClasses.length===0){
                this.showServiceClasses();   
            }         
        } 
    } 

    componentDidUpdate=(prevProps)=>{
        if(!this.state.departureFrom && prevProps.localCity!==this.props.localCity) {            
            this.setState({departureFrom:{
                city:this.props.localCity.city,
                name:this.props.localCity.to_airport_name,
                country:this.props.localCity.field_country,
                id:this.props.localCity.field_iata_code
            },departureFromCity:this.props.localCity.city})            
        }
    }
    
    handleChange = async (e) => {
        const val=e.target.value;
        this.setState({
            departureFromCity:val,
            loadingDept: true
        });
        const res=await getAllSearchResult(val);
        if(res.status) {
            this.setState({
            loadingDept:false,
            locationsDept: res.data ,
            name: val,
            address: val,
            showResults:true
            });
        } else {
            this.setState({
            loadingDept:false,
            locationsDept: [] ,
            name: val,
            address: val,
            showResults:true
            });
        }
        if(!this.state.departureFromCity){
            this.setState({
            showResults:false
            });
        }
        };
    // handleChange = async (e) => {        
    //     const val=e.target.value;             
    //     this.setState({
    //         departureFromCity:val
    //     });                    
    //     if(this.state.departureFromCity && this.state.departureFromCity.length>=2){
    //         this.setState({                     
    //             loadingDept:true
    //         });

    //         const res=await getAllSearchResult(val);            
    //         if(res.status) {              
    //             this.setState({ 
    //                 loadingDept:false,
    //                 locationsDept: res.data ,
    //                 name: val,
    //                 address: val,
    //                 showResults:true
    //             }); 
    //         } else  {
    //             this.setState({ 
    //                 loadingDept:false,
    //                 locationsDept: [] ,
    //                 name: val,
    //                 address: val,
    //                 showResults:true
    //             }); 
    //         }         
    //         if(!this.state.departureFromCity){                
    //             this.setState({                     
    //                 showResults:false
    //             });
    //         }
    //     } else {            
    //         this.setState({                     
    //             showResults:false
    //         });
    //     } 
    // };

    handleArrival = async (e) => {        
        const val=e.target.value;         
        this.setState({
            arrivalToCity:val,
            loadingArr:true
        }); 
        const res=await getAllSearchResult(val);             
            if(res.status)  {             
                this.setState({ 
                    loadingArr:false,
                    locationsArr: res.data ,
                    name: val,
                    address: val,
                    showResultsArr:true
                }); 
            } else  {
                this.setState({ 
                    loadingArr:false,
                    locationsArr: [] ,
                    name: val,
                    address: val,
                    showResultsArr:true
                });  
            }         
        if(!this.state.arrivalToCity){                
            this.setState({                     
                showResultsArr:false
            });
        }        
    };

    // handleArrival = async (e) => {        
    //     const val=e.target.value;         
    //     this.setState({
    //         arrivalToCity:val
    //     });             
    //     if(this.state.arrivalToCity && this.state.arrivalToCity.length>=2){
    //         this.setState({                     
    //             loadingArr:true
    //         });
    //         const res=await getAllSearchResult(val);             
    //         if(res.status)  {             
    //             this.setState({ 
    //                 loadingArr:false,
    //                 locationsArr: res.data ,
    //                 name: val,
    //                 address: val,
    //                 showResultsArr:true
    //             }); 
    //         } else  {
    //             this.setState({ 
    //                 loadingArr:false,
    //                 locationsArr: [] ,
    //                 name: val,
    //                 address: val,
    //                 showResultsArr:true
    //             });  
    //         }         
    //         if(!this.state.arrivalToCity){                
    //             this.setState({                     
    //                 showResultsArr:false
    //             });
    //         }
    //     } else {            
    //         this.setState({                     
    //             showResultsArr:false
    //         });
    //     }
    // };

    selectDepartureFrom=async (obj)=>{
        await this.setState({ 
            departureFrom:obj,           
            departureFromCity:obj.city,
            showResults:false
        })        
        this.showResults=false;
        if(this.state.arrivalToCity)
            await this.showServiceClasses();
    }
    
    selectArrivalTo=async (obj)=>{        
        await this.setState({ 
            arrivalTo:obj,           
            arrivalToCity:obj.city,
            showResultsArr:false
        })        
        this.showResultsArr=false;
        if(this.state.departureFromCity) {
            await this.showServiceClasses();
        }
    }

    disabledDate=(current)=> {
        // Can not select days before today 
        return current < moment().startOf('day')
    }
    
    handleActiveTab=(tabName)=>{
        const obj={activeTab:tabName};
        if(tabName==="Oneway" && this.state.toDate) {
            obj.toDate='';
        }
        this.setState((state)=>{return obj})
        console.log(this.state.toDate);
    }

    updateTraveller=(key,type)=>{
        let disbAdltPlus,disbAdltMinus,disbChildPlus,disbChildMinus,disbInfantPlus,disbInfantMinus=false;
        if(this.state.adult===1) { disbAdltMinus=true;}
        if(this.state.adult===9) {disbAdltPlus=true; disbChildPlus=true;}
        if(this.state.children===0) {disbChildMinus=true; }
        if(this.state.children===8) {disbChildPlus=true;}
        if(this.state.infant===0) {disbInfantMinus=true;}
        if(this.state.infant===9) {disbInfantPlus=true;}
        this.setState({
            disbAdltPlus:disbAdltPlus,
            disbAdltMinus:disbAdltMinus,
            disbChildPlus:disbChildPlus,
            disbChildMinus:disbChildMinus,
            disbInfantPlus:disbInfantPlus,
            disbInfantMinus:disbInfantMinus,
        })
        if(type==="p" && (key === "adult" || key === "children") ) { console.log("***",this.state.adult,this.state.children);
            if(+this.state.adult+this.state.children === 9){ console.log("*3");
                if(key==="adult" && this.state.adult==1) this.setState({children:0})                 
                else if(key==="adult") this.setState({disbAdltPlus:true,children:0}) 
                else this.setState({disbAdltPlus:false}) 
                if(key==="children") this.setState({disbChildPlus:true})
                else this.setState({disbChildPlus:false})
                return;             
            }            
        }

        if(type==="m" && key === "adult") {
            if((+this.state.adult+this.state.children == 9) && this.state.adult==this.state.infant && this.state.adult>1){
                this.setState({adult:this.state.adult-1,infant:0})
                return;             
            }            
        }
        //adult should not greater than 9
        if(type==="p" && key === "adult" && this.state[key]===9) {
            if(key==="adult") {
                this.setState({disbAdltPlus:true});
            } else {
                this.setState({disbAdltPlus:false});
            }
            return;
        }
        //Children should not greater than 8
        if(type==="p" && key === "children" && this.state[key]===8) {
            if(key=="children") {
                this.setState({disbChildPlus:true});
            } else {
                this.setState({disbChildPlus:false});
            }
            return;
        }
        //Infants should not greater than 9
        if(type==="p" && key === "infant" && this.state[key]===9) {
            if(key=="infant") {
                this.setState({disbInfantPlus:true});
            } else { 
                this.setState({disbInfantPlus:false});
            }
            return;
        }
        //Infants should not greater than adults
        if(type==="p" && key === "infant" && this.state[key]===this.state.adult) {
            if(key==="infant") {
                this.setState({disbInfantPlus:true});
            } else {
                this.setState({disbInfantPlus:false});
            }
            return;
        }        
        if(type==="p" && key === "children") {
            if((this.state.adult===1 && this.state.children===8) || (this.state.adult===2 && this.state.children===7) || (this.state.adult===3 && this.state.children===6) || (this.state.adult===4 && this.state.children===5) || (this.state.adult===5 && this.state.children===4)
            || (this.state.adult===6 && this.state.children===3) || (this.state.adult===7 && this.state.children===2) || (this.state.adult===8 && this.state.children===1)){
                if(key==="children") {
                    this.setState({disbChildPlus:true});
                } else {
                    this.setState({disbChildPlus:false});
                }
                return;
            }
        }
        if(type==="m" && this.state[key]>0) {
            if(!(key==="adult" && this.state[key]===1)) {
                this.setState({[key]:this.state[key]-1})
            }
        }
        if(type==="p" && this.state[key]>-1) {
            this.setState({[key]:this.state[key]+1})
        }
    }
    
    handleSearch=async () => { 
       // const [cookies, setCookie] = useCookies(['recentSearch']);        
        await this.setState({error:null});        
        if(this.state.activeTab=="Oneway") {
            if(!this.state.departureFrom || !this.state.departureFrom.id ){
                this.setState({error:"Departure location is required !"});
                return;
            }
            if(!this.state.arrivalTo || !this.state.arrivalTo.id){
                this.setState({error:"Arrival to location is required !"});   
                return;         
            }
            if(!this.state.fromDate) {
                this.setState({error:"Departure date is required !"});
                return;
            }
        }
        if(this.state.activeTab=="Roundtrip") {
            if(!this.state.departureFrom || !this.state.departureFrom.id ){
                this.setState({error:"Departure from location is required !"});
                return;
            }
            if(!this.state.arrivalTo || !this.state.arrivalTo.id) {
                this.setState({error:"Arrival to location is required !"});
                return;
            }
            if(!this.state.fromDate) {
                this.setState({error:"Departure date is required !"});
                return;
            }
            if(!this.state.toDate) {
                this.setState({error:"Return date is required !"});
                return;
            }
        }        
        if(!this.state.error) {
            let data={
                fromLoc:this.state.departureFrom,
                toLoc:this.state.arrivalTo,
                startDate:this.state.fromDate,
                endDate:this.state.toDate,
                adult:this.state.adult,
                child:this.state.children,
                infant:this.state.infant,
                tripType:this.state.activeTab,
                serviceClass:this.state.serviceClass?this.state.serviceClass:null,//,
                direct:this.state.direct?this.state.direct:false
            } 
            this.setState({loadingList:true,isActive:true});
            /***newcode */
            let passanger=JSON.stringify({adult:data.adult,infant:data.infant,child:data.child});            
            //history.push(`/flight-listing?fromDate=${data.startDate}&toDate=${data.endDate}&fromLocation=${JSON.stringify({id:data.fromLoc.id,city:data.fromLoc.city})}&toLocation=${JSON.stringify({id:data.toLoc.id,city:data.toLoc.city})}&passanger=${passanger}&serviceClass=${data.serviceClass}&tripType=${data.tripType}&direct=${data.direct}`);
            if(data.tripType==='Roundtrip' && this.state.departureFrom.countryID===this.state.arrivalTo.countryID && domestic.indexOf(this.state.arrivalTo.countryID)>-1) {               
               history.push(`/flight-listing/dom?fromDate=${data.startDate}&toDate=${data.endDate}&fromLocation=${JSON.stringify({id:data.fromLoc.id,city:data.fromLoc.city,country:data.fromLoc.country})}&toLocation=${JSON.stringify({id:data.toLoc.id,city:data.toLoc.city,country:data.toLoc.country})}&passanger=${passanger}&serviceClass=${data.serviceClass}&tripType=${data.tripType}&direct=${data.direct}`);   
            } else {
                history.push(`/flight-listing?fromDate=${data.startDate}&toDate=${data.endDate}&fromLocation=${JSON.stringify({id:data.fromLoc.id,city:data.fromLoc.city,country:data.fromLoc.country})}&toLocation=${JSON.stringify({id:data.toLoc.id,city:data.toLoc.city,country:data.toLoc.country})}&passanger=${passanger}&serviceClass=${data.serviceClass}&tripType=${data.tripType}&direct=${data.direct}`);
            }
            /**end */
        } 
    }

    handleServiceClassChange= async(changeEvent)=> {
        this.setState({
          serviceClass: changeEvent.target.value
        });
    }
    
    
    showServiceClasses = async()=>{
        if(//!this.state.toggleSelection && 
        ((this.state.activeTab=="Oneway" && this.state.departureFrom && this.state.arrivalTo && this.state.fromDate) ||
        (this.state.activeTab=="Roundtrip" && this.state.departureFrom && this.state.arrivalTo && this.state.fromDate && this.state.fromDate))
        ) {
            let data={
                fromLoc:this.state.departureFrom,
                toLoc:this.state.arrivalTo,
                startDate:this.state.fromDate,
                endDate:this.state.toDate,
                adult:this.state.adult,            
                tripType:this.state.activeTab
            } 
            this.setState({activeSpin:true});
            let res=await getSearchFlights(data); console.log(res);            
            if(res.status && res.res.response.data.length>1 && res.res.response.availableFiltersIndex) {
                let filterArr=res.res.response.availableFiltersIndex[0].item;               
                let serviceClasses=searchObj(filterArr,"name","serviceclass").values;
                await this.setState({serviceClasses:[...serviceClasses],activeSpin:false}); //,toggleSelection:!this.state.toggleSelection           
            } else {  
                await this.setState({activeSpin:false});  //toggleSelection:!this.state.toggleSelection,  
                if(res.message) {   
                    message.error(res.code +" : "+res.message,messageDuration);
                } else {
                    message.warning("Service Classes Not available");
                }
            }
        } else {
            this.setState({serviceClasses:[]});//,toggleSelection:!this.state.toggleSelection
        }
    }

    renderDay=(day)=> {
        const date = day.getDate();
        return (
          <div>
            <div>{date}</div>
            {
              date===15?
                <div >
                   {`$100`}
                </div> : null
              }
          </div>
        );
      }

    showTabContent=() =>{
        return (
            <Fragment>
                <div className="row select-flight-option">
                    <div className="col-md-6 col">
                        <div className="input-field">
                            <label htmlFor="from" className="font-tajawal">Depart From</label>
                            {/* <input type="text" id="from-place" className="form-control from-city font-tajawal" value={this.state.departureFromCity||''}                                
                                onChange={(e) => this.handleChange(e)}
                                placeholder="Depart From"/> */}
                            <DebounceInput type="text" id="from-place" className="form-control from-city font-tajawal" minLength={3} debounceTimeout={500} value={this.state.departureFromCity||''} onChange = {(e) => this.handleChange(e)} placeholder="Depart From" />
                                
                            {this.state.loadingDept ? <Spinner animation="border" role="status" /> : null}
                            <ul className="location-result" style={{ display: this.state.showResults ? "block" : "none" }}>
                                {
                                    this.state.locationsDept.map(location => <li key={location.id} onClick={() => this.selectDepartureFrom(location)}> {location.address} </li>)
                                }
                            </ul>
                            <div className="citycode font-tajawal"> {this.state.departureFrom ? this.state.departureFrom.id : ""} </div>

                        </div>
                    </div>
                    <div className="col-md-6 col">
                        <div className="input-field city-going-to">
                            <label htmlFor="from" className="font-tajawal">Going To</label>
                            {/* <input type="text" className="form-control to-city font-tajawal" id="to-place"
                                value={this.state.arrivalToCity}
                                onChange={(e) => this.handleArrival(e)} placeholder="Going To" /> */}
                            <DebounceInput type="text" id="from-place" className="form-control to-city font-tajawal" minLength={3} debounceTimeout={500} value={this.state.arrivalToCity||''} onChange = {(e) => this.handleArrival(e)} placeholder="Going To" />
                            {this.state.loadingArr ? <Spinner animation="border" role="status" /> : null}
                            <ul className="location-result" style={{ display: this.state.showResultsArr ? "block" : "none" }}>
                                {
                                    this.state.locationsArr.map(location => <li key={location.id} onClick={() => this.selectArrivalTo(location)}> {location.address} </li>)
                                }
                            </ul>
                            <div className="citycode font-tajawal"> {this.state.arrivalTo ? this.state.arrivalTo.id : ""} </div>
                        </div>
                    </div>
                </div>
                <div className="row select-flight-option">
                    {/*Oneway custom calendar*/}
                    
                    <div className={this.state.showCalendar?"custom-datepicker d-none":"custom-datepicker d-block"} >
                        <div className="hide-custom-calendar">
                            <button onClick={this.handleCalenderDisable}><i className="fa fa-arrow-left"></i> Back </button>
                        </div>
                        <DayPicker numberOfMonths={12} pagedNavigation                            
                        className={this.state.showCalendar?"d-none":"d-block"}
                        disabledDays={[{ before: new Date()}]}
                        renderDay={this.renderDay}
                        selectedDays={moment(this.state.fromDate)._d}
                        onDayClick={(date)=>{
                            const dateToCompare=moment(date).format('YYYY-MM-DD');
                            const currentDate=moment().format('YYYY-MM-DD');
                            if(moment(dateToCompare).isSame(currentDate)||moment(dateToCompare).isAfter(currentDate)) {
                                this.setState({fromDate:moment(date).format("YYYY-MM-DD"),showCalendar:!this.state.showCalendar})                                 
                            }
                        }} /> 
                    </div>

                    {/*Roundtrip custom calendar starts*/}

                    <div className={this.state.showReturnCalendar?"custom-datepicker d-none":"custom-datepicker d-block"} >
                        <div className="hide-custom-calendar">
                            <button onClick={this.handleCalenderDisable}><i className="fa fa-arrow-left"></i> Back </button>
                        </div>
                        <DayPicker numberOfMonths={12} pagedNavigation                            
                        className={this.state.showReturnCalendar?"d-none":"d-block"}
                       // disabledDays={[{ before: new Date()}]}
                        disabledDays={[{ before: this.state.fromDate?moment(this.state.fromDate)._d:moment()._d}]}
                        renderDay={this.renderDay}
                        selectedDays={moment(this.state.toDate)._d}
                        onDayClick={(date)=>{ 
                            const dateToCompare=moment(date).format('YYYY-MM-DD');
                            const currentDate=this.state.fromDate?moment(this.state.fromDate).format('YYYY-MM-DD'):moment(date).format('YYYY-MM-DD');                            
                            if(moment(dateToCompare).isSame(currentDate)||moment(dateToCompare).isAfter(currentDate)) {
                            date?this.setState({toDate:moment(date).format("YYYY-MM-DD"),
                                    activeTab:"Roundtrip",showReturnCalendar:!this.state.showReturnCalendar}) 
                            : this.setState({toDate:null,activeTab:"Oneway",showReturnCalendar:!this.state.showReturnCalendar});
                            }

                        }} />
                    </div>

                    <div className="col-md-6 col">
                        <div className="input-field">
                            <label htmlFor="from" className="font-tajawal">Depart Date</label>
                            <input type="text" value={this.state.fromDate?moment(this.state.fromDate).format("Do MMM YY"):moment().format("Do MMM YY")} 
                                onFocus={()=>{
                                    this.setState({showCalendar:!this.state.showCalendar})
                                }}
                                readOnly
                            />                            
                            <div className="citycode font-tajawal"> {moment(this.state.fromDate, "YYYY-MM-DD").format('dddd')} </div>
                        </div>
                    </div>

                    <div className="col-md-6 col">
                        <div  className={this.state.toDate && this.state.hover?'input-field city-going-to active-custom-calendar':'input-field city-going-to'}>
                            <label htmlFor="from" className="font-tajawal">Return Date</label>                            
                            <input type="text" value={this.state.toDate?moment(this.state.toDate).format("Do MMM YY"):''} 
                                onFocus={()=>{
                                    this.setState((state)=>{return{showReturnCalendar:!this.state.showReturnCalendar}})
                                }}
                                onMouseEnter={()=>{this.setState({hover:!this.state.hover})}}
                               // onMouseLeave={()=>{this.setState({hover:!this.state.hover})}}                                
                                readOnly
                            /> 
                            <button onClick={()=>{
                                this.setState((state)=>{
                                    return {toDate:'',activeTab:"Oneway"}
                                    })                                
                                }} className="city-going-to__clear"><i className="fas fa-times-circle"></i></button>
                            <div className="return-label" style={{ display: !this.state.toDate ? "block" : "none" }}>  {"Book Round Trip to save extra"}</div>
                            <div className="citycode font-tajawal" style={{ display: this.state.toDate ? "block" : "none" }}> {moment(this.state.toDate, "YYYY-MM-DD").format('dddd')} </div>

                        </div>
                    </div>

                </div>

                <div className="row">
                    <div className="col-md-12">
                        <div className="select-box-wrapper">
                            <Spin tip="Loading..." spinning={this.state.activeSpin}>
                                <label htmlFor="class" className="font-tajawal">Traveller(s) {this.state.serviceClass ? ", Class" : ""}</label>
                                <div className="custum-select font-tajawal" /*onClick={()=>{this.showServiceClasses()}}*/ onClick={() => this.setState({ toggleSelection: !this.state.toggleSelection })}>
                                    {this.state.adult + this.state.children + this.state.infant > 1 ? this.state.adult + this.state.children + this.state.infant + " Travellers" + `${this.state.serviceClass ? ", " + this.state.serviceClass : ""}` : this.state.adult + this.state.children + this.state.infant + " Traveller" + `${this.state.serviceClass ? ", " + this.state.serviceClass : ""}`}
                                </div>
                            </Spin>
                            {this.state.toggleSelection && (
                                <div className="open-select">
                                    <div className="count-sec-wrapper d-flex font-tajawal">
                                        <div className="custom-count-sec">
                                            <div className="count-sec__label"> {this.state.adult > 1 ? "Adults" : "Adult"} </div>
                                            <div className="count-sec">
                                                <span className="ddSpinnerMinus" ><button type="button" onClick={() => this.updateTraveller("adult", "m")} disabled={this.state.disbAdltMinus}> - </button></span>
                                                <span className="count">{this.state.adult}</span>
                                                <span className="ddSpinnerPlus"> <button type="button" onClick={() => this.updateTraveller("adult", "p")} disabled={this.state.disbAdltPlus}> + </button> </span>
                                            </div>
                                        </div>

                                        <div className="custom-count-sec">
                                            <div className="count-sec__label"> {this.state.children > 1 ? "Children" : "Child"} <small>(2-12 YRS)</small> </div>
                                            <div className="count-sec">
                                                <span className="ddSpinnerMinus" >  <button type="button" onClick={() => this.updateTraveller("children", "m")} disabled={this.state.disbChildMinus}>-</button> </span>
                                                <span className="count">{this.state.children}</span>
                                                <span className="ddSpinnerPlus" >  <button type="button" onClick={() => this.updateTraveller("children", "p")} disabled={this.state.disbChildPlus}> +</button> </span>
                                            </div>
                                        </div>

                                        <div className="custom-count-sec">
                                            <div className="count-sec__label"> {this.state.infant > 1 ? "Infants" : "Infant"} <small>( Below 2 YRS)</small> </div>
                                            <div className="count-sec">
                                                <span className="ddSpinnerMinus" >  <button type="button" onClick={() => this.updateTraveller("infant", "m")} disabled={this.state.disbInfantMinus}>-</button> </span>
                                                <span className="count">{this.state.infant}</span>
                                                <span className="ddSpinnerPlus">  <button type="button" onClick={() => this.updateTraveller("infant", "p")} disabled={this.state.disbInfantPlus}> +</button> </span>
                                            </div>
                                        </div>

                                    </div>
                                    <div className="economy-wrapper">
                                        <div className="economy">
                                            {
                                                this.state.serviceClasses.map((serviceClass, index) => {
                                                    return <div className="economy__checkbox" key={"serviceClass" + index}>
                                                        <input type="radio" id="economy" value={serviceClass} checked={this.state.serviceClass === serviceClass} onChange={this.handleServiceClassChange} />
                                                        <label htmlFor="economy"> {serviceClass}</label>
                                                    </div>
                                                })
                                            }
                                        </div>
                                    </div>

                                </div>
                            )}
                        </div>
                    </div>
                </div>

            </Fragment>        
        )
    }

    handleCalenderDisable=()=>{
        if(!this.state.showReturnCalendar||!this.state.showCalendar) {
            this.setState({showReturnCalendar:true,showCalendar:true});
        }
    }
    render(){                  
        return(            
            <div>
            <LoadingOverlay
                active={this.state.isActive}
                spinner={true}
                text='Loading your content...'
                >
                </LoadingOverlay>
            <div className="left-panel">            
                <div className="left-panel__booking-option">
                     <ul className="nav nav-tabs w-tabs" >
                        <li className={this.state.activeTab=="Oneway"?"active":""}><a data-toggle="tab" href="#tab1" onClick={()=>this.handleActiveTab("Oneway")}>One Way</a></li>
                        <li className={this.state.activeTab=="Roundtrip"?"active":""}><a data-toggle="tab" href="#tab2" onClick={()=>this.handleActiveTab("Roundtrip")}>Round Trip</a></li>
                        {/*<li className={this.state.activeTab=="multiCity"?"active":""}><a data-toggle="tab" href="#tab3" onClick={()=>this.handleActiveTab("multiCity")}>Multi-City</a></li> */}
                    </ul>
                    <div className="tab-content">
                    <div className="error" style={{color:"red"}}> {this.state.error} </div>
                        <div id="tab1" className="tab-pane in active"> 
                            {this.showTabContent()}
                        </div>
                        <div id="tab2" className="tab-pane">                  
                            {this.showTabContent()}
                        </div>

                        {/* <div id="tab3" className="tab-pane">
                            <h3>Multi-City</h3>
                            <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam.</p>                            
                        </div> */}

                    </div>

                    <div className="flight-non-stop">
						<label htmlFor="select-check"> <input type="checkbox" id="select-check" value="direct" name="direct"
                  
                  onChange={()=>this.setState({direct : !this.state.direct})}/> Non Stop Flights </label>
					</div>                   
                    <div className="search-flight">
                        {/*this.state.loadingList?<Spinner animation="border" role="status" />:null*/ }
                        <button className="btn btn-primary" onClick={()=>this.handleSearch()}>Search Flights</button>                        
                    </div>
                </div> 
                </div>               
            </div>
            
        )
    }
}

const mapStateToProps = (state) => {
    return {
        recentSearch: state.user.recentSearch
    };
};
export default (connect(mapStateToProps, actions))(LeftPanel);