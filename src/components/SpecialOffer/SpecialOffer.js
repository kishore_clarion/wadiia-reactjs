import React from 'react';
import {siteBaseUrl} from "../../services/constants";
//import '../../containers/Home/Home.css';


function SpecialOffer(props) {     
    return (  
        <div className="bg-wadiia-special-wrap" key={"special"+props.index}>
            <span className="wadiia-special__air">
                <img src={`${siteBaseUrl()}images/icon_flight.png`}  alt="flight" />
            </span>
            <div className="wadiia-special__title">{props.spec.field_flight} Flights</div>
            <div className="bg-wadiia-special" onClick={()=>props.handleShow(props.index)}> {/**/}
                <span className="wadiia-special__air">
                    <img src="images/icon_flight.png" alt="flight" />
                </span>
                <a href="" className="wadiia-special__img" onClick={()=>props.handleShow(props.index)}>
                    <img  width="100" src={props.spec.field_url} alt="Offer 01" />                                
                </a>
                <div className="bg-wadiia-special__save"> 
                    <div> Save Upto </div>
                    <div> <strong> { props.spec.field_save } </strong> </div>
                    <div className="text-ellipses"> on {props.spec.field_flight} Flights </div>
                </div>
                <div className="bg-wadiia-special__code"> 
                    <div className="code-label">Code </div>
                    <div className="code"> { props.spec.field_code } </div>
                </div>
            </div>
        </div>  
    );
}

export default SpecialOffer;