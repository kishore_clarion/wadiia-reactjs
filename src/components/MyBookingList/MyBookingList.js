import React, { Component } from 'react';
import { myBookings } from "../../services/flightBooking";
import { message,Pagination,Alert,DatePicker } from 'antd';
import LoadingOverlay from 'react-loading-overlay';
import './MyBookingList.css';
import  moment  from "moment";
import history from '../../utils/history';
const { RangePicker } = DatePicker;
const dateFormat = 'YYYY-MM-DD';


class MyBookingList extends Component {
    state = {
        loading: false,
        myBookings: [],
        current: 1,
        pageLength: 10,
        total: 10,
        filterType:"upcoming",
        actionParam:null,        
        minValue:moment().format('YYYY-MM-DD'),
        maxValue:moment().add(1, 'months').format('YYYY-MM-DD'),
        show:false
    }

    componentDidMount = async () => {
        const filtersIndexs=this.prepareFilterArray();
        this.setState({ loading: true });
        const res = await myBookings({current:this.state.current,pageIndex:0,type:this.state.filterType,filtersIndex:filtersIndexs,pageLength:this.state.pageLength});       
        if (res.status) {
            const actionParam=this.props.match.params.action; console.log(actionParam);
            let objState={ loading: false, myBookings: res.data.data,total:res.data.pageInfoIndex[0].item.totalResults};
            if(actionParam) {
                objState={ loading: false, myBookings: res.data.data,total:res.data.pageInfoIndex[0].item.totalResults,actionParam:actionParam};
            }
            this.setState({...objState});
        } else {
            message.error(res.message);
            this.setState({ loading: false });
        }
    }    

    showList = () => {
        if (this.state.myBookings.length > 0) {
            return this.state.myBookings.map((item, index) => {
                const keys = Object.keys(item);
                return <div className="booking-status-table white-bg" key={"mybooking"+index}>
                            <div className="booking-status-table__heading">
                                <strong>Itinerary ID: {item[keys[0]][0].itineraryRefNo}</strong>
                                <span><a href={"/manage-bookings/air/view/"+item[keys[0]][0].bookingRefNo+"/"+item[keys[0]][0].itineraryRefNo}>More details</a></span>
                            </div>
                            <div className="row m-b-20">
                                <div className="col-md-4">
                                    <div>Business Name :</div>
                                    <div className="m-b-20"> <strong>{item[keys[0]][0].details}</strong> </div>
                                    <div>Booking Date :</div>
                                    <div className="m-b-20"> <strong>{moment(item[keys[0]][0].bookingDate).format('DD/MM/YY')}</strong> </div>
                                    <div>Guest Name :</div>
                                    <div className="m-b-20"> <strong>{item[keys[0]][0].firstName +" "+item[keys[0]][0].lastName}</strong> </div>
                                </div>
                                <div className="col-md-4">
                                    <div>Booking Reference Number :</div>
                                    <div className="m-b-20"> <strong>{item[keys[0]][0].bookingRefNo}</strong> </div>
                                    <div>Check-In :</div>
                                    <div className="m-b-20"> <strong>{moment(item[keys[0]][0].dateInfo.startDate).format('DD/MM/YY')}</strong> </div>
                                    <div>Email :</div>
                                    <div className="m-b-20"> <strong>{item[keys[0]][0].email}</strong> </div>
                                </div>
                                <div className="col-md-4">
                                    <div>Booking Status :</div>
                                    <div className="m-b-20"> <strong className="text-success">{item[keys[0]][0].bookingStatus}</strong> </div>
                                    <div>Check-Out :</div>
                                    <div className="m-b-20"> <strong>{moment(item[keys[0]][0].dateInfo.endDate).format('DD/MM/YY')}</strong> </div>
                                    <div>Deadline Date :</div>
                                    <div className="m-b-20"> <strong>{moment(item[keys[0]][0].deadlineDate).format('DD/MM/YY')}</strong> </div>
                                    <div className="manage-btn-wrap">
                                    {this.state.actionParam==="view" || !this.state.actionParam ? <button className="btn" onClick={()=>{history.push("/manage-bookings/air/view/"+item[keys[0]][0].bookingRefNo+"/"+item[keys[0]][0].itineraryRefNo)}}>View</button>:null}
                                        {this.state.filterType==="upcoming" && (
                                            <React.Fragment>        
                                            {/* {this.state.actionParam==="cancel" || !this.state.actionParam ? <button className="btn" onClick={()=>{history.push("/manage-bookings/air/cancel/"+item[keys[0]][0].bookingRefNo+"/"+item[keys[0]][0].itineraryRefNo)}}>Cancel</button>:null}
                                            {this.state.actionParam==="modify" || !this.state.actionParam ? <button className="btn" onClick={()=>{history.push("/manage-bookings/air/modify/"+item[keys[0]][0].bookingRefNo+"/"+item[keys[0]][0].itineraryRefNo)}}>Modify</button>:null} */}
                                            {moment(item[keys[0]][0].dateInfo.startDate).isSameOrAfter(moment()) && (this.state.actionParam==="cancel" || !this.state.actionParam) ? <button className="btn" onClick={()=>{history.push("/manage-bookings/air/cancel/"+item[keys[0]][0].bookingRefNo+"/"+item[keys[0]][0].itineraryRefNo)}}>Cancel</button>:null}
                                            {moment(item[keys[0]][0].dateInfo.startDate).isSameOrAfter(moment()) && (this.state.actionParam==="modify" || !this.state.actionParam) ? <button className="btn" onClick={()=>{history.push("/manage-bookings/air/modify/"+item[keys[0]][0].bookingRefNo+"/"+item[keys[0]][0].itineraryRefNo)}}>Modify</button>:null}
                                            </React.Fragment>
                                        )}
                                        {this.state.actionParam==="voucher" || !this.state.actionParam ? <button className="btn" onClick={()=>{history.push("/manage-bookings/air/voucher/"+item[keys[0]][0].bookingID+"/"+item[keys[0]][0].itineraryID)}}>Voucher</button>:null}
                                        {this.state.actionParam==="invoice" || !this.state.actionParam ? <button className="btn" onClick={()=>{history.push("/manage-bookings/air/invoice/"+item[keys[0]][0].bookingID+"/"+item[keys[0]][0].itineraryID)}}>Invoice</button>:null}
                                    </div>
                                </div>
                            </div> 
                            { 
                                item[keys[0]].length>1 && (
                                <React.Fragment>                                
                                <div className="row m-b-20">
                                    <div className="col-md-4">
                                        <div>Business Name :</div>
                                        <div className="m-b-20"> <strong>{item[keys[0]][1].details}</strong> </div>
                                        <div>Booking Date :</div>
                                        <div className="m-b-20"> <strong>{moment(item[keys[0]][1].bookingDate).format('DD/MM/YY')}</strong> </div>
                                        <div>Guest Name :</div>
                                        <div className="m-b-20"> <strong>{item[keys[0]][1].firstName +" "+item[keys[0]][1].lastName}</strong> </div>
                                    </div>
                                    <div className="col-md-4">
                                        <div>Booking Reference Number :</div>
                                        <div className="m-b-20"> <strong>{item[keys[0]][1].bookingRefNo}</strong> </div>
                                        <div>Check-In :</div>
                                        <div className="m-b-20"> <strong>{moment(item[keys[0]][1].dateInfo.startDate).format('DD/MM/YY')}</strong> </div>
                                        <div>Email :</div>
                                        <div className="m-b-20"> <strong>{item[keys[0]][1].email}</strong> </div>
                                    </div>
                                    <div className="col-md-4">
                                        <div>Booking Status :</div>
                                        <div className="m-b-20"> <strong className="text-success">{item[keys[0]][1].bookingStatus}</strong> </div>
                                        <div>Check-Out :</div>
                                        <div className="m-b-20"> <strong>{moment(item[keys[0]][1].dateInfo.endDate).format('DD/MM/YY')}</strong> </div>
                                        <div>Deadline Date :</div>
                                        <div className="m-b-20"> <strong>{moment(item[keys[0]][1].deadlineDate).format('DD/MM/YY')}</strong> </div>
                                        <div className="manage-btn-wrap">
                                        {this.state.actionParam==="view" || !this.state.actionParam ? <button className="btn" onClick={()=>{history.push("/manage-bookings/air/view/"+item[keys[0]][1].bookingRefNo+"/"+item[keys[0]][1].itineraryRefNo)}}>View</button>:null}
                                            {this.state.filterType==="upcoming" && (
                                                <React.Fragment>        
                                                {/* {this.state.actionParam==="cancel" || !this.state.actionParam ? <button className="btn" onClick={()=>{history.push("/manage-bookings/air/cancel/"+item[keys[0]][1].bookingRefNo+"/"+item[keys[0]][1].itineraryRefNo)}}>Cancel</button>:null}
                                                {this.state.actionParam==="modify" || !this.state.actionParam ? <button className="btn" onClick={()=>{history.push("/manage-bookings/air/modify/"+item[keys[0]][1].bookingRefNo+"/"+item[keys[0]][1].itineraryRefNo)}}>Modify</button>:null} */}
                                                {moment(item[keys[0]][0].dateInfo.startDate).isSameOrAfter(moment()) && (this.state.actionParam==="cancel" || !this.state.actionParam) ? <button className="btn" onClick={()=>{history.push("/manage-bookings/air/cancel/"+item[keys[0]][0].bookingRefNo+"/"+item[keys[0]][0].itineraryRefNo)}}>Cancel</button>:null}
                                                {moment(item[keys[0]][0].dateInfo.startDate).isSameOrAfter(moment()) && (this.state.actionParam==="modify" || !this.state.actionParam) ? <button className="btn" onClick={()=>{history.push("/manage-bookings/air/modify/"+item[keys[0]][0].bookingRefNo+"/"+item[keys[0]][0].itineraryRefNo)}}>Modify</button>:null}
                                                </React.Fragment>
                                            )}
                                            {this.state.actionParam==="voucher" || !this.state.actionParam ? <button className="btn" onClick={()=>{history.push("/manage-bookings/air/voucher/"+item[keys[0]][1].bookingID+"/"+item[keys[0]][1].itineraryID)}}>Voucher</button>:null}
                                            {this.state.actionParam==="invoice" || !this.state.actionParam ? <button className="btn" onClick={()=>{history.push("/manage-bookings/air/invoice/"+item[keys[0]][1].bookingID+"/"+item[keys[0]][1].itineraryID)}}>Invoice</button>:null}
                                        </div>
                                    </div>
                                </div>
                                </React.Fragment>
                                )
                            }
                        </div>                
            })
        }
    }

    onChange = async(page) => {      
        this.setState({ loading: true });
        const res = await myBookings({pageIndex:page-1,type:this.state.filterType,filtersIndex:this.state.filtersIndex});        
        if (res.status) {            
            this.setState({ current: page,loading: false, myBookings: res.data.data,total:res.data.pageInfoIndex[0].item.totalResults });
        } else {
            message.error(res.message);
            this.setState({ loading: false });
        }       
    };

    sizeChange = async(current,size) => {        
        this.setState({ loading: true });        
        const res = await myBookings({pageIndex:current-1,pageLength:size,type:this.state.filterType,filtersIndex:this.state.filtersIndex});       
        if (res.status) {
            this.setState({ current: current,loading: false, myBookings: res.data.data,total:res.data.pageInfoIndex[0].item.totalResults,pageLength:size});
        } else {
            message.error(res.message);
            this.setState({ loading: false });
        }       
    };

    filterData=async()=>{              
        const filtersIndex=this.prepareFilterArray();        
        if(filtersIndex) {
            this.setState({ loading: true });    
            const res = await myBookings({pageIndex:0,pageLength:this.state.pageLength,type:this.state.filterType,filtersIndex:filtersIndex});       
            if (res.status) {
                this.setState({ current: 1,loading: false, myBookings: res.data.data,total:res.data.pageInfoIndex[0].item.totalResults,pageLength:this.state.pageLength});
            } else {
                message.error(res.message);
                this.setState({ loading: false });
            }
        }
    }

    prepareFilterArray=()=>{ 
        const idExp= /^[A-Z0-9\-]+$/;        
        const item=[];
        if(this.state.bookingstatus) {
            item.push({Name: "bookingstatus", DefaultValue: this.state.bookingstatus});
        }
        if(this.state.maxValue && this.state.maxValue<this.state.minValue) {
            message.error("Date range is not valid!");
            return
        }
        if(this.state.maxValue && this.state.minValue) {
            item.push({Name: "bookingdaterange",minValue: moment(this.state.minValue).format('YYYY-MM-DD'),maxValue:moment(this.state.maxValue).format('YYYY-MM-DD')});
        }        
        if(this.state.bookingreferencenumber && !idExp.test(this.state.bookingreferencenumber)) {            
            message.error("Booking reference number is not valid!");
            return
        }else if(this.state.bookingreferencenumber) {
            item.push({Name: "bookingreferencenumber", DefaultValue: this.state.bookingreferencenumber});
        }
        if(this.state.itineraryreferencenumber && !idExp.test(this.state.itineraryreferencenumber)) {
            message.error("Itinerary reference number is not valid!");
            return
        }else if(this.state.itineraryreferencenumber){
            item.push({Name: "itineraryreferencenumber", DefaultValue: this.state.itineraryreferencenumber});            
        }
        if(this.state.transactiontoken && !idExp.test(this.state.transactiontoken)) {
            message.error("Transaction token is not valid!");
            return
        }else if(this.state.transactiontoken){
            item.push({Name: "transactiontoken", DefaultValue: this.state.transactiontoken});            
        }        
        const filtersIndex=[{item:[]}];
        filtersIndex[0].item=[...item]
        this.setState({filtersIndex:[...filtersIndex]});
        return filtersIndex;
    }

    render() {
        return (
            <LoadingOverlay
                active={this.state.loading}
                spinner={true}
                text='Loading your content...'
            >   
                {!this.state.loading &&
                <div className="container m-t-20">
                    <h1>My Bookings</h1>
                    <div className="my-booking-filters white-bg">
                        <div className="my-booking-filters__heading d-flex align-items-center justify-content-between">
                            <h3> <i className="fa fa-filter"></i> Filters </h3>
                            <h4><a onClick={()=>{this.setState({show:!this.state.show})}}>{!this.state.show?"Show":"Hide"} more filters</a></h4>
                        </div>
                        <div className="row my-booking-filters__options">
                        {(this.state.actionParam!=="cancel" && this.state.actionParam!=="modify") && 
                            <React.Fragment>
                            <div className="col-lg-2 col-md-3">
                                <div>Type</div>
                                <select name="filterType" value={this.state.filterType} className="form-control" 
                                onChange={async(e)=>{await this.setState({filterType:e.target.value})}}>
                                    <option value="all">All</option>
                                    <option value="upcoming">Upcoming</option>
                                    <option value="completed">Completed</option>
                                    <option value="cancelled">Cancelled</option>
                                    <option value="other">Other</option>
                                </select>
                            </div> 
                            <div className="col-lg-2 col-md-3">
                                <div>Status</div>
                                <select name="bookingstatus" value={this.state.bookingstatus} className="form-control" onChange={async(e)=>{await this.setState({bookingstatus:e.target.value})}}>                                    
                                    <option value="">All</option>
                                    <option value="Booked">Booked</option>
                                    <option value="Confirmed">Confirmed</option>
                                    <option value="Cancel">Cancel</option>
                                    <option value="CancelRequest">Cancel Request</option>
                                    <option value="ModifyRequest">Amend Request</option>
                                    <option value="ProcessedRequest">Request In Process</option>
                                    <option value="Expired">Expired Request</option>
                                    <option value="Denied">Denied Request</option>
                                    <option value="Modify">Modified Successfully</option>
                                    <option value="AutoCancel">Auto Cancel</option>
                                    <option value="AutoCancelFail">Auto Cancel Failure</option>
                                    <option value="SystemVoid">System Void</option>
                                </select>
                            </div>
                            </React.Fragment>
                        }
                            <div className="col-lg-3 col-md-4">                               
                                <div>Date Range</div>
                                <RangePicker
                                    format= "YYYY-MM-DD"                                   
                                    value={[moment(this.state.minValue), moment(this.state.maxValue)]}
                                    onChange={(date, dateString)=>{                                       
                                        this.setState({minValue: dateString[0],maxValue: dateString[1]})
                                    }}
                                />
                            </div>                            
                            <div className="col-lg-2 col-md-3" className={!this.state.show?"d-none":'col-lg-2 col-md-3'}>
                                <div>Transaction Token</div>
                                <input type="text" maxLength="15" className="form-control" name="transactiontoken" value={this.state.transactiontoken} onChange={(e)=>{this.setState({transactiontoken:e.target.value})}}/>
                            </div>
                            <div className="col-lg-2 col-md-3" className={!this.state.show?"d-none":'col-lg-2 col-md-3'}>
                                <div>Booking Ref. Number</div>
                                <input type="text" maxLength="15" className="form-control" name="bookingreferencenumber" value={this.state.bookingreferencenumber} onChange={(e)=>{this.setState({bookingreferencenumber:e.target.value})}}/>
                            </div>
                            <div className="col-lg-2 col-md-3" className={!this.state.show?"d-none":'col-lg-2 col-md-3'}>
                                <div>Iternary Ref. Number</div>
                                <input type="text" maxLength="15" className="form-control" name="itineraryreferencenumber" value={this.state.itineraryreferencenumber} onChange={(e)=>{this.setState({itineraryreferencenumber:e.target.value})}}/>
                            </div>                            
                            <div className="col-lg-2 col-md-3">
                                <div>&nbsp;</div>
                                <button className="btn btn-primary wdth-100" onClick={()=>this.filterData()}>Apply Filter</button>
                            </div>
                        </div>
                    </div>
                    {this.showList()}                    
                    {this.state.myBookings.length>0 && !this.state.loading &&                   
                    <Pagination current={this.state.current} onShowSizeChange={this.sizeChange} onChange={this.onChange} total={this.state.total} pageSize={this.state.pageLength} />
                    }
                    {
                        this.state.myBookings.length==0 && <Alert message={"No record(s) found matching your criteria."} type="info" />
                    }
                </div>
                }
            </LoadingOverlay>
        );
    }
}

export default MyBookingList;