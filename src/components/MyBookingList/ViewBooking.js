import React, { Component } from "react";
import moment from 'moment';
import { searchObj, minHHMM, capitalize } from "../../utils/commonFunc";
import { viewCart, myBookingDetails,showInvoiceOrVoucher,invoiceVoucherNotification,cancelOrModifyBooking } from "../../services/flightBooking";
import LoadingOverlay from 'react-loading-overlay';
import * as queryString from "query-string";
import { message, Button } from 'antd';
import { messageDuration } from "../../services/constants";
import "../BookingStatus/BookingStatus.css";
import history from '../../utils/history';
import { Modal } from 'react-bootstrap';
import {getUser} from "../../services/user";
import { Formik, Field, Form, ErrorMessage } from "formik";
import * as Yup from "yup";
import PhoneInput from 'react-phone-input-2';
import parse from 'html-react-parser';
import { connect } from "react-redux";
import * as actions from "../../redux/index";
import {ga4Pageview} from '../../services/Analytics/ga4';
import {pageLoadTimeWithPageview} from '../../services/Analytics/universalProp';

class ViewBooking extends Component {
    state = {        
        loading: false,
        travelType: { ADT: "Adult", CHD: "Child", INF: "Infant" },
        mybookingDetail:null,
        fareBreakup:null,
        showModal:false,
        loadingButton:false,
        invoiceHtml:null,
        cancelModufyStatus:false,
        pageTitle:"View Booking Details"
    }

    emailRegExp=/^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/

    validationShape = Yup.object().shape({
        email: Yup.string()
            .email("Email is invalid.")
            .required("Email is required."),       
        phone: Yup.string()
            .matches(this.phoneRegExp, 'Phone number is not valid.Mobile Number must be between 8 and 14 characters long.')
            .required("Phone Number is required."),
        comment: Yup.string()            
            .required("Comment is required."),
        termCondition: Yup.bool()
            .oneOf([true], "Please accept the terms and conditions.")
    })

    setInitialValues=()=>{
        return {
            email: this.state.email?this.state.email:"",
            //phoneNumberCountryCode:this.state.phoneNumberCountryCode?this.state.phoneNumberCountryCode:"",
            phone: this.state.phone?this.state.phoneNumberCountryCode+this.state.phone:"",
            comment:"",
            termCondition:false,
            ccEmail:""
        }
    }
    componentDidMount = async () => {
        //document.body.classList.add('hide-header-menu');
        this.setState({ loading: true });
        const bookingRef=this.props.match.params.bookingRef;
        const iternaryNo=this.props.match.params.iternaryNo;
        const actionParam=this.props.match.params.action; console.log(actionParam);
        const type=this.props.match.params.type;
        if(actionParam ==="invoice" || actionParam ==="voucher"){
            const bookingID=this.props.match.params.bookingRef;
            const iternaryID=this.props.match.params.iternaryNo;
            this.setState({bookingID:bookingID,iternaryID:iternaryID});
            const dataObj={ itineraryID: iternaryID,bookingID:bookingID,isvoucher:false,type:type};
            dataObj.isvoucher=actionParam ==="voucher"?true:false;
            const res = await showInvoiceOrVoucher(dataObj);
            if (res.status) {
                let html=res.data;
                if(html) {
                    html=html.replace(`<html>`, ``)                    
                    html=html.replace(`</html>`, ``)
                    html=html.replace(`<head>`, ``)
                    html=html.replace(`</head>`, ``)
                    html=html.replace(`<body>`, ``)
                    html=html.replace(`</body>`, ``)
                    // remove newline / carriage return
                    html=html.replace(/\n/g, "");    
                    // remove whitespace (space and tabs) before tags
                    html=html.replace(/[\t ]+\</g, "<");                        
                    // remove whitespace between tags
                    html=html.replace(/\>[\t ]+\</g, "><");                        
                    // remove whitespace after tags
                    html=html.replace(/\>[\t ]+$/g, ">");
                }
                this.setState({invoiceHtml:html,itineraryID: iternaryID,bookingID:bookingID,type:type,actionParam:actionParam});
            } else {
                this.setState({ loading: false })
                message.error(res.message);
            }
        } else {
            if (bookingRef && iternaryNo) {            
                const res = await myBookingDetails({ bookingReferenceNo: bookingRef,itineraryRefNo:iternaryNo});                
                if (res.status) {
                    const userRes = await getUser(); 
                    let email="";let phoneNumber=""; let phoneNumberCountryCode="";let userName="";
                    let portalPhone,customerCareEmail="";
                    let setting = {...this.props.appSetting};//localStorage.getItem("setting");
                    if (setting) {
                        //setting = typeof setting == "string" ? JSON.parse(setting) : null;
                        if (setting && setting.portalPhone) {
                            portalPhone = setting.portalPhone;
                            customerCareEmail=setting.customerCareEmail;
                        }
                    }
                    if (userRes && userRes.status) {
                        const user = userRes.data;
                        email=user.contactInformation.email;
                        phoneNumber=user.contactInformation.phoneNumber;
                        phoneNumberCountryCode=user.contactInformation.phoneNumberCountryCode;
                        userName=user.firstName;
                    } else {
                        message.error(userRes.message);
                    }                       
                    await this.setState({type:type,userName:userName,
                        phone:phoneNumber,email:email, 
                        phoneNumberCountryCode:phoneNumberCountryCode,
                        bookingRef:bookingRef,iternaryNo:iternaryNo, 
                        mybookingDetail:{...res.data},fareBreakup:[...res.fareBreakup],
                        loading: false,actionParam:actionParam,
                        portalPhone:portalPhone,customerCareEmail:customerCareEmail })                    
                } else {
                    message.error(res.message);
                    
                }
            }
        }
        pageLoadTimeWithPageview({location:window.location.pathname,loadTime:Math.round(performance.now())});
        ga4Pageview({location:window.location.pathname,title:this.state.pageTitle});
        this.setState({ loading: false })         
    }
    
    travellerList = () => {
        return this.state.mybookingDetail.travellerDetails.map((traveller, index) => {
            let eticket="";
            if(this.state.mybookingDetail) {
                eticket=this.state.mybookingDetail.travellerDetails[index].bookingDetails.eTicketNo;
            }
            return <tr key={"travellers" + index}>
                <td>{this.state.travelType[traveller.typeString]}</td>
                <td>{traveller.details.firstName}</td>
                <td>{traveller.details.genderDesc}</td>
                <td>{moment(traveller.details.birthDate).format('DD/MM/YYYY')}</td>                
                {traveller.documents.length>0 ? <td>{traveller.documents[0].type + ":" + traveller.documents[0].id}</td> : <td></td>}                
                <td>{eticket}</td>
            </tr>
        })
    }

    fareBreakup = () => {        
        if(this.state.fareBreakup && this.state.fareBreakup.length>0) {            
            return this.state.fareBreakup.map((fareObj, index) => {                
                return fareObj.item[0].displayRateInfo.map((fare,index)=>{                   
                    return <div className="row" key={"fare" + index}>
                            <div className="col-md-3 col text-right">{fare.description} </div>
                            <div className="col-md-8 col"><b>{fare.displayAmount}</b></div>
                            </div>
                })                
            })
        }
    }   

    flightStops = (items) => {
        return items.map((stop, index) => { console.log(stop,stop.vendors[0]);
            const time = minHHMM(stop.journeyDuration);
            return <div key={"stop" + index}>
                <h4>{stop.locationInfo.fromLocation.city} -
                {stop.locationInfo.fromLocation.name} ({stop.locationInfo.fromLocation.id})
                To {stop.locationInfo.toLocation.city} - {stop.locationInfo.toLocation.name}
                    ({stop.locationInfo.toLocation.id})</h4>
                <div className="white-bg">
                    <div className="row">
                        <div className="col-md-3 col text-right">Operated by: </div>
                        <div className="col-md-8 col"><b>{stop.vendors[0].item.name}</b></div>
                    </div>
                    <div className="row">
                        <div className="col-md-3 col text-right">Flight: </div>
                        <div className="col-md-8 col"><b>{stop.code}</b></div>
                    </div>
                    <div className="row">
                        <div className="col-md-3 col text-right">Class: </div>
                        <div className="col-md-8 col"><b>{searchObj(stop.tpExtension, "key", "cabinClass").value}</b></div>
                    </div>
                    <div className="row">
                        <div className="col-md-3 col text-right">Depart: </div>
                        <div className="col-md-8 col"><b>{moment(stop.dateInfo.startDate).format('DD/MM/YYYY hh:mm A')}</b></div>
                    </div>
                    <div className="row">
                        <div className="col-md-3 col text-right">Arrive: </div>
                        <div className="col-md-8 col"><b>{moment(stop.dateInfo.endDate).format('DD/MM/YYYY hh:mm A')}</b></div>
                    </div>
                    <div className="row">
                        <div className="col-md-3 col text-right">Travel Time: </div>
                        <div className="col-md-8 col"><b>{`${time.hrs} Hrs ${time.min} Min`}</b></div>
                    </div>
                </div>
            </div>
        });
    }

    action=()=>{
        if(this.state.actionParam==="cancel") {
            return <div className="">
                <button className="btn btn-default text-primary" onClick={()=>{
                this.setState({showModal:true})
            }}>Cancel</button> <button className="btn btn-default text-primary" onClick={()=>{history.goBack()}}>Back</button></div>
        } else if(this.state.actionParam==="modify") {
            return <div className="row"><button className="btn btn-default text-primary" onClick={()=>{
                this.setState({showModal:true})
            }}>Modify</button> <button className="btn btn-default text-primary" onClick={()=>{history.goBack()}}>Back</button></div>
        } else {
            return  <div className="">
                        <button className="btn btn-default text-primary" onClick={()=>{history.goBack()}}>Back</button>
                    </div>
        }
    }

    handleClose=()=>{
        this.setState({showModal:false})
    }

    sendRequest=async ()=>{ 
        let err=false;
        let arr=[];
        if(!this.state.email || !this.state.email.match(this.emailRegExp)) {
            message.error("Email should be valid.");
            err=true;
            return;
        } 
        if(this.state.ccEmail && this.state.ccEmail.trim()) { console.log("###");
            const ccemail=this.state.ccEmail.trim();
            arr=ccemail.split(",");
            if(arr && arr.length>0) {
                arr.forEach((email)=>{
                    if(!email.match(this.emailRegExp)) {
                        message.error("Email not valid in field E-mail CC");
                        err=true;
                        return
                    }
                })
            }            
        }
        if(!err) { 
            const isvoucher=this.state.actionParam ==="voucher"?true:false;            
            const res=await invoiceVoucherNotification({itineraryID: this.state.iternaryID,bookingID:this.state.bookingID,isvoucher:isvoucher,type:this.state.type,email:this.state.email,ccEmail:arr.join(",")});
            if(res.status) {
                message.success("Email sent successfully");
            } else {
                message.error(res.message);
            }
        }
    }
    handleChange = (e, name) => {
        const value = e.target.value;        
        this.setState({ [name]: value });
    }
    render() {    
        let tripType=""    ;
        tripType=this.state.mybookingDetail && this.state.mybookingDetail.businessObject.items.length===1?"One Way":"";
        tripType=this.state.mybookingDetail && this.state.mybookingDetail.businessObject.items.length===2?"Round Trip":"";
        return (
            <LoadingOverlay
                active={this.state.loading}
                spinner={true}
                text='Loading your content...'
            >
                <Modal className="manage-booking-popup" show={this.state.showModal} onHide={this.handleClose} size="lg">
                    <Modal.Header closeButton>
                        <Modal.Title>{this.state.actionParam} Reservation</Modal.Title>
                    </Modal.Header>
                    <Modal.Body> 
                    {!this.state.cancelModufyStatus && (
                    <Formik
                        initialValues={!this.state.loading?this.setInitialValues():null}
                        //Validation schema
                        validationSchema={this.validationShape}
                        //Post data to API
                        onSubmit={async (data, actions) => { 
                            this.setState({loadingButton:true});                          
                            const phoneNumber = data.phone.slice(this.state.phoneNumberCountryCode.length);
                            let dialCode=this.state.phoneNumberCountryCode;
                            data.phoneNumber=phoneNumber;
                            data.dialCode=dialCode;
                            data.reqType=this.state.actionParam;                            
                            data.BookingRefNo=this.state.bookingRef;
                            data.ItineraryRefNo=this.state.iternaryNo;
                            data.type=this.state.type;
                            const res=await cancelOrModifyBooking(data);
                            if(res.status) {
                                this.setState({cancelModufyStatus:true});
                            } else {
                                message.error(res.text);
                            }
                            this.setState({loadingButton:false});
                        }}
                    >
                        {({values, errors,touched, handleChange,handleBlur,handleSubmit,isSubmitting,setFieldValue}) => (
                        <Form>                        
                        <div className="manage-booking-popup__heading">
                            <div className="">Booking Reference Number: </div>
                                <span className="booking-status__box--code bg-primary"> {this.state.bookingRef} </span>
                        </div>
                        <div className="manage-booking-popup__heading">
                            <div className=""> Itinerary Number: </div>
                            <span className="booking-status__box--code"> {this.state.iternaryNo} </span>
                        </div>
                        <div className="input-wrapper">
                            Email: <Field type="text" name="email" id="email" maxLength="100" className={ `form-control` + (errors.email && touched.email ? " is-invalid" : "")  }/>
                            <ErrorMessage name="email" component="div" className="invalid-feedback"/>
                        </div>
                        <div className="input-wrapper">
                         Mobile Number: 
                        <PhoneInput
                            name={"phone"}
                            placeholder={"Phone Number"}
                            enableSearch={true}
                            inputClass={`form-control` + (errors.phone ? " is-invalid" : "")}
                            country={this.state.phoneNumberCountryCode ? this.state.phoneNumberCountryCode : "ke"}
                            value={this.state.phone ?  this.state.phoneNumberCountryCode + this.state.phone :  "ke"+""}
                            onChange={(value, data, event, formattedValue) => {                                                               
                                this.setState({phoneNumberCountryCode:data.dialCode});                                
                                setFieldValue("phone", value);
                            }}
                        />                        
                        <ErrorMessage name="phone" maxLength="12" component="div" className="invalid-feedback show"/>                        
                        </div> 
                        <div className="input-wrapper">
                        Comments: 
                        <Field type="text" maxLength="200" name="comment" className={
                                                    `form-control` +
                                                    (errors.comment && touched.comment ? " is-invalid" : "")
                                                }/>
                        <ErrorMessage name="comment" component="div" className="invalid-feedback"/>                                              
                        </div> 
                        <div className="terms">
                        <Field type="checkbox" name="termCondition" className={                                                    
                                                    (errors.termCondition && touched.termCondition ? " is-invalid" : "")
                                                }/>I have READ and AGREED to all the and Privacy Policy Terms and Conditions
                        <ErrorMessage name="termCondition" component="div" className="invalid-feedback"/>
                        </div>                        
                        <div>
                            {/* <button type="submit" className="btn btn-success" >Send Request</button> */}
                            <Button htmlType="submit" loading={this.state.loadingButton} className="btn btn-success" 
                            >Send Request</Button>
                        </div>
                        </Form>
                        )}
                    </Formik>
                    )
                    }
                    {
                        this.state.cancelModufyStatus && !this.state.loadingButton && (
                            <div>
                                <p className="text-success first-letter-capital">
                                    <strong> {this.state.actionParam} request has been successfully sent. </strong>
                                </p>
                                <p className="text-success">
                                    <em> Thank you for your request. We will respond to it via email as soon as possible. </em>
                                </p>
                                <p className="text-success">
                                    You can try Three Ways to Find Answers
                                </p>
                                <p>
                                Search by My Bookings to find answers to your questions.Call Customer Support at {this.state.portalPhone} We are available 24 hours a day, 7 days a week. Send us an email at {this.state.customerCareEmail} with any non-urgent questions.
                                </p>
                                <p className="text-success">
                                We respond to inquires within 24 hours.
                                </p>
                            </div>
                        )
                    }
                    </Modal.Body>
                </Modal>
                <div className="container booking-status m-t-20">
                    {
                        this.state.mybookingDetail && (
                            <React.Fragment>
                                <h1>View Reservation</h1>                               
                                <div>
                                    <h3 className="text-primary"> Flight Reservation Details {this.state.mybookingDetail.businessDetails}</h3>
                                    <div className="d-flex justify-content-between">
                                        <h4>Reservation Details</h4>
                                        {this.action()}
                                    </div>
                                    <div className="white-bg">
                                        <div className="row">
                                            <div className="col-md-3 col text-right">Booking Date: </div>
                                            <div className="col-md-8 col"><b>{moment(this.state.mybookingDetail.bookingDate).format('DD/MM/YYYY hh:mm A')}</b></div>
                                        </div>
                                        <div className="row">
                                            <div className="col-md-3 col text-right">Booking reference Number: </div>
                                            <div className="col-md-8 col"><b>{this.state.mybookingDetail.bookingRefNo}</b></div>
                                        </div>
                                        <div className="row">
                                            <div className="col-md-3 col text-right">Status: </div>
                                            <div className="col-md-8 col"><b>{this.state.mybookingDetail.bookingStatus}</b></div>
                                        </div>
                                        <div className="row">
                                            <div className="col-md-3 col text-right">Itinerary Name: </div>
                                            <div className="col-md-8 col"><b>{this.state.mybookingDetail.itineraryName}</b></div>
                                        </div>
                                        <div className="row">
                                            <div className="col-md-3 col text-right">Itinerary Number: </div>
                                            <div className="col-md-8 col"><b>{this.state.mybookingDetail.itineraryRefNo}</b></div>
                                        </div>
                                        <div className="row">
                                            <div className="col-md-3 col text-right">Transaction Token: </div>
                                            <div className="col-md-8 col"><b>{this.state.mybookingDetail.transactionToken}</b></div>
                                        </div>
                                        <div className="row">
                                            <div className="col-md-3 col text-right">Booking Direction: </div>
                                            <div className="col-md-8 col"><b>{tripType}</b></div>                                          
                                        </div>
                                        <div className="row">
                                            <div className="col-md-3 col text-right">Note: </div>
                                            <div className="col-md-8 col"><b>
                                                <p> {this.state.mybookingDetail.travellerDetails.length} Ticket {tripType} </p>
                                                <p> All flight times are local to each city </p>
                                                <p> Seat assignmgnet at airport Check-in desk only </p>
                                                </b></div>
                                        </div>
                                        
                                        
                                    </div>
                                </div>


                                

                                {this.flightStops(this.state.mybookingDetail.businessObject.items[0].item)}
                                {this.state.mybookingDetail.businessObject.items.length > 1 ? this.flightStops(this.state.mybookingDetail.businessObject.items[1].item) : null}

                                <div>
                                    <h4>Traveler</h4>
                                    <div className="table-responsive booking-status__table white-bg">
                                        <table className="table table-bordered">
                                            <thead>
                                                <tr>
                                                    <th>Traveler Type</th>
                                                    <th>First Name</th>
                                                    <th>Gender</th>
                                                    <th>Birth Date</th>                                                    
                                                    <th>Document</th>
                                                    <th>E-Ticket</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                {this.travellerList()}
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                               

                                <div>
                                    <h4>Price Details</h4>
                                    <div className="white-bg">
                                        {this.fareBreakup()}
                                    </div>
                                </div>

                               
                            </React.Fragment> 
                            )                           
                    }
                    {
                        this.state.invoiceHtml && (
                            <React.Fragment>
                            <div className="table-responsive">
                            {parse(this.state.invoiceHtml)} 
                            </div>
                            <div className="req-voucher">
                            <div className="row req-voucher-wrapper m-t-15">
                                <div className="col-md-3">E-mail To :</div> 
                                <div className="col-md-8"><input className="form-control" type="text" name="email" onChange={(e)=>this.handleChange(e,"email")} maxLength={100}/></div>
                            </div>
                            <div className="row req-voucher-wrapper">
                                <div className="col-md-3">E-mail CC : </div> 
                                <div className="col-md-8"><input className="form-control" type="text" name="ccEmail" onChange={(e)=>this.handleChange(e,"ccEmail")} maxLength={500}/></div>
                            </div>
                            <div className="row m-b-15">
                                <div className="col-md-3"></div>
                                <div className="col-md-8">
                                    (Separate multiple addresses with commas)
                                </div>
                            </div>
                            <div className="row">
                                <div className="col-md-3"></div>
                                <div className="col-md-8">
                                    <button className="btn btn-success" onClick={()=>this.sendRequest()}>Send Email</button>
                                </div>
                            </div>
                            </div>
                            </React.Fragment>
                        )
                    }
                </div>
            </LoadingOverlay>
        )
    }
}

const mapStateToProps = (state) => {
    return {       
        appSetting: state.appSetting.setting
    };
};

export default connect(mapStateToProps, actions)(ViewBooking);
