import React, { Component } from "react";
import moment from 'moment';
import { searchObj, minHHMM } from "../../utils/commonFunc";
import { viewCart, myBookingDetails } from "../../services/flightBooking";
import LoadingOverlay from 'react-loading-overlay';
import { connect } from "react-redux";
import { compose } from "redux";
import * as actions from "../../redux/index";
import { message } from 'antd';
import { messageDuration } from "../../services/constants";
import { Helmet } from "react-helmet";
import {setTiming,pageLoadTimeWithPageview} from '../../services/Analytics/universalProp';
import {ga4Pageview} from '../../services/Analytics/ga4';

import "./BookingStatus.css";

class BookingStatus extends Component {
    state = {
        cartData: null,
        loading: false,
        travelType: { ADT: "Adult", CHD: "Child", INF: "Infant" },
        mybookings:null,
        pageTitle:"Booking Status"
    }
    componentDidMount = async () => {
        document.body.classList.add('hide-header-menu');
        this.setState({ loading: true });
        const cartId = this.props.cartLocal;//localStorage.getItem("cartLocal");
        //let res = await viewCart(this.props.cartId);
        if (cartId) {
            this.setState({ loading: true });
            const res = await viewCart(cartId);
            console.log(res);
            //this.setState({ cartData: { ...res.data } })
            const cartData={...res.data}
            let setting = this.props.appSettings?{...this.props.appSettings}:null;//localStorage.getItem("setting");           
            if (setting) {
                //setting = typeof setting == "string" ? JSON.parse(setting) : null;
                //await this.setState({ setting: { ...setting }, loading: false })                
                if (cartData && (cartData.items[0].bookingReferenceCode || cartData.itineraryReferenceNumber)) {            
                    const res = await myBookingDetails({ bookingReferenceNo: cartData.items[0].bookingReferenceCode,itineraryRefNo:cartData.itineraryReferenceNumber });
                    if (res.status) {                        
                        await this.setState({ mybookings:{...res.data},cartData: { ...cartData },setting: { ...setting }, loading: false, })
                    } else {
                        await this.setState({ cartData: { ...cartData },setting: { ...setting }, loading: false })
                    }
                }
            }
            pageLoadTimeWithPageview({location:window.location.pathname,loadTime:Math.round(performance.now())})
            ga4Pageview({location:window.location.pathname,title:this.state.pageTitle});
            this.setState({ loading: false });
            //localStorage.removeItem("cartLocal");
            this.props.setCart({cartLocal:''});
        } else {
            //rediect to home
            this.setState({ loading: false });
            message.error(`Cart details not available to show booking status`, messageDuration);
        }        
    }

    componentWillUnmount() {
        document.body.classList.remove('hide-header-menu');
    }

    componentDidUpdate = async () => {
        let setting = this.props.appSettings?{...this.props.appSettings}:null;//localStorage.getItem("setting");
        if (!this.state.setting && setting) {
            //setting = typeof setting == "string" ? JSON.parse(setting) : null;
            await this.setState({ setting: { ...setting } })
        }
    }

    travellerList = () => {
        return this.state.cartData.travellers[this.state.cartData.items[0].cartItemID].map((traveller, index) => {
            let eticket="";
            if(this.state.mybookings) {
                eticket=this.state.mybookings.travellerDetails[index].bookingDetails.eTicketNo;
            }
            return <tr key={"travellers" + index}>
                <td>{this.state.travelType[traveller.typeString]}</td>
                <td>{traveller.details.firstName}</td>
                <td>{traveller.details.genderDesc}</td>
                <td>{moment(traveller.details.birthDate).format('DD/MM/YYYY')}</td>
                <td>{traveller.documents[0].type + ":" + traveller.documents[0].id}</td>
                <td>{eticket}</td>
            </tr>
        })
    }

    fareBreakup = () => {
        return this.state.cartData.items[0].fareBreakup.map((fare, index) => {
            return <div className="row" key={"fare" + index}>
                <div className="col-md-3 col text-right">{fare.item[0].displayRateInfo[0].description} </div>
                <div className="col-md-8 col"><b>{fare.item[0].displayRateInfo[0].displayAmount}</b></div>
            </div>
        })
    }

    passangerList = () => {
        return this.state.cartData.travellers[this.state.cartData.items[0].cartItemID].map((traveller, index) => {
            return <React.Fragment key={"traveller" + index}>
                <div className="row">
                    <div className="col-md-3 col text-right">Passanger{index + 1}: </div>
                    <div className="col-md-8 col"><b>{traveller.details.firstName + " " + traveller.details.lastName}</b></div>
                </div>
                <div className="row">
                    <div className="col-md-3 col text-right">Passanger type: </div>
                    <div className="col-md-8 col"><b>{traveller.typeString}</b></div>
                </div>
            </React.Fragment>
        })
    }

    flightStops = (items) => {
        return items.map((stop, index) => {
            const time = minHHMM(stop.journeyDuration);
            const startDate=moment(stop.dateInfo.startDate).format('DD/MM/YYYY hh:mm A');
            const endDate=moment(stop.dateInfo.endDate).format('DD/MM/YYYY hh:mm A');
            return <div key={"stop" + index}>
                <h4>{stop.locationInfo.fromLocation.city} -
                {stop.locationInfo.fromLocation.name} ({stop.locationInfo.fromLocation.id})
                To {stop.locationInfo.toLocation.city} - {stop.locationInfo.toLocation.name}
                    ({stop.locationInfo.toLocation.id})</h4>
                <div className="white-bg">
                    <div className="row">
                        <div className="col-md-3 col text-right">Operated by: </div>
                        <div className="col-md-8 col"><b>{stop.vendors[0].item.name}</b></div>
                    </div>
                    <div className="row">
                        <div className="col-md-3 col text-right">Flight: </div>
                        <div className="col-md-8 col"><b>{stop.code}</b></div>
                    </div>
                    <div className="row">
                        <div className="col-md-3 col text-right">Class: </div>
                        <div className="col-md-8 col"><b>{searchObj(stop.tpExtension, "key", "cabinClass").value}</b></div>
                    </div>
                    <div className="row">
                        <div className="col-md-3 col text-right">Depart: </div>
                        <div className="col-md-8 col"><b>{startDate}</b></div>
                    </div>
                    <div className="row">
                        <div className="col-md-3 col text-right">Arrive: </div>
                        <div className="col-md-8 col"><b>{endDate}</b></div>
                    </div>
                    <div className="row">
                        <div className="col-md-3 col text-right">Travel Time: </div>
                        <div className="col-md-8 col"><b>{`${time.hrs} Hrs ${time.min} Min`}</b></div>
                    </div>
                </div>
            </div>
        });
    }

    contactDetails = () => {
        if (this.state.setting) {
            return (
                <React.Fragment>
                    <p className="text-success"> You can Contact us, using below details, in case you require assitance. </p>
                    <h3><a className="text-primary" href={this.state.setting.portalUrl}>{this.state.setting.portalName}</a></h3>
                    <div className="white-bg">
                        <p>{this.state.setting.portalAddress}</p>
                        <p>Call: {this.state.setting.portalPhone}</p>
                        <p>email: <a herf="#">{this.state.setting.customerCareEmail}</a></p>
                    </div>
                </React.Fragment>
            )
        }
    }

    render() {        
        return (
            <React.Fragment>               
            <Helmet>
                <title>{this.state.pageTitle}</title>
                <meta name="description" content='Booking Status' />
            </Helmet> 
            <LoadingOverlay
                active={this.state.loading || this.props.loading || !this.state.setting}
                spinner={true}
                text='Loading your content...'
            >
                <div className="container booking-status m-t-20">
                    {
                        this.state.cartData && this.state.cartData.items.length > 0 && this.state.cartData.items[0].bookingReferenceCode ?
                            <React.Fragment>
                                <h1>Book Status</h1>
                                <div className="row booking-status__flight">
                                    <div className="col-md-7 booking-status__left">

                                        <React.Fragment>
                                            <div className="h2"><img src={process.env.PUBLIC_URL +"images/icon_flight-white.png"} title="Flight" alt="Flight" /> Your booking has been successfully completed</div>
                                            <div className="booking-status__box">
                                                <p>Thank you for booking your travel with us. You will receive your confirmation by email in the next few minutes. If you do not receive any confirmation email in the following minutes, please contact our Customer Support representatives.</p>
                                                <p>Iternary Number: <span className="booking-status__box--code"> {this.state.cartData.itineraryReferenceNumber} </span></p>
                                                <p>Please use above Itinerary Number for further communication related to this booking</p>
                                            </div>
                                        </React.Fragment>
                                    </div>

                                    <div className="col-md-5 booking-status__right">
                                        <div className="h2"><img src={process.env.PUBLIC_URL +"images/icon_flight-white.png"} title="Flight" alt="Flight" /> Flight Details</div>
                                        <div className="booking-status__box">
                                            <div className="booking_flight_details">
                                                <div className="row">
                                                    <div className="col text-right"><strong>Service</strong></div>
                                                    <div className="col">{this.state.cartData.items[0].data.business === "air" ? "Flight" : this.state.cartData.items[0].data.business}</div>
                                                </div>
                                                <div className="row">
                                                    <div className="col text-right"><strong>Name</strong></div>
                                                    <div className="col">{this.state.cartData.items[0].data.locationInfo.fromLocation.city + " - " + this.state.cartData.items[0].data.locationInfo.toLocation.city}</div>
                                                </div>
                                                <div className="row">
                                                    <div className="col text-right"><strong>Booking reference number</strong></div>
                                                    <div className="col">{this.state.cartData.items[0].bookingReferenceCode}</div>
                                                </div>
                                                <div className="row">
                                                    <div className="col text-right"><strong>Status</strong></div>
                                                    <div className="col">{this.state.cartData.items[0].bookingStatus === 1 ? "Confirmed" : this.state.cartData.items[0].bookingStatus}</div>
                                                </div>
                                                <div className="row">
                                                    <div className="col text-right"><strong>Total amount</strong></div>
                                                    <div className="col">{searchObj(this.state.cartData.items[0].fareBreakup, "code", "10").item[0].displayTotalAmount}</div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div>
                                    <h3 className="text-primary"> Flight Reservation Details </h3>
                                    <h4>Reservation Details</h4>
                                    <div className="white-bg">
                                        <div className="row">
                                            <div className="col-md-3 col text-right">Booking reference Number: </div>
                                            <div className="col-md-8 col"><b>{this.state.cartData.items[0].bookingReferenceCode}</b></div>
                                        </div>
                                        <div className="row">
                                            <div className="col-md-3 col text-right">Status: </div>
                                            <div className="col-md-8 col"><b>{this.state.cartData.items[0].bookingStatus}</b></div>
                                        </div>
                                        <div className="row">
                                            <div className="col-md-3 col text-right">Booking Date: </div>
                                            <div className="col-md-8 col"><b>{moment(this.state.cartData.items[0].createdTime).format('DD/MM/YYYY hh:mm A')}</b></div>
                                        </div>
                                        <div className="row">
                                            <div className="col-md-3 col text-right">Itinerary Number: </div>
                                            <div className="col-md-8 col"><b>{this.state.cartData.itineraryReferenceNumber}</b></div>
                                        </div>
                                        <div className="row">
                                            <div className="col-md-3 col text-right">Itinerary Name: </div>
                                            <div className="col-md-8 col"><b>ss</b></div>
                                        </div>
                                        <div className="row">
                                            <div className="col-md-3 col"></div>
                                            <div className="col-md-8 col"><a href="#">Booking terms</a> <a href="#">Terms &amp; Conditions</a> </div>
                                        </div>
                                    </div>
                                </div>

                                <div>
                                    <h4>Passanger Details</h4>
                                    <div className="white-bg">
                                        {this.passangerList()}
                                        <div className="row">
                                            <div className="col-md-3 col text-right">Booking reference number: </div>
                                            <div className="col-md-8 col"><b>{this.state.cartData.items[0].bookingReferenceCode}</b></div>
                                        </div>
                                    </div>
                                </div>

                                <div>
                                    <h4>Flights</h4>
                                    <div className="white-bg">
                                        <p> {this.state.cartData.travellers[this.state.cartData.items[0].cartItemID].length} Ticket {this.state.cartData.items[0].data.tripType} </p>
                                        <p> All flight times are local to each city </p>
                                        <p> Seat assignmgnet at airport Check-in desk only </p>
                                    </div>
                                </div>

                                {this.flightStops(this.state.cartData.items[0].data.items[0].item)}
                                {this.state.cartData.items[0].data.items.length > 1 ? this.flightStops(this.state.cartData.items[0].data.items[1].item) : null}

                                <div>
                                    <h4>Traveler</h4>
                                    <div className="table-responsive booking-status__table white-bg">
                                        <table className="table table-bordered">
                                            <thead>
                                                <tr>
                                                    <th>Traveler Type</th>
                                                    <th>First Name</th>
                                                    <th>Gender</th>
                                                    <th>Birth Date</th>                                                    
                                                    <th>Document</th>
                                                    <th>E-Ticket</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                {this.travellerList()}
                                            </tbody>
                                        </table>
                                    </div>
                                </div>

                                <div>
                                    <h4>Booked by</h4>
                                    <div className="white-bg">
                                        <div className="row">
                                            <div className="col-md-3 col text-right">Portal: </div>
                                            <div className="col-md-8 col"><b>Luxury brand</b></div>
                                        </div>
                                        <div className="row">
                                            <div className="col-md-3 col text-right">Name: </div> {/*agent name*/}
                                            <div className="col-md-8 col"><b>{this.state.cartData.travellers.contactDetails[0].details.firstName}</b></div>
                                        </div>
                                        <div className="row">
                                            <div className="col-md-3 col text-right">Location: </div> {/*branch name*/}
                                            <div className="col-md-8 col"><b>{this.state.cartData.travellers.contactDetails[0].details.location.city}</b></div>
                                        </div>
                                    </div>
                                </div>

                                <div>
                                    <h4>Price Details</h4>
                                    <div className="white-bg">
                                        {this.fareBreakup()}
                                    </div>
                                </div>

                                <div>
                                    {this.contactDetails()}
                                </div>
                            </React.Fragment>
                            : this.state.cartData && this.state.setting && (
                                <div className="">
                                <div className="">
                                    <React.Fragment>
                                        <h1>Booking Failed</h1>
                                        <p>Tansaction Token: <span className="booking-status__box--code"> {this.state.cartData.paymentTransaction.paymentToken} </span></p>
                                        {this.state.cartData.paymentTransaction.paymentStatusID === "98" && this.state.cartData.items[0].bookingStatus !== 1 && <p>Prepay Amount: <span className="rs">{this.state.setting.portalCurrency.symbol} <span>{this.state.cartData.paymentTransaction.paymentAmount}</span> </span></p>}
                                        <p>Please use above Itinerary Number for further communication related to this booking</p>
                                    </React.Fragment>
                                </div>
                                <div>
                                    {this.contactDetails()}
                                </div>
                                </div>)
                    }
                </div>
            </LoadingOverlay>
        </React.Fragment>
        )
    }
}
const mapStateToProps = (state) => {
    return {
        loading: state.flights.settingLoad,
        cartLocal:state.user.cartLocal,
        appSettings: Object.keys(state.appSetting.setting).length>0?{...state.appSetting.setting}:null
    };
};

export default compose(connect(mapStateToProps, actions))(BookingStatus);

