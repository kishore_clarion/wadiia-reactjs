import {
  SETTING_REQUEST,SETTING_REQUEST_SUCCESS
} from "./appSetupTypes";
import { act } from "react-dom/test-utils";

const initialState = {
  loading: false ,
  setting:{} 
};

export default (state = initialState, action) => {
  switch (action.type) {
    case SETTING_REQUEST:
        return {
          ...state, 
          loading: true 
        };
        break;
    case SETTING_REQUEST_SUCCESS:
        return {
          ...state, 
          setting:{...action.payload},
          loading:false   
        }
        break;
    default:
      return state;
  }
};
