
import {
  SETTING_REQUEST, SETTING_REQUEST_SUCCESS
} from "./appSetupTypes";
import {api} from "../../services/Api";
import {errorSet} from "../error/errorActions"



export const appSettingSuccess = (obj) => {
  return {
    type: SETTING_REQUEST_SUCCESS,
    payload:obj
  };
};

export const appSettingRequest = () => {
  return {
    type: SETTING_REQUEST
  };
};


export const appSetting = () => {
  return async dispatch => {  
    dispatch(appSettingRequest());  
    try {
      const req={"request": {}};      
      const response = await api.post("/application/environment",req);
      console.log(response); 
      if(response && response.data.status.code===0)         
        dispatch(appSettingSuccess(response.data.response)); 
      else {
        dispatch(appSettingSuccess({})); 
      }    
    } catch (error) {
      dispatch(errorSet(error.message))
      console.log("Redux error - ", error);      
    }
  };
};

