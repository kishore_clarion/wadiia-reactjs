import {
  USERS_SEARCH_REQUEST,
  SORT_REQUEST,
  SORT_REQUEST_SUCCESS,
  FILTER_REQUEST_SUCCESS,
  FILTER_REQUEST,
  PAGINATION_REQUEST_SUCCESS,
  PAGINATION_REQUEST,
  SETTING_REQUEST_SUCCESS,
  SETTING_REQUEST,  
  FLEXI_DATE_SEARCH_REQUEST
} from "./flightTypes";
import moment from 'moment';

const initialState = {
  loading: false,  
  token: "",
  response:null,
  fromLocation:null,
  toLocation:null,
  tripType:"Oneway",
  fromDate:moment().format("YYYY-MM-DD"),
  toDate:null,
  passanger:{
    adult:1,
    child:0,
    infant:0
  },
  serviceClass:null,
  reqType:null,
  //cartId:null,
  settingLoad:null,
  settings:null,
  message:null,
  flexiStartDate:null,
  flexiEndDate:null
};
export default (state = initialState, action) => {   
  switch (action.type) {
    case USERS_SEARCH_REQUEST:
      if(action.err) {
        if(action.payload)
          return { 
            ...state,           
            token:null,
            response:null,//response.data[0].item //action.payload.res.firstPage.response
            fromLocation:action.payload.fromLoc,
            toLocation:action.payload.toLoc,
            tripType:action.payload.tripType,
            fromDate:action.payload.startDate,
            toDate:action.payload.endDate,
            passanger:{
              adult:action.payload.adult,
              child:action.payload.child,
              infant:action.payload.infant
            },
            serviceClass:action.payload.serviceClass,
            reqType:null, 
            loading:false
          };
        else 
          return {
            ...state, 
            loading:false
          };
      }
      if(action.payload.type=="search") {
        const obj=createStateObj(action);
        return {
          ...state,
          ...obj
        };  
      }     
      if(action.payload.type=="filter") {
        const obj=createStateObj(action);
        return {
          ...state,
          ...obj
        };     
      }  
      if(action.payload.type=="pagination")
        return {
          ...state, 
          loading:false,
          reqType:"pagination",         
          response:action.payload.res
          
        }; 
           
        break;
    case SORT_REQUEST: 
        return {
          ...state,          
          loading: true,
          reqType:null    
        };
        break;
    case SORT_REQUEST_SUCCESS: 
        return {
          ...state,         
          response:action.payload,
          loading:false,
          reqType:null     
        };
        break;
    case FILTER_REQUEST: 
        return {
          ...state, 
          reqType:"filter",         
          loading:true
        }
        break;
    case FILTER_REQUEST_SUCCESS: 
      return {
        ...state,
        reqType:"filter",
        response:action.payload,
        loading:false
      }
      break;
    case PAGINATION_REQUEST: 
      return {
        ...state,                 
        loading:true
      }
      break;
    case PAGINATION_REQUEST_SUCCESS: 
      return {
        ...state,       
        response:action.payload,
        loading:false
      }
      break;    
    case SETTING_REQUEST_SUCCESS:
     if(action.data==="NA") {
      return {
        ...state,          
        settingLoad: false
      }
     } else if(action.data) {
       return {
        ...state,          
        settingLoad: false,
        settings:{...action.data}
      }
     } else {
       return {
        ...state,          
        settingLoad: false,
        settings:null
      }
     }
    case SETTING_REQUEST:
      return {
        ...state,          
        settingLoad: true
      };
      break;
    case FLEXI_DATE_SEARCH_REQUEST:
      return {
        ...state,
        flexiStartDate:action.payload.startDate,
        flexiEndDate:action.payload.endDate
      };
      break;
    default:
      return state;
  }
};

const createStateObj=(action)=>{
  return {
    loading: false,
    token:action.payload.token,
    response:action.payload.res.response,//response.data[0].item
    fromLocation:action.payload.fromLocation,
    toLocation:action.payload.toLocation,
    tripType:action.payload.tripType,
    fromDate:action.payload.fromDate,
    toDate:action.payload.toDate,
    passanger:{
      adult:action.payload.adult,
      child:action.payload.child,
      infant:action.payload.infant
    },
    serviceClass:action.payload.serviceClass,
    reqType:null
  }
}

//search
// loading: false,
// token:action.payload.token,
// response:action.payload.res.response,//response.data[0].item //action.payload.res.firstPage.response
// fromLocation:action.payload.fromLocation,
// toLocation:action.payload.toLocation,
// tripType:action.payload.tripType,
// fromDate:action.payload.fromDate,
// toDate:action.payload.toDate,
// passanger:{
//   adult:action.payload.adult,
//   child:action.payload.child,
//   infant:action.payload.infant
// },
// serviceClass:action.payload.serviceClass,
// reqType:null

//filter
//  loading: false,
// token:action.payload.token,
// response:action.payload.res.response,//response.data[0].item
// fromLocation:action.payload.fromLocation,
// toLocation:action.payload.toLocation,
// tripType:action.payload.tripType,
// fromDate:action.payload.fromDate,
// toDate:action.payload.toDate,
// passanger:{
//   adult:action.payload.adult,
//   child:action.payload.child,
//   infant:action.payload.infant
// },
// serviceClass:action.payload.serviceClass,
// reqType:null