
import {
  USERS_SEARCH_REQUEST, SORT_REQUEST, SORT_REQUEST_SUCCESS,FILTER_REQUEST_SUCCESS,FILTER_REQUEST,PAGINATION_REQUEST_SUCCESS,
  PAGINATION_REQUEST,SETTING_REQUEST_SUCCESS,SETTING_REQUEST,FLEXI_DATE_SEARCH_SUCCESS,FLEXI_DATE_SEARCH_REQUEST
} from "./flightTypes";
import {api} from "../../services/Api";
import {filterReqFormat,sortingIndexSeparate,pageInfoIndexSeparate,filterSeparate} from "../../services/requestFormat";
import {store} from "../store";
import {errorSet} from "../error/errorActions"

export const searchRequest = (obj,err=false) => {
  return {
    type: USERS_SEARCH_REQUEST,
    payload:obj,
    err:err
  };
};

export const sortSuccess = (obj) => {
  return {
    type: SORT_REQUEST_SUCCESS,
    payload:obj
  };
};

export const sortRequest = () => {
  return {
    type: SORT_REQUEST
  };
};

export const filterSuccess = (obj) => {
  return {
    type: FILTER_REQUEST_SUCCESS,
    payload:obj
  };
};

export const filterRequest = () => {
  return {
    type: FILTER_REQUEST
  };
};

export const paginationSuccess = (obj) => {
  return {
    type: PAGINATION_REQUEST_SUCCESS,
    payload:obj
  };
};

export const paginationRequest = () => {
  return {
    type: PAGINATION_REQUEST
  };
};
export const sorting = data => {
  const token =store.getState().flights.response.token;
  const filter=[...store.getState().flights.response.appliedFiltersIndex[0].item];//newly added
  return async dispatch => {  
    dispatch(sortRequest());     
    try {
      const reqFormat={...filterReqFormat};      
      reqFormat.request.token=token;
      reqFormat.request.filtersIndex[0].item=[...filter];//newly added
      if(data && data.type) {
        const sortIndex=[...sortingIndexSeparate];       
        const index=sortIndex.findIndex(obj=>{
          return obj.code==data.type
        })
        if(index>-1) {
          sortIndex[index].item={...data.sort};
        }
        reqFormat.request.pageInfoIndex=[...pageInfoIndexSeparate];     
        reqFormat.request.pageInfoIndex[0].item={"currentPage": 0,"pageLength": 10}
        reqFormat.request.pageInfoIndex[1].item={"currentPage": 0,"pageLength": 10}       
        reqFormat.request.sortIndex=[...sortIndex];
      } else {
        reqFormat.request.pageInfoIndex[0].item={"currentPage": 0,"pageLength": 10}        
        reqFormat.request.sortIndex[0].item={...data.sort};
      }      
      const response = await api.post("air/search/page",reqFormat );             
      dispatch(sortSuccess(response.data.response));     
    } catch (error) {
      dispatch(errorSet(error.message))     
    }
  };
};

export const filter = data => {
  const token =store.getState().flights.response.token;
  return async dispatch => {  
    dispatch(filterRequest());  
    try {
      const reqFormat={...filterReqFormat};
      reqFormat.request.pageInfoIndex[0].item={"currentPage": 0,"pageLength": 10} 
      reqFormat.request.token=token;
      if(data.type==="separate") {  
        reqFormat.request.filtersIndex=[...filterSeparate];  
        reqFormat.request.pageInfoIndex=[...pageInfoIndexSeparate];  
        reqFormat.request.filtersIndex[0].item=[...data.filters];
        reqFormat.request.filtersIndex[1].item=[...data.default];
        reqFormat.request.pageInfoIndex[0].item={"currentPage": 0,"pageLength": 10} 
        reqFormat.request.pageInfoIndex[1].item={"currentPage": 0,"pageLength": 10};
        reqFormat.request.sortIndex[0].item={name: "rate",order: 1};//new added
        reqFormat.request.sortIndex[1].item={name: "rate",order: 1};//new added
      } else {
        reqFormat.request.filtersIndex[0].item=[...data.filters];
        reqFormat.request.sortIndex[0].item={name: "rate",order: 1};//new added
      }
      const response = await api.post("air/search/page",reqFormat );
      dispatch(filterSuccess(response.data.response));     
    } catch (error) {
      dispatch(errorSet(error.message))    
    }
  };
};

export const pagination = data => { 
  const token =store.getState().flights.response.token;
  return async dispatch => {  
    dispatch(paginationRequest());  
    try {
      const reqFormat={...filterReqFormat};
      reqFormat.request.token=token;
      reqFormat.request.pageInfoIndex[0].item={"currentPage": data.page,"pageLength": 10} 
      //applied filter
      if(data.filter) {
        reqFormat.request.filtersIndex[0].item=[...data.filters];
      } 
      const response = await api.post("air/search/page",reqFormat );                 
      dispatch(paginationSuccess(response.data.response));     
    } catch (error) {
      dispatch(errorSet(error.message))    
    }
  };
};

export const settingSuccess = (obj) => {
  return {
    type: SETTING_REQUEST_SUCCESS
  };
};

export const settingRequest = () => {  
  return {
    type: SETTING_REQUEST
  };
};

// export const storeCartId = (cartId) => {
//   return {
//     type: CART_REQUEST,
//     payload:{cartId:cartId}
//   };
// };

export const setSearchWithFlexiDate = (obj) => {
  let data={startDate:obj.startDate};
  if(obj.tripType==="Roundtrip") {
    data.endDate=obj.endDate;
  }
  return {
    type: FLEXI_DATE_SEARCH_REQUEST,
    payload:obj
  };
}