
import {
  LOGIN_REQUEST, LOGIN_REQUEST_SUCCESS,LOGOUT_REQUEST,LOGOUT_REQUEST_SUCCESS,SIGNUP_REQUEST,SIGNUP_REQUEST_SUCCESS,SET_CART
} from "./userTypes";
import {api} from "../../services/Api";
import history from "../../utils/history";
import {signupReqFormat} from "../../services/requestFormat";
import firebase from "../../firebaseConfig";
import {errorSet} from "../error/errorActions";


export const loginSuccess = (obj) => {
  return {
    type: LOGIN_REQUEST_SUCCESS,
    payload:obj
  };
};

export const loginRequest = () => {
  return {
    type: LOGIN_REQUEST
  };
};

export const logoutSuccess = (obj) => {
  return {
    type: LOGOUT_REQUEST_SUCCESS,
    payload:obj
  };
};

export const logoutRequest = () => {
  return {
    type: LOGOUT_REQUEST
  };
};

export const signupSuccess = (obj) => {
  return {
    type: SIGNUP_REQUEST_SUCCESS,
    payload:obj
  };
};

export const signupRequest = () => {
  return {
    type: SIGNUP_REQUEST
  };
};

export const setCart=(obj)=>{
  return {
    type: SET_CART,
    payload: obj
  }
}
export const logout = (data) => {
  return async dispatch => {  
    dispatch(logoutRequest());  
    try {
      const req={"request": ""}           
      const response = await api.post("user/logout",req);       
      if(response.data.status.code===0) {  
        firebase.auth().signOut().then(function() {                       
          dispatch(logoutSuccess({status:true}));
        }).catch(function(error) {
          dispatch(errorSet(error.message))
          dispatch(logoutSuccess({status:false}));
        })     
      } else {
        dispatch(logoutSuccess({status:false,message:response.data.status.message})); 
      }    
    } catch (error) {
      dispatch(logoutSuccess({status:false,message:error.message}));  
    }
  };
};


export const openIdSignup = (dataObj) => {
  let data={...dataObj};  
  return async dispatch => {
    try {  
      dispatch(loginRequest());   
      let provider;
      if(dataObj.type=="google"){
        provider = new firebase.auth.GoogleAuthProvider();
      }
      if(dataObj.type=="facebook"){
        provider =new firebase.auth.FacebookAuthProvider();
      }        
      firebase.auth().signInWithPopup(provider).then(async function(result) {       
        const token = result.credential.accessToken;
        const user = result.user;
        if(user) {              
          const reqObj={request:{contactInformation:{},ProfilePicture:{}}}//{...signupReqFormat};
          reqObj.request.contactInformation.email=user.email;
          if(!user.email) {
            reqObj.request.contactInformation.email=result.additionalUserInfo.profile.email;
          }
          reqObj.request.contactInformation.name=user.displayName; 
          if(user.phoneNumber) {
            reqObj.request.contactInformation.phoneNumber=user.phoneNumber;
          }
          reqObj.request.loginName=user.displayName.split(" ").join("");         
          reqObj.request.firstName=user.displayName.split(" ")[0];
          reqObj.request.lastName=user.displayName.split(" ")[1];          
          if(user.photoURL) {
            reqObj.request.ProfilePicture.URL=user.photoURL;             
          }          
          if(dataObj.type=="google"){
            reqObj.request.OpenIDs={
              "google":result.additionalUserInfo.profile.id//user.providerData[0].uid //
            }
          }
          if(dataObj.type=="facebook"){
            reqObj.request.OpenIDs={
              "facebook":result.additionalUserInfo.profile.id//user.providerData[0].uid //
            }
          }         
          const response = await api.post("user/signup",reqObj);         
          if(response && response.data.status.code===0 && response.data.response) {             
            dispatch(loginSuccess({status:true,data:response.data.response,message:"",redirect:dataObj.redirect}));
          } else if(response && response.data.status.code===0 && !response.data.response) {
            const rq={"Request": "","Flags": {}}
              const userRes = await api.post("user/details",rq);
              if(userRes && userRes.data.status.code===0) {                
                dispatch(loginSuccess({status:true,data:userRes.data.response,message:"",redirect:dataObj.redirect}));
              }else {
                dispatch(loginSuccess({status:false,message:userRes.data.status.message,data:{},redirect:dataObj.redirect}));
              } 
          }else{
            //If signup to open id but not in travelcarma
            firebase.auth().signOut().then(function() {
              let message;
              message=response.data.status.message;             
              dispatch(loginSuccess({status:false,message:message,data:{},redirect:dataObj.redirect}));
            }).catch(function(error) {              
              dispatch(loginSuccess({status:false,message:error.message,data:{},redirect:dataObj.redirect}));
            })             
          } 
        }
      }).catch(function(error) {         
        dispatch(loginSuccess({status:false,message:error.message,data:{},redirect:dataObj.redirect}));
      });         
    } catch (error) {      
      dispatch(loginSuccess({status:false,message:error.message,data:{},redirect:dataObj.redirect}));    
    }
  };
};