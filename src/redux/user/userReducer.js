import {
  LOGIN_REQUEST,LOGIN_REQUEST_SUCCESS,LOGOUT_REQUEST,LOGOUT_REQUEST_SUCCESS,SIGNUP_REQUEST,SIGNUP_REQUEST_SUCCESS,SET_LOCATION,SET_RECENT_SEARCH,SET_CART
} from "./userTypes";
import { act } from "react-dom/test-utils";

const initialState = {
  loading: false ,  
  message:"",  
  status:"",
  localCountry:"",
  localLoc:"",  
  recentSearch:[],
  redirect:'',
  cartLocal:''
};

export default (state = initialState, action) => {  
  switch (action.type) {
    case LOGIN_REQUEST:
        return {
          ...state, 
          loading: true 
        };
        break;
    case LOGIN_REQUEST_SUCCESS:
    if(action.payload.status)
      return {
        ...state, 
        loading: false ,        
        message:action.payload.message,
        status:"in",
        redirect:action.payload.redirect?action.payload.redirect:''
      }
    else {
      return {
        ...state, 
        loading: false ,        
        message:action.payload.message,
        status:""
      }
      }
      break;      
    case LOGOUT_REQUEST:
          return {
            ...state, 
            loading: true 
          };
          break;
    case LOGOUT_REQUEST_SUCCESS: 
      if(action.payload.status)
          return {
            ...state,            
            loading:false,
            message:"",
            status:"out" 
          }
      else 
        return {
          ...state,        
          loading:false,
          message:action.payload.message  
        }
        break;
    case SIGNUP_REQUEST:
      return {
        ...state, 
        loading: true 
      };
      break;
    case SIGNUP_REQUEST_SUCCESS:      
      return {
        ...state,       
        loading:false , 
        status:"in",
        redirect:action.payload.redirect?action.payload.redirect:''
      }
      break;
    case SET_LOCATION:      
      return {
        ...state,     
        localCountry:action.payload.country,
        localLoc:action.payload.latLon
      }
      break;
    case SET_RECENT_SEARCH:      
      return {
        ...state,     
        recentSearch:[...action.payload]
      }
      break; 
    case SET_CART:
      return {
        ...state,
        cartLocal:action.payload.cartLocal
      }  
    default:
      return state;
  }
};
