export const LOGIN_REQUEST = "LOGIN_REQUEST";
export const LOGIN_REQUEST_SUCCESS= "LOGIN_REQUEST_SUCCESS";
export const LOGOUT_REQUEST = "LOGOUT_REQUEST";
export const LOGOUT_REQUEST_SUCCESS= "LOGOUT_REQUEST_SUCCESS";
export const SIGNUP_REQUEST = "SIGNUP_REQUEST";
export const SIGNUP_REQUEST_SUCCESS= "SIGNUP_REQUEST_SUCCESS";
export const SET_LOCATION = 'SET_LOCATION';
export const SET_RECENT_SEARCH= 'SET_RECENT_SEARCH';
export const SET_CART='SET_CART'