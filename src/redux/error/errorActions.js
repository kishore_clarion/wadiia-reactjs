
import {
  CLEAR_ERROR, SET_ERROR
} from "./errorTypes";
import {api} from "../../services/Api";



export const errorClose = () => {
  return {
    type: CLEAR_ERROR    
  };
};

export const errorOpen = (message) => {
  return {
    type: SET_ERROR,
    payload:message
  };
};


export const errorSet = (message) => {
  return async dispatch => {  
    dispatch(errorOpen(message));  
  };
};

export const errorClear = () => {
  return async dispatch => {  
    dispatch(errorClose());  
  };
};