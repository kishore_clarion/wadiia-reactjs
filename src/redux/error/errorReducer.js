import {
  SET_ERROR,CLEAR_ERROR
} from "./errorTypes";
import { act } from "react-dom/test-utils";

const initialState = {
  err:null
};
export default (state = initialState, action) => {
  switch (action.type) {
    case SET_ERROR:
        return {
          err:action.payload 
        };
        break;
    case CLEAR_ERROR: 
        return {
          err:null
        }
        break;
    default:
      return state;
  }
};
