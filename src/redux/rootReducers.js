import { combineReducers } from "redux";

import flightReducer from "./flight/flightReducer";
import appSettingReducer from "./appSetup/appSetupReducer";
import userReducer from "./user/userReducer";
import errorReducer from "./error/errorReducer";


import { persistReducer } from 'redux-persist';
import storage from 'redux-persist/lib/storage';

const persistConfig = {
  key: 'root',
  storage,
  whitelist: ['user','appSetting'] // which reducer want to store
};

const rootReducer = combineReducers({
  flights: flightReducer,
  user: userReducer,
  error: errorReducer,
  appSetting: appSettingReducer
});

//export default rootReducer;
export default persistReducer(persistConfig, rootReducer);