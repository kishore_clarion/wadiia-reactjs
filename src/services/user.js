import {api} from "./Api";
import {signupReqFormat} from "./requestFormat";
import {AppData} from "./constants";
import {readUploadedFileAsbase64} from "./../utils/commonFunc"
import {store} from "../redux/store";

export const login = async (data) => {
    try{
        const req={"request": {"contactInformation": {},"password": data.password } };
        if(data.email) {
            req.request.contactInformation.email=data.email;
        }
        if(data.phone.phoneNumber)  {
            req.request.contactInformation.phoneNumber=data.phone.phoneNumber;
            req.request.contactInformation.phoneNumberCountryCode=data.phone.phoneNumberCountryCode;
        }      
        const response = await api.post("user/login",req);    
        if(response.data.status.code===0) {        
            return ({status:true,data:response.data.response});     
        } else {
            return ({status:false,message:response.data.status.message}); 
        }
    } catch(err) {
        store.dispatch({type:"SET_ERROR",payload:err.message})
        return ({status:false,message:err.message});
    } 
}

export const signup = async (dataObj) => {
    const data = { ...dataObj };
    try {
        data.country = typeof data.country == "string" ? JSON.parse(data.country) : { ...data.country };
        const reqObj = { ...signupReqFormat };
        reqObj.request.contactInformation.email = data.email;
        reqObj.request.contactInformation.name = data.firstName + " " + data.lastName;
        if (data.contact && data.contact.PhoneNumber) {
            reqObj.request.contactInformation.phoneNumber = data.contact.PhoneNumber;
            reqObj.request.contactInformation.PhoneNumberCountryCode=data.contact.PhoneNumberCountryCode
        }
        reqObj.request.loginName = data.firstName + data.lastName;
        reqObj.request.firstName = data.firstName;
        reqObj.request.lastName = data.lastName;
        reqObj.request.password = data.password;
        reqObj.request.gender = data.gender;
        if (data.file) {
            reqObj.request.ProfilePicture.URL = data.file.name;
            const reader = new FileReader();
            let rawdata = await readUploadedFileAsbase64(data.file);
            rawdata = rawdata.replace(/^data:image\/[a-z]+;base64,/, "");
            reqObj.request.ProfilePicture.RawData = rawdata;
        }
        reqObj.request.location = {
            countryID: data.country.code,
            country: data.country.name
        }
        if(data.otp) {
            reqObj.SmsOtp=data.otp;
        }       
        const response = await api.post("user/signup", reqObj);               
        if (response && response.data.status.code === 0) {            
            return ({ status: true, data: response.data.response });
        } else {
            return ({ status: false, message: response.data.status.message });
        }
    } catch (error) {
        store.dispatch({type:"SET_ERROR",payload:error.message})
        return ({status:false,message:error.message});
    }
};


export const forgotPassword = async (data) => {
    try{
        const req={
            "request": {
                "contactInformation": {
                    "PhoneNumber": "",
                    "PhoneNumberCountryCode": "",
                    "Email": ""
                }
            }
        }
        if(data.email) {
            req.request.contactInformation.Email=data.email;
        }
        if(data.phone) { 
            req.request.contactInformation.PhoneNumber=data.phone; 
            req.request.contactInformation.PhoneNumberCountryCode=data.phoneCode;
        }     
        const response = await api.post("user/forgotpassword",req);    
        if(response.data.status.code===0) {         
            return ({status:true});     
        } else {
            return ({status:false,message:response.data.status.message}); 
        }
    } catch(err) {
        store.dispatch({type:"SET_ERROR",payload:err.message})
        return ({status:false,message:err.message});
    } 
}

export const getUser = async () => {
    try{
        const response = await api.post("user/details", { "Request": "", "Flags": {} });         
        if(response && response.data.status.code===0) {       
            return ({status:true,data:response.data.response});     
        } else {
            return ({status:false,message:response.data.status.message}); 
        }
    } catch(err) {
        store.dispatch({type:"SET_ERROR",payload:err.message})
        return ({status:false,message:err.message});
    }        
}


export const updateUser = async (userData) => {
    try{
        const response = await api.post("user/update", userData);         
        if(response && response.data.status.code===0) {       
            return ({status:true});     
        } else {
            return ({status:false,message:response.data.status.message}); 
        }
    } catch(err) {
        store.dispatch({type:"SET_ERROR",payload:err.message})
        return ({status:false,message:err.message});
    }        
}

export const generateOTP = async (data) => {
    try{
        let req={
            "Request": {
                "ContactInformation": {
                    "PhoneNumber": data.PhoneNumber,
                    "PhoneNumberCountryCode": data.PhoneNumberCountryCode
                }
            }
        }
        const response = await api.post("user/otp/generate", req);         
        if(response && response.data.status.code===0) {       
            return ({status:true});     
        } else {
            return ({status:false,message:response.data.status.message}); 
        }
    } catch(err) {
        store.dispatch({type:"SET_ERROR",payload:err.message})
        return ({status:false,message:err.message});
    } 
}