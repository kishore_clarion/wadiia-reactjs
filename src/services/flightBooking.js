import {createCartReqFormat,addTravellerToCartFormat,mybooking} from "./requestFormat";
import {api} from "./Api";
import {siteBaseUrl} from "./constants";
import { setContactDetails,setTravellerDetails,searchObj} from "../utils/commonFunc"
import {store} from "../redux/store";

const returnResponse=(res,onlySuccess)=>{
    if(res && res.data.status.code!==0) {
            return ({status:false,message:res.data.status.message,code:res.data.status.code})
    } else { 
            const success={status:true};
            if(!onlySuccess) {
                success.data=res.data.response;
            }
            return(success);
    }
}
const setError=(err)=>{
    store.dispatch({type:"SET_ERROR",payload:err.message})
    return ({status:false,message:err.message});
}
export const createCart=async function(searchToken,optionToken,arrivalToken=null) {
    try{  
        const req={...createCartReqFormat};
        req.Request.Token=searchToken;
        req.Request.Data[0].Key=optionToken;
        req.Request.Data[0].Value=optionToken;
        if(arrivalToken) {
            req.Request.Data.push({"Key": "","Value": ""});
            req.Request.Data[1].Key=arrivalToken;
            req.Request.Data[1].Value=arrivalToken;
        } 
        const res=await api.post('/cart/create', req);
        if(res && res.data.status.code!==0) {
            return ({status:false,message:res.data.status.message,code:res.data.status.code,cartId: res.data.response})
        } else {
            return({status:true,cartId: res.data.response})
        }
    } catch(err) {
        // store.dispatch({type:"SET_ERROR",payload:err.message})
        // return ({status:false,message:err.message});
        return setError(err);
    }
}

export const viewCart=async function(cartId) {
    try{    
        const req={"request": cartId};    
        const res=await api.post('/cart', req);    
        return returnResponse({...res});        
        //return resStatus;
        // if(res && res.data.status.code!==0) {
        //     return ({status:false,message:res.data.status.message,code:res.data.status.code})
        // } else {
        //     return({status:true,data: res.data.response})
        // }
    } catch(err) {
        // store.dispatch({type:"SET_ERROR",payload:err.message})
        // return ({status:false,message:err.message});
        return setError(err);
    }
}

export const clearCart=async(cartId)=>{
    try{    
        const req={"Request": {"CartID": cartId},"Flags": {}};    
        const res=await api.post('/cart/clear', req);    
        return returnResponse({...res},true);
        // if(res && res.data.status.code!==0) {
        //     return ({status:false,message:res.data.status.message,code:res.data.status.code})
        // } else {
        //     return({status:true})
        // }
    } catch(err) {
        // store.dispatch({type:"SET_ERROR",payload:err.message})
        // return ({status:false,message:err.message});
        return setError(err);
    }
}

export const addTravellerToCart=async(data)=>{
    try{
        const req={...addTravellerToCartFormat};
        const contactDetails=setContactDetails(data,req.ContactDetails);
        req.ContactDetails={...contactDetails}    
        const travellerFormat={...req.Request.Data[0].Item[0]};
        req.Request.Data[0].Item=[];
        if(data.travellerList.length>0) {
            data.travellerList.map((traveller)=>{
                const objTraveller=setTravellerDetails(traveller,travellerFormat);
                req.Request.Data[0].Item.push(objTraveller);
            });
        }
        req.Request.CartID=data.cartId;
        req.Request.Data[0].Code=req.Request.Data[0].Type=data.cartItemId;
        req.Request.Token=data.Token;
        const res=await api.post('/cart/travellers/add', req);  
        return returnResponse({...res},true);  
        // if(res && res.data.status.code!==0) {
        //     return ({status:false,message:res.data.status.message,code:res.data.status.code})
        // } else {
        //     return({status:true})
        // }
    } catch(err) {
        // store.dispatch({type:"SET_ERROR",payload:err.message})
        // return ({status:false,message:err.message});
        return setError(err);
    }
}

export const bookFlight=async(data)=>{
    try{
        const req={
            "PaymentGatewayID": data.paymentGatewayId,
            "PaymentReturnUrl": siteBaseUrl()+"booking-status",        
            "Request": data.cartId
            }
        const res=await api.post('/cart/book', req);
        if(res && res.data.status.code!==0) {
            return ({status:false,message:res.data.status.message,code:res.data.status.code,cartId:res.data.response})
        } else {            
            return({status:true,token:res.data.response.data})
        }
    } catch(err) {
        // store.dispatch({type:"SET_ERROR",payload:err.message})
        // return ({status:false,message:err.message});
        return setError(err);
    }
}

export const removeCartItem=async(cartItem,cartId,searchToken)=>{
    try{    
        const req={
            "Request": {
                "CartID": cartId,
                "Token": searchToken,
                "Data": {
                    "Key":cartItem,
                    "Value": cartItem
                }
            }
        }
        const res=await api.post('/cart/remove', req);   
        //return returnResponse({...res}); 
        if(res && res.data.status.code!==0) {
            return ({status:false,message:res.data.status.message,code:res.data.status.code})
        } else {        
            return({status:true,token:res.data.response.data})
        }
    } catch(err) {
        // store.dispatch({type:"SET_ERROR",payload:err.message})
        // return ({status:false,message:err.message});
        return setError(err);
    }
}

export const applyCoupon=async(data)=>{
    try{
        const req={
            "request": {
            "cartID": data.cartId,
            "data": [
                {
                "key": "promocode",
                "value": [
                    data.coupon
                ],
                "keyName": null,
                "valueName": null
                }
            ]
            }
        }
            const res=await api.post('cart/inputs/add', req);
            if(res && res.data.status.code!==0) {
                return ({status:false,message:res.data.status.message,code:res.data.status.code,cartId:res.data.response})
            } else {            
                const discount=searchObj(res.data.response,"description","Discount").amount;
                return({status:true,discount:discount})
            }
    } catch(err) {
        // store.dispatch({type:"SET_ERROR",payload:err.message})
        // return ({status:false,message:err.message});
        return setError(err);
    }
}

export const myBookingDetails=async(data)=>{
    try{        
        const req={...mybooking}
        //req.Request.Filters[0].DefaultValue=bookingReferenceNo;
        req.Request.ItineraryRefNo=data.itineraryRefNo;
        req.Request.BookingRefNo=data.bookingReferenceNo;

        const res=await api.post('mybookings/details', req);    
        if(res && res.data.status.code!==0) {
            return ({status:false,message:res.data.status.message,code:res.data.status.code})
        } else {        
            return({status:true,data:res.data.response,fareBreakup:res.data.fareBreakup})
        }
    } catch(err) {
        // store.dispatch({type:"SET_ERROR",payload:err.message})
        // return ({status:false,message:err.message});
        return setError(err);
    }
}

export const fareRules=async(data)=>{
    try{       
        const req={
            "request": {
                "token": data.searchToken,
                "data": data.flightToken
            }
        }        
        const res=await api.post('air/farerules', req);   
        return returnResponse({...res}); 
        // if(res && res.data.status.code!==0) {
        //     return ({status:false,message:res.data.status.message,code:res.data.status.code})
        // } else {        
        //     return({status:true,data:res.data.response})
        // }
    } catch(err) {
        // store.dispatch({type:"SET_ERROR",payload:err.message})
        // return ({status:false,message:err.message});
        return setError(err);
    }
}

export const myBookings=async(obj)=>{
    try{       
        const req={
            "Request": {
                "Data": obj.type, 
                "PageInfoIndex": [
                    {
                      "Item": {
                        "CurrentPage": 0,
                        "PageLength": 10
                      }
                    }
                  ]
            }
        }   
        if(obj.category){
            req.Request.Data=obj.category
        }
        req.Request.PageInfoIndex[0].Item.CurrentPage=obj.pageIndex?obj.pageIndex:0;
        if(obj.pageLength) {
            req.Request.PageInfoIndex[0].Item.PageLength=obj.pageLength;
            req.Request.PageInfoIndex[0].Item.CurrentPage=obj.pageIndex;
        } 
        if(obj.filtersIndex){
            req.Request.filtersIndex=[...obj.filtersIndex]
        }
        const res=await api.post('mybookings', req);    
        return returnResponse({...res});
        // if(res && res.data.status.code!==0) {
        //     return ({status:false,message:res.data.status.message,code:res.data.status.code})
        // } else {        
        //     return({status:true,data:res.data.response})
        // }
    } catch(err) {
        // store.dispatch({type:"SET_ERROR",payload:err.message})
        // return ({status:false,message:err.message});
        return setError(err);
    }
}

export const showInvoiceOrVoucher=async(data)=>{ console.log(data);
    try{       
        const req={
            "Request": {
              "itineraryID": data.itineraryID,
              "bookingID": data.bookingID,
              "businessShortDescription": data.type,
              "isvoucher": data.isvoucher
            }
          }       
        const res=await api.post('mybookings/invoice', req);   
        return returnResponse({...res}); 
        // if(res && res.data.status.code!==0) {
        //     return ({status:false,message:res.data.status.message,code:res.data.status.code})
        // } else {        
        //     return({status:true,data:res.data.response})
        // }
    } catch(err) {
        // store.dispatch({type:"SET_ERROR",payload:err.message})
        // return ({status:false,message:err.message});
        return setError(err);
    }
}

export const invoiceVoucherNotification=async(data)=>{
    try{       
        const req={
            "Request": {
              "itineraryID": data.itineraryID,
              "bookingID": data.bookingID,
              "businessShortDescription": data.type,
              "isvoucher": data.isvoucher,
              "email":data.email,
              "ccEmail":data.ccEmail              
            }
          }       
        const res=await api.post('mybookings/invoice/notification', req);    
        return returnResponse({...res});
        // if(res && res.data.status.code!==0) {
        //     return ({status:false,message:res.data.status.message,code:res.data.status.code})
        // } else {        
        //     return({status:true,data:res.data.response})
        // }
    } catch(err) {
        // store.dispatch({type:"SET_ERROR",payload:err.message})
        // return ({status:false,message:err.message});
        return setError(err);
    }
}

export const cancelOrModifyBooking=async(data)=>{ 
    try{   
        console.log(data);
        let reqType;    
        let req={"Request":
            {
                "BookingRefNo":data.BookingRefNo,
                "ItineraryRefNo":data.ItineraryRefNo,
                "BusinessShortDescription":data.type,
                "Email":data.email,
                "Phone":data.phoneNumber,
                "Comment":data.Comment
            }
        }
        if(data.reqType==="cancel") {
            reqType="cancelrequest"
        }
        if(data.reqType==="modify") {
            reqType="modifyrequest"
        }
        if(reqType) {
            const res=await api.post('mybookings/'+reqType, req);  
            console.log(res);  
            return returnResponse({...res});
            // if(res && res.data.status.code!==0) {
            //     return ({status:false,message:res.data.status.message,code:res.data.status.code})
            // } else {        
            //     return({status:true,data:res.data.response})
            // }
        } else {
            return ({status:false,message:"Request type is required!"});
        }
    } catch(err) {
        // store.dispatch({type:"SET_ERROR",payload:err.message})
        // return ({status:false,message:err.message});
        return setError(err);
    }
}

export const addCartAddons=async(data)=>{
    try{       
        const req={
            "Request": {
                "CartID": data.cartId,
                "Data": [...data.addons]
            },
            "Flags": {}
        }      
        const res=await api.post('cart/addons/add', req); 
        return returnResponse({...res});   
        // if(res && res.data.status.code!==0) {
        //     return ({status:false,message:res.data.status.message,code:res.data.status.code})
        // } else {        
        //     return({status:true,data:res.data.response})
        // }
    } catch(err) {
        // store.dispatch({type:"SET_ERROR",payload:err.message})
        // return ({status:false,message:err.message});
        return setError(err);
    }
}