const airSearch={
    "request": {
      "criteriaInfo": [
        {
          "locationInfo": {
            "fromLocation": {
              "id": "DXB"
            },
            "toLocation": {
              "id": "SIN"
            }
          },
          "dateInfo": {
            "startDate": "2020-04-21T16:43:49",
            "endDate": null
          },
          "sequenceNumber": 1,          
          "flexiSearchDays": 3
        }
      ],
      "paxInfo": [
        {
          "code": null,
          "sequence": 0,
          "type": null,
          "properties": {},
          "item": [
            {
              "type": 0,
              "typeString": "ADT", 
              "quantity": 1
              },
            {
              "type": 1,
              "typeString": "CHD",
              "quantity": 0
            },
            {
              "type": 2,
              "typeString": "INF",
              "quantity": 0
            }
          ]
        }
      ],
      "tripType": "Oneway",
      "business": "air",
      "businessId": "4",
      "flags": {
        "isAirDomesticRoundTripEnabled": false,
        "isIndividualRoute": false//true
      }
    }//,
    // "flags": {
    //   "feature:disableseparateroundtrip": false
    // }
  }

const filterReqFormat = {
  "request": {
    "filtersIndex": [
      {
        "code": "default",
        "sequence": 0,
        "type": null,
        "properties": {},
        "item": [          
        ]
      }
    ],
    "sortIndex": [
      {
        "code": "default",
        "sequence": 0,
        "type": null,
        "properties": {},
        "item": {
          "type": null,
          "order": 1,
          "name": "rate"
        },
        "config": [],
        "flags": {}
      },
      {
        "code": "advertisement",
        "sequence": 0,
        "type": null,
        "properties": {},
        "item": {
          "type": null,
          "order": 1,
          "name": "rate"
        },
        "config": [],
        "flags": {}
      }
    ],    
    "pageInfoIndex": [
      {
        "code": "default",
        "sequence": 0,
        "type": null,
        "properties": {},
        "item": {
          "currentPage": 0,
          "pageLength": 10,
        },
        "config": [],
        "flags": {}
      }      
    ],
    "token": "",
    "data": null
  }
}

const signupReqFormat = {
	"request": {
		"ProfilePicture": {},
		"loginName": "",
		"firstName": "",
		"lastName": "",
		"password": "",
		"location": {
			"countryID": "",
			"country": ""
		},
		"contactInformation": {
			"name": "",						
			"email": ""
		},		
		"genderDesc": "",
		"gender": ""
	},
	"flags": {}
}
const createCartReqFormat = {
  "Request": {
      "Token":"",
      "Data": [
          {
              "Key": "",
              "Value": ""
          }
      ]
  },
  "Flags": {}
}

const addTravellerToCartFormat = {
  "ContactDetails": {
    "EntityID": "0",
    "UserID": "0",
    "AgentID": "0",
    "CustomerID": "0",
    "ProfilePicture": null,
    "FirstName": "",
    "LastName": "",
    "Password": null,
    "Location": {
      "CountryID": null,
      "Country": null,
      "City": ""
    },
    "ContactInformation": {
      "PhoneNumber": "",
      "ActlFormatPhoneNumber": "",
      "PhoneNumberCountryCode": "",
      "Email": ""
    },
    "CardType": null,
    "CardNumber": null,
    "NationalityCode": null,
    "GenderDesc": "",
    "Gender": "",
    "ActlGender": "",
    "BirthDate": "",
    "OpenIDs": {

    },
    "PassportExpirationDate": "",
    "DocumentType": null,
    "DocumentNumber": null
  },
  "Request": {
    "CartID": "",
    "LocationInfo": null,
    "DateInfo": null,
    "Config": null,
    "Token": "",
    "Data": [{
      "Code": "",
      "Sequence": 0,
      "Type": "",
      "Properties": {

      },
      "Item": [
        {
          "IsMainPax": true,
          "LocationInfo": {
            "FromLocation": null,
            "ToLocation": null
          },
          "DateInfo": null,
          "Details": {
            "LoginName": "",
            "FirstName": "",
            "LastName": "",
            "Location": {
              "Name": null,
              "CountryID": null,
              "Country": null,
              "City": "",
              "State": null,
              "ZipCode": null,
              "District": null
            },
            "ContactInformation": {
              "Name": "",
              "Description": null,
              "PhoneNumber": "",
              "ActlFormatPhoneNumber": "",
              "HomePhoneNumber": null,
              "PhoneNumberCountryCode": "",
              "HomePhoneNumberCountryCode": null,
              "ActlFormatHomePhoneNumber": null,
              "Fax": null,
              "Email": "",
              "WorkEmail": null
            },
            "GenderDesc": "",
            "Gender": "",
            "ActlGender": "",
            "BirthDate": "",
            "OpenIDs": {

            },
            "PassportExpirationDate": "",
            "DocumentType": null,
            "DocumentNumber": null
          },
          "BaggageInfo": null,
          "Documents": [{
            "Type": "",
            "Id": "",
            "OriginId": null,
            "DateInfo": {
              "StartDate": "",
              "EndDate": "",
              "StartTime": null,
              "EndTime": null,
              "Type": null
            }
          }]
        }]
    }]
  }
}

const sortingIndexSeparate = [
  {
    "code": "departure",
    "sequence": 0,
    "properties": {},
    "item": {
      "order": 1,
      "name": "rate"
    },
    "config": [],
    "flags": {}
  },
  {
    "code": "arrival",
    "sequence": 0,
    "properties": {},
    "item": {
      "order": 1,
      "name": "rate"
    },
    "config": [],
    "flags": {}
  },
  {
    "code": "advertisement",
    "sequence": 0,
    "properties": {},
    "item": {
      "order": 1,
      "name": "rate"
    },
    "config": [],
    "flags": {}
  }
]

const pageInfoIndexSeparate = [ 
    {
      "code": "departure",
      "sequence": 0,
      "type": null,
      "properties": {},
      "item": {
        "currentPage": 0,
        "pageLength": 10,
      },
      "config": [],
      "flags": {}
    },
    {
      "code": "arrival",
      "sequence": 0,
      "type": null,
      "properties": {},
      "item": {
        "currentPage": 0,
        "pageLength": 10,
      },
      "config": [],
      "flags": {}
    }
]

const filterSeparate=  [
  {
    "code" : "departure",
    "item" : [
      
    ]
  },
{
    "code" : "arrival",
    "item" : [
      
    ]
}
]

const mybooking = {
    "Request": {
      "ItineraryRefNo": "GAT0717-AA00577",
      "BookingRefNo": "AEV3T92"         
  },
  "Flags": {}
} 

//{
//   "Request": {
//     "Data": "upcoming",
//     Filters: [      
//       {
//         "Name" : "bookingreferencenumber",
//         "DefaultValue": "0412456"
//       }
//     ]
//   },
//   "Flags": {}
// }
export {
  airSearch,
  filterReqFormat,
  signupReqFormat,
  createCartReqFormat,
  addTravellerToCartFormat,
  sortingIndexSeparate,
  pageInfoIndexSeparate,
  filterSeparate,
  mybooking
}