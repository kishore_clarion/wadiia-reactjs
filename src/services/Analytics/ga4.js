// import GA4React from 'ga-4-react';
// export const ga4react = new GA4React(
//   process.env.REACT_APP_ga4Property,
//   5000 
// );
// ga4react.initialize().then((ga4) => { console.log("****1");
//   ga4.pageview('path')
//   ga4.gtag('event','pageview','path') // or your custom gtag event
// },(err) => {
//   console.error(err)
// });
import {transaction} from './universalProp.js';
//////GA4//////////
const GTMURL=process.env.REACT_APP_gtmURL;
(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
      new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
      j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
      `${GTMURL}?id=`+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-MTSWCR6');
    
export const dataLayer = window.dataLayer || [];
function gtag(){dataLayer.push(arguments);}
gtag('js', new Date());
gtag('config', process.env.REACT_APP_ga4Property); //process.env.REACT_APP_ga4Property
///////////////////

  
  // window.gtag('event', 'add_shipping_info', {
  //   coupon: 'SUMMER_FUN',
  //   currency: 'USD',
  //   items: [{
  //       item_id: 'SKU_12345',
  //       item_name: 'jeggings',
  //       coupon: 'SUMMER_FUN',
  //       discount: 2.22,
  //       affiliation: 'Google Store',
  //       item_brand: 'Gucci',
  //       item_category: 'pants',
  //       item_variant: 'Black',
  //       price: 1,
  //       currency: 'USD',
  //       quantity: 1
  //   }],
  //   shipping_tier: 'Ground',
  //   value: 7.77
  //   });


  //export const dataLayer=window.dataLayer = window.dataLayer || [];
export const ga4Pageview=(page)=>{
  gtag(
      'event', 'Pageview',{
      'pagePath': page.location,
      'pageTitle': page.title //some arbitrary name for the page/state
      });
  }
  
export const selectItem=(item)=>{
  // window.dataLayer = window.dataLayer || [];
  // window.dataLayer.push({'event': 'select_item',
  // items:[
  //   {
  //     id:item.token,
  //     amount:item.displayAmount,
  //     tripType: item.tripType
  //   }
  // ]
  // });  
  gtag(
    'event', 'select_item',{
      send_to:[process.env.REACT_APP_ga4Property],
      currency:item.displayAmount.split('')[0],
      items:[
          {
            id:item.token,
            amount:item.displayAmount,
            tripType: item.tripType
          }
        ]
    });
  }

export const addToCart=(item)=>{
  gtag('event', 'add_to_cart',{
  currency:item.displayAmount.split('')[0],
  items:[
    {
      id:item.token,
      amount:item.displayAmount,
      tripType: item.tripType
    }
  ]
  })  
}

export const viewCartGA=(item)=>{
  gtag('event', 'view_cart',{
  currency:item.items[0].data.displayAmount.split('')[0],
  items:[
    {
      id:item.items[0].token,
      amount:item.items[0].data.displayAmount,
      tripType: item.items[0].data.tripType
    }
  ]
  }) 
}

export const viewPromotion=(item)=>{
  gtag('event', 'view_promotion',{
  promotion_id:item.id
  }) 
}
  
export const removeFromCart=(item)=>{
  gtag('event', 'remove_from_cart',{
  currency:item.items[0].data.displayAmount.split('')[0],
  item:    [{
    id:item.items[0].token,
    amount:item.items[0].data.displayAmount,
    tripType: item.items[0].data.tripType
    }]
  }) 
}

export const purchase=(item)=>{console.log(item);
  gtag('event','purchase',{
  currency:item.currency,
  items:   [ {
      id:item.items.token,
      amount:item.items.data.displayAmount,
      tripType: item.items.data.tripType
    } ]
  }); console.log("transaction payload",{id:item.items.token,
      revenue:item.items.data.displayAmount.split(' ')[1],currency:item.currency});
  transaction({id:item.items.token,
      revenue:item.items.data.displayAmount.split(' ')[1],currency:item.currency})
}

export const addPaymentInfo=(item)=>{
  gtag('event', 'add_payment_info',{
  currency:item.currency,
  coupon:item.coupon,
  payment_type:item.paymentType,
  // items:   [ {
  //     id:item.token,
  //     amount:item.displayAmount,
  //     tripType: item.tripType
  //   } ]
  }) 
}


export const selectPromotion=(item)=>{
  gtag('event', 'select_promotion',{
  promotion_id:item.id
  }) 
}

export const searchFor=(item)=>{
  // window.dataLayer = window.dataLayer || [];
  // window.dataLayer.push({'event': 'search', 
  //   'formLocation': item.fromLoc.city,
  //   'toLocation': item.toLoc.city
  // });
  gtag(
    'event', 'search',{
      send_to:[process.env.REACT_APP_ga4Property],
      formLocation: item.fromLoc.city,
      toLocation: item.toLoc.city
    });
}

// dataLayer.push({
//   event: 'view_cart',
//   currency: '$',
//   items: [{id: '1dd66747-1ad8-43ad-8898-176d717e8d08', amount: '$ 84.40', tripType: 'Oneway'}],
//   gtm.uniqueEventId: 2
// })