import ReactGA from 'react-ga';
//ReactGA.initialize(process.env.REACT_APP_universalProperty,{debug: true, gaOptions: {siteSpeedSampleRate: 100 }});//
//ReactGA.initialize(process.env.REACT_APP_universalProperty,{gaOptions: {siteSpeedSampleRate: 100 }});//
ReactGA.initialize(process.env.REACT_APP_universalProperty);//,{gaOptions: {siteSpeedSampleRate: 100 }}
ReactGA.plugin.require('ecommerce')
export const setTiming=(timingObj)=>{
// Feature detects Navigation Timing API support.
  ReactGA.timing({
      category: timingObj.category,
      variable: timingObj.variable, 
      label:timingObj.label, 
      value: timingObj.value
  });  
}

export const pageLoadTimeWithPageview=(page)=>{
    ReactGA.ga('send', 'pageview', {'page':page.location,'metric1':  page.loadTime});
}

export const createGAevent=(obj)=>{
    ReactGA.event({
        category: obj.category,
        action: obj.action,
        label:obj.label
    });
}

export const transaction=(payload)=>{
    ReactGA.plugin.execute('ecommerce', 'addTransaction', payload);
    ReactGA.plugin.execute('ecommerce', 'send');
}
