import {cmsapi} from "./Api";
import {airSearch,filterReqFormat} from "./requestFormat";
import {AppData} from "./constants";
import axios from "axios";
import {store} from "../redux/store";
import _ from 'lodash';

export const fetchLocalCountry=async function() {
    try{
        const country=store.getState().user.localCountry;//localStorage.getItem("localCountry");          
        if(!country||country=="undefined") {        
            const res=await axios.get(`https://api.ipdata.co/?api-key=${process.env.REACT_APP_IPDATA_APIKEY}`);  
            store.dispatch({type:"SET_LOCATION",payload:{country:res.data.country_name, latLon:res.data.latitude+","+res.data.longitude}})      
            //localStorage.setItem("localCountry",res.data.country_name);
            //localStorage.setItem("localLoc",res.data.latitude+","+res.data.longitude);
            return res.data.country_name;
        } else {
            return country;
        }
    } catch(err) {
        // store.dispatch({type:"SET_ERROR",payload:err.message})
        // return ({status:false,message:err.message});
        return setError(err);
    }
}

export const getHomePageContent=async function (data) {
    try{   
        const res=await cmsapi.get('/api/block/1');    
        return res;
    } catch(err) {
        // store.dispatch({type:"SET_ERROR",payload:err.message})
        // return ({status:false,message:err.message});
        return setError(err);
    }   
}

export const getSpecialContent=async function (data) {
    try{    
        const res=await cmsapi.get('/api/special');    
        return res;
    } catch(err) {
        // store.dispatch({type:"SET_ERROR",payload:err.message})
        // return ({status:false,message:err.message});
        return setError(err);
    }  
}

export const getTermsConditions = async function(data) {
    try{
        const res = await cmsapi.get('/api/block/3');    
        return res;
    } catch(err) {
        // store.dispatch({type:"SET_ERROR",payload:err.message})
        // return ({status:false,message:err.message});
        return setError(err);
    }
}

export const getHomePageTopContent=async function (data) {
    try{ 
        const country =await fetchLocalCountry();  
        const city = await getNearbyCity();  
        const topres = await cmsapi.get(`api/wadiia-top-destinations/${country}`);
        //await cmsapi.get(`/api/top-destination?country=${country}`);
        topres.data = topres.data.filter(topResObj => topResObj.field_iata_code !== '' && topResObj.city !== '' && topResObj.city.toLowerCase() !== city.city.toLowerCase());  
        return topres;
    } catch(err) {
        // store.dispatch({type:"SET_ERROR",payload:err.message})
        // return ({status:false,message:err.message});
        return setError(err);
    }
}

export const getSpecialRoute=async function (data) {
    try{   
        const country =await fetchLocalCountry();   
        //const res=await cmsapi.get(`/api/popular-flight-routes?from_country=${country}`); 
        const res=await cmsapi.get(`/api/wadiia-top-routes/${country}`); 
        if(res.data.length>0) {
            const domestic = res.data.filter((route)=> {
                return route.from_country.toLowerCase() === country.toLowerCase() && route.to_country.toLowerCase() === country.toLowerCase();
            });  
            const domesticFlight = removeDuplicates(domestic);      
            const international = res.data.filter((route)=> {
                return route.to_country.toLowerCase() !== country.toLowerCase();
            });
            const internationalFlight = removeDuplicates(international);
            return res.data={domestic:[...domesticFlight],international:[...internationalFlight]};        
        }
    } catch(err) {
        // store.dispatch({type:"SET_ERROR",payload:err.message})
        // return ({status:false,message:err.message});
        return setError(err);
    }   
}

export const getNearbyCity=async function (data) {
    try{ 
        const latlon=store.getState().user.localLoc;//localStorage.getItem("localLoc");
        if(latlon) { 
            const city = await cmsapi.get(`/api/geolocation/${latlon}<=150km`);
            if(city.data.length>0) {
                return city.data[0];
            };
        }
    } catch(err) {
        // store.dispatch({type:"SET_ERROR",payload:err.message})
        // return ({status:false,message:err.message});
        return setError(err);
    }
}

export const sendSearchReq=async (obj)=>{
    try {  
        if(obj && obj.fromLoc.country && obj.toLoc.country && obj.price) {     
            const title=`${obj.fromLoc.city} to ${obj.toLoc.city}`;
            const req={ 
                "type":"search_data",
                "title": title, 
                "field_current_location": obj.country,
                "field_source_location": obj.fromLoc.city,
                "field_source_iata":obj.fromLoc.id,
                "field_source_country": obj.fromLoc.country,
                "field_destination_location": obj.toLoc.city,
                "field_destination_iata": obj.toLoc.id,
                "field_destination_country": obj.toLoc.country,
                "field_min_price":obj.price
            };
            const topres = await cmsapi.post(`/api/post/searchdata`,req)//cmsapi.post(`entity/node`,req);    
            return topres;
        }
    } catch(err) {
        // store.dispatch({type:"SET_ERROR",payload:err.message})
        // return ({status:false,message:err.message});
        return setError(err);
    }
}

const setError=(err)=>{
    store.dispatch({type:"SET_ERROR",payload:err.message})
    return ({status:false,message:err.message});
}

const removeDuplicates = (flightData) => {
    var finalFlightData = [];
    flightData.filter((route) =>{
        let similarItem = _.find(finalFlightData, { 'from_city': route.from_city, "to_city": route.to_city });       
        if(!similarItem){
            finalFlightData.push(route);
        } else {
            if(Number(similarItem.route_price.replace(/[^0-9.-]+/g,"")) > Number(route.route_price.replace(/[^0-9.-]+/g,""))) {
                let index = _.findIndex(finalFlightData, { 'from_city': route.from_city, "to_city": route.to_city });
                finalFlightData.splice(index, 1);
                finalFlightData.push(route);
            }
        }
    });
    return finalFlightData;
}