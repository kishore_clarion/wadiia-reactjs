import {api} from "./Api";
import {airSearch,filterReqFormat,filterSeparate} from "./requestFormat";
import {AppData} from "./constants";
import axios from "axios";
import {store} from "../redux/store";
import {getCurrency} from "./../utils/commonFunc";

const filterObjServiceClass= {
    "type": "checkBox",  
    "name": "serviceclass",
    "values": []
} 
const filterObjStopsDirect={  
  "type": "checkBox",
  "name": "stops",
  "values": ["direct"]
}


export const getFilteredFlights= async function(token,serviceClass,direct) { 
    try{ 
        const filterReq={...filterReqFormat};
        filterReq.request.filtersIndex[0].item=[];
        if(serviceClass) {
            filterObjServiceClass.values=[];
            filterObjServiceClass.values.push(serviceClass)
            filterReq.request.filtersIndex[0].item.push(filterObjServiceClass);
        }
        if(direct) {
            filterReq.request.filtersIndex[0].item.push(filterObjStopsDirect);
        }
        filterReq.request.token=token;  
        const res=await api.post('air/search/page',  filterReq);  
        return res;
    } catch(err) {
        store.dispatch({type:"SET_ERROR",payload:err.message})
        return ({status:false,message:err.message});
    } 
}

export const getAllSearchResult=async function (data) {
    try{  
        data.trim();
        const res=await api.post('air/search/location', {   
                "Request": data
            }
        )  
        if(res && res.data.status.code===0) {
            const locations=res.data;       
            return ({status:true,data:locations.response[0].item});     
        } else {
            return ({status:false,message:res.data.status.message}); 
        } 
    } catch(err) {
        store.dispatch({type:"SET_ERROR",payload:err.message})
        return ({status:false,message:err.message});
    }      
}

export const getSearchFlights=async function(data) {
    try{
        const reqFormat={...airSearch};
        reqFormat.request.criteriaInfo[0].locationInfo.fromLocation.id=data.fromLoc.id;
        reqFormat.request.criteriaInfo[0].locationInfo.toLocation.id=data.toLoc.id;
        reqFormat.request.criteriaInfo[0].dateInfo.startDate=data.startDate;
        reqFormat.request.criteriaInfo[0].dateInfo.endDate=data.endDate && data.tripType==='Roundtrip'?data.endDate:null;//
        reqFormat.request.paxInfo[0].item[0].quantity=data.adult;
        reqFormat.request.paxInfo[0].item[1].quantity=data.child;
        reqFormat.request.paxInfo[0].item[2].quantity=data.infant;
        reqFormat.request.tripType=data.tripType;               
        const res=await api.post('air/search',  reqFormat); //res.data=testData;//static
        if(res && res.data.status.code===0) {
            const token=res.data.response.token; 
            let filters=res.data.firstPage.response
            if(data.serviceClass || data.direct) {
                const res=await getFilteredFlights(token,data.serviceClass,data.direct);
                return ({status:true,type:"filter",res:res.data,
                token:token,fromLocation:data.fromLoc,
                toLocation:data.toLoc,fromDate:data.startDate,toDate:data.endDate?data.endDate:null,
                tripType:data.tripType,adult:data.adult,child:data.child,infant:data.infant,
                serviceClass:data.serviceClass?data.serviceClass:null
                });
            } else { 
                return ({status:true,type:"search",res:res.data.firstPage,token:token
                ,fromLocation:data.fromLoc,
                toLocation:data.toLoc,fromDate:data.startDate,toDate:data.endDate?data.endDate:null,
                tripType:data.tripType,adult:data.adult,child:data.child,infant:data.infant,
                serviceClass:data.serviceClass?data.serviceClass:null
                });
            }
        } else {
            return ({status:false,code:res.data.status.code,message:res.data.status.message});
        }
    } catch(err) { 
        store.dispatch({type:"SET_ERROR",payload:err.message})
        return ({status:false,message:err.message});
    }
}



export const getListFlights=async function(token,filter,currentpage=0,pageInfoIndex) {
    try{
        const filterReq={...filterReqFormat};    
        filterReq.request.token=token;
        if(filter && filter.length>0 && !pageInfoIndex) {
            filterReq.request.filtersIndex[0].item=[...filter]
        } else if(filter && filter.length>0 && pageInfoIndex){
        filterReq.request.filtersIndex=[...filterSeparate]; 
        filterReq.request.filtersIndex[0].item=[...filter[0].item];
        filterReq.request.filtersIndex[1].item=[...filter[1].item];
        }
        if(pageInfoIndex) {
        filterReq.request.pageInfoIndex=[...pageInfoIndex];
        filterReq.request.pageInfoIndex[0].item.currentPage=currentpage;
        filterReq.request.pageInfoIndex[1].item.currentPage=currentpage;
        } else {
        filterReq.request.pageInfoIndex[0].item.currentPage=currentpage;
        }    
        let res=await api.post('air/search/page',  filterReq);    
        if(res.data.status.message==="Success") {
        let message=null;
        if(res.data.status.code!==0)
            message=res.data.status.message;    
            return ({status:true,result:res.data.response,code:res.data.status.code,message:message});
        } else {
            return ({status:false,code:res.data.status.code,message:res.data.status.message})
        }
    } catch(err) {
        store.dispatch({type:"SET_ERROR",payload:err.message})
        return ({status:false,message:err.message});
    }
}

export const auth=async function() {
    try{
        const res=await api.post('/authc', AppData.username); 
        const currency= await getCurrency();       
        if ((typeof document !== 'undefined' && window.location.pathname==="/")  || !currency) {
        //if (typeof document !== 'undefined' && (window.location.pathname==="/" || !getCurrency())) {
            const req={"request": {}};
            const response = await api.post("/application/environment",req); 
            console.log(response.data);     
            if(response && response.data.status.code===0) {
           // localStorage.setItem("setting",JSON.stringify(response.data.response));
            store.dispatch({type:"SETTING_REQUEST_SUCCESS",payload:response.data.response})
            } else {
            //localStorage.setItem("setting","");
            store.dispatch({type:"SETTING_REQUEST_SUCCESS",payload:null})
            }
        }
        return res;
    } catch(err) {
        store.dispatch({type:"SET_ERROR",payload:err.message})
        return ({status:false,message:err.message});
    }
}