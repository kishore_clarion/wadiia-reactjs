import axios from "axios";

const api= axios.create({
  baseURL: process.env.REACT_APP_TRAVELCARMA_API,//"https://dxcore.travelcarma.com/api/v1/",
  timeout: 60000,//timeout 1 min
  accept: "application/json",
  withCredentials : true
});

const cmsapi= axios.create({
  baseURL: process.env.REACT_APP_CMS_API,
  accept: "application/json" 
});

export {
  api,
  cmsapi  
}