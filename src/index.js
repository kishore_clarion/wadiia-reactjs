import React from 'react';
import ReactDOM from 'react-dom';
import 'bootstrap/dist/css/bootstrap.min.css';
import './index.css';
import App from './App';
import * as serviceWorker from './serviceWorker';
import 'bootstrap/dist/js/bootstrap.bundle.min';
//import { CookiesProvider } from 'react-cookie';
import { BrowserRouter } from 'react-router-dom';
import history from "./utils/history";


// ReactDOM.render(<CookiesProvider><App /></CookiesProvider>, document.getElementById('root'));

// // If you want your app to work offline and load faster, you can change
// // unregister() to register() below. Note this comes with some pitfalls.
// // Learn more about service workers: https://bit.ly/CRA-PWA
// serviceWorker.unregister();
if (typeof window !== 'undefined') {
    ReactDOM.hydrate(
	  <BrowserRouter history={history}>
			<App/>
	  </BrowserRouter>,
	  document.getElementById('root')
	);
}
serviceWorker.unregister();