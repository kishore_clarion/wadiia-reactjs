import React from 'react'

const InitialLoadContext = React.createContext()

export const InitialLoadProvider = InitialLoadContext.Provider
export const InitialLoadConsumer = InitialLoadContext.Consumer

export default InitialLoadContext