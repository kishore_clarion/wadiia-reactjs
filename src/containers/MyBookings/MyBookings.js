import React, { Component } from "react";
import './MyBookings.css';
import Slider from "react-slick";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import { getSpecialContent } from "../../services/homeContent";
import {siteBaseUrl} from "../../services/constants";
import { Modal } from 'react-bootstrap';
import parse from 'html-react-parser';
import {getUser} from "../../services/user";
import { message } from "antd";
import history from '../../utils/history';
import { Helmet } from "react-helmet";
import {pageLoadTimeWithPageview} from '../../services/Analytics/universalProp';
import {ga4Pageview} from '../../services/Analytics/ga4';
import SpecialOffer from '../../components/SpecialOffer/SpecialOffer';

class MyBookings extends Component {

    state={
        special:[],
        specialIndex:0,
        show:false,
        profileInfo:null,
        pageTitle:"My Bookings"
        }

    componentDidMount=async()=>{
        let special=[];        
        special = await getSpecialContent();
        if(special && special.status!==false) {
            special=special.data;
        }
        const res=await getUser();
        let profileInfo={...this.state.profileInfo}
        if(res.status) {
            profileInfo={...res.data};
        } else {
            message.error(res.message);            
        }
        pageLoadTimeWithPageview({location:window.location.pathname,loadTime:Math.round(performance.now())})
        ga4Pageview({location:window.location.pathname,title:this.state.pageTitle});
        this.setState({special:[...special],profileInfo:{...profileInfo}});        
    }

    handleShow = (ind) => {
        this.setState({show:true,specialIndex:ind});       
    }

    specialContent = () =>{        
        if(this.state.special.length>0) {
            return this.state.special.map((spec,index)=>{ 
                return <SpecialOffer spec={spec} key={"specOff"+index} index={index} handleShow={(ind)=>this.handleShow(ind)}/>                                   
            });            
        }
    }

    render(){
        const settings = {
            dots: false,            
            speed: 500,
            slidesToShow: 3,
            slidesToScroll: 1,
            arrows: true,
            infinite: false,
            responsive: [
                {
                    breakpoint: 1024,
                    settings: {
                        slidesToShow: 3,
                        slidesToScroll: 3,
                        infinite: true,
                        dots: true
                    }
                },
                {
                    breakpoint: 600,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 2,
                        initialSlide: 2
                    }
                },
                {
                    breakpoint: 480,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1
                    }
                }
            ]
        };       
       return(
            <React.Fragment>               
                <Helmet>
                    <title>{this.state.pageTitle}</title>
                    <meta name="description" content='My booking details' />
                </Helmet>
                <Modal show={this.state.show} onHide={()=>this.setState({show:false})} size="md">
                    <Modal.Header closeButton>
                        <Modal.Title>Offer Detail</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        {this.state.special[this.state.specialIndex] && typeof this.state.special[this.state.specialIndex].field_coupon_detail=="string"  && (<div> {parse(this.state.special[this.state.specialIndex].field_coupon_detail)} </div>)} 
                    </Modal.Body>
                </Modal>
                <div className="container m-t-20">
                        <h1>My Booking</h1>
                        <div className="my-booking">
                            <div className="my-booking__title">Manage Booking</div>
                            <div className="row my-booking__options">
                                <div className="col-md-4 text-center my-booking__options--box">
                                    <h3>
                                        <span><i className="far fa-edit"></i></span>
                                        MODIFY BOOKING</h3>
                                    <ul>
                                        <li><a  onClick={()=>history.push("/manage-bookings/list/cancel")}> Cancel </a></li>
                                        <li><a  onClick={()=>history.push("/manage-bookings/list/modify")}> Modify </a></li>
                                    </ul>
                                </div>
                                <div className="col-md-4 text-center my-booking__options--box">
                                    <h3>
                                        <span><i className="fas fa-ticket-alt"></i></span>
                                        TICKET/VOUCHERS</h3>
                                    <ul>
                                        <li><a  onClick={()=>history.push("/manage-bookings/list/voucher")}> Voucher </a></li>
                                        <li><a  onClick={()=>history.push("/manage-bookings/list/invoice")}> Invoice </a></li>
                                        <li><a  onClick={()=>history.push("/manage-bookings/list/view")}> Print </a></li>
                                    </ul>
                                </div>
                                <div className="col-md-4 text-center my-booking__options--box">
                                    <h3>
                                        <span><i className="far fa-file-alt"></i></span>
                                        BOOKING DETAILS</h3>
                                    <ul>
                                        <li><a href="/manage-bookings/list"> View All Bookings </a></li>
                                        <li className="d-none"><a href="#"> Itinerary/Fare Details </a></li>
                                    </ul>
                                </div>
                                {/*<div className="col-md-3 text-center my-booking__options--box d-none">
                                    <h3>
                                        <span><i className="fas fa-undo-alt"></i></span>
                                        CLAIM REFUND</h3>
                                    <ul>
                                        <li><a href="#"> Booked </a></li>
                                        <li><a href="#"> Confirmed </a></li>
                                        <li><a href="#"> Cancelled </a></li>
                                    </ul>
                                </div>*/}
                            </div>
                            <div className="row my-booking__bottom">
                                <div className="col-md-8"> 
                                    <div className="my-booking__title"> Wadiia Specials </div>
                                    <div className="my-booking__bottom--box">
                                        <Slider {...settings}>
                                                {
                                                this.state.special.length>0 ? this.specialContent() : null
                                                }                                            
                                        </Slider>
                                    </div>
                                </div>
                                <div className="col-md-3 d-none"> 
                                    <div className="my-booking__title"> eCash </div>
                                    <div className="my-booking__bottom--box">
                                        
                                    </div>
                                </div>
                                <div className="col-md-4"> 
                                    <div className="white-bg text-center">
                                        <div className="profile-info__image m-auto">                                            
                                            {this.state.profileInfo ? <img className="img-fluid" src={this.state.profileInfo.profilePicture.url}/>:<i className="far fa-user"></i>}
                                        </div>
                                        {
                                        this.state.profileInfo ?(
                                            <React.Fragment>
                                            <div className="profile-info__name text-uppercase">{ this.state.profileInfo.firstName } { this.state.profileInfo.lastName }</div>
                                            <div className="profile-info__email">{ this.state.profileInfo.contactInformation.email }</div>
                                            <div className="profile-info__phone">Phone: { this.state.profileInfo.contactInformation.phoneNumber }</div>
                                            </React.Fragment>)
                                        : null }
                                    </div>
                                </div>
                            </div>
                        </div>
                </div>
            </React.Fragment>
       ) 
    }
}

export default MyBookings;