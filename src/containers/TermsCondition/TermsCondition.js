import React, { Component } from "react";
import { getTermsConditions } from "../../services/homeContent";
import parse from 'html-react-parser';
import './TermConditions.css';
import { Helmet } from "react-helmet";


class TermsCondition extends Component {
	state = {
		TermsConditions: null
	}

	componentDidMount = async () => {
		const termContent = await getTermsConditions();
        this.setState({ TermsConditions: termContent });
	}

	render() {
		return(
			<React.Fragment>               
			<Helmet>
				<title>{'Term and conditions'}</title>
				<meta name="description" content='Term and conditions' />
			</Helmet>
				<div className="page-terms container m-t-20">
					{this.state.TermsConditions && (<div> {parse(this.state.TermsConditions.data[0].body)} </div>)}
				</div>
			</React.Fragment>
			)
	}
}

export default TermsCondition;