import React, { Component } from 'react';
import _ from 'lodash';
import {signUp} from "../../services/user";
import { connect } from "react-redux";
import * as actions from "../../redux/index";

class OpenId extends Component {
    state = { 
        user:{...this.props.user}
    }
    
    handleLogin= async (type)=>{                             
        await this.props.openIdSignup({type:type,redirect: this.props.redirect});        
    }
    
    render() {          
        return ( 
            <React.Fragment>
                <div className="login-with">
                    <button className="btn btn-default" onClick={()=>this.handleLogin("facebook")}><i className="fa fa-facebook"></i> sign in with Facebook</button>
                </div>
                <div className="login-with">
                    <button className="btn btn-default" onClick={()=>this.handleLogin("google")}><i className="fa fa-google"></i> sign in with Google</button>
                </div>
            </React.Fragment>
         );
    }
}

const mapStateToProps = (state) => {
    return {
        message: state.user.message
    };
};
//export default Login;
export default (connect(mapStateToProps, actions))(OpenId);