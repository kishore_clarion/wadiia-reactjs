import React, { Component } from "react";
import './Login.css'
import "antd/dist/antd.css";
import { Radio } from 'antd';
import { Formik, Field, Form, ErrorMessage } from "formik";
import * as Yup from "yup";
import { connect } from "react-redux";
import * as actions from "../../redux/index";
import {forgotPassword} from "../../services/user";
import LoadingOverlay from 'react-loading-overlay';
import { message,Alert} from 'antd';
import {messageDuration} from "../../services/constants";


class ForgotPwd extends Component {
    state = {
        loginType: "Email",
        loading:false,
        emailsent:false
    }

    onChange = e => {
        console.log('radio checked', e.target.value);
        this.setState({
            loginType: e.target.value,
        });
    }; 

    // prepareWarningMess=()=>{       
    //     return <div style={{marginBottom: "14px"}}><Alert message={"Sent email. Please check your email."} type="info" /></div>
    // }
    
    render() {
        let validationShape = Yup.object().shape({
            email: Yup.string().email("Email is invalid")
                .required("Email is required")
        })
        if (this.state.loginType === "Mobile Phone Number") {
            const phoneRegExp = /^((\\+[1-9]{1,4}[ \\-]*)|(\\([0-9]{2,3}\\)[ \\-]*)|([0-9]{2,4})[ \\-]*)*?[0-9]{3,4}?[ \\-]*[0-9]{3,4}?$/
            validationShape = Yup.object().shape({
                phone: Yup.string()
                    .matches(phoneRegExp, 'Phone number is not valid')
                    .required("Phone is required")
            })
        }    
        return (
            <div>                
            <LoadingOverlay
                active={this.state.loading || this.props.loading}
                spinner={true}
                text='Loading your content...'
            >
            
                <div className="container">
                    <div className="login-wrapper">
                        <div className="login-header">
                            <h2>Welcome to Wadiia</h2>                            
                        </div>
                        <div className="login-header">
                            {/*formik */}

                            <Radio.Group onChange={this.onChange} value={this.state.loginType}>
                                <Radio value={"Email"}>Email</Radio>
                                <Radio value={"Mobile Phone Number"}>Mobile Phone Number</Radio>
                            </Radio.Group>
                        </div>
                        <Formik
                            initialValues={{
                                email: "",
                                phone: ""
                            }}
                            //Validation schema
                            validationSchema={validationShape}

                            //Post data to API
                            onSubmit={async (data, actions) => {                                
                                this.setState({loading:true})                          
                                let res=await forgotPassword(data); 
                                console.log(res);                               
                                //this.props.loginSuccess();                                
                                if(res && res.status) {                                    
                                    await this.setState({emailSent:true,loading:false});                                  
                                } else {
                                    this.setState({message:res.message,loading:false})
                                    message.error(res.message,messageDuration)
                                } 
                            }}>

                            {({
                                values,
                                errors,
                                touched,
                                handleChange,
                                handleBlur,
                                handleSubmit,
                                isSubmitting,
                                /* and other goodies */
                            }) => (
                                    <Form>

                                        {this.state.message && (<div className="text-danger"> {this.state.message}</div>)}
                                        {this.state.loginType === "Email" && (<div className="login-wrapper__inp">


                                            <Field
                                                name="email"
                                                id="email"
                                                type="text"
                                                placeholder="Email"
                                                className={
                                                    `form-control` +
                                                    (errors.email && touched.email ? " is-invalid" : "")
                                                }
                                                maxlength="100"
                                            />
                                            <ErrorMessage
                                                name="email"
                                                component="div"
                                                className="invalid-feedback"
                                            />
                                        </div>)}
                                        {this.state.loginType === "Mobile Phone Number" && (<div className="login-wrapper__inp">

                                            <Field
                                                name="phone"
                                                id="phone"
                                                type="text"
                                                placeholder="Phone Number"
                                                className={
                                                    `form-control ` +
                                                    (errors.phone && touched.phone ? " is-invalid" : "")
                                                }
                                            />
                                            <ErrorMessage
                                                name="phone"
                                                component="div"
                                                className="invalid-feedback"
                                            />
                                        </div>)}                                       
                                        <button type="submit" className="btn btn-primary login-continue">Continue</button>
                                        {this.state.emailSent && <div style={{marginBottom: "14px"}}><Alert message={"Email sent. Please check your email."} type="info" /></div>}
                                        <a href="/login">Back to Login</a>
                                    </Form>

                                )}

                        </Formik>                        
                    </div>
                </div>
            </LoadingOverlay>
            </div>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        message: state.user.message,
        loading: state.user.loading
    };
};
//export default Login;
export default (connect(mapStateToProps, actions))(ForgotPwd);