import React, { PureComponent } from "react";
import './Login.css'
import "antd/dist/antd.css";
import { Radio } from 'antd';
import { Formik, Field, Form, ErrorMessage } from "formik";
import * as Yup from "yup";
import history from '../../utils/history';
import { connect } from "react-redux";
import * as actions from "../../redux/index";
import { login } from "../../services/user";
import LoadingOverlay from 'react-loading-overlay';
import { Helmet } from "react-helmet";
import OpenId from "./openIdSignup"
import { message } from 'antd';
import { messageDuration } from "../../services/constants";
import PhoneInput from 'react-phone-input-2';
import { ga4Pageview } from '../../services/Analytics/ga4';
import { pageLoadTimeWithPageview } from '../../services/Analytics/universalProp';

class Login extends PureComponent {
    state = {
        loginType: "email",
        loading: false,
        pageTitle: "Login"
    }

    onChange = e => {
        this.setState({
            loginType: e.target.value,
        });
    };

    componentDidMount = async () => {
        pageLoadTimeWithPageview({ location: window.location.pathname, loadTime: Math.round(performance.now()) });
        ga4Pageview({ location: window.location.pathname, title: this.state.pageTitle });
    }

    componentDidUpdate = (prevProps, prevState) => {
        if (this.props.message && !this.props.status) {
            this.setState({ message: this.props.message });
            message.config({ duration: 10, maxCount: 1 })
            message.error(this.props.message)
        }
    }

    render() {
        let validationShape;
        if (this.state.loginType === "email") {
            validationShape = Yup.object().shape({
                email: Yup.string().email("Email is invalid")
                    .required("Email is required"),
                password: Yup.string().required("Password is required"),
            })
        }
        if (this.state.loginType === "phone") {
            const phoneRegExp = /^[0-9]+$/
            validationShape = Yup.object().shape({
                phone: Yup.object().shape({
                    phoneNumberCountryCode: Yup.string()
                        .required("Phone country code is required"),
                    phoneNumber: Yup.string()
                        .required("Phone Number is required")
                        .matches(phoneRegExp, 'Phone number is not valid')
                }),
                password: Yup.string().required("Password is required")
            })
        }
        return (
            <div>
                <Helmet>
                    <title>{this.state.pageTitle}</title>
                    <meta name="description" content={this.state.description} />
                </Helmet>
                <LoadingOverlay
                    active={this.state.loading || this.props.loading}
                    spinner={true}
                    text='Loading your content...'
                >

                    <div className="container">
                        <div className="login-wrapper">
                            <div className="login-header">
                                <h2>Welcome to Wadiia</h2>
                                <p>Please Login to continue</p>
                            </div>
                            <div className="login-header"> 
                                <Radio.Group onChange={this.onChange} value={this.state.loginType}>
                                    <Radio value={"email"}>Email</Radio>
                                    <Radio value={"phone"}>Mobile Phone Number</Radio>
                                </Radio.Group>
                            </div>
                            <Formik
                                initialValues={{ email: "", phone: "", password: "" }}
                                //Validation schema
                                validationSchema={validationShape}
                                //Post data to API
                                onSubmit={async (data, actions) => {
                                    this.setState({ loading: true });
                                    if (this.state.loginType === 'email') {
                                        data.PhoneNumber = '';
                                    }
                                    if (this.state.loginType === 'phone') {
                                        data.email = '';
                                    }
                                    const res = await login(data);
                                    if (res && res.status) {
                                        const path = this.props.location.state ? this.props.location.state.path : "/";
                                        this.props.loginSuccess({ message: "", status: true, redirect: path });
                                        //this.props.location.state ? history.push(this.props.location.state.path) : history.push("/")
                                    } else {
                                        this.props.loginSuccess({ message: "", status: false });
                                        this.setState({ message: res.message, loading: false })
                                        message.error(res.message, messageDuration)
                                    }
                                }} >
                                {({ values, errors, touched,handleChange,handleBlur,handleSubmit,isSubmitting,setFieldValue }) => (
                                    <Form>
                                        {this.state.message && (<div className="text-danger"> {this.state.message}</div>)}
                                        {this.state.loginType === "email" && (
                                        <div className="login-wrapper__inp">
                                            <Field name="email" id="email" type="text" placeholder="Email" className={ `form-control` + (errors.email && touched.email ? " is-invalid" : "") }  />
                                            <ErrorMessage name="email" component="div" className="invalid-feedback" />
                                        </div>)}
                                        {this.state.loginType === "phone" && (
                                        <div className="login-wrapper__inp">                                             
                                            <PhoneInput name={"phone"} country={'ke'} placeholder={"Phone Number"} enableSearch={true}
                                                inputClass={`form-control` + (errors.phone && (errors.phone.phoneNumberCountryCode || errors.phone.phoneNumber) ? " is-invalid" : "")}
                                                onChange={(value, data, event, formattedValue) => {
                                                    if (data.dialCode) {
                                                        setFieldValue("phone", { phoneNumberCountryCode: data.dialCode, phoneNumber: value.slice(data.dialCode.length) });
                                                    }
                                                }}
                                            />
                                            {errors.phone && errors.phone.phoneNumberCountryCode && <span className="invalid-feedback"> {errors.phone.phoneNumberCountryCode} </span>}
                                            {errors.phone && errors.phone.phoneNumber && <span className="invalid-feedback">{errors.phone.phoneNumber}</span>}
                                        </div>)}
                                        <div className="login-wrapper__inp">
                                            <Field type="password" id="password" name="password" placeholder="Password" className={ `form-control ` + (errors.password && touched.password ? " is-invalid": "") }/>
                                            <ErrorMessage name="password" component="div" className="invalid-feedback"/>
                                        </div>
                                        <button type="submit" className="btn btn-primary login-continue">Continue</button>
                                        <div className="login-privacy"> By proceeding, you agree with our<br /> <a href="">Terms of Service</a>,  <a href="">Privacy Policy</a> </div>
                                        <div className="login-privacy"> <a href="/forgot-pwd">Forgot Password</a></div>
                                        </Form>
                                    )}
                            </Formik>
                            <OpenId redirect={this.props.location.state ? this.props.location.state.path : "/"} />                           
                        </div>
                    </div>
                </LoadingOverlay>
            </div>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        message: state.user.message,
        loading: state.user.loading
    };
};
//export default Login;
export default (connect(mapStateToProps, actions))(Login);