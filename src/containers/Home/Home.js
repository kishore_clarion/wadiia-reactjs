import React, { PureComponent } from "react";
import LeftPanel from '../../components/LeftPanel/LeftPanel';
import './Home.css';
import moment from 'moment';
import history from '../../utils/history';
import { removeRecentSearch } from '../../utils/commonFunc';
import Slider from "react-slick";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import { getHomePageContent,getSpecialContent,getHomePageTopContent,getSpecialRoute,getNearbyCity } from "../../services/homeContent";
import parse from 'html-react-parser';
import LoadingOverlay from 'react-loading-overlay';
import { Modal } from 'react-bootstrap';
import { connect } from "react-redux";
import { compose } from "redux";
import * as actions from "../../redux/index";
import {siteBaseUrl} from "../../services/constants";
import {viewPromotion} from '../../services/Analytics/ga4';
import {createGAevent} from '../../services/Analytics/universalProp';
import { message } from 'antd';
import SpecialOffer from '../../components/SpecialOffer/SpecialOffer';
import _ from 'lodash';

class Home extends PureComponent {   
    state = {
        recentSearchUpdating: false,
        recentSearch: [],
        whyHomePape: null,
        topdestination: [],
        specialDomestic:null,
        setting:null,
        special:[],
        specialIndex:0,
        show:false,
        copyActive:[]
    }
    handleShow = (ind) => {
        this.setState({show:true,specialIndex:ind});
        viewPromotion({id:ind});
        createGAevent({category:'Offer',action:`User checking offer ${ind+1}`,label:'Offer'});
    }
    handleClose = () => this.setState({show:false});
    componentDidMount = async () => {
        //try{    
        let setting=this.props.appSettings?{...this.props.appSettings}:null;//localStorage.getItem("setting");       
        this.setState({loading:true});
        if(setting) {
            //setting=typeof setting=="string"?JSON.parse(setting):null;
            await this.setState({setting:{...setting}})
        }       
        let recentSearch = this.props.recentSearch?[...this.props.recentSearch]:[];//localStorage.getItem('recentSearch');
        if (recentSearch.length > 0) {
            recentSearch.reverse();
            this.setState({ recentSearch: [...recentSearch] });
        }        
        let content = await getHomePageContent();
        if(content && content.status!==false)
        this.setState({ whyHomePape: content });

        let topDestination = await getHomePageTopContent();
        if(topDestination && topDestination.status!==false)
        this.setState({ topdestination: topDestination.data });

        let special = await getSpecialContent();
        if(special && special.status!==false) {
            const len=special.data.length;
            const copyActive=new Array(len).fill(1);
            this.setState({special:special.data,copyActive:[...copyActive]});
        }

        let domesticRoute = await getSpecialRoute();
        if(domesticRoute && domesticRoute.status!==false)        
        this.setState({specialDomestic:[...domesticRoute.domestic],specialInternational:[...domesticRoute.international]});

        let localCity= await getNearbyCity();
        if(localCity)
         this.setState({localCity:{...localCity}});
        // let InternationalRoute= this.state.
        // } catch(err) {
        //     this.props.errorSet(err.message);
        // }
    }
    componentDidUpdate = async () => {                
        let setting=this.props.appSettings?{...this.props.appSettings}:null;//localStorage.getItem("setting");        
        if(!this.state.setting && setting) {            
            //setting=typeof setting=="string"?JSON.parse(setting):null;
            await this.setState({setting:{...setting}})
        }        
    }
    deleteRecentSearch = async (index) => {
        this.setState({ recentSearchUpdating: true })
        let res = await removeRecentSearch(index);
        if (res && res.length > 0) {
            await this.setState({ recentSearch: [...res], recentSearchUpdating: false });
        } else {
            await this.setState({ recentSearch: [], recentSearchUpdating: false });
        }
    }
    specialContent = () =>{        
        if(this.state.special.length>0) {  
            return this.state.special.map((spec,index)=>{   
                return <SpecialOffer spec={spec} key={"specOff"+index} index={index} handleShow={(ind)=>this.handleShow(ind)}/>                                   
            })
        }
    }
    getRoute=(international=false)=>{
        let routes=[...this.state.specialDomestic];
        let key="domesticRoute"
        if(international) {
            routes=[...this.state.specialInternational];
            key="internationalRoute"
        }
        return routes.map((domestic,index)=>{
            return <a key={key+index} onClick={()=>{
                createGAevent({category:{key},action:`User checking ${key}`,label:{key}});
                history.push(`/flight-listing?fromDate=${moment().add(15,'days').format('YYYY-MM-DD')}&toDate=null&fromLocation=${JSON.stringify({id:domestic.form_iata_code,city:domestic.from_city})}&toLocation=${JSON.stringify({id:domestic.to_iata_code,city:domestic.to_city})}&passanger=${JSON.stringify({adult:1,infant:0,child:0})}&tripType=Oneway&direct=null`)}}>
            <div className="" >
                <div className="w-popular-destinatios__item">
                    <div className="origin-destination">
                        <div className="elip">{domestic.from_city}</div>
                        <div className="time">{moment().add(15,'days'). format('ddd, DD MMM') }</div>
                        <div className="elip">{domestic.to_city}</div>
                    </div>
                    <div className="low-price">
                        <span>Starting From</span>
                        <span className="rs"></span> {domestic.route_price}                        
                        {/*<a onClick={()=>{history.push(`/flight-listing?fromDate=${moment().add(domestic.field_days,'days').format('YYYY-MM-DD')}&toDate=null&fromLocation=${JSON.stringify({id:domestic.form_iata_code,city:domestic.from_city})}&toLocation=${JSON.stringify({id:domestic.to_iata_code,city:domestic.to_city})}&passanger=${JSON.stringify({adult:1,infant:0,child:0})}&tripType=Oneway&direct=null`)}}>HERE</a>*/}
                    </div>
                </div>
            </div>
            </a>
        })
    }
    handleTopDestinationRedirection=(toLoc)=>{        
        if(this.state.localCity) {
            createGAevent({category:'Top Destination',action:`User checking top destination city ${toLoc.city}`,label:'Top destination'});
            let domestic={...this.state.localCity}
            history.push(`/flight-listing?fromDate=${moment().add(15,'days').format('YYYY-MM-DD')}&toDate=null&fromLocation=${JSON.stringify({id:domestic.field_iata_code,city:domestic.city})}&toLocation=${JSON.stringify({id:toLoc.field_iata_code,city:toLoc.city})}&passanger=${JSON.stringify({adult:1,infant:0,child:0})}&tripType=Oneway&direct=null`)
        }
    } 
    copyToClipboard=(index)=>{
        const actives=[...this.state.copyActive];
        actives[index]=false;        
        navigator.clipboard.writeText(parse(this.state.special[index].field_code));
        this.handleClose();
        message.success("Offer code copied successfully",2);
        this.setState({copyActive:[...actives]});
    }    
    render() {
        let content;
        var settings = {
            dots: false,            
            speed: 500,
            slidesToShow: 3,
            slidesToScroll: 1,
            arrows: true,
            infinite: false,
            responsive: [
                {
                    breakpoint: 1024,
                    settings: {
                        slidesToShow: 3,
                        slidesToScroll: 3,
                        infinite: true,
                        dots: true
                    }
                },
                {
                    breakpoint: 600,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 2,
                        initialSlide: 2
                    }
                },
                {
                    breakpoint: 480,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1
                    }
                }
            ]
        };
        //if (!this.props.loading && Object.keys(this.props.setting).length > 0 && !this.state.recentSearchUpdating && this.state.recentSearch && this.state.recentSearch.length > 0) {
        if(!this.props.loading && this.state.setting && !this.state.recentSearchUpdating && this.state.recentSearch && this.state.recentSearch.length > 0) {
            content = this.state.recentSearch.map((rS, index) => {              
                if (new Date(rS.fromDate).getTime() < new Date().getTime()) {
                    rS.fromDate = moment().format("YYYY-MM-DD");
                    if(rS.toDate && rS.tripType==='Roundtrip' && new Date(rS.toDate).getTime() < new Date().getTime()){
                        rS.toDate = moment().format("YYYY-MM-DD"); 
                    } else {
                        rS.toDate = null;
                    }
                    rS.url = `/flight-listing?fromDate=${rS.fromDate}&toDate=${rS.toDate}&fromLocation=${JSON.stringify({ id: rS.fromLocation.id, city: rS.fromLocation.city,country:rS.fromLocation.country })}&toLocation=${JSON.stringify({ id: rS.toLocation.id, city: rS.toLocation.city,country:rS.toLocation.country })}&passanger=${rS.passanger}&serviceClass=${rS.serviceClass}&tripType=${rS.tripType}&direct=${rS.direct}`
                }
                return (
                    <div className="recent-search" key={index}>
                        <div className="recent-search__item">
                            <button className="recent-search__item-cancel" onClick={() => this.deleteRecentSearch(index)}> &#10060; </button>
                            <div className="recent-search__img">
                                <img src={process.env.PUBLIC_URL +"images/icon_flight_grey.png"} width="22" />
                            </div>
                            <div className="recent-search__flight">
                                <span> {rS.fromLocation.city} </span>
                                <span> -&gt; </span>
                                <span> {rS.toLocation.city} </span>
                            </div>
                            <div className="original-date">{moment(rS.fromDate).format('ddd, D MMM')} </div>
                            <div className="low-price">
                                <span>Starting From</span>
                                <div className="low-price__amount">
                                    <span className="rs"> {this.state.setting.portalCurrency.symbol} <span>{rS.amt}</span> </span>
                                    <button className="recent-search__item-more" onClick={() => {
                                        createGAevent({category:'Recent Search',action:'User checking recent search',label:'Recent Search'});
                                        history.push(`${rS.url}`)}}> &#10141; </button>
                                </div>
                            </div>
                        </div>
                    </div>
                )
            })
            // content.push(
            //     <div className="recent-search" key={"explore"} >
            //         <div className="recent-search__item">
            //             <span> Explore the world </span>
            //             <div className="original-date">
            //                 Start planning your next
            //             </div>
            //             <a href="#" className="btn btn-primary"> Explore </a>
            //         </div>
            //     </div>)
        }        
        return (
            <div>                               
                <Modal show={this.state.show} onHide={this.handleClose} size="md">
                    <Modal.Header closeButton>
                        <Modal.Title>Offer Detail</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                         {this.state.special[this.state.specialIndex] && typeof this.state.special[this.state.specialIndex].field_code=="string"  && (<div className="modal-special-offer"> <div className="modal-special-offer__code">{parse(this.state.special[this.state.specialIndex].field_code)} </div> <i className="far fa-copy" 
                    className={this.state.copyActive[this.state.specialIndex]?'far fa-copy':'far fa-copy disabled'}
                    onClick={() => {this.copyToClipboard(this.state.specialIndex)}}></i> </div>)} 
                    </Modal.Body>
                </Modal>
                <LoadingOverlay
                active={this.props.loading || !this.state.setting }
                spinner={true}
                text='Loading your content...'
                >
                </LoadingOverlay>
                {/*!this.props.loading && Object.keys(this.props.setting).length > 0 && (<div className="container">*/}
                {this.state.setting && (<div className="container">
                    <div className="row">
                        <div className="col-lg-5 left-section-container">
                            <LeftPanel localCity={this.state.localCity?{...this.state.localCity}:null} />
                        </div>
                        <div className="col-lg-7 right-section-container">
                            <div className="right-section">
                                {/*Recent Search*/}
                                {content && (<div className="right-section__wrapper" key={content.length}>
                                    <h2>Recent Search</h2>
                                    <div className="right-section__wrapper-recent-search">
                                        <Slider {...settings}>
                                            {content}
                                        </Slider>
                                    </div>
                                </div>)}

                                {/*Wadiia Special*/}
                                {this.state.special.length>0 && <div className="right-section__wrapper wadiia-special">
                                    <h2>Wadiia special</h2>
                                    <div className="row wadiia-special-slider">
                                    <Slider {...settings}>
                                        {
                                           this.specialContent() 
                                        }
                                    </Slider>                                       
                                    </div>
                                </div>}

                                {/*Top destinations*/}
                                {this.state.topdestination && this.state.topdestination.length>0 &&
                                <div className="right-section__wrapper w-top-destinatios">
                                    <h2>Top Destination</h2>
                                    <div className="w-top-destination__inner">
                                        <div>


                                        </div>
                                        <div>
                                            <ul>
                                                {!this.state.topdestination.data && this.state.topdestination.map((topdestination, index) => (
                                                    <li key={"topDestination"+index}>
                                                        <a onClick={()=>{this.handleTopDestinationRedirection(topdestination)}}>
                                                            <span>Flights to <strong> {topdestination.city} </strong></span>
                                                        </a>
                                                    </li>
                                                ))}
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                }

                                {/*Popular Domestic Flight Routes*/}
                                { this.state.specialDomestic && this.state.specialDomestic.length>0 &&
                                <div className="right-section__wrapper w-popular-destinatios">
                                    <h2>Popular Domestic Flight Routes</h2>
                                    {/*<div className="w-popular-destinatios__list">
                                        <span>From - </span>
                                        <ul>
                                            <li><a className="active" href="#">Pune</a></li>
                                            <li><a href="#">Mumbai</a></li>
                                            <li><a href="#">Bangalore</a></li>
                                            <li><a href="#">Kolkatta</a></li>
                                        </ul>
                                    </div>*/}

                                    <div className="w-popular-destinatios__popular">
                                        <div className="row">
                                            <div className="col-md-12">
                                            { this.state.specialDomestic && 
                                                <Slider {...{...settings,slidesToShow:4}} >
                                                {
                                                    this.getRoute()
                                                }
                                                </Slider>                                       
                                            }
                                            </div>                                                
                                        </div>
                                    </div>
                                </div>
                                }

                                {/*Popular International Flight Routes*/}
                                {this.state.specialInternational && this.state.specialInternational.length>0 &&
                                <div className="right-section__wrapper w-popular-destinatios">
                                    <h2>Popular International Flight Routes</h2>                                    
                                    <div className="w-popular-destinatios__popular">
                                        <div className="row">
                                            <div className="col-md-12">
                                                { this.state.specialInternational && 
                                                    <Slider {...{...settings,slidesToShow:4}} >
                                                    {
                                                        this.getRoute(true)
                                                    }
                                                    </Slider>                                       
                                                }                                              
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                }

                                {/*Top International and Domestic Flights*/}
                                <div className="right-section__wrapper w-topflight-destination" className="d-none">
                                    <div className="row">
                                        <div className="col-md-6">
                                            <h2>Top International Holiday Destinations</h2>
                                            <ul>
                                                <li>
                                                    <div className="w-topflight-destination__list">
                                                        <div className="img-sec"> <span>img</span></div>
                                                        <div className="w-topflight-destination__list-right">
                                                            <span className="destination_information">
                                                                <span className="destination_name elip">New Zealand</span>
                                                                <span className="price_go dest">
                                                                    <p className="low_price">
                                                                        <span className="rs"> Rs. </span>199,990 <span>/ Person</span>
                                                                    </p>
                                                                </span>
                                                                <span className="destination_day_night">6 Nights / 7 Days</span>
                                                                <i className="go demo-icon icon-go"></i>
                                                            </span>
                                                        </div>
                                                    </div>
                                                </li>
                                            </ul>
                                        </div>

                                        <div className="col-md-6" className="d-none">
                                            <h2>Top Domestic Holiday Destinations</h2>
                                            <ul>
                                                <li>
                                                    <div className="w-topflight-destination__list">
                                                        <div className="img-sec"> <span>img</span></div>
                                                        <div className="w-topflight-destination__list-right">
                                                            <span className="destination_information">
                                                                <span className="destination_name elip">New Zealand</span>
                                                                <span className="price_go dest">
                                                                    <p className="low_price">
                                                                        <span className="rs"> Rs. </span>199,990 <span>/ Person</span>
                                                                    </p>
                                                                </span>
                                                                <span className="destination_day_night">6 Nights / 7 Days</span>
                                                                <i className="go demo-icon icon-go"></i>
                                                            </span>
                                                        </div>
                                                    </div>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>

                                <div className="right-section__wrapper">
                                    <div className="why-wadiia">
                                       {this.state.whyHomePape && (<div> {parse(this.state.whyHomePape.data[0].body)} </div>)}                                       
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>)}               
            </div>
        )
    }
}

const mapStateToProps = (state) => {
    return {        
        loading: state.flights.settingLoad,
        recentSearch: state.user.recentSearch,
        appSettings: Object.keys(state.appSetting.setting).length>0?{...state.appSetting.setting}:null
    };
};

export default compose(connect(mapStateToProps, actions))(Home);