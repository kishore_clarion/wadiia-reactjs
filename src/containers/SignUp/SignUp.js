import React, { Component } from "react";
import './SignUp.css'
import 'antd/dist/antd.css';
import { Radio } from 'antd';
import { Formik, Field, Form, ErrorMessage } from 'formik';
import * as Yup from 'yup';
import { countries } from "../../services/constants";
import { signup, generateOTP } from "../../services/user";
import { connect } from "react-redux";
import * as actions from "../../redux/index";
import 'antd/dist/antd.css';
import LoadingOverlay from 'react-loading-overlay';
import history from '../../utils/history';
import { Select, message } from 'antd';
import { Helmet } from "react-helmet";
import PhoneInput from 'react-phone-input-2';
import { Modal } from 'react-bootstrap';
import {ga4Pageview} from '../../services/Analytics/ga4';
import {pageLoadTimeWithPageview} from '../../services/Analytics/universalProp';
const { Option } = Select;


class SignUp extends Component {
    state = {
        signupLoading: false,
        show: false,
        otp: "",
        formData: null,
        pageTitle:"Sign Up"
    }
    //phoneRegExp = /^((\\+[1-9]{1,4}[ \\-]*)|(\\([0-9]{2,3}\\)[ \\-]*)|([0-9]{2,4})[ \\-]*)*?[0-9]{3,4}?[ \\-]*[0-9]{3,4}?$/;
    nameRegExp = /^[A-Za-z]+$/;
    FILE_SIZE = 4 * 1024 * 1024;
    SUPPORTED_FORMATS = [
        "image/jpg",
        "image/jpeg",
        "image/png"
    ];
    validationShape = Yup.object().shape({
        email: Yup.string()
            .email("Email is invalid")
            .required("Email is required"),
        password: Yup.string()
            .required("Password is required")
            .min(2, "Password must be at least 8 characters"),
        confirmPassword: Yup.string()
            .oneOf(
            [Yup.ref("password"), null], "Confirm password is not matched with password")
            .required("Confirm Password is required"),
        firstName: Yup.string()
            .matches(this.nameRegExp, 'First name is not valid')
            .required("Firstname is required"),
        lastName: Yup.string()
            .matches(this.nameRegExp, 'Last name is not valid')
            .required("Lastname is required"),
        country: Yup.string()
            .required("Country is required"),
        contact: Yup.object().shape({
            PhoneNumberCountryCode: Yup.string()
                .required("Phone country code is required"),
            PhoneNumber: Yup.string()
                .required("Phone Number is required")
                //.matches(this.phoneRegExp, 'Phone number is not valid')
        }),
        gender: Yup.string().required("Gender is required"),
        file: Yup.mixed()
            .when({
                is: (value) => value !== undefined,
                then: Yup.mixed().test(
                    "fileSize",
                    "File is large than 4mb",
                    value => value && value.size <= this.FILE_SIZE
                )
                    .test(
                    "fileFormat",
                    "Unsupported Format",
                    value => value && this.SUPPORTED_FORMATS.includes(value.type)
                    )
            })
    })
    initialValues = {
        email: "",
        contact: { PhoneNumberCountryCode: 'KE', PhoneNumber: "" },
        password: "",
        confirmPassword: "",
        firstName: "",
        lastName: "",
        country: JSON.stringify({ "name": "Kenya", "flag": "🇰🇪", "code": "KE", "dial_code": "+254" }),
        gender: "M",
        file: undefined
    }
    countryList = () => {
        let options = "";
        options = countries.map(country => {
            return <option value={JSON.stringify(country)} key={country.code}>{country.name}</option>
        })
        return options;
    }
    filter = (input, option) => {
        if (option.value.toLowerCase().indexOf(input.toLowerCase()) >= 0) {
            return true;
        }
    }
        
    signUp = async (data) => {
        this.setState({ signupLoading: true })
        const res = await signup(data);        
        if (res && res.status) {
            // this.props.signupSuccess();
            // this.props.location.state ? history.push(this.props.location.state.path) : history.push("/")
            const path=this.props.location.state ? this.props.location.state.path : "/";
            this.props.signupSuccess({redirect: path}); 
        } else {
            this.setState({ signupLoading: false })
            message.error(res.message);
        }
    }
    handleSighupWithOTP = () => {
        let data = { ...this.state.formData };
        if (this.state.otp) {
            data.otp = this.state.otp;
            this.signUp(data);
            this.handleClose()
        }
    }
    handleClose = () => {
        this.setState({ show: !this.state.show })
    }

    componentDidMount = async () => {
        pageLoadTimeWithPageview({location:window.location.pathname,loadTime:Math.round(performance.now())});
        ga4Pageview({location:window.location.pathname,title:this.state.pageTitle});
    }
    render() {
        return (
            <div>
                <Modal className="signup-otp" show={this.state.show} onHide={this.handleClose} size="md">
                    <Modal.Header closeButton>
                        <Modal.Title>Verify OTP</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        <div className="signup-otp__form">
                            <input class="form-control" type="text" maxLength="6" name="otp" value={this.state.otp} onChange={(e) => this.setState({ otp: e.target.value })} />
                            <button class="btn btn-primary" type="submit" onClick={() => this.handleSighupWithOTP()} disabled={!this.state.otp || !/^[0-9]{6}$/.test(this.state.otp)}> Submit </button>
                        </div>
                    </Modal.Body>
                </Modal>
                <Helmet>
                    <meta charSet="utf-8" />
                    <title>{this.state.pageTitle}</title>
                    <meta name="description" content={'Signup'} />
                </Helmet>
                <LoadingOverlay
                    active={this.state.signupLoading}
                    spinner={true}
                    text='Loading your content...'
                />
                <div className="container">
                    <div className="sign-wrapper">
                        <div className="signup-header">
                            <h2>Welcome to Wadiia</h2>
                            <p>Please signup to continue</p>
                        </div>
                        <Formik
                            initialValues={this.initialValues}
                            //Validation schema
                            validationSchema={this.validationShape}

                            //Post data to API
                            onSubmit={async (data, actions) => {
                                //if contact no added then process generate otp flow
                                if (data.contact.PhoneNumber) {
                                    let res = await generateOTP({ ...data.contact });
                                    if (res && res.status) {
                                        this.setState({ show: !this.state.show, formData: { ...data } });
                                    } else {
                                        message.error(res.message);
                                    }
                                } else {
                                    data.contact.PhoneNumberCountryCode=null;
                                    data.contact.PhoneNumber=null;
                                    this.signUp(data);                                   
                                }
                            }}
                        >
                            {({ values, errors, touched, handleChange, handleBlur, handleSubmit, isSubmitting, setFieldValue }) => (
                                <Form>
                                    {this.props.message && (<div className="row"><div className="text-danger"> {this.props.message}</div></div>)}                                    
                                    <div className="row">
                                        <div className="col-md-6">
                                            <div className="sign-wrapper__inp">
                                                <Field maxLength="100" name="firstName" id="firstName" type="text" placeholder="First Name"
                                                    className={
                                                        `form-control` +
                                                        (errors.firstName && touched.firstName ? " is-invalid" : "")
                                                    }
                                                />
                                                <ErrorMessage name="firstName" component="div" className="invalid-feedback" />
                                            </div>
                                        </div>
                                        <div className="col-md-6">
                                            <div className="sign-wrapper__inp">
                                                <Field maxLength="100" name="lastName" id="lastName" type="text" placeholder="Last Name"
                                                    className={
                                                        `form-control` +
                                                        (errors.lastName && touched.lastName ? " is-invalid" : "")
                                                    }
                                                />
                                                <ErrorMessage name="lastName" component="div" className="invalid-feedback" />
                                            </div>
                                        </div>
                                    </div>

                                    <div className="row">
                                        <div className="col-md-6">
                                            <div className="sign-wrapper__inp">
                                                <Field maxLength="200" name="email" id="email" type="email" placeholder="Email"
                                                    className={
                                                        `form-control` +
                                                        (errors.email && touched.email ? " is-invalid" : "")
                                                    }
                                                />
                                                <ErrorMessage name="email" component="div" className="invalid-feedback" />
                                            </div>
                                        </div>
                                        <div className="col-md-6">
                                            <div className="sign-wrapper__inp">                                                
                                                {errors.contact && errors.contact.phoneNumberCountryCode}
                                                <PhoneInput
                                                    name={"contact"}
                                                    country={'ke'}
                                                    placeholder={"Phone Number"}
                                                    enableSearch={true}
                                                    inputClass={`form-control` + (errors.contact && (errors.contact.PhoneNumberCountryCode || errors.contact.PhoneNumber) ? " is-invalid" : "")}
                                                    onChange={(value, data, event, formattedValue) => {
                                                        if (data.dialCode) {
                                                            setFieldValue("contact", { PhoneNumberCountryCode: data.dialCode, PhoneNumber: value.slice(data.dialCode.length) });
                                                        }
                                                    }}
                                                />
                                                {errors.contact && errors.contact.PhoneNumberCountryCode && <span className="invalid-feedback"> {errors.contact.PhoneNumberCountryCode} </span>}
                                                {errors.contact && errors.contact.PhoneNumber && <span className="invalid-feedback">{errors.contact.PhoneNumber}</span>}
                                            </div>
                                        </div>
                                    </div>

                                    <div className="row">
                                        <div className="col-md-6">
                                            <div className="sign-wrapper__inp">
                                                <Field maxLength="10" name="password" id="password" type="password" placeholder="Password"
                                                    className={
                                                        `form-control` +
                                                        (errors.password && touched.password ? " is-invalid" : "")
                                                    }
                                                />
                                                <ErrorMessage name="password" component="div" className="invalid-feedback" />
                                            </div>
                                        </div>
                                        <div className="col-md-6">
                                            <div className="sign-wrapper__inp">
                                                <Field maxLength="10" name="confirmPassword" id="confirmPassword" type="password" placeholder="Confirm Password"
                                                    className={
                                                        `form-control` +
                                                        (errors.confirmPassword && touched.confirmPassword ? " is-invalid" : "")
                                                    }
                                                />
                                                <ErrorMessage name="confirmPassword" component="div" className="invalid-feedback" />
                                            </div>
                                        </div>
                                    </div>

                                    <div className="row">
                                        <div className="col-md-6">
                                            <div className="sign-wrapper__inp">
                                                <Field name="gender" id="gender" as="select" placeholder="Gender"
                                                    className={`form-control` + (errors.gender && touched.gender ? " is-invalid" : "")}>
                                                    <option value="M">Male</option>
                                                    <option value="F">Female</option>
                                                </Field>
                                                <ErrorMessage name="gender" component="div" className="invalid-feedback" />
                                            </div>
                                        </div>
                                        <div className="col-md-6">
                                            <div className="sign-wrapper__inp">
                                                <Field name="country" id="country" as="select" placeholder="Favorite Color" className={
                                                    `form-control` +
                                                    (errors.country && touched.country ? " is-invalid" : "")
                                                }>
                                                    <option value="">Select Country</option>
                                                    {this.countryList()}

                                                </Field>
                                                <ErrorMessage name="country" component="div" className="invalid-feedback" />
                                            </div>
                                        </div>
                                    </div>
                                    <div className="row">
                                        <div className="col-md-12">
                                            <div className="sign-wrapper__inp d-flex">
                                                <label htmlFor="">Profile image</label>
                                                <input id="file" name="file" type="file" className={`form-control` + (errors.file && touched.file ? " is-invalid" : "")}
                                                    onChange={(event) => {
                                                        setFieldValue("file", event.currentTarget.files[0]);
                                                    }} />
                                                <ErrorMessage name="file" component="div" className="invalid-feedback" />
                                            </div>
                                        </div>
                                    </div>
                                    <div className="row">
                                        <div className="col-md-12">
                                            <button className="btn btn-primary sign-continue">Continue</button>
                                        </div>
                                    </div>
                                </Form>
                            )}
                        </Formik>
                        {/* <div className="row m-t-25">
                        <div className="col-md-6">
                            <div className="sign-with">
                                <button className="btn btn-default"><i className="fa fa-facebook"></i> sign in with Facebook</button>
                            </div>
                        </div>
                        <div className="col-md-6">
                            <div className="sign-with">
                                <button className="btn btn-default"><i className="fa fa-google"></i> sign in with Google</button>
                            </div>
                        </div>
                    </div> */}

                        <div className="row">
                            <div className="col-md-12">
                                <div className="sign-privacy text-center">
                                    By proceeding, you agree with our <br /> <a href="">Terms of Service</a>, <a href="">Privacy Policy</a>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        )
    }
}

//export default SignUp;
const mapStateToProps = (state) => {
    return {
        message: state.user.message,
        loading: state.user.loading
    };
};
export default (connect(mapStateToProps, actions))(SignUp);