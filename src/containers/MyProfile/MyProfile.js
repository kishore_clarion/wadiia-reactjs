import React, { Component } from 'react';
import { Modal } from 'react-bootstrap';
import { getUser, updateUser } from '../../services/user';
import history from '../../utils/history';
import PhoneInput from 'react-phone-input-2';
import { countries } from '../../services/constants';
import { DatePicker, message } from 'antd';
import moment from 'moment';
import { Formik, Field, Form, ErrorMessage } from 'formik';
import * as Yup from 'yup';
import { readUploadedFileAsbase64 } from '../../utils/commonFunc';
import { Helmet } from "react-helmet";
import {pageLoadTimeWithPageview} from '../../services/Analytics/universalProp';
import {ga4Pageview} from '../../services/Analytics/ga4';
import './MyProfile.css';

class MyProfile extends Component {
    state = {
        showElement: true,
        show: false,
        loading: true,
        contact: {
            firstName: '',
            lastName: '',
            contactInformation: '',
        },
        profileInfo: {
            profilePicture: {},
            location: {},
            contactInformation: {
                phoneNumberCountryCode: 254,
                phoneNumber: ''
            }
        },
        country: "KE",
        issueCountry: "KE",
        nationality: "KE",
        PhoneNumberCountryCode: "KE",
        password: "",
        confirmPassword: "",
        pageTitle:"My Profile"
    }

    setPassword = () => {
        if (this.state.password !== this.state.confirmPassword) {
            this.setState({ passwordError: "Password and Confirm password is not matched!" });
        } else {
            if (this.state.password && this.state.confirmPassword) {
                if ((Object.keys(this.state.password).length < 6 || Object.keys(this.state.confirmPassword).length < 6)) {
                    this.setState({ passwordError: "Password length should be 8" });
                } else {
                    this.setState({ show: !this.state.show })
                }
            }
        }
    }
    // Show hide Profile element
    handleHideElement = () => {
        this.setState({
            showElement: !this.state.showElement
        })
    }

    // Show Modal popup
    handleShowHideModal = () => {
        this.setState(
            {
                show: !this.state.show
            }
        )
    }

    getUserDetails = async () => {
        const res = await getUser();
        if (res.status) {
            this.setState({ profileInfo: { ...res.data }, loading: false })
        } else {
            message.error(res.message);
            this.setState({ loading: false })
        }
    }
    componentDidMount = async () => {
        this.getUserDetails();
        pageLoadTimeWithPageview({location:window.location.pathname,loadTime:Math.round(performance.now())});
        ga4Pageview({location:window.location.pathname,title:this.state.pageTitle});
    }


    countryList = () => {
        let options = "";
        options = countries.map(country => {
            return <option value={country.code} key={country.code}>{country.name}</option>
        })
        return options;
    }

    phoneRegExp = /^((\\+[1-9]{1,4}[ \\-]*)|(\\([0-9]{2,3}\\)[ \\-]*)|([0-9]{2,4})[ \\-]*)*?[0-9]{3,4}?[ \\-]*[0-9]{3,4}?$/;
    nameRegExp = /^[A-Za-z]+$/;
    FILE_SIZE = 4 * 1024 * 1024;
    SUPPORTED_FORMATS = [
        "image/jpg",
        "image/jpeg",
        "image/png"
    ];
    validationShape = Yup.object().shape({
        firstName: Yup.string()
            .matches(this.nameRegExp, 'First name is not valid')
            .required("Firstname is required"),
        lastName: Yup.string()
            .matches(this.nameRegExp, 'Last name is not valid')
            .required("Lastname is required"),
        location: Yup.object().shape({
            countryID: Yup.string().notRequired(),//required("Country is required"),
            address: Yup.string().notRequired(),//.required("Address is required"), //NR
            city: Yup.string().notRequired(),//.required("City is required"),  //NR          
            zipCode: Yup.string().notRequired()//.required("Zip code is required") //NR
        }),
        password: Yup.string()
            .notRequired(),
        //.min(2, "Password must be at least 8 characters"),
        confirmPassword: Yup.string()
            .when('password', {
                is: (value) => value != undefined,
                then: Yup.string()
                    .oneOf(
                    [Yup.ref("password"), null], "Confirm password is not matched with password")
                    .required("Confirm Password is required")
            }),
        contactInformation: Yup.object().shape({
            phoneNumber: Yup.string().notRequired(),
            phoneNumberCountryCode: Yup.string().notRequired(),
            email: Yup.string().required("Email is required"),
        }),
        birthDate: Yup.string().notRequired(),
        anniversaryDate: Yup.string().notRequired(),
        profilePicture: Yup.object().shape({
            url: Yup.string().notRequired(),//.required("Profile image is required"),
            file: Yup.mixed()
                .when({
                    is: (value) => value !== undefined,
                    then: Yup.mixed().test(
                        "fileSize",
                        "File is large than 4mb",
                        value => value && value.size <= this.FILE_SIZE
                    )
                        .test(
                        "fileFormat",
                        "Unsupported Format",
                        value => value && this.SUPPORTED_FORMATS.includes(value.type)
                        )
                })
        }),
        gender: Yup.string().required("Gender is required"),
        documentType: Yup.string()
            .notRequired(),
        nationalityCode: Yup.string()
            .when('documentType', {
                is: (value) => value != undefined,
                then: Yup.string()
                    .required("Nationality is required")
            }),
        issuingCountryCode: Yup.string()
            .when('documentType', {
                is: (value) => value != undefined,
                then: Yup.string()
                    .required("Issue country is required")
            })
    })

    setInitialValues = () => {
        return {
            firstName: this.state.profileInfo.firstName ? this.state.profileInfo.firstName : "",
            lastName: this.state.profileInfo.lastName ? this.state.profileInfo.lastName : "",
            gender: this.state.profileInfo.gender ? this.state.profileInfo.gender : "M",
            location: {
                countryID: this.state.profileInfo.location.countryID ? this.state.profileInfo.location.countryID : "ke",
                address: this.state.profileInfo.location.address ? this.state.profileInfo.location.address : "",
                city: this.state.profileInfo.location.city ? this.state.profileInfo.location.city : "",
                zipCode: this.state.profileInfo.location.zipCode ? this.state.profileInfo.location.zipCode : ""
            },
            contactInformation: {
                phoneNumber: this.state.profileInfo.contactInformation.phoneNumber ? this.state.profileInfo.contactInformation.phoneNumber : "",
                phoneNumberCountryCode: this.state.profileInfo.contactInformation.phoneNumberCountryCode ? this.state.profileInfo.contactInformation.phoneNumberCountryCode : 254,
                email: this.state.profileInfo.contactInformation.email ? this.state.profileInfo.contactInformation.email : "",
            },
            birthDate: this.state.profileInfo.birthDate && this.state.profileInfo.birthDate !== "0001-01-01T00:00:00" ? this.state.profileInfo.birthDate : "",
            anniversaryDate: this.state.profileInfo.anniversaryDate && this.state.profileInfo.anniversaryDate !== "0001-01-01T00:00:00" ? this.state.profileInfo.anniversaryDate : "",
            profilePicture: {
                url: this.state.profileInfo.profilePicture.url ? this.state.profileInfo.profilePicture.url : "",
                file: undefined
            },
            documentType: this.state.profileInfo.documentType ? this.state.profileInfo.documentType : "",
            nationalityCode: this.state.profileInfo.nationalityCode ? this.state.profileInfo.nationalityCode : "KE",
            issuingCountryCode: this.state.profileInfo.issuingCountryCode ? this.state.profileInfo.issuingCountryCode : "KE"
        }
    }

    render() {
        const profileInfo = this.state.userProfile;
        return (
            <React.Fragment>
                <Helmet>
                    <title>{this.state.pageTitle}</title>
                    <meta name="description" content='Profile Details' />
                </Helmet>
            <div className="page-my-profile container m-t-20">
                <h1>My Profile</h1>
                <div className="row">
                    <div className="col-md-8 page-my-profile__left">
                        {this.state.showElement ? <div className="my-profile">
                            <div className="page-my-profile__title">MY PROFILE</div>
                            <div className="white-bg">
                                <div className="profile-info d-flex">
                                    <div className="profile-info__image">
                                        <i className="far fa-user"></i>
                                    </div>
                                    <div className="profile-info__desc">
                                        <div className="profile-info__desc--edit" onClick={() => this.handleHideElement()}><i className="fa fa-edit"></i></div>
                                        {this.state.profileInfo ? <div>
                                            <div className="profile-info__name">{this.state.profileInfo.firstName}</div>
                                            <div className="profile-info__email"><i className="fas fa-envelope"></i> {this.state.profileInfo.contactInformation.email} </div>
                                            <div className="profile-info__phone"><i className="fas fa-phone-square-alt"></i> {this.state.profileInfo.contactInformation.phoneNumber} </div>
                                        </div>
                                            : null}
                                    </div>
                                </div>
                            </div>

                            <div className="page-my-profile__title" className="d-none">SAVED TRAVELLERS</div>
                            <div className="white-bg" className="d-none">
                                <div className="profile-info d-flex">
                                    <div className="profile-info__image">
                                        <i className="fas fa-users"></i>
                                    </div>
                                    <div className="profile-info__desc">
                                        <div>No Travellers added</div>
                                        <div>Add Traveller for a faster booking experience.</div>
                                    </div>
                                </div>
                            </div>
                        </div> :
                            <div className="edit-profile">
                                <div className="page-my-profile__title">EDIT PROFILE</div>
                                {this.state.profileInfo && !this.state.loading ?
                                    <div className="white-bg">
                                        <Formik
                                            enableReinitialize={false}
                                            validateOnChange={false}
                                            validateOnBlur={false}
                                            validateOnMount={false}
                                            initialValues={!this.state.loading ? this.setInitialValues() : null}
                                            //Validation schema
                                            validationSchema={this.validationShape}

                                            //Post data to API
                                            onSubmit={async (data, actions) => {
                                                if (data.profilePicture.file) {
                                                    const reader = new FileReader();
                                                    let rawdata = await readUploadedFileAsbase64(data.profilePicture.file);
                                                    rawdata = rawdata.replace(/^data:image\/[a-z]+;base64,/, "");
                                                    data.profilePicture.RawData = rawdata;
                                                    data.profilePicture.url = data.profilePicture.file.name;
                                                }
                                                data.FirstName = data.firstName;
                                                data.LastName = data.lastName
                                                data.EntityID = this.state.profileInfo.entityID;
                                                data.UserID = this.state.profileInfo.userID;
                                                data.LoginName = data.firstName + data.lastName;
                                                if (this.state.password) {
                                                    data.password = this.state.password;
                                                }
                                                const res = await updateUser({ "Request": data });
                                                if (res.status) {
                                                    message.success("User details updated successfully!");
                                                    this.handleHideElement();
                                                    this.getUserDetails();
                                                } else {
                                                    message.error(res.message);
                                                }
                                            }}
                                        >
                                            {({ values, errors, touched, handleChange, handleBlur, handleSubmit, isSubmitting, setFieldValue }) => (
                                                <Form>
                                                    <div className="inp-wrapper wd-flex-center">
                                                        <label> User Name: </label>
                                                        <div className="wd-flex-center col-md-8">
                                                            <div className="d-flex wdth-100">
                                                                <div className="inp-wrapper__sec">
                                                                    <Field maxLength={20} name="firstName" placeholder="First Name" className={
                                                                        `form-control` +
                                                                        (errors.firstName && touched.firstName ? " is-invalid" : "")
                                                                    } />
                                                                    <ErrorMessage name="firstName" component="div" className="invalid-feedback" />
                                                                </div>
                                                                <div className="inp-wrapper__sec">
                                                                    <Field maxLength="20" name="lastName" placeholder="Last Name" className={`form-control` + (errors.lastName && touched.lastName ? " is-invalid" : "")} /> {/* defaultValue={ this.state.profileInfo.lastName } onChange={(e) => this.updateUserProfile(e, "lastName")}*/}
                                                                    <ErrorMessage name="lastName" component="div" className="invalid-feedback" />
                                                                </div>
                                                            </div>
                                                        </div>

                                                    </div>
                                                    <div className="inp-wrapper wd-flex-center">
                                                        <label> Gender: </label>
                                                        <div className="wd-flex-center col-md-8">
                                                            <div className="d-flex wdth-100">
                                                                <div className="inp-wrapper__sec">
                                                                    <Field className="form-control" name={`gender`} value={this.state.profileInfo.gender} className={`form-control` + (errors.gender && touched.gender ? " is-invalid" : "")} as="select" placeholder="Gender" >
                                                                        <option value="">Select Gender</option>
                                                                        <option value="M">Male</option>
                                                                        <option value="F">Female</option>
                                                                    </Field>
                                                                    <ErrorMessage name="gender" component="div" className="invalid-feedback" />
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div className="inp-wrapper wd-flex-center">
                                                        <label> Email: </label>
                                                        <div className="wd-flex-center col-md-8">
                                                            <div className="d-flex wdth-100">
                                                                <div className="inp-wrapper__sec">
                                                                    <Field type="text" maxLength="100" name="contactInformation.email" placeholder="Email" className={`form-control` + (errors.contactInformation && errors.contactInformation.email && touched.contactInformation.email ? " is-invalid" : "")} />
                                                                    <ErrorMessage name="contactInformation.email" component="div" className="invalid-feedback" />
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div className="inp-wrapper wd-flex-center">
                                                        <label> Password: </label>
                                                        <div className="wd-flex-center col-md-8">
                                                            <span className="text-primary col cursor-pointer" onClick={() => this.handleShowHideModal()}>Change Password</span>
                                                        </div>
                                                    </div>

                                                    <div className="inp-wrapper wd-flex-center">
                                                        <label> Phone: </label>
                                                        <div className="wd-flex-center col-md-8">
                                                            <div className="d-flex wdth-100">
                                                                <div className="inp-wrapper__sec">
                                                                    <PhoneInput name={"contactInformation"}
                                                                        placeholder={"Phone Number"}
                                                                        enableSearch={true}
                                                                        inputClass={`form-control col ` + (errors.contactInformation && errors.contactInformation.phoneNumber ? " is-invalid" : "")}
                                                                        country={this.state.profileInfo.contactInformation.phoneNumberCountryCode ? this.state.profileInfo.contactInformation.phoneNumberCountryCode : 254}
                                                                        value={this.state.profileInfo.contactInformation.phoneNumber && this.state.profileInfo.contactInformation.phoneNumberCountryCode ? this.state.profileInfo.contactInformation.phoneNumberCountryCode + this.state.profileInfo.contactInformation.phoneNumber : (this.state.profileInfo.contactInformation.phoneNumber && !this.state.profileInfo.contactInformation.phoneNumberCountryCode) ? "254" + this.state.profileInfo.contactInformation.phoneNumber : "254" + ""}
                                                                        onChange={(value, data, event, formattedValue) => {
                                                                            if (data.dialCode) {
                                                                                setFieldValue("contactInformation.phoneNumber", value.slice(data.dialCode.length));
                                                                                setFieldValue("contactInformation.phoneNumberCountryCode", data.dialCode);
                                                                            }
                                                                        }}
                                                                    />
                                                                    <ErrorMessage name={"contactInformation.phoneNumber"} component="div" className="invalid-feedback show" />
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div className="inp-wrapper wd-flex-center">
                                                        <label> Address: </label>
                                                        <div className="wd-flex-center col-md-8 flex-direction-column">
                                                            <div className="d-flex wdth-100 m-b-15">

                                                                <div className="inp-wrapper__sec">
                                                                    <Field as="select" id="location.countryID" name="location.countryID"
                                                                        className={`form-control col ` + (errors.location && errors.location.countryID && errors.location.countryID && touched.location.countryID ? " is-invalid" : "")}
                                                                        placeholder="Country" >
                                                                        <option value="">Select Country</option>
                                                                        {this.countryList()}
                                                                    </Field>
                                                                    <ErrorMessage name={"location.countryID"} component="div" className="invalid-feedback" />
                                                                </div>
                                                            </div>
                                                            <div className="d-flex wdth-100 m-b-15">
                                                                <div className="inp-wrapper__sec">
                                                                    <Field name="location.city" maxLength="20" className={`form-control col ` + (errors.location && errors.location.city ? " is-invalid" : "")} placeholder="Enter the City" /> {/*defaultValue={ this.state.profileInfo.location.city }*/}
                                                                    <ErrorMessage name={"location.city"} component="div" className="invalid-feedback" />
                                                                </div>

                                                                <div className="inp-wrapper__sec">
                                                                    <Field name="location.zipCode" className={`form-control col ` + (errors.location && errors.location.zipCode ? " is-invalid" : "")} placeholder="Enter the pincode" /> {/*defaultValue={ this.state.profileInfo.location.zipCode }*/}
                                                                    <ErrorMessage name={"location.zipCode"} component="div" className="invalid-feedback" />
                                                                </div>
                                                            </div>

                                                            <div className="d-flex wdth-100">
                                                                <div className="inp-wrapper__sec">
                                                                    <Field name="location.address" maxLength="100" type="textarea" className={`form-control col ` + (errors.location && errors.location.address ? " is-invalid" : "")} placeholder="Enter address" /> {/*defaultValue={ this.state.profileInfo.location.address }*/}
                                                                    <ErrorMessage name={"location.address"} component="div" className="invalid-feedback" />
                                                                </div>
                                                            </div>

                                                        </div>
                                                    </div>
                                                    <div className="inp-wrapper wd-flex-center">
                                                        <label> Birth Date and Anniversary: </label>
                                                        <div className="wd-flex-center col-md-8 flex-direction-column">
                                                            <div className="d-flex wdth-100">
                                                                <div className="inp-wrapper__sec">
                                                                    <DatePicker
                                                                        className={`form-control col ` + (errors.birthDate ? " is-invalid" : "")}
                                                                        dateFormat="yyyy-MM-dd"
                                                                        name={`birthDate`}
                                                                        placeholder="Birth Date"
                                                                        value={this.state.profileInfo.birthDate && this.state.profileInfo.birthDate !== "0001-01-01T00:00:00" ? moment(this.state.profileInfo.birthDate) : null}
                                                                        bordered={true}
                                                                        onChange={async (date, dateString) => {
                                                                            setFieldValue("birthDate", dateString);
                                                                        }}
                                                                        disabledDate={(current) => current > moment().endOf('day')}
                                                                    />
                                                                    <ErrorMessage name={"birthDate"} component="div" className="invalid-feedback" />
                                                                </div>

                                                                <div className="inp-wrapper__sec">
                                                                    <DatePicker
                                                                        className={`form-control col ` + (errors.anniversaryDate ? " is-invalid" : "")}
                                                                        dateFormat="yyyy-MM-dd"
                                                                        name={`anniversaryDate`}
                                                                        placeholder="Anniversary"
                                                                        value={this.state.profileInfo.anniversaryDate && this.state.profileInfo.anniversaryDate !== "0001-01-01T00:00:00" ? moment(this.state.profileInfo.anniversaryDate) : null}
                                                                        bordered={true}
                                                                        onChange={async (date, dateString) => {
                                                                            setFieldValue("anniversaryDate", dateString);
                                                                        }}
                                                                        disabledDate={(current) => current > moment().endOf('day')} allowClear={true}
                                                                    />
                                                                    <ErrorMessage name={"anniversaryDate"} component="div" className="invalid-feedback" />
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div className="inp-wrapper wd-flex-center">
                                                        <label> Profile picture: </label>
                                                        <div className="wd-flex-center col-md-8 flex-direction-column">
                                                            <div className="d-flex align-items-center wdth-100">
                                                                <input name={`profilePicture.file`} type="file" className={`form-control col ` + (errors.profilePicture && errors.profilePicture.file ? " is-invalid" : "")} placeholder="Profile"
                                                                    onChange={(event) => {
                                                                        setFieldValue(`profilePicture.file`, event.currentTarget.files[0]);
                                                                    }}
                                                                />
                                                                <ErrorMessage name={"profilePicture.file"} component="div" className="invalid-feedback" />
                                                                {this.state.profileInfo.profilePicture.url ?
                                                                    <img className="profile-img" src={this.state.profileInfo.profilePicture.url} width="26" height="26" /> :
                                                                    <div className="profile-info__image m-auto">
                                                                        <i className="far fa-user"></i>
                                                                    </div>
                                                                }
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div className="inp-wrapper wd-flex-center">
                                                        <label> Document Information: </label>
                                                        <div className="wd-flex-center col-md-8 flex-direction-column">
                                                            <div className="d-flex wdth-100">
                                                                <div className="inp-wrapper__sec">
                                                                    <Field as="select" className={`form-control col ` + (errors.documentType ? " is-invalid" : "")} name={"documentType"} >
                                                                        <option value="">Document Type</option>
                                                                        <option value="passport">Passport</option>
                                                                    </Field>
                                                                    <ErrorMessage name={"documentType"} component="div" className="invalid-feedback" />
                                                                </div>
                                                                <div className="inp-wrapper__sec">
                                                                    <Field as="select" className={`form-control col ` + (errors.nationalityCode ? " is-invalid" : "")} id="nationalityCode" name="nationalityCode"
                                                                        placeholder="Country" >
                                                                        <option value="">Nationality</option>
                                                                        {this.countryList()}
                                                                    </Field>
                                                                    <ErrorMessage name={"nationalityCode"} component="div" className="invalid-feedback" />
                                                                </div>
                                                                <div className="inp-wrapper__sec">
                                                                    <Field as="select" className={`form-control col ` + (errors.issuingCountryCode ? " is-invalid" : "")} id="issuingCountryCode" name="issuingCountryCode"
                                                                        placeholder="Country" >
                                                                        <option value="">Issuing Country</option>
                                                                        {this.countryList()}
                                                                    </Field>
                                                                    <ErrorMessage name={"issuingCountryCode"} component="div" className="invalid-feedback" />
                                                                </div>
                                                            </div>
                                                        </div>

                                                    </div>
                                                    {/* <div className="inp-wrapper wd-flex-center">
                                    <label> My Subscription: </label> 
                                    <div className="col-md-8">
                                        <label htmlFor="sms-alert"><input id="sms-alert" type="checkbox"/> SMS alert</label>
                                        <label htmlFor="promotion"><input id="promotion" type="checkbox"/> Subscribe to receive special promotions and offers by Wadiia.</label>
                                    </div>
                                </div> */}
                                                    <div className="inp-wrapper wd-flex-center">
                                                        <label> </label>
                                                        <div className="col-md-8">
                                                            <button type="submit" className="btn btn-primary" >Submit</button>
                                                            <button className="btn btn-default" onClick={() => this.handleHideElement()}>Cancel</button>
                                                        </div>
                                                    </div>
                                                </Form>
                                            )}
                                        </Formik>
                                    </div> : null}
                            </div>}
                    </div>
                    <div className="col-md-4 page-my-profile__right">
                        <div className="white-bg text-center">
                            <div className="profile-info__image m-auto">
                                {this.state.profileInfo.profilePicture && this.state.profileInfo.profilePicture.url ? <img className="img-fluid" src={this.state.profileInfo.profilePicture.url} /> : <i className="far fa-user"></i>}
                            </div>
                            {Object.keys(this.state.profileInfo).length > 0 ? <div>
                                <div className="profile-info__name text-uppercase">{this.state.profileInfo.firstName} {this.state.profileInfo.lastName}</div>
                                <div className="profile-info__email">{this.state.profileInfo.contactInformation.email}</div>
                                <div className="profile-info__phone">Phone: {this.state.profileInfo.contactInformation.phoneNumber}</div>
                            </div> : null
                            }
                        </div>
                    </div>
                </div>

                <Modal className="profile-popup" show={this.state.show} onHide={() => this.handleShowHideModal()} size="md">
                    <Modal.Header closeButton>
                        <Modal.Title>Change Password</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        <div>{this.state.passwordError}</div>
                        <div className="inp-wrapper wd-flex-center">
                            <label className="col-sm-4"> New Password: </label>
                            <div className="wd-flex-center col-sm-8">
                                <input name="password" onChange={(e) => { this.setState({ password: e.target.value }) }} value={this.state.password} type="password"
                                    placeholder="" className={`form-control col `} />

                            </div>
                        </div>
                        <div className="inp-wrapper wd-flex-center">
                            <label className="col-sm-4"> Confirm Password: </label>
                            <div className="wd-flex-center col-sm-8">
                                <input name="confirmPassword" type="password"
                                    onChange={(e) => { this.setState({ confirmPassword: e.target.value }) }}
                                    value={this.state.confirmPassword} placeholder=""
                                    className={`form-control col `} />

                            </div>
                        </div>
                        <div className="text-right m-b-15 col-sm-12"><button className="btn btn-primary" onClick={this.setPassword}>Reset Password</button></div>
                    </Modal.Body>
                </Modal>

            </div>
            </React.Fragment>
        )
    }
}

export default MyProfile;