import React, { Component } from "react";
import { connect } from "react-redux";
import { compose } from "redux";
import * as actions from "../../redux/index";
import FlightListTopSearch from '../../components/FlightListTopSearch/FlightListTopSearch';
import FlightListSideFilter from '../../components/FlightListSideFilter/FlightListSideFilter';
import FlightLists from '../../components/FlightLists/FlightLists';
import RoundTripFlight from '../../components/RoundTripFlight/RoundTripFlight';
import {getAllSearchResult,getSearchFlights} from '../../services/search';
import LoadingOverlay from 'react-loading-overlay';
import NoResult from '../../components/Common/NoResult/No-result'
import moment from 'moment';
import { isBrowser } from "react-device-detect";
import * as queryString from "query-string"
import history from "../../utils/history";
import _ from 'lodash';
import {setRecentSearch,checkInvalidStrVal,searchObj} from "../../utils/commonFunc";
import { message } from 'antd';
import {messageDuration} from "../../services/constants";


class FlightListingSeparate extends Component {   
    constructor(props) {
        super(props);
        this.state = {  
            tripType:null,
            departureFromCity:"",
            loadingDept:false,
            locationsDept: [] ,
            locationsArr: [] ,
            name: "",
            address: "",
            showResults:false,
            arrivalToCity:"",
            loadingArr:false,
            showResultsArr:false,
            toggleSelection:false,
            adult:0,
            children:0,
            infant:0,           
            disbAdltPlus:false,
            disbAdltMinus:false,//true,
            disbChildPlus:false,
            disbChildMinus:false,//true,
            disbInfantPlus:false,
            disbInfantMinus:false,//true,
            fromLocUpdate:false,
            toLocUpdate:false,
            departureFrom:{},
            arrivalTo:{},
            isActive:true,
            queryParams:{},
            initialState:true,
            serviceClasses:[]
        };
    }
   /*  
    updateQueryParam=async (key,val)=> {               
        const obj={...this.state.queryParams}
        obj[key]=val;
        if(key=="tripType" && val=="Oneway") {
            obj.endDate=null;
        }
        if(key=="endDate" || key=="toDate") {
            obj.tripType="Roundtrip";
        }
        await this.setState({queryParams:{...obj}});
        const queryObj={...this.state.queryParams};        
        const passanger={adult:this.state.adult,child:this.state.children,infant:this.state.infant};       
        const newurl=`/flight-listing?fromDate=${queryObj.startDate}&toDate=${queryObj.endDate}&fromLocation=${JSON.stringify({...queryObj.fromLoc})}&toLocation=${JSON.stringify({...queryObj.toLoc})}&passanger=${JSON.stringify({...passanger})}&serviceClass=${decodeURIComponent(queryObj.serviceClass)}&tripType=${queryObj.tripType}&direct=${queryObj.direct}`;                
        if (history.push) {           
            window.history.pushState({path:newurl},'',newurl);
        }
    }
    //Use to check if response came with only flexi data without for searched date
    isOnlyFlexiData=(list)=>{
        //check list of response with search date from props not state as response is only reflect when user click search button and hit api
        const index=list.findIndex(item=>moment(item.dateInfo.startDate).format('YYYY-MM-DD')===this.props.fromDate);
        if(index>-1) {
            return false;
        } else {
            return true;
        }
    }
    componentDidMount=async ()=> { 
        const url = window.location.search;
        const params = queryString.parse(url);
        if (Object.keys(params).length > 0 && params.tripType && this.validateSearchParams(params)&&params.toDate) {
            const passanger = JSON.parse(params.passanger);
            const fromLoc = JSON.parse(params.fromLocation);
            const toLoc = JSON.parse(params.toLocation);
            if (params.adult === 0) {
                this.updateQueryParam("adult", 1);
            }
            let newData = {
                fromLoc: fromLoc,
                toLoc: toLoc,
                startDate: params.fromDate,
                endDate: params.toDate,
                adult: passanger.adult === 0 ? 1 : passanger.adult,
                child: passanger.child,
                infant: passanger.infant,
                tripType: params.tripType,
                serviceClass: decodeURIComponent(params.serviceClass),
                direct: params.direct == "true" ? true : false
            }
            this.setState({ fromDate: params.fromDate, toDate: params.toDate });
            newData = checkInvalidStrVal(newData);
            const data = { ...newData };
            this.setState({ queryParams: { ...data }, adult: data.adult, children: data.child, infant: data.infant, isActive: true, tripType: newData.tripType, toDate: newData.endDate, fromDate: newData.startDate });
            //check if url parameter not match with prop or not exist prop then excecute this
            const res = await getSearchFlights(data);
            if (res.status && res.res.status.code === 0 && res.res.response.data[0].type !== "advertisement" && res.res.response.data[0].item.length > 0) {
                this.props.searchRequest(res);
                let serviceClasses = [];
                if (res.res.response.availableFiltersIndex) {
                    const filterArr = res.res.response.availableFiltersIndex[0].item;
                    const valueArr = searchObj(filterArr, "name", "serviceclass")
                    if (valueArr) {
                        serviceClasses = valueArr.values;
                    }
                }
                if (res.status && res.res.response.data.length > 0 && res.res.response.data[0].type !== "advertisement" && res.res.response.data[0].item.length > 0) {
                    data.amt = res.res.response.data[0].item[0].paxInfo[0].displayRateInfo
                    data.serviceClasses = [...serviceClasses]
                    setRecentSearch(data);
                }
                await this.setState({ serviceClasses: [...serviceClasses], departureFrom: { ...fromLoc }, arrivalTo: { ...toLoc } });
            } else {
                //handle in two ways 
                //if status is 0 and no data show search and filter and show no resilt set redux
                //if status is not 0 show error page with search and reset empty all redux
                if (res.status) {
                    this.props.searchRequest(res);
                    let serviceClasses = [];
                    if (res.status && res.res.response.data.length > 0 && res.res.response.data[0].type !== "advertisement" && res.res.response.data[0].item.length > 0 && res.res.response.availableFiltersIndex) {
                        const filterArr = res.res.response.availableFiltersIndex[0].item;
                        const valueArr = searchObj(filterArr, "name", "serviceclass");
                        if (valueArr) {
                            serviceClasses = valueArr.values;
                        }
                    }
                    await this.setState({ serviceClasses: [...serviceClasses], departureFrom: { ...fromLoc }, arrivalTo: { ...toLoc } });
                }
                else {
                    this.props.searchRequest(newData, res.message);
                    message.error(res.message, messageDuration);
                }
            }
            this.setState({ isActive: false });
        } else {
            this.props.searchRequest(null, "Input Parameters are not valid!");
            this.setState({ isActive: false, tripType: "Oneway", fromDate: moment().format("YYYY-MM-DD"), adult: 1 });
            message.error("Input parameters are not valid.", messageDuration);
        }
    }
    componentDidUpdate=async(prevProps,prevState) =>{
        // Typical usage (don't forget to compare props):
        if (this.state.departureFromCity !== prevState.departureFromCity && this.state.fromLocUpdate===false) {
          this.setState({fromLocUpdate:true})
        }
        if (this.state.arrivalToCity !== prevState.arrivalToCity && this.state.toLocUpdate===false) {
            this.setState({toLocUpdate:true})
        }   
        //When user click on flexi search date then check search date set in redux and accordingly call search api       
        if(this.props.flexiStartDate !== prevProps.flexiStartDate || this.props.flexiEndDate !== prevProps.flexiEndDate ) {           
            if(this.props.flexiStartDate && !this.props.flexiEndDate) {
                await this.setState({fromDate:this.props.flexiStartDate});                
                this.updateQueryParam("startDate",this.props.flexiStartDate);
                this.enableDisableSearch();
            } 
            if(this.props.flexiStartDate && this.props.flexiEndDate) {                
                await this.setState({fromDate:this.props.flexiStartDate,toDate:this.props.flexiEndDate});
                this.updateQueryParam("startDate",this.props.flexiStartDate);
                this.updateQueryParam("endDate",this.props.flexiEndDate);
                this.enableDisableSearch();
            }
            this.handleSearch();
        }
    }
    handleTripChange=async(tripType)=>{        
        const obj={tripType:tripType,initialState:false}
        if(tripType==="Oneway") {
            obj.toDate=null;            
        }       
        await this.setState(obj);   
        this.updateQueryParam("tripType",obj.tripType);
        this.enableDisableSearch()
    }
    handleDepartureChange = async (e) => {
        const val=e.target.value;                
        this.setState({
            departureFromCity:val
        });        
        if(this.state.departureFromCity && this.state.departureFromCity.length>=2){
            this.setState({                     
                loadingDept:true
            });

            let res=await getAllSearchResult(val); 
            if(res.status)  {             
                this.setState({ 
                    loadingDept:false,
                    locationsDept: res.data ,
                    name: val,
                    address: val,
                    showResults:true
                }); 
            } else { 
                this.setState({ 
                    loadingDept:false,
                    locationsDept: [] ,
                    name: val,
                    address: val,
                    showResults:true
                });
            }
        } else {
            this.setState({     
                showResults:false
            });                        
        }        
        this.enableDisableSearch()   
    };
    handleArrivalChange = async (e) => {        
        const val=e.target.value;         
        this.setState({
            arrivalToCity:val
        });             
        if(this.state.arrivalToCity && this.state.arrivalToCity.length>=2){
            this.setState({                     
                loadingArr:true
            });
            const res=await getAllSearchResult(val);
            if(res.status) {              
            this.setState({ 
                loadingArr:false,
                locationsArr: res.data ,
                name: val,
                address: val,
                showResultsArr:true
            }); } else  {
            this.setState({ 
                loadingArr:false,
                locationsArr: [] ,
                name: val,
                address: val,
                showResultsArr:true
            });                          
         } 
        } else {
            this.setState({                     
                showResultsArr:false
            });            
        }
        this.enableDisableSearch();
    };
    handleSelectDepartureFrom=async (val)=>{
        await this.setState({ 
            departureFrom:val,           
            departureFromCity:val.city,
            showResults:false
        });
        this.updateQueryParam("fromLoc",{id:val.id,city:val.city});
        this.enableDisableSearch()
        if(this.state.arrivalTo) {
            await this.showServiceClasses();
        }
    }
    handleSelectArrivalTo=async (val)=>{        
        await this.setState({ 
            arrivalTo:val,           
            arrivalToCity:val.city,
            showResultsArr:false
        });
        this.updateQueryParam("toLoc",{id:val.id,city:val.city});
        this.enableDisableSearch();        
        if(this.state.departureFrom)
            await this.showServiceClasses();
    }
    handleToggleClass=()=>{
        this.setState({toggleSelection:!this.state.toggleSelection})
    }
    handleServiceClassChange= async(changeEvent)=> {
        this.setState({
          serviceClass: changeEvent.target.value
        });
        this.updateQueryParam("serviceClass",changeEvent.target.value);
        this.enableDisableSearch()
    }
    showServiceClasses = async()=>{
        if(//!this.state.toggleSelection && 
        ((this.state.tripType=="Oneway" && this.state.departureFrom && this.state.arrivalTo && this.state.fromDate) ||
        (this.state.tripType=="Roundtrip" && this.state.departureFrom && this.state.arrivalTo && this.state.fromDate && this.state.toDate))
        ) {
            const data={
                fromLoc:this.state.departureFrom,
                toLoc:this.state.arrivalTo,
                startDate:this.state.fromDate,
                endDate:this.state.toDate,
                adult:this.state.adult,            
                tripType:this.state.tripType
            } 
            this.setState({activeSpin:true});
            const res=await getSearchFlights(data);             
            if(res.status && res.res.response.availableFiltersIndex) {
                let filterArr=res.res.response.availableFiltersIndex[0].item;               
                let serviceClasses=[]
                let valueArr=searchObj(filterArr,"name","serviceclass");
                if(valueArr) {
                    serviceClasses=valueArr.values;
                }
                await this.setState({serviceClasses:[...serviceClasses],activeSpin:false}); //,toggleSelection:!this.state.toggleSelection           
            } else {  
                await this.setState({activeSpin:false});  //toggleSelection:!this.state.toggleSelection,        
                message.error(res.code +" : "+res.message,messageDuration);
            }
        } else {
            this.setState({serviceClasses:[]});//,toggleSelection:!this.state.toggleSelection
        }
    }
    handleUpdateTraveller=(key,type)=>{        
        let disbAdltPlus,disbAdltMinus,disbChildPlus,disbChildMinus,disbInfantPlus,disbInfantMinus=false;
        if(this.state.adult===1) {disbAdltMinus=true;}
        if(this.state.adult===9) {disbAdltPlus=true; disbChildPlus=true;}
        if(this.state.children===0) {disbChildMinus=true;       }
        if(this.state.children===8) {disbChildPlus=true;}
        if(this.state.infant===0) {disbInfantMinus=true;}
        if(this.state.infant===9) {disbInfantPlus=true;}
        this.setState({
            disbAdltPlus:disbAdltPlus,
            disbAdltMinus:disbAdltMinus,
            disbChildPlus:disbChildPlus,
            disbChildMinus:disbChildMinus,
            disbInfantPlus:disbInfantPlus,
            disbInfantMinus:disbInfantMinus,
        })
        if(type==="p" && (key === "adult" || key === "children") ) { 
            if(+this.state.adult+this.state.children === 9){
                if(key==="adult" && this.state.adult===1) {this.setState({children:0})                 }
                else if(key==="adult") {this.setState({disbAdltPlus:true,children:0}) }
                else {this.setState({disbAdltPlus:false}) }
                if(key==="children") {this.setState({disbChildPlus:true})}
                else {this.setState({disbChildPlus:false})}
                return;             
            }            
        }
        if(type==="m" && key === "adult") { 
            if((+this.state.adult+this.state.children === 9) && this.state.adult===this.state.infant && this.state.adult>1){
                this.setState({adult:this.state.adult-1,infant:0})
                return;             
            }            
        }
        //adult should not greater than 9
        if(type==="p" && key === "adult" && this.state[key]===9) { 
            if(key==="adult") {this.setState({disbAdltPlus:true})}
            else {this.setState({disbAdltPlus:false})}
            return;
        }
        //Children should not greater than 8
        if(type==="p" && key === "children" && this.state[key]===8) {  
            if(key==="children"){this.setState({disbChildPlus:true});}
            else {this.setState({disbChildPlus:false})}
            return;
        }
        //Infants should not greater than 9
        if(type==="p" && key === "infant" && this.state[key]===9) { 
            if(key==="infant"){this.setState({disbInfantPlus:true});}
            else {this.setState({disbInfantPlus:false})}
            return;
        }
        //Infants should not greater than adults
        if(type==="p" && key === "infant" && this.state[key]===this.state.adult) { 
            if(key==="infant"){this.setState({disbInfantPlus:true})}
            else {this.setState({disbInfantPlus:false})}
            return;
        }        
        if(type==="p" && key === "children") {  
            if((this.state.adult===1 && this.state.children===8) || (this.state.adult===2 && this.state.children===7) || (this.state.adult===3 && this.state.children===6) || (this.state.adult===4 && this.state.children===5) || (this.state.adult===5 && this.state.children===4)
            || (this.state.adult===6 && this.state.children===3) || (this.state.adult===7 && this.state.children===2) || (this.state.adult===8 && this.state.children===1)){
                if(key==="children"){this.setState({disbChildPlus:true})}
                else {this.setState({disbChildPlus:false})}
                return;
            }
        }
        if(type==="m" && this.state[key]>0) { 
            if(!(key==="adult" && this.state[key]===1)) {  
                this.setState({[key]:this.state[key]-1})
            }
        }
        if(type==="p" && this.state[key]>-1) { 
            this.setState({[key]:this.state[key]+1})
        }
        this.updateQueryParam("adult",this.state.adult);
        this.updateQueryParam("child",this.state.children);
        this.updateQueryParam("infant",this.state.infant);
    }
    
    enableDisableSearch=async()=> {
        await this.setState({error:null});
        if(this.state.tripType=="Oneway") { 
            if(!this.state.departureFrom && !this.state.departureFrom.id && !this.props.fromLocation ){
                this.setState({error:true});//"Departure location is required !"
                return;
            }
            if(!this.state.arrivalTo && !this.state.arrivalTo.id && !this.props.toLocation ){
                this.setState({error:true});   //"Arrival to location is required !"
                return;         
            }
            if(!this.state.fromDate && !this.props.fromDate) {
                this.setState({error:true});//"Departure date is required !"
                return;
            }                
        }
        if(this.state.tripType=="Roundtrip") {         
            const fromDate = this.state.fromDate//?this.state.fromDate:this.props.fromDate;
            const toDate = this.state.toDate//?this.state.toDate:this.props.toDate;           
            if(!this.state.departureFrom && !this.state.departureFrom.id && !this.props.fromLocation ){
                this.setState({error:true}); //"Departure from location is required !"
                return;
            }
            if(!this.state.arrivalTo && !this.state.arrivalTo.id && !this.props.toLocation) {
                this.setState({error:true});//"Arrival to location is required !"
                return;
            }
            if(!fromDate) {
                this.setState({error:true});//"Departure date is required !"
                return;
            }
            if(!toDate) {
                this.setState({error:true});//"Return date is required !"
                return;
            } 
            if(moment(fromDate).isAfter(toDate)) {
                this.setState({error:true});//"Arrival date is not valid !"
                return;
            }
        }         
    }

    validateSearchParams=(params)=> {        
        if(params.tripType && params.fromLocation && params.toLocation && params.fromDate && params.passanger)  {
            const passanger=JSON.parse(params.passanger);            
            const fromLoc=JSON.parse(params.fromLocation);
            const toLoc=JSON.parse(params.toLocation);         
            if(params.tripType && params.tripType==="Oneway") {                             
            if(!params.fromLocation || !fromLoc.id || !fromLoc.city){
                return false
            } 
            if(!params.toLocation || !toLoc.id || !toLoc.city){
                this.setState({error:true});   //"Arrival to location is required !"
                return false;         
            }
            if(!params.fromDate) {                
                return false;
            }  
            if(passanger.adult<1) {                
                return false;
            } 
            return true             
        } else if(params.tripType && params.tripType==="Roundtrip") {
            if(!params.fromLocation || !fromLoc.id || !fromLoc.city){   
                return false
            }
            if(!params.toLocation || !toLoc.id || !toLoc.city) {  
                return false
            }
            if(!params.fromDate || !params.toDate || moment(params.fromDate).isAfter(params.toDate)) {  
                return false
            } 
            if(passanger.adult<1) {               
                return false;
            }  
            return true         
        }
     } else return false
    }
    
    handleSearch=async () => {         
        this.enableDisableSearch();
        if(!this.state.error) {            
            let newData={
                fromLoc:Object.keys(this.state.departureFrom).length>0?this.state.departureFrom:this.props.fromLocation,
                toLoc:Object.keys(this.state.arrivalTo).length>0?this.state.arrivalTo:this.props.toLocation,
                startDate:this.state.fromDate?this.state.fromDate:this.props.fromDate,
                endDate:this.state.toDate?this.state.toDate:this.props.toDate,
                adult:this.state.adult?this.state.adult:this.props.passanger.adult,
                child:this.state.children?this.state.children:this.props.passanger.child,
                infant:this.state.infant?this.state.infant:this.props.passanger.infant,
                tripType:this.state.tripType?this.state.tripType:this.props.tripType,
                serviceClass:this.state.serviceClass?this.state.serviceClass:this.props.serviceClass              
            }
            newData=checkInvalidStrVal(newData);
            let data={...newData};                    
            this.setState({loadingList:true,isActive:true});
            let res=await getSearchFlights(data);
            this.setState({loadingList:false});            
            if(res.status && res.res.status.code===0 && res.res.response.data[0].type!=="advertisement" && res.res.response.data[0].item.length>0) {               
                this.props.searchRequest(res);
                this.setState({isActive:false});                
                if(res.status && res.res.response.data.length>0 && res.res.response.data[0].type!=="advertisement" && res.res.response.data[0].item.length>0){
                    data.amt=res.res.response.data[0].item[0].paxInfo[0].displayRateInfo
                    data.serviceClasses=[...this.state.serviceClasses]
                    setRecentSearch(data);
                }                                 
            }else {               
                if(res.status){ 
                    this.props.searchRequest(res);
                    this.setState({isActive:false});
                } else { 
                    this.props.searchRequest(newData,res.message);
                    this.setState({isActive:false});
                    message.error(res.message,messageDuration);
                }                                    
            }            
        } 
    }
    handleFromDateChange=(date)=>{
        let dateFrom;
        if(date) {
            dateFrom=moment(date._d,"YYYY-MM-DD").format("YYYY-MM-DD");
        } else {
            dateFrom=null
        }
        this.setState({fromDate:dateFrom});
        this.updateQueryParam("startDate",dateFrom);
        this.enableDisableSearch();
    }
    handleToDateChange=async(date)=>{
        let dateTo,tripType="Roundtrip";
        if(date) {
            dateTo=moment(date._d,"YYYY-MM-DD").format("YYYY-MM-DD");
        } else {
            dateTo=null;
            tripType="Oneway";
        }
        await this.setState({toDate:dateTo,tripType:tripType});        
        this.updateQueryParam("endDate",dateTo);
        this.updateQueryParam("tripType",tripType);
        this.enableDisableSearch();        
    }
    handleFilterShow=(action)=>{
        if(action==="show") {
            this.setState({filterShow:"block"})
        }
        if(action==="hide") {
            this.setState({filterShow:"none"})
        }
    }

    prepareTopSearchElement=()=>{
       return  isBrowser && <FlightListTopSearch
        changeTripType={this.handleTripChange}
        selectDepartureFrom={this.handleSelectDepartureFrom}
        selectArrivalTo={this.handleSelectArrivalTo}
        changeDeparture={this.handleDepartureChange}
        changeArrival={this.handleArrivalChange}
        locationsDept={this.state.locationsDept}
        locationsArr={this.state.locationsArr}
        showResults={this.state.showResults}
        showResultsArr={this.state.showResultsArr}
        toggleClass={this.handleToggleClass}
        toggleSelection={this.state.toggleSelection}
        handleServiceClass={this.handleServiceClassChange}
        updateTraveller={this.handleUpdateTraveller}
        disbAdltPlus={this.state.disbAdltPlus}
        disbAdltMinus={this.state.disbAdltMinus}
        disbChildPlus={this.state.disbChildPlus}
        disbChildMinus={this.state.disbChildMinus}
        disbInfantPlus={this.state.disbInfantPlus}
        disbInfantMinus={this.state.disbInfantMinus}
        updateFromDate={this.handleFromDateChange}
        updateToDate={this.handleToDateChange}
        disableSearch={this.state.error}
        search={this.handleSearch} 
        serviceClasses={[...this.state.serviceClasses]}                    
        searchData={{ 
            fromLocation:(this.state.fromLocUpdate)?this.state.departureFromCity:this.props.fromLocation,
            toLocation:(this.state.toLocUpdate)? this.state.arrivalToCity : this.props.toLocation,
            tripType:this.state.tripType?this.state.tripType:this.props.tripType,
            fromDate:this.state.fromDate,//this.state.fromDate?this.state.fromDate:this.props.fromDate,
            toDate:this.state.toDate,//this.state.toDate || !this.state.initialState?this.state.toDate:this.props.toDate,            
            passanger:{
                adult:this.state.adult,
                child:this.state.children,
                infant:this.state.infant
            },
            serviceClass:this.state.serviceClass?this.state.serviceClass:this.props.serviceClass
        }}
    />
    }
*/
    render(){
        let content =<NoResult/>;
        /*let content,innerContent;
        if(!this.state.isActive && this.props.resFlight && Object.keys(this.props.resFlight).length > 0) { 
            let rangeFilObj=this.props.resFlight.availableFiltersIndex[0].item.find(function(filter, index) {            
                if(filter.name == "pricerange")
                    return true;
                else 
                    return false;
            });
            if((this.props.resFlight.data[0].type!=="advertisement"  && this.props.resFlight.data[0].item.length>0 && !this.isOnlyFlexiData(this.props.resFlight.data[0].item)) || 
            (searchObj(this.props.resFlight.data,"type","departure") && (this.props.resFlight.data[0].item.length>0 || this.props.resFlight.data[1].item.length>0)) ) {
                innerContent= <div className="row">
                                <div className="col-lg-3 w-filters-sidebar-wprapper" style={{display:this.state.filterShow}}>
                                    <FlightListSideFilter key={rangeFilObj && rangeFilObj.minValue}
                                    departure={this.props.fromLocation} 
                                    arrival={this.props.toLocation}
                                    hideFilter={this.handleFilterShow}
                                    />
                                </div>
                                <div className="col-lg-9 w-filters-details-wprapper"> 
                                <FlightLists  showFilter={this.handleFilterShow} 
                                        fromLocation={this.state.departureFromCity?this.state.departureFromCity:this.props.fromLocation}
                                        toLocation={this.state.arrivalToCity?this.state.arrivalToCity:this.props.toLocation}
                                        traveller={this.state.adult? (this.state.adult+this.state.infant+this.state.children):(this.props.passanger.adult+this.props.passanger.child+this.props.passanger.infant)}
                                        fromDate={this.state.fromDate?this.state.fromDate:this.props.fromDate}
                                        total={this.props.resFlight.pageInfoIndex[0].item.totalResults}
                                        sort={this.props.resFlight.availableSortingIndex}
                                />            
                                </div>
                            </div>
            } else {           
                if(this.props.type==="filter") {
                    innerContent= <div className="row">
                    <div className="col-lg-3 w-filters-sidebar-wprapper" style={{display:this.state.filterShow}}>
                        <FlightListSideFilter
                        key={rangeFilObj && rangeFilObj.minValue}
                        departure={this.props.fromLocation} 
                        arrival={this.props.toLocation}
                        hideFilter={this.handleFilterShow}
                        />
                    </div>
                    <div className="col-lg-9 w-filters-details-wprapper">
                    <NoResult/> 
                    </div>
                    </div>
                } else {
                    innerContent = <NoResult/>
                }
            }
            content=    <div> 
                            <div className="error" style={{color:"red"}}> {this.state.error} </div>       
                           { isBrowser && this.prepareTopSearchElement()}
                            <div className="container">
                                {innerContent}
                            </div>
                        </div>
        } else {
            if(!this.state.isActive) {             
                content=<React.Fragment>
                {this.prepareTopSearchElement()}
                <NoResult/>
                </React.Fragment>
            }
        } */
       return(
           <div>
               <LoadingOverlay
               // active={this.state.isActive}
                spinner={true}
                text='Loading your content...'
                >
                    {content}              
                </LoadingOverlay>
           </div>
       ) 
    }
}


const mapStateToProps = (state) => {
    return { 
        type:state.flights.reqType,
        tkn : state.flights.token,
        resFlight:state.flights.response,
        fromLocation:state.flights.fromLocation,
        toLocation:state.flights.toLocation,
        tripType:state.flights.tripType,
        fromDate:state.flights.fromDate,
        toDate:state.flights.toDate,
        passanger:state.flights.passanger,
        serviceClass:state.flights.serviceClass,
        flexiStartDate:state.flights.flexiStartDate,
        flexiEndDate:state.flights.flexiEndDate
    };
  };

export default compose(connect(mapStateToProps, actions))(FlightListingSeparate);
//export default FlightSearch;