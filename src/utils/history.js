// import { createBrowserHistory } from "history";
// export default createBrowserHistory();//{forceRefresh:true}
import ReactGA from 'react-ga';

let history;
if (typeof document !== 'undefined') {    
 const createBrowserHistory = require("history").createBrowserHistory;
  history =  createBrowserHistory({forceRefresh:true})//
}
history.listen((location) => {
  ReactGA.set({ page: location.pathname });//+window.location.search
  ReactGA.pageview(location.pathname);
});
export default history