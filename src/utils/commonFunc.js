import moment from 'moment';
import React from 'react';
import {store} from "../redux/store";
export const minHHMM=(num)=>{ 
const hours = Math.floor(num / 60);  
const minutes = num % 60;
 return {hrs:hours,min:minutes};         
}

export const addDefaultSrc=(ev)=>{
    ev.target.src = 'images/ThumbHandler.jpg'
}

export const addDefaultProfileSrc=(ev)=>{
    ev.target.src = 'images/icon_placeholder.png'
}

export const searchObj=(arr,prop,val,caseChk=false)=>{ 
    if(!caseChk) {
        return arr.find(element => element[prop]===val);
    }
    else {
        return arr.find(element => element[prop].toLowerCase()===val)
    }
}

const findAndPrepareArrayRecentSearch=(arr,data,recentArrLimit=false)=>{
    const res=arr.findIndex( (element) => {
        if(data.tripType==="Oneway" && element["fromDate"]===data.fromDate && element.fromLocation.id===data.fromLocation.id && element.toLocation.id===data.toLocation.id) {
           return true 
        }
        if(data.tripType==="Roundtrip" && element["fromDate"]===data.fromDate && ((element["toDate"] && data.toDate && element["toDate"]==data.toDate)) && element.fromLocation.id===data.fromLocation.id && element.toLocation.id===data.toLocation.id){
            return true;
        }
    })          
    if(res>-1){               
        arr.splice(res,1);               
        arr.push({...data});
        
    } else {
        arr.push({...data});
        if(recentArrLimit) {
            arr.shift();
        }
    }
    return arr;
}

export const setRecentSearch=(obj)=>{
    let recentSearch=store.getState().user.recentSearch?[...store.getState().user.recentSearch]:[];//localStorage.getItem('recentSearch');
    const passanger=JSON.stringify({adult:obj.adult,infant:obj.infant,child:obj.child});
    const amt=searchObj(obj.amt,"description","TOTALAMOUNT");
    const data={
        fromDate:obj.startDate,
        toDate:obj.endDate==="undefined" || obj.endDate=="null"?null:obj.endDate,
        fromLocation:obj.fromLoc,
        toLocation:obj.toLoc,
        passanger:passanger,
        serviceClass:obj.serviceClass,
        tripType:obj.tripType,
        direct:obj.direct,
        amt:amt.amount,
        serviceClasses:obj.serviceClasses
        //currency:curr.symbol
    };   
    data.url=`/flight-listing?fromDate=${data.fromDate}&toDate=${data.toDate}&fromLocation=${JSON.stringify({id:data.fromLocation.id,city:data.fromLocation.city})}&toLocation=${JSON.stringify({id:data.toLocation.id,city:data.toLocation.city})}&passanger=${passanger}&serviceClass=${data.serviceClass}&tripType=${data.tripType}&direct=${data.direct}`
    let arr;
    if(recentSearch.length>0) { 
        //recentSearch=typeof recentSearch === "string"?JSON.parse(recentSearch):recentSearch;            
        arr=[...recentSearch];        
        if(arr.length<10) {             
            const retArr=findAndPrepareArrayRecentSearch([...arr],data);
            arr=[...retArr];

        }else {
            const retArr=findAndPrepareArrayRecentSearch([...arr],data,true);
            arr=[...retArr];                       
        }
    }else {        
        arr=[];
        arr.push(data);
    }
    store.dispatch({type:"SET_RECENT_SEARCH",payload:[...arr]});
    //localStorage.setItem('recentSearch', JSON.stringify(arr));
    //localStorage.setItem('lastSearch', JSON.stringify(data));    
}


export const removeRecentSearch=(index)=>{    
    let recentSearch=store.getState().user.recentSearch?[...store.getState().user.recentSearch]:[];//localStorage.getItem('recentSearch');    
    let newArr=[];
    let length=recentSearch.length;
    let newIndex=length-(index+1);
    recentSearch.forEach((item,ind)=>{        
        if(ind!==newIndex) {
            newArr.push(item)
        }       
    })
    store.dispatch({type:"SET_RECENT_SEARCH",payload:[...newArr]});
    let newRecentSearch=store.getState().user.recentSearch?[...store.getState().user.recentSearch]:[];
    return newRecentSearch.reverse();
    // console.log(recentSearch.length,recentSearch);
    // let length=recentSearch.length;
    // let newIndex=length-(index+1);
    // let newArr=recentSearch.splice(newIndex,1);
    // console.log(recentSearch);
    // store.dispatch({type:"SET_RECENT_SEARCH",payload:recentSearch});
}

export const checkInvalidStrVal=(obj)=>{
    for (const prop in obj) {
        if(obj[prop]==="undefined" || obj[prop]==="null") {
            obj[prop]=null;
        }
    }    
    return obj;
}

export const setContactDetails=(data,obj)=>{
    const newObj={...obj}
    newObj.FirstName=data.firstName
    newObj.LastName=data.lastName
    newObj.Location={
        "CountryID": data.country,
        "Country": null,
        "City": data.city			
    }
    newObj.ContactInformation={			
        "PhoneNumber": data.phoneNumber,//.slice(data.phoneCode.length),
        "ActlFormatPhoneNumber": "",			
        "PhoneNumberCountryCode": data.phoneCode,		
        "Email": data.email	
    }
    newObj.NationalityCode= null;
    newObj.GenderDesc= "";
    newObj.Gender= data.gender;
    newObj.ActlGender= "";
    return newObj;
}

export const setTravellerDetails=(data,obj)=>{
    const newObj={...obj};
    newObj.IsMainPax=data.type==="ADT"?true:false;
    newObj.TypeString=data.type;
    newObj.Details= {        
        "FirstName": data.firstName,
        "LastName": data.lastName,
        "Location": {          
          "CountryID": data.country.code,
          "Country": data.country.name          
        }
    }
    newObj.Details.ContactInformation= {
        "Name": data.firstName,        
        // "PhoneNumber": "",
        // "ActlFormatPhoneNumber": "",
        // "HomePhoneNumber": null,
        // "PhoneNumberCountryCode": "",
        // "HomePhoneNumberCountryCode": null,
        // "ActlFormatHomePhoneNumber": null,
        // "Fax": null,
        // "Email": "",
        // "WorkEmail": null
      }
    newObj.Details.Gender=data.gender;    
    newObj.Details.BirthDate= data.birthDate;
    //newObj.Details.PassportExpirationDate= "";
    //newObj.Details.IssuingCountryCode=data.issueCountry.code;
    // newObj.Details.DocumentType= "";
    // newObj.Details.DocumentNumber= "";
    if(data.docType && data.docType==="passport") {
        newObj.Details.PassportExpirationDate= data.expiryDate;
        newObj.Details.IssuingCountryCode=data.issueCountry.code;
        newObj.Documents= [{
            "Type": "PASSPORTNUMBER",
            "Id": data.passportNumber,
            "OriginId": null,
            "DateInfo": {
                //"StartDate": "",
                "EndDate": data.expiryDate,
                "StartTime": null,
                "EndTime": null,
                "Type": null
            }
        }] 
    } 
    
    return newObj;
}

export const preparePaymentGatewayAvailableList=(viewArr,envArr)=>{
    const newArr=[];
    for (const code in viewArr) {
        if (viewArr.hasOwnProperty(code)) {           
            const obj={id:[code]}
            const res=searchObj(envArr,"code",code,true);
            if(res) {
                obj.name=res.name;
                obj.values=[...viewArr[code]];
                newArr.push(obj);
            }
        }
    }
    return newArr;
}

export const getStorageFor=(prop)=>{ 
    //let data=localStorage.getItem(prop);  
    const appSetting=store.getState().appSetting.setting;
    let data={...appSetting};   
    if(data && Object.keys(data).length>0) {
        //data= typeof data=="string"?JSON.parse(data):null       
        return data;
    } else {
        return null;
    }
}

export const readUploadedFileAsbase64 = (inputFile) => {
    const temporaryFileReader = new FileReader();
    return new Promise((resolve, reject) => {
        temporaryFileReader.onerror = () => {
            temporaryFileReader.abort();
            reject(new DOMException("Problem parsing input file."));
        };
        temporaryFileReader.onload = () => {
            resolve(temporaryFileReader.result);
        };
        temporaryFileReader.readAsDataURL(inputFile);
    });
};

export const dateDiffHrsMin=(fromDate,toDate)=>{
    const milsec=Math.abs(new Date(toDate) - new Date(fromDate));
    const min=Math.floor(milsec / 60000);
    return minHHMM(min);
}

export const prepareMessageAvalability=(resView,code,message)=>{
    const cartData=resView.data;
    let msg=`${code} : ${message}`;
    if(cartData.items.length>0) {
        cartData.items.map((item)=>{
            if(item.availabilityStatus===1) {
                return msg= `${code} : ${message}
                Price Changed ${item.data.config[1].value}: Old Price ${item.data.displayOriginalAmount} New Price ${item.data.displayAmount}.
                you can change flight!\n`;
            } else if(item.availabilityStatus===2 || item.availabilityStatus===3) {
                return msg+= `${code} : ${message} : ${item.data.config[1].value}\n
                        Selected flight is not available. Please Select another flight\n`;
            }
        })
        
    }
    return msg;        
}

export const isSiteUnderMaintenance=()=>{   
    const appSetting=store.getState().appSetting.setting;
    let setting={...appSetting};//localStorage.getItem("setting"); 
    if(setting) {
        //setting=typeof setting=="string"?JSON.parse(setting):null;
        return setting.isPortalUnderMaintenance;
    }    
    return false;  
}

export const getCurrency=async ()=>{ 
    const appData=await store.getState();   
    const appSetting= appData.appSetting.setting; //let setting=localStorage.getItem("setting"); 
    let setting={...appSetting};//
    if(setting) {
        //setting=typeof setting=="string"?JSON.parse(setting):null;
        return setting&&setting.portalCurrency?setting.portalCurrency.symbol:"";
    }
    return "";
}

export const prepareFareDetailsSearch=async (paxInfo)=>{   
    const fareType=[{code:"1",name:"Base Rate"},{code:"7",name:"Subcharges & Tax"}];
    const paxArr=["Adult","Child","Infant"];
    const fareSummary=[];
    let total=0;
    let currency= await getCurrency();
    fareType.map((fareType)=>{  
        const mainObj={}; 
        const val=[];  
        paxInfo.map(pax=>{            
            const res=searchObj(pax.displayRateInfo,"purpose",fareType.code);            
            if(res && pax.quantity) {
                const obj={};                
                obj.rate=res.amount/pax.quantity;
                obj.pax=paxArr[pax.type]
                obj.quantity=pax.quantity;                  
                total=total+(obj.rate* obj.quantity)                           
                return val.push(obj);
            }            
        })
        if(val.length>0){
            mainObj.fareType=fareType.name
            mainObj.values=[...val]
            return fareSummary.push(mainObj);
        }       
    })
    return {fareSummary:fareSummary,total:total.toFixed(2),currency:currency};
}

export const returnFlightLogo=(url,isMobileOnly)=>{
    if(url) {
        const filename = url.substring(url.lastIndexOf('/')+1);
        const code=filename.split('.').slice(0, -1).join('.');        
        const imgUrl= `${process.env.REACT_APP_airLogoUrl}`;
        if(isMobileOnly)  {
            return imgUrl +"Square/"+code+".png";
        }
        return imgUrl +"Rectangular/"+code+".png";
    } else {
        return ""
    }
}

export const capitalize=(str)=>{
    if (!str || typeof str !== 'string'){ 
        return '';
    }
    return str.charAt(0).toUpperCase() + str.slice(1)
}

export const checkIsRefundable=(itemsArray)=>{
    let refundable=true;
    itemsArray.map(item=>{
        const items=item.item;        		
        return items.map(flight=>{
            //console.log("flight is refundable flag",flight.flags);
            if(!flight.flags.isRefundable || flight.flags.isRefundable===false) {
                refundable=false;
                return false;
            }
        })
    })    
    return refundable;
}

export const checkPartiallyRefundable=(itemsArray)=>{
    let refundable=true;
    const items=itemsArray.item;
    items.map(flight=>{
        //console.log("flight is refundable flag",flight.flags);
        if(!flight.flags.isRefundable || flight.flags.isRefundable===false) {
            refundable=false;
            return false;
        }
    })
    return refundable;
}

export const distinctAirline=(items)=>{        
    const airlines=[];
    items.forEach(route=>{
        route.item.forEach(flight=>{
            const code=flight.vendors[0].item.code;
            const url=flight.images[0].url;
            const name=flight.vendors[0].item.name;
            const index=airlines.findIndex(listedairline=>{ return listedairline.code===code});
            if(index<0) {
                airlines.push({code:code,url:url,name:name});
            }
        });
    });
    return airlines;
}

export const getDatesBetweenDates = (startDate, endDate) => {
    let dates = []
    //to avoid modifying the original date
    const theDate = new Date(startDate);    
    while (theDate < new Date(endDate)) { 
      dates = [...dates, moment(theDate).format('YYYY-MM-DD')]
      theDate.setDate(theDate.getDate() + 1)
    }
    dates.push(moment(endDate).format('YYYY-MM-DD'));    
    return dates
  }

export const statucDateSectionFlight=()=>{
    return (<div className="airline-fare-rules">
    <div className="airline-text m-b-10">       
    </div>
    <div className="d-flex rules-heading">
        <div className="time-gap-cond">
            <p className="text-black LatoBold append_bottom2">Time frame</p>
            <p className="font10 lightGreyText">(From Scheduled flight departure)</p>
        </div>
        <div className="time-gap-cond">
            <p className="text-black LatoBold append_bottom2">Airline Fee + MMT Fee + Fare difference</p>
            <p className="font10 lightGreyText">(Per passenger)</p>
        </div>
    </div>
    <div className="d-flex">
        <div className="time-gap-cond">
            <p className="text-black LatoBold append_bottom2">0 hours to 4 hours*</p>
        </div>
        <div className="time-gap-cond text-black">
            <p>ADULT :
                <b>Non Changeable</b>
                <br />
            </p>
        </div>
    </div>
    <div className="d-flex">
        <div className="time-gap-cond">
            <p className="text-black LatoBold append_bottom2">4 hours to 3 days*</p>
        </div>
        <div className="time-gap-cond text-black">
            <p>ADULT :
                <b>₹ 3,500 + ₹ 0  + Fare difference</b>
                <br />
            </p>
        </div>
    </div>
    <div className="d-flex">
        <div className="time-gap-cond">
            <p className="text-black LatoBold append_bottom2">3 days to 365 days*</p>
        </div>
        <div className="time-gap-cond text-black">
            <p>ADULT :
                <b>₹ 3,000 + ₹ 0  + Fare difference</b>
                <br />
            </p>
        </div>
    </div>
    <p className="append_top10 append_bottom5">*From the Date of Departure</p>
</div>)
}

export const staticViewFare=()=>{
    return (
        <div className="more-flights" >
        <div className="more-flights__wrap">
            <h3>Spicesaver</h3>
            <div className="more-flights__details">
                <div className="more-flights_details">
                    <p> <i className="fa fa-check-circle" aria-hidden="true"></i> Cabin Baggage 7 Kgs, Check-in Baggage 15 Kgs</p>
                    <p> <i className="fa fa-check-circle" aria-hidden="true"></i> Free seat selection</p>
                    <p> <i className="fa fa-check-circle" aria-hidden="true"></i> Partially-refundable fare</p>
                    <p> <i className="fa fa-check-circle" aria-hidden="true"></i> Date change chargeable</p>
                </div>
                <div className="more-flights__details-price">
                    <div className="air-flight-price">
                        <span>₹ 6,090</span>
                        <button className="btn btn-primary"> Book Now </button>
                    </div>
                </div>
            </div>
        </div>

        <div className="more-flights__wrap">
            <h3>Spicemax</h3>
            <div className="more-flights__details">
                <div className="more-flights_details">
                    <p> <i className="fa fa-check-circle" aria-hidden="true"></i> Cabin Baggage 7 Kgs, Check-in Baggage 15 Kgs</p>
                    <p> <i className="fa fa-check-circle" aria-hidden="true"></i> Complimentary meals</p>
                    <p> <i className="fa fa-check-circle" aria-hidden="true"></i> Free seat selection</p>
                    <p> <i className="fa fa-check-circle" aria-hidden="true"></i> Partially-refundable fare</p>
                    <p> <i className="fa fa-check-circle" aria-hidden="true"></i> Date change chargeable</p>
                </div>
                <div className="more-flights__details-price">
                    <div className="air-flight-price">
                        <span>₹ 12,023</span>
                        <button className="btn btn-primary"> Book Now </button>
                    </div>
                </div>
            </div>
        </div>

    </div>
    )
}