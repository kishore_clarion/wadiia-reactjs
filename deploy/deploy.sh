#!/bin/bash

set -e

cd "$(dirname "$0")/../"

VERSION=$1
VERSION_PREFIX="v"
Parse the major version (e.g., "v2" is the major version in "v2.32.2")
MAJOR_VERSION=$VERSION_PREFIX$(echo $VERSION | sed -n 's/^\([0-9]*\).*/\1/p')
FULL_VERSION=$VERSION_PREFIX$VERSION
ENVIRONMENT=$2
GIT_TAG=$VERSION

npm install
pwd
ls -la
npm run build