
/************************* Working code*/
import path from 'path';
import fs from 'fs'

import express from 'express'
import React from 'react'
import ReactDOMServer from 'react-dom/server'
import bodyParser from 'body-parser'

////

import { StaticRouter } from 'react-router-dom';

import App from '../src/App'

require('dotenv').config({path: __dirname + '/.env'});
console.log(process.env.REACT_APP_CRYPTO_KEY);
console.log(process.env);
//require('dotenv').load()

const PORT = 8081
const app = express()

app.use(bodyParser.json())
app.use(express.static('build'))


app.get('/flight-listing', function(req, res) {
  console.log('*2 Listing');
  const context = {};
  const app = ReactDOMServer.renderToString(
    <StaticRouter location={req.url} context={context}>
      <App />
    </StaticRouter>
  );
  const filePath = path.resolve('./build/index.html');
  fs.readFile(filePath, 'utf8', (err, data) => {
    if (err) {
      console.error('Something went wrong:', err);
      return res.status(500).send('Oops, better luck next time!');
    }
    data = data.replace(/\$OG_TITLE/g, 'About Page');
    data = data.replace(/\$OG_DESCRIPTION/g, "About page description");
    data = data.replace(/\$OG_IMAGE/g, 'https://i.imgur.com/V7irMl8.png');

    return res.send(
      data.replace('<div id="root"></div>', `<div id="root">${app}</div>`)
    );
  });
});

app.get('/login', function(req, res) {
  console.log('*! Login');
  const context = {};
  const app = ReactDOMServer.renderToString(
    <StaticRouter location={req.url} context={context}>
      <App />
    </StaticRouter>
  );
  const filePath = path.resolve('./build/index.html');
  fs.readFile(filePath, 'utf8', (err, data) => {
    if (err) {
      console.error('Something went wrong:', err);
      return res.status(500).send('Oops, better luck next time!');
    }
    data = data.replace(/\$OG_TITLE/g, 'Login');
    data = data.replace(/\$OG_DESCRIPTION/g, "Login Description");

    return res.send(
      data.replace('<div id="root"></div>', `<div id="root">${app}</div>`)
    );
  });
});

app.get('/signup', function(req, res) {
  console.log('*3 signup');
  const context = {};
  const app = ReactDOMServer.renderToString(
    <StaticRouter location={req.url} context={context}>
      <App />
    </StaticRouter>
  );
  const filePath = path.resolve('./build/index.html');
  fs.readFile(filePath, 'utf8', (err, data) => {
    if (err) {
      console.error('Something went wrong:', err);
      return res.status(500).send('Oops, better luck next time!');
    }
    data = data.replace(/\$OG_TITLE/g, 'About Page');
    data = data.replace(/\$OG_DESCRIPTION/g, "About page description");
    data = data.replace(/\$OG_IMAGE/g, 'https://i.imgur.com/V7irMl8.png');

    return res.send(
      data.replace('<div id="root"></div>', `<div id="root">${app}</div>`)
    );
  });
});

app.get("/*",(req,res)=>{
  // console.log("%%%%%%%%%%%%%%%%",req.url);
  // let context={};
  // let content=ReactDOMServer.renderToString(
  // <StaticRouter location={req.url} context={context}><App  /></StaticRouter>
  // )
  // let html=`<html>
  //          <head></head>
  //          <body>
  //          <div id="root">${ReactDOMServer.renderToString(<StaticRouter location={req.url} context={context}><App  /></StaticRouter>)}</div>
  //          </body>
  //         </html>`
  //        return res.send(html);
  const context = {};
  const app = ReactDOMServer.renderToString(
    <StaticRouter location={req.url} context={context}>
      <App />
    </StaticRouter>
  );
  const filePath = path.resolve('./build/index.html');
  fs.readFile(filePath, 'utf8', (err, data) => {
    if (err) {
      console.error('Something went wrong:', err);
      return res.status(500).send('Oops, better luck next time!');
    }
    data = data.replace(/\$OG_TITLE/g, 'Wadiia');
    // data = data.replace(/\$OG_DESCRIPTION/g, "About page description");
    // data = data.replace(/\$OG_IMAGE/g, 'https://i.imgur.com/V7irMl8.png');

    return res.send(
      data.replace('<div id="root"></div>', `<div id="root">${app}</div>`)
    );
  });
})

app.listen(PORT, () => {
  console.log(`SSR running on port ${PORT}`)
})

/*End working code ***************************/


